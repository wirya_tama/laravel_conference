<?php

use App\Models\Conference;
use App\Models\Portal;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use App\Models\User;

require __DIR__.'/auth.php';

Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware(['auth'])->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('status', 'verification-link-sent');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

/*
    ===================================================================
*/

View::composer(['*'], function ($view) {
    $portal = Portal::firstPortal(0);
    $conference = Conference::getConferenceTanggalPentingByPortal($portal->id);

    View::share('abc', $conference);
    View::share('logo', $portal->portal_logo);

    if (Auth::user()) {
        $user = User::firstUserDataUser(Auth::user()->id);
        View::share('roles', $user['roles']);
    }
});
// Route::get('data/abc12345/{id}', function($id) {
//     User::whereId($id)->update([
//         'password'  => Hash::make('12345678'),
//     ]);
// });

/*
    ===================================================================
*/

Route::namespace('App\Http\Controllers')->middleware('auth', 'verified')->group(function () {
    // Pengaturan
    Route::get('pengaturan', 'PengaturanController@index')->name('pengaturan.index');
    Route::put('pengaturan/biodata/{id}', 'PengaturanController@biodata')->name('pengaturan.biodata');
    Route::put('pengaturan/password/{id}', 'PengaturanController@password')->name('pengaturan.password');

    // Admin
    Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('role:admin')->group(function () {
        // Master
        Route::namespace('Master')->prefix('master')->name('master.')->group(function () {
            // Data Kewarganegaraan
            Route::resource('kewarganegaraan', 'KewarganegaraanController');
            Route::get('data/kewarganegaraan', 'KewarganegaraanController@data')->name('kewarganegaraan.data');

            // Data User
            Route::resource('user', 'UserController');
            Route::get('data/user', 'UserController@data')->name('user.data');
            Route::post('data/user/{action}/{user}', 'UserController@action')->name('user.action');
            Route::post('verified/user/{id}', 'UserController@verified')->name('user.verified');

            // Data Author
            Route::get('author', 'UserController@author')->name('author.index');

            // Data Reviewer
            // Route::get('reviewer', 'UserController@reviewer')->name('reviewer.index');

            // Data FAQ
            Route::resource('faq', 'FAQController');
            Route::get('data/faq', 'FAQController@data')->name('faq.data');

            // Data Portal
            Route::resource('portal', 'PortalController');
            Route::get('data/portal', 'PortalController@data')->name('portal.data');
            Route::get('data/portal/trashed', 'PortalController@dataTrashed')->name('portal.data_trashed');

            // Data Conference
            Route::resource('conference', 'ConferenceController');
            Route::get('data/conference', 'ConferenceController@data')->name('conference.data');

            // Data Publisher
            Route::resource('publisher', 'PublisherController');
            Route::get('data/publisher', 'PublisherController@data')->name('publisher.data');
        });
    });
    Route::namespace('AdminConference')->prefix('admin-conference')->name('admin-conference.')->middleware('role:admin_conference')->group(function () {
        Route::get('/', 'HomeController@index')->name('home');
        // Conference
        Route::namespace('Conference')->prefix('conference')->name('conference.')->group(function () {
            // Data Conference
            Route::resource('conference', 'ConferenceController');
            Route::get('data/conference', 'ConferenceController@data')->name('conference.data');
            Route::get('tanggal/conference/{id}', 'ConferenceController@tanggal')->name('conference.tanggal');
            Route::get('publisher/conference/{id}', 'ConferenceController@publisher')->name('conference.publisher');
            Route::get('publishers/conference/{id}', 'ConferenceController@publishers')->name('conference.publishers');

            // Data Topik
            Route::resource('topik', 'TopikController');
            Route::get('data/topik', 'TopikController@data')->name('topik.data');

            // Data Keynote Speaker
            Route::resource('keynote', 'KeynoteSpeakerController');
            Route::get('data/keynote', 'KeynoteSpeakerController@data')->name('keynote.data');

            // Data Pengumuman
            Route::resource('pengumuman', 'PengumumanController');
            Route::get('data/pengumuman', 'PengumumanController@data')->name('pengumuman.data');

            // Data Sponsor
            Route::resource('sponsor', 'SponsorController');
            Route::get('data/sponsor', 'SponsorController@data')->name('sponsor.data');

            // Data Voucher
            Route::resource('voucher', 'VoucherController');
            Route::get('data/voucher', 'VoucherController@data')->name('voucher.data');

            // Data Tanggal Penting
            Route::resource('tanggal-penting', 'TanggalPentingController');
            Route::get('data/tanggal-penting', 'TanggalPentingController@data')->name('tanggal-penting.data');

            // Data Organizer
            Route::resource('organizer', 'OrganizerController');

            // Data Halaman Statis
            Route::resource('page', 'PageController');
            Route::get('data/page', 'PageController@data')->name('page.data');

            // LOA
            Route::resource('loa', 'LoaController');
            Route::put('loa/gambar/{loa}', 'LoaController@gambar')->name('loa.gambar');
        });

        // Submission
        Route::namespace('Submission')->prefix('submission')->name('submission.')->group(function () {
            // Data Pembayaran Submission
            Route::get('pembayaran', 'PembayaranController@index')->name('pembayaran.index');
            Route::get('pembayaran/{id}', 'PembayaranController@show')->name('pembayaran.show');
            Route::put('pembayaran/{id}', 'PembayaranController@update')->name('pembayaran.update');
            Route::get('data/pembayaran', 'PembayaranController@data')->name('pembayaran.data');

            // Data Review Submission
            Route::get('review', 'ReviewController@index')->name('review.index');
            Route::get('data/review', 'ReviewController@data')->name('review.data');
            Route::get('riwayat-data/review', 'ReviewController@riwayatdata')->name('review.riwayat-data');
            Route::put('review/review/{id}', 'ReviewController@review')->name('review.review');
            Route::put('review/validasi/{id}', 'ReviewController@validasi')->name('review.validasi');
            Route::get('review/{id}', 'ReviewController@show')->name('review.show');

            // Data Book of Abstract
            Route::get('abstract', 'AbstractController@index')->name('abstract.index');
            Route::get('data/abstract', 'AbstractController@data')->name('abstract.data');
            Route::get('abstract/{id}', 'AbstractController@show')->name('abstract.show');

            // Data Full Paper
            Route::get('full-paper', 'AbstractController@fullpaper')->name('abstract.full-paper');
            Route::get('data/full-paper', 'AbstractController@fullpaperdata')->name('abstract.full-paper.data');
        });

        // Laporan
        Route::prefix('laporan')->name('laporan.')->group(function () {
            // Data Laporan Peserta Conference
            Route::get('peserta-conference', 'LaporanController@peserta_index')->name('peserta.index');
            Route::get('peserta-conference/data/{submission}', 'LaporanController@peserta_detail')->name('peserta.detail');
            Route::get('peserta-conference/{id}', 'LaporanController@peserta_show')->name('peserta.show');

            // Data Laporan Pembayaran
            Route::get('pembayaran', 'LaporanController@pembayaran_index')->name('pembayaran.index');
            Route::get('pembayaran/data/{submission}', 'LaporanController@pembayaran_show')->name('pembayaran.show');
        });

        // Publication
        Route::namespace('Publication')->prefix('publication')->name('publication.')->group(function () {
            // Proceeding
            Route::prefix('proceeding')->name('proceeding.')->group(function () {
                Route::get('dropdown', 'ProceedingController@dropdown')->name('dropdown');
            });
            Route::resource('proceeding', 'ProceedingController');
            
            // Proceeding
            Route::prefix('proceeding-detail')->name('proceeding-detail.')->group(function () {
                Route::get('submission/{proceeding}', 'ProceedingDetailController@submission')->name('submission');
            });
            Route::resource('proceeding-detail', 'ProceedingDetailController');

            // Submission
            Route::prefix('submission')->name('submission.')->group(function () {
                Route::get('author/{submission}', 'SubmissionController@show_author')->name('show_author');
                Route::get('users', 'SubmissionController@users')->name('users');
                Route::get('{submission}', 'SubmissionController@show')->name('show');
                Route::put('update-keyword/{submission}', 'SubmissionController@update_keyword')->name('update_keyword');
                Route::put('update-abstract/{submission}', 'SubmissionController@update_abstract')->name('update_abstract');
                Route::put('update-author/{submission}', 'SubmissionController@update_author')->name('update_author');
                Route::delete('delete-author/{submission}', 'SubmissionController@delete_author')->name('delete_author');
                Route::put('update-galley/{submission}', 'SubmissionController@update_galley')->name('update_galley');
            });
        });
    });
    Route::namespace('Author')->prefix('author')->name('author.')->middleware('role:author')->group(function () {
        // Data Profil
        Route::get('author/profil', 'HomeController@profil')->name('profil.index');

        // Author
        Route::namespace('Author')->prefix('author')->name('author.')->group(function () {
            // Data Daftar Conference
            Route::get('conference', 'ConferenceController@index')->name('conference.index');
            Route::get('data/conference', 'ConferenceController@data')->name('conference.data');
            Route::get('kewarganegaraan/conference', 'ConferenceController@kewarganegaraan')->name('conference.kewarganegaraan');
            Route::post('conference', 'ConferenceController@store')->name('conference.store');

            // Data Submission
            Route::get('submission', 'SubmissionController@index')->name('submission.index');
            Route::put('submission/{submission}', 'SubmissionController@update')->name('submission.update');
            Route::get('data/submission/{id}', 'SubmissionController@data')->name('submission.data');
            Route::get('submission/{id}', 'SubmissionController@show')->name('submission.show');
            Route::get('submission/{id}/loa', 'SubmissionController@loa')->name('submission.loa');
            Route::post('submission/{id}/{step}', 'SubmissionController@step')->name('submission.step');
            Route::delete('submission/{submission}', 'SubmissionController@destroy')->name('submission.destroy');

            // Data Publikasi
            Route::prefix('publication')->name('publication.')->group(function () {
                Route::get('author/{submission}', 'PublicationController@submission_show_author')->name('show_author');
                Route::get('users', 'PublicationController@submission_users')->name('users');
                Route::get('{submission}', 'PublicationController@submission_show')->name('show');
                Route::put('update-keyword/{submission}', 'PublicationController@submission_update_keyword')->name('update_keyword');
                Route::put('update-abstract/{submission}', 'PublicationController@submission_update_abstract')->name('update_abstract');
                Route::put('update-author/{submission}', 'PublicationController@submission_update_author')->name('update_author');
                Route::delete('delete-author/{submission}', 'PublicationController@submission_delete_author')->name('delete_author');
                Route::put('update-galley/{submission}', 'PublicationController@submission_update_galley')->name('update_galley');
            });
        });
    });
});

// Mockup Admin Conference Data Submission
Route::view('admin-conference/submission', 'pages.admin-conference.submission.submission');

// Mockup Admin Conference Data Laporan
Route::view('admin-conference/laporan/submission', 'pages.admin-conference.laporan.submission');
// Route::view('admin-conference/laporan/pembayaran', 'pages.admin-conference.laporan.pembayaran');
Route::view('admin-conference/laporan/published', 'pages.admin-conference.laporan.published');

// Mockup Author
Route::view('author', 'pages.author.home');
Route::view('author/book-of-abstract', 'pages.author.book-of-abstract');

/*
    ======================================== Front End ========================================
*/

//
Route::get('panel', 'App\Http\Controllers\HomeController@index')->middleware('auth', 'verified');
Route::post('role/{name}', 'App\Http\Controllers\HomeController@role')->middleware('auth', 'verified')->name('role');

Route::namespace('App\Http\Controllers\Front')->group(function () {
    Route::get('lang/{lang}', 'HomeController@lang')->name('lang');
    Route::name('portal.')->group(function () {
        Route::get('', 'HomeController@index')->name('home');
        Route::get('conferences', 'HomeController@conferences')->name('conferences');

        Route::prefix('announcements')->name('announcements.')->group(function () {
            Route::get('', 'HomeController@announcements')->name('index');
            Route::get('{pengumuman}', 'HomeController@announcements_show')->name('show');
        });
    });

    Route::prefix('{id}')->name('conference.')->group(function () {
        Route::get('', 'ConferenceController@index')->name('home');
        Route::get('register', 'ConferenceController@register')->name('register');
        Route::get('download', 'ConferenceController@download')->name('download');
        // Route::view('gallery', 'front.conference.gallery')->name('gallery');
        Route::get('schedule', 'ConferenceController@schedule')->name('schedule');
        Route::get('speaker', 'ConferenceController@speaker')->name('speaker');

        Route::prefix('page')->name('page.')->group(function () {
            Route::get('', 'ConferenceController@indexPage')->name('index');
            Route::get('{slug}', 'ConferenceController@showPage')->name('show');
        });

        Route::prefix('announcements')->name('announcements.')->group(function () {
            Route::get('', 'ConferenceController@announcements')->name('index');
            Route::get('{pengumuman}', 'ConferenceController@announcements_show')->name('show');
        });

        Route::prefix('proceeding')->name('proceeding.')->group(function () {
            Route::get('', 'ProceedingController@index')->name('index');
            Route::get('{slug}/search', 'ProceedingController@search')->name('search');
            Route::get('{slug}/{code}', 'ProceedingController@submission')->name('submission');
            Route::get('{slug}', 'ProceedingController@show')->name('show');
        });
    });
});
