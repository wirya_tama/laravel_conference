<?php

namespace App\Http\Controllers\AdminConference\Submission;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Submission;
use Auth;
use Yajra\DataTables\Facades\DataTables;

class PembayaranController extends Controller
{
    public function index()
    {
        return view('pages.admin-conference.submission.pembayaran');
    }

    public function show($id)
    {
        return Submission::firstSubmissionWithConferenceByQuerySelect(
            ['submission.id' => $id],
            ['submission.*', 'conference.conference_bayar_abstrak', 'conference.conference_bayar_publikasi']
        );
    }

    public function update(Submission $id)
    {
        if ($id->submission_status == 1) {
            Submission::updateSubmissionValidasiPembayaran($id->id, 2);
        } else {
            Submission::updateSubmissionValidasiPembayaran($id->id, 5);
        }

        return $this->status(1, 'Pembayaran berhasil divalidasi');
    }

    public function data()
    {
        $submission = Submission::join('conference', 'submission.conference_id', 'conference.id')
                                ->join('author_submission', 'submission.id', 'author_submission.submission_id')
                                ->join('users', 'author_submission.user_id', 'users.id')
                                ->orderBy('author_submission.id', 'ASC')
                                ->groupBy('submission.id')
                                ->where('submission_pembayaran_abstrak_status', 0)
                                ->where('submission_pembayaran_abstrak_file', '!=', NULL)
                                ->where('conference.user_id', Auth::id())
                                ->orWhere('submission_pembayaran_publikasi_status', 0)
                                ->where('submission_pembayaran_publikasi_file', '!=', NULL)
                                ->where('conference.user_id', Auth::id())
                                ->select(['submission.id', 'submission_judul', 'name as nama', 'conference_nama as conference', 'submission_pembayaran_abstrak_file', 'submission_pembayaran_abstrak_status', 'submission_pembayaran_publikasi_file', 'submission_pembayaran_publikasi_status']);
        
        return DataTables::of($submission)
            ->addColumn('aksi', function ($item) {
                if ($item->submission_pembayaran_abstrak_status == 0) {
                    return '<div class="btn-group"><button type="button" class="btn btn-success validasi" data-pembayaran="1" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button></div>';
                } else {
                    return '<div class="btn-group"><button type="button" class="btn btn-success validasi" data-pembayaran="2" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button></div>';
                }
            })
            ->addColumn('status_abstrak', function ($item) {
                if ($item->submission_pembayaran_abstrak_status == 0 && $item->submission_pembayaran_abstrak_file != NULL) {
                    return '<div disabled class="badge badge-md badge-primary">Menunggu Konfirmasi</div>';
                } elseif ($item->submission_pembayaran_abstrak_status == 1) {
                    return '<div disabled class="badge badge-md badge-success">Telah Bayar</div>';
                } else {
                    return '<div disabled class="badge badge-md badge-danger">Belum Bayar</div>';
                }
            })
            ->addColumn('status_publikasi', function ($item) {
                if ($item->submission_pembayaran_publikasi_status == 0 && $item->submission_pembayaran_publikasi_file != NULL) {
                    return '<div disabled class="badge badge-md badge-primary">Menunggu Konfirmasi</div>';
                } elseif ($item->submission_pembayaran_publikasi_status == 1) {
                    return '<div disabled class="badge badge-md badge-success">Telah Bayar</div>';
                } else {
                    return '<div disabled class="badge badge-md badge-danger">Belum Bayar</div>';
                }
            })
            ->rawColumns(['aksi', 'status_abstrak', 'status_publikasi'])
            ->removeColumn('id', 'submission_pembayaran_abstrak_status', 'submission_pembayaran_abstrak_status', 'submission_pembayaran_publikasi_file', 'submission_pembayaran_publikasi_status')
            ->make(true);
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }
}
