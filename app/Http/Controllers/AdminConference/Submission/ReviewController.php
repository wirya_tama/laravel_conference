<?php

namespace App\Http\Controllers\AdminConference\Submission;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Submission;
use Auth;
use App\Models\AuthorSubmission;
use App\Models\ReviewSubmission;
use App\Models\RiwayatSubmission;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ReviewController extends Controller
{
    public function index()
    {
        return view('pages.admin-conference.submission.review');
    }
    
    public function show($id)
    {
        $data['author'] = AuthorSubmission::getAuthorSubmissionBySubmission($id);

        $data['review'] = ReviewSubmission::getReviewSubmissionBySubmission($id);
        foreach ($data['review'] as $key => $value) {
            $data['review'][$key]['tanggall'] = date('d-m-Y', strtotime($data['review'][$key]['tanggal']));
        }
        
        return json_encode($data);
    }

    public function review(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'file'      => 'max:5120'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }

        // Get Total Artikel
        $count = ReviewSubmission::getReviewSubmissionBySubmissionCount($id);

        $image = $request->file('file');
        $file = sprintf("%'04d", $id) . '-'. sprintf("%'02d", $count + 1) . '.' . $image->getClientOriginalExtension();
        $image->move('files/submission/full-paper/', $file);

        // Store Review Submission
        ReviewSubmission::storeReviewSubmission(Auth::id(), $id, $request->pesan, $file, 1);

        return $this->status(1, 'Artikel berhasil direview');
    }

    public function validasi($id)
    {
        // Get Submission
        $submission = Submission::firstSubmission($id);

        // Update Status Submission
        Submission::updateSubmissionStatus($submission->id, 4);

        // Store Riwayat Submission
        RiwayatSubmission::storeRiwayatSubmission($submission->id, 4);

        return $this->status(1, 'Artikel berhasil divalidasi');
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }

    public function data()
    {
        $submission = Submission::leftJoin('author_submission', 'submission.id', 'author_submission.submission_id')
                                ->join('conference', 'submission.conference_id', 'conference.id')
                                ->join('users', 'author_submission.user_id', 'users.id')
                                ->where('submission_status', 3)
                                ->where('conference.user_id', Auth::id())
                                ->groupBy('submission.id')
                                ->select(['submission.id', 'submission_judul', 'users.name']);
        
        return DataTables::of($submission)
            ->addColumn('aksi', function ($item) {
                return '<button type="button" class="btn btn-warning ubah" data-toggle="modal" data-target="#modal" data-form="1" data-value="'. $item->id. '"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button>';
            })
            ->rawColumns(['aksi'])
            ->removeColumn('id')
            ->make(true);
    }

    public function riwayatdata()
    {
        $submission = Submission::leftJoin('author_submission', 'submission.id', 'author_submission.submission_id')
                                ->join('conference', 'submission.conference_id', 'conference.id')
                                ->join('users', 'author_submission.user_id', 'users.id')
                                ->where('submission_status', '>', 3)
                                ->where('conference.user_id', Auth::id())
                                ->groupBy('submission.id')
                                ->select(['submission.id', 'submission_judul', 'users.name']);
        
        return DataTables::of($submission)
            ->addColumn('aksi', function ($item) {
                return '<button type="button" class="btn btn-primary ubah" data-toggle="modal" data-target="#modal" data-form="0" data-value="'. $item->id. '"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></button>';
            })
            ->rawColumns(['aksi'])
            ->removeColumn('id')
            ->make(true);
    }
}
