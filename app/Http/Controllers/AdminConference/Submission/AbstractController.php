<?php

namespace App\Http\Controllers\AdminConference\Submission;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Models\Conference;
use App\Models\Submission;
use Yajra\DataTables\Facades\DataTables;

class AbstractController extends Controller
{
    public function index()
    {
        return view('pages.admin-conference.submission.abstract');
    }

    public function data()
    {
        $conference = Conference::join('portal', 'conference.portal_id', 'portal.id')
                            ->select('conference.id', 'conference_nama as nama', 'portal_nama as portal', 'conference_level as level')
                            ->where('user_id', Auth::id())
                            ->where('conference_status', 1)
                            ->select(['conference.id', 'portal_nama', 'conference_nama']);
        
        return DataTables::of($conference)
            ->addColumn('aksi', function ($item) {
                return '<a href="/admin-conference/submission/abstract/'. $item->id .'" target="_blank" class="btn btn-primary"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg></a>';
            })
            ->rawColumns(['aksi'])
            ->removeColumn('id')
            ->make(true);
    }
    
    public function show($id)
    {
        $data = Submission::getSubmissionBookOfAbstract($id);
        $conference = Conference::firstConferencebySubmission($id);
        
        return view('pages.pdf.book-of-abstract', compact('data', 'conference'));
    }

    public function fullpaper()
    {
        return view('pages.admin-conference.submission.full-paper');
    }

    public function fullpaperdata()
    {
        $conference = Submission::join('conference', 'submission.conference_id', 'conference.id')
                                ->where('conference.user_id', Auth::id())
                                ->where('submission_status', 6)
                                ->select(['submission.id', 'conference_nama', 'submission_judul', 'submission_full_paper as file']);
        
        return DataTables::of($conference)
            ->addColumn('aksi', function ($item) {
                if ($item->file != NULL) {
                    return '<a href="/files/submission/full-paper/'. $item->file .'" class="btn btn-primary"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg></a>';
                }
            })
            ->rawColumns(['aksi'])
            ->removeColumn('id', 'file')
            ->make(true);
    }
}