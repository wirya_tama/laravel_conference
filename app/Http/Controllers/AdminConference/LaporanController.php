<?php

namespace App\Http\Controllers\AdminConference;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Submission;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Conference;
use Auth;

class LaporanController extends Controller
{
    public function peserta_index()
    {
        $data = Conference::getConferencebyUser(Auth::id());

        return view('pages.admin-conference.laporan.peserta', compact('data'));
    }

    public function peserta_show($id)
    {
        $submission = Submission::join('conference', 'submission.conference_id', 'conference.id')
                                ->join('author_submission', 'submission.id', 'author_submission.submission_id')
                                ->join('users', 'author_submission.user_id', 'users.id')
                                ->join('data_user', 'users.id', 'data_user.user_id')
                                ->select(['submission.id', 'submission_judul', 'users.name', 'du_afiliasi', 'submission_pembayaran_abstrak_file', 'submission_pembayaran_abstrak_status', 'submission_pembayaran_publikasi_file', 'submission_pembayaran_publikasi_status'])
                                ->groupBy('submission.id')
                                ->orderBy('author_submission.id', 'ASC')
                                ->where('conference.id', $id);
        
        return DataTables::of($submission)
            ->addColumn('status_abstrak', function ($item) {
                if ($item->submission_pembayaran_abstrak_status == 0 && $item->submission_pembayaran_abstrak_file != NULL) {
                    return "<div class='text-center'>Menunggu Konfirmasi</div>";
                } elseif ($item->submission_pembayaran_abstrak_status == 1) {
                    return "<div class='text-center'>Telah Bayar</div>";
                } else {
                    return "<div class='text-center'>Belum Bayar</div>";
                }
            })
            ->addColumn('status_publikasi', function ($item) {
                if ($item->submission_pembayaran_publikasi_status == 0 && $item->submission_pembayaran_publikasi_file != NULL) {
                    return "<div class='text-center'>Menunggu Konfirmasi</div>";
                } elseif ($item->submission_pembayaran_publikasi_status == 1) {
                    return "<div class='text-center'>Telah Bayar</div>";
                } else {
                    return "<div class='text-center'>Belum Bayar</div>";
                }
            })
            ->addColumn('aksi', function ($item) {
                return '<div class="text-center"><button type="button" class="btn btn-primary" onclick="detail('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></button></div>';
            })
            ->rawColumns(['status_abstrak', 'status_publikasi', 'aksi'])
            ->removeColumn('id', 'submission_pembayaran_abstrak_file', 'submission_pembayaran_abstrak_status', 'submission_pembayaran_publikasi_file', 'submission_pembayaran_publikasi_status')
            ->make(true);
    }

    public function peserta_detail(Submission $submission)
    {
        return $submission;
    }

    public function pembayaran_index()
    {
        $data = Conference::getConferencebyUser(Auth::id());

        return view('pages.admin-conference.laporan.pembayaran', compact('data'));
    }

    public function pembayaran_show($id)
    {
        $submission = Submission::join('conference', 'submission.conference_id', 'conference.id')
                                ->join('author_submission', 'submission.id', 'author_submission.submission_id')
                                ->join('users', 'author_submission.user_id', 'users.id')
                                ->join('data_user', 'users.id', 'data_user.user_id')
                                ->select(
                                    'submission.id', 'users.name', 'du_afiliasi', 'submission_judul',
                                    'submission_pembayaran_abstrak_voucher',
                                    'submission_pembayaran_abstrak_file',
                                    'submission_pembayaran_abstrak_status',
                                    'submission_pembayaran_publikasi_voucher',
                                    'submission_pembayaran_publikasi_file',
                                    'submission_pembayaran_publikasi_status',
                                    'submission_pembayaran_abstrak_tanggal',
                                    'submission_pembayaran_abstrak_nominal',
                                    'submission_pembayaran_abstrak_bank',
                                    'submission_pembayaran_abstrak_rekening',
                                    'submission_pembayaran_abstrak_invoice',
                                    'submission_pembayaran_abstrak_pesan',
                                    'submission_pembayaran_publikasi_tanggal',
                                    'submission_pembayaran_publikasi_nominal',
                                    'submission_pembayaran_publikasi_bank',
                                    'submission_pembayaran_publikasi_rekening',
                                    'submission_pembayaran_publikasi_invoice',
                                    'submission_pembayaran_publikasi_pesan',
                                )
                                ->groupBy('submission.id')
                                ->orderBy('author_submission.id', 'ASC')
                                ->where('conference.id', $id)
                                ->get();
        
        return DataTables::of($submission)
            ->addColumn('status_abstrak', function ($item) {
                if ($item->submission_pembayaran_abstrak_status == 0 && $item->submission_pembayaran_abstrak_file != NULL) {
                    return "<div class='text-center'>Menunggu Konfirmasi</div>";
                } elseif ($item->submission_pembayaran_abstrak_status == 1) {
                    return "<div class='text-center'>Telah Bayar</div>";
                } else {
                    return "<div class='text-center'>Belum Bayar</div>";
                }
            })
            ->addColumn('status_publikasi', function ($item) {
                if ($item->submission_pembayaran_publikasi_status == 0 && $item->submission_pembayaran_publikasi_file != NULL) {
                    return "<div class='text-center'>Menunggu Konfirmasi</div>";
                } elseif ($item->submission_pembayaran_publikasi_status == 1) {
                    return "<div class='text-center'>Telah Bayar</div>";
                } else {
                    return "<div class='text-center'>Belum Bayar</div>";
                }
            })
            ->rawColumns(['status_abstrak', 'status_publikasi'])
            ->make(true);
    }
}