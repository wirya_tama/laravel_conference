<?php

namespace App\Http\Controllers\AdminConference\Conference;

use App\Http\Controllers\Controller;
use App\Models\KeynoteSpeaker;
use Illuminate\Http\Request;

use Yajra\DataTables\Facades\DataTables;
use Auth;
use App\Models\Conference;
use Illuminate\Support\Facades\Validator;

class KeynoteSpeakerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conference = Conference::getConferenceIdNama(Auth::id());

        return view('pages.admin-conference.conference.keynote', compact('conference'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        KeynoteSpeaker::storeKeynoteSpeaker($request);

        return $this->status(1, 'Keynote Speaker berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\KeynoteSpeaker  $keynoteSpeaker
     * @return \Illuminate\Http\Response
     */
    public function show(KeynoteSpeaker $keynote)
    {
        return json_encode($keynote);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\KeynoteSpeaker  $keynoteSpeaker
     * @return \Illuminate\Http\Response
     */
    public function edit(KeynoteSpeaker $keynoteSpeaker)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\KeynoteSpeaker  $keynoteSpeaker
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, KeynoteSpeaker $keynote)
    {
        if ($request->profile_img != NULL) {
            return $this->uploadFile($keynote, $request->profile_img);
        }

        KeynoteSpeaker::updateKeynoteSpeaker($keynote->id, $request);

        return $this->status(1, 'Keynote Speaker berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\KeynoteSpeaker  $keynoteSpeaker
     * @return \Illuminate\Http\Response
     */
    public function destroy(KeynoteSpeaker $keynote)
    {
        KeynoteSpeaker::deleteKeynoteSpeaker($keynote->id);

        return $this->status(1, 'Keynote Speaker berhasil dihapus');
    }

    public function data()
    {
        $keynote = KeynoteSpeaker::join('conference', 'keynote_speaker.conference_id', 'conference.id')
                    ->select(['keynote_speaker.id', 'keynote_speaker.ks_nama', 'conference.conference_nama'])
                    ->where('user_id', Auth::id());
        
        return DataTables::of($keynote)
            ->addColumn('aksi', function ($item) {
                return '<form class="text-center" method="POST"><div class="btn-group"><button type="button" class="btn btn-primary file" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg></button><button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button><button type="button" class="btn btn-danger hapus" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button></div></form>';
            })
            ->rawColumns(['aksi'])
            ->removeColumn('id')
            ->make(true);
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }

    public function uploadFile($keynote, $foto)
    {
        $folderPath = 'images/keynote_speaker/';

        $image_parts = explode(";base64,", $foto);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);

        $imageName = uniqid() . '.png';

        $imageFullPath = $folderPath.$imageName;

        file_put_contents($imageFullPath, $image_base64);

        KeynoteSpeaker::updateFileKeynoteSpeaker($keynote->id, $imageName);

        return back();
    }
}
