<?php

namespace App\Http\Controllers\AdminConference\Conference;

use App\Http\Controllers\Controller;
use App\Models\Pengumuman;
use Illuminate\Http\Request;

use Yajra\DataTables\Facades\DataTables;
use Auth;
use App\Models\Conference;
use Illuminate\Support\Facades\Validator;

class PengumumanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conference = Conference::getConferenceIdNama(Auth::id());

        return view('pages.admin-conference.conference.pengumuman', compact('conference'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Pengumuman::storePengumuman($request);
        
        return $this->status(1, 'Pengumuman berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pengumuman  $pengumuman
     * @return \Illuminate\Http\Response
     */
    public function show(Pengumuman $pengumuman)
    {
        return json_encode($pengumuman);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pengumuman  $pengumuman
     * @return \Illuminate\Http\Response
     */
    public function edit(Pengumuman $pengumuman)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pengumuman  $pengumuman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pengumuman $pengumuman)
    {
        if ($request->gambar != NULL) {
            return $this->uploadFile($pengumuman, $request);
        }

        Pengumuman::updatePengumuman($pengumuman->id, $request);
        
        return $this->status(1, 'Pengumuman berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pengumuman  $pengumuman
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pengumuman $pengumuman)
    {
        Pengumuman::deletePengumuman($pengumuman->id);

        return $this->status(1, 'Pengumuman berhasil dihapus');
    }

    public function data()
    {
        $pengumuman = Pengumuman::join('conference', 'pengumuman.conference_id', 'conference.id')
                        ->join('portal', 'conference.portal_id', 'portal.id')
                        ->select(['pengumuman.id', 'pengumuman.pengumuman_judul', 'conference.conference_nama', 'pengumuman.pengumuman_jumlah', 'pengumuman.updated_at'])
                        ->where('user_id', Auth::id());
        
        return DataTables::of($pengumuman)
            ->addColumn('jumlah', function ($item) {
                return $item->pengumuman_jumlah . "x";
            })
            ->addColumn('tanggal', function ($item) {
                return date('d-m-Y', strtotime($item->updated_at));
            })
            ->addColumn('aksi', function ($item) {
                return '<form class="text-center" method="POST"><div class="btn-group"><button type="button" class="btn btn-primary file" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg></button><button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button><button type="button" class="btn btn-danger" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button></div></form>';
            })
            ->rawColumns(['tanggal', 'jumlah', 'aksi'])
            ->removeColumn('pengumuman_jumlah', 'updated_at', 'id')
            ->make(true);
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }

    public function uploadFile($conference, $request)
    {
        $validator = Validator::make($request->all(), [
            'gambar' => 'max:1024'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }

        $image = $request->file('gambar');
        $gambar = rand() . '.' . $image->getClientOriginalExtension();
        $image->move('images/pengumuman/', $gambar);

        Pengumuman::updateFilePengumuman($conference->id, $gambar);

        return $this->status(1, 'File Pengumuman berhasil diubah');
    }
}
