<?php

namespace App\Http\Controllers\AdminConference\Conference;

use App\Http\Controllers\Controller;
use App\Models\TanggalPenting;
use Illuminate\Http\Request;

use Auth;
use App\Models\Conference;
use Illuminate\Support\Facades\Validator;

class TanggalPentingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conference = Conference::getConferenceIdNama(Auth::id());

        return view('pages.admin-conference.conference.tanggal-penting', compact('conference'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'tanggal_awal' => 'date|after_or_equal:' . date('Y-m-d'),
            'tanggal_akhir' => 'date|after_or_equal:tanggal_awal',
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }

        TanggalPenting::storeTanggalPenting($request->conference, $request->judul, $request->tanggal_awal, $request->tanggal_akhir, 0);

        return $this->status(1, 'Tanggal Penting berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TanggalPenting  $tanggalPenting
     * @return \Illuminate\Http\Response
     */
    public function show(TanggalPenting $tanggal_penting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TanggalPenting  $tanggalPenting
     * @return \Illuminate\Http\Response
     */
    public function edit(TanggalPenting $tanggal_penting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TanggalPenting  $tanggalPenting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TanggalPenting $tanggal_penting)
    {
        $validator = Validator::make($request->all(), [
            'tanggal_awal' => 'date|after_or_equal:' . date('Y-m-d'),
            'tanggal_akhir' => 'date|after_or_equal:tanggal_awal',
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }

        if ($tanggal_penting->tp_conference == 1 && $tanggal_penting->conference_id != $request->conference) {
            return $this->status(0, 'Conference pada Tanggal Penting tidak dapat diubah');
        }

        TanggalPenting::updateTanggalPenting($tanggal_penting, $request);

        return $this->status(1, 'Tanggal Penting berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TanggalPenting  $tanggalPenting
     * @return \Illuminate\Http\Response
     */
    public function destroy(TanggalPenting $tanggal_penting)
    {
        if ($tanggal_penting->tp_conference == 1) {
            return $this->status(0, 'Tanggal Penting tidak dapat dihapus');
        }
        
        TanggalPenting::deleteTanggalPenting($tanggal_penting->id);

        return $this->status(1, 'Tanggal Penting berhasil diubah');
    }

    public function data()
    {
        return TanggalPenting::getTanggalPentingAppCalendar(Auth::id());
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }
}
