<?php

namespace App\Http\Controllers\AdminConference\Conference;

use App\Http\Controllers\Controller;
use App\Models\Topik;
use Illuminate\Http\Request;

use Yajra\DataTables\Facades\DataTables;
use Auth;
use App\Models\Conference;

class TopikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conference = Conference::getConferenceIdNama(Auth::id());

        return view('pages.admin-conference.conference.topik', compact('conference'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Topik::storeTopik($request);

        return $this->status(1, 'Topik berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Topik  $topik
     * @return \Illuminate\Http\Response
     */
    public function show(Topik $topik)
    {
        return json_encode($topik);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Topik  $topik
     * @return \Illuminate\Http\Response
     */
    public function edit(Topik $topik)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Topik  $topik
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Topik $topik)
    {
        Topik::updateTopik($topik->id, $request);

        return $this->status(1, 'Topik berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Topik  $topik
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topik $topik)
    {
        Topik::deleteTopik($topik->id);

        return $this->status(1, 'Topik berhasil dihapus');
    }

    public function data()
    {
        $topik = Topik::join('conference', 'topik.conference_id', 'conference.id')
                    ->select(['topik.id', 'topik.topik_nama', 'conference.conference_nama'])
                    ->where('user_id', Auth::id());
        
        return DataTables::of($topik)
            ->addColumn('aksi', function ($item) {
                return '<form class="text-center" method="POST"><div class="btn-group"><button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button><button type="button" class="btn btn-danger hapus" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button></div></form>';
            })
            ->rawColumns(['aksi'])
            ->removeColumn('id')
            ->make(true);
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }
}
