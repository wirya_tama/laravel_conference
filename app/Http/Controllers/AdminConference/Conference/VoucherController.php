<?php

namespace App\Http\Controllers\AdminConference\Conference;

use App\Http\Controllers\Controller;
use App\Models\Voucher;
use Illuminate\Http\Request;

use App\Models\Conference;
use Yajra\DataTables\Facades\DataTables;
use Auth;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $conference = Conference::getConferenceIdNama(Auth::id());

        return view('pages.admin-conference.conference.voucher', compact('conference'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        for ($i=1; $i <= $request->jumlah; $i++) { 
            Voucher::storeVoucher($request);
        }

        return $this->status(1, 'Voucher berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function show(Voucher $voucher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function edit(Voucher $voucher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Voucher $voucher)
    {
        Voucher::updateVoucher($voucher->id, $request);

        return $this->status(1, 'Voucher berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Voucher $voucher)
    {
        Voucher::deleteVoucher($voucher->id);

        return $this->status(1, 'Voucher berhasil dihapus');
    }

    public function data()
    {
        $voucher = Voucher::join('conference', 'voucher.conference_id', 'conference.id')
                        ->join('portal', 'conference.portal_id', 'portal.id')
                        ->select(['voucher.id', 'voucher_kode', 'conference_id', 'conference_nama', 'voucher_status'])
                        ->where('user_id', Auth::id());
        
        return DataTables::of($voucher)
            ->addColumn('aksi', function ($item) {
                return '<form class="text-center" method="POST"><div class="btn-group"><button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'" data-kode="'. $item->voucher_kode .'" data-conference="'. $item->conference_id .'" data-status="'. $item->voucher_status .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button><button type="button" class="btn btn-danger" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button></div></form>';
            })
            ->addColumn('status', function ($item) {
                if ($item->voucher_status == 1) {
                    return '<div disabled class="badge badge-md badge-success">Aktif</div>';
                } else {
                    return '<div disabled class="badge badge-md badge-danger">Tidak Aktif</div>';
                }
            })
            ->rawColumns(['status', 'aksi'])
            ->removeColumn('id', 'voucher_status', 'conference_id')
            ->make(true);
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }
}
