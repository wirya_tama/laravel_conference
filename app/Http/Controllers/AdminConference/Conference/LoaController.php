<?php

namespace App\Http\Controllers\AdminConference\Conference;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

use App\Models\Loa;
use App\Models\Conference;
use App\Models\LoaTandaTangan;

class LoaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $conference = Conference::query()
                ->join('portal', 'conference.portal_id', 'portal.id')
                ->join('users', 'conference.user_id', 'users.id')
                ->select('conference.id', 'conference.conference_nama', 'portal.portal_nama')
                ->where('users.id', Auth::id())
                ->get();

            return DataTables::of($conference)
                ->addColumn('aksi', function ($item) {
                    return '<button type="button" class="btn btn-warning" onclick="ubah('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button>';
                })
                ->rawColumns(['aksi'])
                ->removeColumn('id')
                ->make(true);
        }

        return view('pages.admin-conference.conference.loa.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'loa_id' => 'required',
            'gambar' => 'required|mimes:jpeg,jpg,png|max:1024',
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }

        $data = LoaTandaTangan::create([
            'loa_id' => $request->loa_id,
            'title' => $request->title,
            'keterangan' => $request->keterangan,
        ]);

        $image = $request->file('gambar');
        $gambar = rand() . '.' . $image->getClientOriginalExtension();
        $image->move('images/loa/', $gambar);

        LoaTandaTangan::whereId($data->id)->update([
            'gambar' => $gambar
        ]);

        return $this->status(1, 'Tanda Tangan LOA berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Loa  $loa
     * @return \Illuminate\Http\Response
     */
    public function show(Conference $loa)
    {
        $data = Loa::where('conference_id', $loa->id)->first();

        if ($data == null) {
            $new = Loa::create([
                'conference_id' => $loa->id
            ]);
            
            $data = Loa::whereId($new->id)->first();
        }

        $data->ttd = LoaTandaTangan::where('loa_id', $data->id)->get();
        
        return view('pages.admin-conference.conference.loa.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Loa  $loa
     * @return \Illuminate\Http\Response
     */
    public function edit(Loa $loa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Loa  $loa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Loa $loa)
    {
        $data = Loa::whereId($loa->id)->update([
            'kop' => $request->kop,
            'isi' => $request->isi,
        ]);
        
        return $this->status(1, 'LOA berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Loa  $loa
     * @return \Illuminate\Http\Response
     */
    public function destroy(LoaTandaTangan $loa)
    {
        LoaTandaTangan::whereId($loa->id)->delete();

        return $this->status(1, 'Tanda Tangan berhasil dihapus');
    }

    public function gambar(Loa $loa, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'gambar1' => 'nullable|mimes:jpeg,jpg,png|max:1024',
            'gambar2' => 'nullable|mimes:jpeg,jpg,png|max:1024',
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }

        if ($request->gambar1 != null) {
            $image = $request->file('gambar1');
        } else {
            $image = $request->file('gambar2');
        }

        $gambar = rand() . '.' . $image->getClientOriginalExtension();
        $image->move('images/loa/', $gambar);

        $data = Loa::whereId($loa->id);
        if ($request->gambar1 != null) {
            $data->update([
                'gambar1' => $gambar
            ]);
        } else {
            $data->update([
                'gambar2' => $gambar
            ]);
        }
        
        return $this->status(1, 'Logo berhasil ditambah');
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }
}