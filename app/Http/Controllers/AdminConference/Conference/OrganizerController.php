<?php

namespace App\Http\Controllers\AdminConference\Conference;

use App\Http\Controllers\Controller;
use App\Models\Organizer;
use Illuminate\Http\Request;

use Auth;
use App\Models\Conference;
use Illuminate\Support\Facades\Validator;

class OrganizerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Organizer::getDataByUser(Auth::id());
        $conference = Conference::getConferenceIdNama(Auth::id());

        return view('pages.admin-conference.conference.organizer', compact('data', 'conference'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama'          => 'required',
            'conference_id' => 'required',
            'link'          => 'nullable|url',
            'gambar'        => 'nullable|mimes:jpeg,jpg,png|max:1024'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }

        $data = Organizer::storeAllData($request);

        if ($request->gambar != NULL) {
            $this->uploadGambar($data->id, $request);
        }

        return $this->status(1, 'Organizer berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Organizer  $organizer
     * @return \Illuminate\Http\Response
     */
    public function show(Organizer $organizer)
    {
        return $organizer;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Organizer  $organizer
     * @return \Illuminate\Http\Response
     */
    public function edit(Organizer $organizer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Organizer  $organizer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organizer $organizer)
    {
        $validator = Validator::make($request->all(), [
            'nama'          => 'required',
            'conference_id' => 'required',
            'link'          => 'nullable|url',
            'gambar'        => 'nullable|mimes:jpeg,jpg,png|max:1024'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }

        Organizer::updateAllData($organizer->id, $request);

        if ($request->gambar != NULL) {
            $this->uploadGambar($organizer->id, $request);
        }

        return $this->status(1, 'Organizer berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Organizer  $organizer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Organizer $organizer)
    {
        Organizer::deleteDataById($organizer->id);

        return $this->status(1, 'Organizer berhasil dihapus');
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }

    public function uploadGambar($id, $request)
    {
        $image = $request->file('gambar');
        $gambar = rand() . '.' . $image->getClientOriginalExtension();
        $image->move('images/organizer/', $gambar);

        Organizer::updateSingleDataByQuery($id, 'gambar', $gambar);
    }
}
