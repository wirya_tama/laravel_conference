<?php

namespace App\Http\Controllers\AdminConference\Conference;

use App\Http\Controllers\Controller;
use App\Models\Conference;
use Illuminate\Http\Request;

use Yajra\DataTables\Facades\DataTables;
use Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\TanggalPenting;
use App\Models\Publisher;
use App\Models\PublisherConference;
use Str;

class ConferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.admin-conference.conference.conference');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Conference  $conference
     * @return \Illuminate\Http\Response
     */
    public function show(Conference $conference)
    {
        return json_encode($conference);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Conference  $conference
     * @return \Illuminate\Http\Response
     */
    public function edit(Conference $conference)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Conference  $conference
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conference $conference)
    {
        if ($request->file == 1) {
            if ($request->profile_img != NULL) {
                return $this->uploadFileBanner($conference, $request->profile_img);
            }

            return $this->uploadFile($conference, $request);
        }

        if ($request->publisher != NULL) {
            return $this->uploadPublisher($conference, $request);
        }

        if ($request->template != NULL) {
            return $this->uploadTemplate($conference, $request);
        }

        if ($request->tp != NULL) {
            return $this->uploadTanggalPenting($conference->id, $request);
        }
        
        $validator = Validator::make($request->all(), [
            'latitude'      => 'nullable|max:20',
            'longitude'     => 'nullable|max:20',
            'google_maps'   => 'nullable|url',
            'email'         => 'nullable|email',
            'facebook'      => 'nullable|url',
            'instagram'     => 'nullable|url',
            'twitter'       => 'nullable|url',
            'youtube'       => 'nullable|url',
            'kontak_hp1'    => 'nullable|max:15',
            'kontak_hp2'    => 'nullable|max:15',
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }

        if (Conference::checkSlug($request->slug, $conference->id) != NULL) {
            Conference::updateConference($conference->id, $request, ($request->slug).'-'.Str::random(3));
        } else {
            Conference::updateConference($conference->id, $request, $request->slug);
        }
        
        return $this->status(1, 'Conference berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Conference  $conference
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conference $conference)
    {
        //
    }

    public function data()
    {
        $conference = Conference::join('portal', 'conference.portal_id', 'portal.id')
                        ->select(['conference.id', 'conference.conference_nama', 'portal.portal_nama', 'conference.conference_status', 'conference_level'])
                        ->where('user_id', Auth::id());
        
        return DataTables::of($conference)
            ->addColumn('status', function ($item) {
                if ($item->conference_status == 1) {
                    return '<div disabled class="badge badge-md badge-success">Aktif</div>';
                } else if ($item->conference_status == 2) {
                    return '<div disabled class="badge badge-md badge-warning">Draft</div>';
                } else {
                    return '<div disabled class="badge badge-md badge-danger">Tidak Aktif</div>';
                }
            })
            ->addColumn('aksi', function ($item) {
                return '<form class="text-center" method="POST"><div class="btn-group"><button type="button" class="btn btn-success tanggal" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg></button><button type="button" class="btn btn-danger publisher" data-value="'. $item->id .'" data-publisher="'. $item->conference_level .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg></button><button type="button" class="btn btn-primary file" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg></button><button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button></div></form>';
            })
            ->rawColumns(['status', 'aksi'])
            ->removeColumn('conference_status', 'id', 'conference_level')
            ->make(true);
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }

    public function uploadFile($conference, $request)
    {
        $data = [];

        $validator = Validator::make($request->all(), [
            'pamflet'   => 'nullable|max:1024',
            'logo'      => 'nullable|max:1024'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        
        if ($request->logo == NULL) {
            $data['logo'] = $conference->conference_logo;
        } else {
            $image = $request->file('logo');
            $logo = rand() . '.' . $image->getClientOriginalExtension();
            $image->move('images/conference/logo/', $logo);

            $data['logo'] = $logo;
        }

        if ($request->pamflet == NULL) {
            $data['pamflet'] = $conference->conference_pamflet;
        } else {
            $image = $request->file('pamflet');
            $pamflet = rand() . '.' . $image->getClientOriginalExtension();
            $image->move('images/conference/pamflet/', $pamflet);

            $data['pamflet'] = $pamflet;
        }

        Conference::updateFileConference($conference->id, $data);

        return $this->status(1, 'File Conference berhasil diubah');
    }

    public function uploadFileBanner($conference, $banner)
    {
        $folderPath = 'images/conference/banner/';

        $image_parts = explode(";base64,", $banner);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);

        $imageName = uniqid() . '.png';

        $imageFullPath = $folderPath.$imageName;

        file_put_contents($imageFullPath, $image_base64);

        Conference::updateFileBannerConference($conference->id, $imageName);

        return back();
    }

    public function tanggal($id)
    {
        return json_encode(TanggalPenting::getTanggalPenting($id));
    }

    public function uploadTanggalPenting($id, $request)
    {
        foreach ($request->tp as $key => $value) {
            if ($value['tanggal_akhir'] < $value['tanggal_awal']) {
                return $this->status(0, 'Tanggal Akhir pada judul ' . $value['judul'] . ' lebih kecil dari Tanggal Awal');
            }
        }

        TanggalPenting::deleteTanggalPentingByConference($id);

        foreach ($request->tp as $key => $value) {
            TanggalPenting::storeTanggalPenting($id, $value['judul'], $value['tanggal_awal'], $value['tanggal_akhir'], $value['conference']);
        }

        return $this->status(1, 'Tanggal Conference berhasil diubah');
    }

    public function uploadPublisher($conference, $request)
    {
        PublisherConference::deletePublisherConferenceByConference($conference->id);

        foreach ($request->publisher as $key => $value) {
            PublisherConference::storePublisherConference($conference->id, $value);
        }

        return $this->status(1, 'Publisher Conference berhasil diubah');
    }

    public function publisher($id)
    {
        return json_encode(Publisher::getPublisherbyLevel($id));
    }

    public function publishers($id)
    {
        return json_encode(PublisherConference::getPublisherByConference($id));
    }

    public function uploadTemplate($conference, $request)
    {
        $validator = Validator::make($request->all(), [
            'template'   => 'required|max:1024'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        
        $file = $request->file('template');
        $template = rand() . '.' . $file->getClientOriginalExtension();
        $file->move('files/template/', $template);

        Conference::updateSingleByQuery($conference->id, 'conference_template', $template);

        return $this->status(1, 'File Conference berhasil diubah');
    }
}
