<?php

namespace App\Http\Controllers\AdminConference\Publication;

// Laravel Framework Service Providers
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

// Application Classes
use App\Models\Proceeding;
use App\Models\ProceedingDetail;
use App\Models\Submission;

// Package Service Providers
use Yajra\DataTables\Facades\DataTables;

class ProceedingDetailController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProceedingDetail  $proceeding_detail
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Proceeding $proceeding_detail)
    {
        if ($request->ajax()) {
            $data = ProceedingDetail::query()
                ->orderBy('id', 'desc')
                ->where('proceeding_id', $proceeding_detail->id)
                ->with(['proceeding', 'submission'])
                ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    return '<div class="btn-group"><button type="button" class="btn btn-danger btn-sm p-1" onclick="hapus('. $row->id .')" title="'. __("messages.delete") .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button><button type="button" class="btn btn-primary btn-sm p-1" onclick="detail('. $row->submission_id .')" title="'. __("messages.detail") .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></button></div>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProceedingDetail  $proceeding_detail
     * @return \Illuminate\Http\Response
     */
    public function edit(ProceedingDetail $proceeding_detail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProceedingDetail  $proceeding_detail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proceeding $proceeding_detail)
    {
        if (ProceedingDetail::where('submission_id', $request->submission_id)->first() !== NULL) {
            ProceedingDetail::where('submission_id', $request->submission_id)->update([
                'proceeding_id' => $proceeding_detail->id,
                'submission_id' => $request->submission_id,
            ]);
        } else {
            $proceeding_detail = ProceedingDetail::create([
                'proceeding_id' => $proceeding_detail->id,
                'submission_id' => $request->submission_id,
                'kode'          => sprintf("%'08d", rand(1, 99999999))
            ]);
        }

        return $this->sendResponse($proceeding_detail, trans('messages.flash.created', ['data' => trans('messages.proceeding detail')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProceedingDetail  $proceeding_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProceedingDetail $proceeding_detail)
    {
        $proceeding_detail->delete();

        return $this->sendResponse([], trans('messages.flash.deleted', ['data' => trans('messages.proceeding detail')]));
    }

    public function submission(Request $request, Proceeding $proceeding)
    {
        if ($request->ajax()) {
            $data = Submission::query()
                ->select('submission.id', 'submission_judul')
                ->join('conference', 'submission.conference_id', 'conference.id')
                ->leftJoin('proceeding_details', 'submission.id', 'proceeding_details.submission_id')
                ->where('conference_id', $proceeding->conference_id)
                ->where('submission_status', 6)
                ->whereNull('proceeding_details.id')
                ->orderBy('proceeding_details.id')
                ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    return '<button type="button" class="btn btn-success btn-sm px-50" onclick="tambah('. $row->id .')" title="'. __("messages.add") .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg></button>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }
}
