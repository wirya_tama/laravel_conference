<?php

namespace App\Http\Controllers\AdminConference\Publication;

// Laravel Framework Service Providers
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Auth;
use Validator;

// Application Classes
use App\Http\Controllers\BaseController;
use App\Models\Conference;
use App\Models\Proceeding;
use App\Models\ProceedingDetail;

// Package Service Providers
use Yajra\DataTables\Facades\DataTables;

class ProceedingController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Proceeding::query()
                ->orderBy('id', 'desc')
                ->with('conference')
                ->get();

            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    return '<div class="btn-group btn-sm"><button type="button" class="btn btn-warning" onclick="ubah('. $row->id .')" title="'. __("messages.change") .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button><button type="button" class="btn btn-primary" onclick="detail('. $row->id .')" title="'. __("messages.detail") .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></button><button type="button" class="btn btn-danger hapus" onclick="hapus('. $row->id .')" title="'. __('messages.delete') .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button></div>';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('pages.admin-conference.publication.proceeding.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        // Request validation
        $validator = Validator::make($input, [
            'title'         => 'required',
            'slug'          => 'required|unique:proceedings,slug',
            'conference_id' => 'required|exists:conference,id',
            'editor'        => 'nullable',
            'volume'        => 'nullable',
            'issn'          => 'nullable',
            'isbn'          => 'nullable',
            'tanggal'       => 'required|date',
        ]);
   
        if ($validator->fails())
            return $this->sendError('Validation Error.', $validator->errors()->first());

        // Save Proceeding
        $data = Proceeding::create($validator->validated());

        return $this->sendResponse($data, trans('messages.flash.created', ['data' => trans('messages.proceeding')]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Proceeding  $proceeding
     * @return \Illuminate\Http\Response
     */
    public function show(Proceeding $proceeding)
    {
        return view('pages.admin-conference.publication.proceeding.show', compact('proceeding'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Proceeding  $proceeding
     * @return \Illuminate\Http\Response
     */
    public function edit(Proceeding $proceeding)
    {
        $proceeding->conference;

        return $this->sendResponse($proceeding, trans('messages.flash.retrieved', ['data' => trans('messages.proceeding')]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Proceeding  $proceeding
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proceeding $proceeding)
    {
        $input = $request->all();
   
        // Request validation
        $validator = Validator::make($input, [
            'title'         => 'required',
            'slug'          => ['required', Rule::unique('proceedings')->ignore($proceeding->id)],
            'conference_id' => 'required|exists:conference,id',
            'editor'        => 'nullable',
            'volume'        => 'nullable',
            'issn'          => 'nullable',
            'isbn'          => 'nullable',
            'tanggal'       => 'required|date',
        ]);
   
        if ($validator->fails())
            return $this->sendError('Validation Error.', $validator->errors()->first());

        // Save Proceeding
        $data = Proceeding::whereId($proceeding->id)->update($validator->validated());

        return $this->sendResponse($data, trans('messages.flash.updated', ['data' => trans('messages.proceeding')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Proceeding  $proceeding
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proceeding $proceeding)
    {
        if (count($proceeding->proceeding_details) > 0) {
            return $this->sendError(trans('messages.error flash.deleted', ['data' => trans('messages.proceeding')]), trans('messages.proceeding not empty'));
        } else {
            $proceeding->delete();

            return $this->sendResponse([], trans('messages.flash.deleted', ['data' => trans('messages.proceeding')]));
        }
    }

    public function dropdown(Request $request)
    {
        $conference = Conference::query()
            ->select('conference_nama as nama', 'conference.id')
            ->where('user_id', Auth::id())
            ->where('conference_nama', 'like', "%". $request->search ."%")
            ->simplePaginate(30);

        foreach ($conference as $key => $value) {
            $data['results'][$key]['id'] = $value->id;
            $data['results'][$key]['text'] = $value->nama;
        }

        if ($conference->nextPageUrl() != NULL) {
            $data['pagination'] = ['more' => true];
        } else {
            $data['pagination'] = ['more' => false];
        }

        return $data;
    }
}
