<?php

namespace App\Http\Controllers\AdminConference\Publication;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Auth;
use Validator;

use App\Models\AuthorSubmission;
use App\Models\Conference;
use App\Models\KataKunciAbstrak;
use App\Models\Submission;
use App\Models\User;

use Yajra\DataTables\Facades\DataTables;

class SubmissionController extends BaseController
{
    public function show(Submission $submission)
    {
        // if ($submission->submission_status !== 6)
        //     return abort(404);

        $kata_kunci_1 = "";
        $kata_kunci_2 = "";
        $awal_1 = true;
        $awal_2 = true;

        foreach ($submission->kata_kunci_abstrak as $key => $value) {
            if ($value->kka_level === 1) {
                if ($awal_1) {
                    $kata_kunci_1 = $value->kka_kata_kunci;

                    $awal_1 = false;
                } else {
                    $kata_kunci_1 .= ";". $value->kka_kata_kunci;
                }
            } else {
                if ($awal_2) {
                    $kata_kunci_2 = $value->kka_kata_kunci;
                    
                    $awal_2 = false;
                } else {
                    $kata_kunci_2 .= ";". $value->kka_kata_kunci;
                }
            }
        }

        $conference = Conference::findOrFail($submission->conference_id);

        return view('pages.author.author.submission.publication', compact('submission', 'conference', 'kata_kunci_1', 'kata_kunci_2'));
    }

    public function show_author(Submission $submission)
    {
        $data = AuthorSubmission::query()
            ->join('users', 'author_submission.user_id', 'users.id')
            ->where('submission_id', $submission->id)
            ->select(['author_submission.id', 'users.name', 'users.no_induk']);

        return DataTables::of($data)
            ->addColumn('aksi', function ($item) {
                return '<button type="button" class="btn btn-danger btn-sm px-50" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button>';
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }

    public function update_keyword(Request $request, Submission $submission)
    {
        KataKunciAbstrak::where('submission_id', $submission->id)->delete();

        $kata_kunci_1 = explode(";", $request->kata_kunci_1);
        foreach ($kata_kunci_1 as $key => $value) {
            KataKunciAbstrak::create([
                'submission_id'     => $submission->id,
                'kka_kata_kunci'    => $value,
                'kka_level'         => 1
            ]);
        }
        
        if ($submission->conference->conference_level === 2) {
            $kata_kunci_2 = explode(";", $request->kata_kunci_2);
            foreach ($kata_kunci_2 as $key => $value) {
                KataKunciAbstrak::create([
                    'submission_id'     => $submission->id,
                    'kka_kata_kunci'    => $value,
                    'kka_level'         => 2
                ]);
            }
        }

        return $this->sendResponse(KataKunciAbstrak::where('submission_id', $submission->id)->get(), trans('messages.flash.updated', ['data' => trans('messages.keyword')]));
    }

    public function update_galley(Request $request, Submission $submission)
    {
        $validator = Validator::make($request->all(), [
            'submission_full_paper' => 'required|max:2048|mimes:pdf',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors()->first());
        }

        $image = $request->file('submission_full_paper');
        $submission_full_paper = sprintf("%'04d", $submission->id) . '.' . $image->getClientOriginalExtension();
        $image->move('files/submission/full-paper/', $submission_full_paper);

        $submission->update([
            'submission_full_paper' => $submission_full_paper
        ]);

        return $this->sendResponse($submission, trans('messages.flash.updated', ['data' => trans('messages.galley')]));
    }

    public function update_abstract(Request $request, Submission $submission)
    {
        $validator = Validator::make($request->all(), [
            'submission_judul'          => 'required',
            'submission_abstrakindo'    => 'nullable',
            'submission_abstrak'        => 'nullable',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors()->first());
        }
        
        $submission->update($validator->validated());

        return $this->sendResponse($submission, trans('messages.flash.updated', ['data' => trans('messages.abstract')]));
    }

    public function users(Request $request)
    {
        $user = User::getDropdownUserByRole(3, $request->search);

        foreach ($user as $key => $value) {
            $data['results'][$key]['id'] = $value->id;
            $data['results'][$key]['text'] = $value->name;
        }

        if ($user->nextPageUrl() != NULL) {
            $data['pagination'] = ['more' => true];
        } else {
            $data['pagination'] = ['more' => false];
        }

        return $data;
    }

    public function update_author(Request $request, Submission $submission)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
        ]);
        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors()->first());
        }
        
        $data = AuthorSubmission::updateOrCreate(
            [
                'user_id'       => $request->user_id,
                'submission_id' => $submission->id,
            ],
            [
                'user_id'       => $request->user_id,
                'submission_id' => $submission->id,
            ]
        );

        return $this->sendResponse($data, trans('messages.flash.created', ['data' => trans('messages.author')]));
    }

    public function delete_author(AuthorSubmission $submission)
    {
        if ($submission->user_id === Auth::id()) {
            return $this->sendError('Error.', "Proccess failed");
        } else {
            $submission->delete();
        }

        return $this->sendResponse([], trans('messages.flash.deleted', ['data' => trans('messages.author')]));
    }
}
