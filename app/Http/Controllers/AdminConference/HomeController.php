<?php

namespace App\Http\Controllers\AdminConference;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Models\Conference;

class HomeController extends Controller
{
    public function index()
    {
        $data = Conference::getConferenceDatabyUser(Auth::id());

        return view('pages.admin-conference.home', compact('data'));
    }
}
