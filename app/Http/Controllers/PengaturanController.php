<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\DataUser;
use App\Models\Kewarganegaraan;
use Auth;
use Illuminate\Support\Facades\Validator;

class PengaturanController extends Controller
{
    public function index()
    {
        $data = User::firstUserDataUser(Auth::id());
        $kewarganegaraan = Kewarganegaraan::getKewarganegaraan();

        return view('pages.pengaturan', compact('data', 'kewarganegaraan'));
    }

    public function biodata(Request $request, $id)
    {
        if ($request->gambar != NULL) {
            return $this->updateGambar($request, $id);
        }

        $validator = Validator::make($request->all(), [
            'email'     => 'unique:users,email,'. $id
        ]);
        if ($request->no_induk != NULL) {
            $validator = Validator::make($request->all(), [
                'no_induk'  => 'unique:users,no_induk,'. $id,
            ]);
        }
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }

        User::updateData($id, $request->name, $request->email, $request->no_induk);
        DataUser::updateData($id, $request->kewarganegaraan, $request->alamat, $request->afiliasi, $request->telepon, $request->jenis_kelamin);

        return $this->status(1, 'Biodata berhasil diubah');
    }

    public function password(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'password'  => 'same:konfirmasi_password'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }

        User::updatePassword($id, $request->password);

        return $this->status(1, 'Password berhasil diubah');
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }

    public function updateGambar($request, $id)
    {
        $validator = Validator::make($request->all(), [
            'gambar'      => 'max:1024'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }

        $image = $request->file('gambar');
        $gambar = rand() . '.' . $image->getClientOriginalExtension();
        $image->move('images/profil/', $gambar);

        $user = User::updateGambar($gambar, $id);

        return [
            'id'            => 1,
            'status'        => 'Berhasil',
            'keterangan'    => 'Gambar berhasil diubah',
            'gambar'        => $gambar
        ];
    }
}
