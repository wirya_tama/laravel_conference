<?php

namespace App\Http\Controllers\Author;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use App\Models\User;

class HomeController extends Controller
{
    public function profil()
    {
        $data = User::firstUserDataUserKewarganegaraan(Auth::id());

        return view('pages.author.author.profil', compact('data'));
    }
}
