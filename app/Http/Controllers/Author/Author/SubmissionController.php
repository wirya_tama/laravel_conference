<?php

namespace App\Http\Controllers\Author\Author;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cookie;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;

use App\Models\Submission;
use App\Models\KataKunciAbstrak;
use App\Models\AuthorSubmission;
use App\Models\Conference;
use App\Models\Voucher;
use App\Models\RiwayatSubmission;
use App\Models\User;
use App\Models\ListRole;
use App\Models\DataUser;
use App\Models\ReviewSubmission;
use App\Models\Loa;
use App\Models\LoaTandaTangan;

class SubmissionController extends Controller
{
    public function index()
    {
        $data = Submission::getSubmissionByUserOnSubmission(Auth::id());

        return view('pages.author.author.submission.index', compact('data'));
    }

    public function show($id)
    {
        $data['data'] = Submission::firstSubmission($id);
        $data['author'] = AuthorSubmission::getAuthorSubmissionBySubmission($id);
        $data['conference'] = Conference::firstConferenceBySubmission($data['data']['conference_id']);
        $data['review'] = ReviewSubmission::getReviewSubmissionBySubmission($id);
        
        $katakunci = [];
        foreach (KataKunciAbstrak::getKataKunciAbstrakBySubmissionLevel($id, 1) as $key => $value) {
            $katakunci[1][$key] = $value['kata_kunci'];
        }
        foreach (KataKunciAbstrak::getKataKunciAbstrakBySubmissionLevel($id, 2) as $key => $value) {
            $katakunci[2][$key] = $value['kata_kunci'];
        }

        return view('pages.author.author.submission.show', compact('data', 'katakunci'));
    }

    public function data($id)
    {
        $data['data'] = Submission::firstSubmission($id);
        $data['kata_kunci'] = KataKunciAbstrak::getKataKunciAbstrakBySubmission($id);
        $data['author'] = AuthorSubmission::getAuthorSubmissionBySubmission($id);
        $data['conference'] = Conference::firstConferenceBySubmission($data['data']['conference_id']);

        $katakunci = [];

        foreach (KataKunciAbstrak::getKataKunciAbstrakBySubmissionLevel($id, 1) as $key => $value) {
            $katakunci[1][$key] = $value['kata_kunci'];
        }
        foreach (KataKunciAbstrak::getKataKunciAbstrakBySubmissionLevel($id, 2) as $key => $value) {
            $katakunci[2][$key] = $value['kata_kunci'];
        }

        $data['katakunci'] = $katakunci;
        
        return json_encode($data);
    }

    public function step(Request $request, $id, $step)
    {
        $submission = Submission::firstSubmission($id);

        if ($step == 2) {
            if ($request->kode != NULL || $request->file != NULL) {
                return $this->stepBayar($request, $submission, $step);
            }

            return back()->with('danger', 'Data Kosong');
        } else if ($step == 3) {
            return $this->step3($request, $submission, $step);
        } else if ($step == 4) {
            return $this->step4($request, $submission);
        } else if ($step == 5) {
            if ($request->kode != NULL || $request->file != NULL) {
                return $this->stepBayar($request, $submission, $step);
            }

            return back()->with('danger', 'Data Kosong');
        } else if ($step == 6) {
            return $this->step6($request, $submission, $step);
        }
    }

    // Store Step Bayar
    public function stepBayar($request, $id, $step)
    {
        if ($request->file != NULL) {
            $validator = Validator::make($request->all(), [
                'file'      => 'required|max:1024',
                'tanggal'   => 'required|date',
                'nominal'   => 'required|numeric',
                'bank'      => 'required',
                'invoice'   => 'required|numeric',
            ]);
            if ($validator->fails()) {
                return back()->with('danger', $validator->errors()->first());
            }

            $image = $request->file('file');
            $file = rand() . '.' . $image->getClientOriginalExtension();
            if ($step == 2) {
                $image->move('images/submission/abstrak/', $file);
                Submission::updateDataByQuery($id->id, [
                    'submission_pembayaran_abstrak_file'        => $file,
                    'submission_pembayaran_abstrak_tanggal'     => $request->tanggal,
                    'submission_pembayaran_abstrak_nominal'     => $request->nominal, 
                    'submission_pembayaran_abstrak_bank'        => $request->bank,
                    'submission_pembayaran_abstrak_rekening'    => $request->nama, 
                    'submission_pembayaran_abstrak_invoice'     => $request->invoice,
                    'submission_pembayaran_abstrak_pesan'       => $request->pesan
                ]);
            } else {
                $image->move('images/submission/publikasi/', $file);
                Submission::updateDataByQuery($id->id, [
                    'submission_pembayaran_publikasi_file'        => $file,
                    'submission_pembayaran_publikasi_tanggal'     => $request->tanggal,
                    'submission_pembayaran_publikasi_nominal'     => $request->nominal, 
                    'submission_pembayaran_publikasi_bank'        => $request->bank,
                    'submission_pembayaran_publikasi_rekening'    => $request->nama, 
                    'submission_pembayaran_publikasi_invoice'     => $request->invoice,
                    'submission_pembayaran_publikasi_pesan'       => $request->pesan
                ]);
            }
        } else {
            $voucher = Voucher::firstVoucher($request->kode);
            if ($voucher == NULL) {
                return back()->with('danger', 'Kode Voucher tidak tersedia');
            } else {
                Voucher::updateVoucherStatus($voucher->voucher_kode);
                if ($step == 2) {
                    Submission::updateSubmissionAbstrakVoucher($id->id, $voucher->voucher_kode, $step);
                } else {
                    Submission::updateSubmissionPublikasiVoucher($id->id, $voucher->voucher_kode, $step);
                }
            }
        }

        RiwayatSubmission::storeRiwayatSubmission($id->id, $step);

        if ($step == 2) {
            return back()->with('success', 'Pembayaran Abstrak berhasil');
        } else {
            return back()->with('success', 'Pembayaran Publikasi berhasil');
        }
    }

    // Store Step 3
    public function step3($request, $submission, $step)
    {
        // Validasi File
        $validator = Validator::make($request->all(), [
            'file'      => 'nullable|max:5120'
        ]);
        if ($validator->fails()) {
            return back()->with('danger', $validator->errors()->first());
        }

        // Upload File
        $image = $request->file('file');
        $file = sprintf("%'04d", $submission->id) . '-'. sprintf("%'02d", 1) . '.' . $image->getClientOriginalExtension();
        $image->move('files/submission/full-paper/', $file);
        ReviewSubmission::storeReviewSubmission(Auth::id(), $submission->id, "Artikel Full Paper", $file, 0);

        // Update Judul & Abstrak
        Submission::updateSubmissionJudulAbstrak($submission->id, $request->judul, $request->abstrak, $request->abstrakindo);

        // Validasi Author Baru
        foreach ($request->author as $key => $author) {
            if (array_key_exists('email', $author)) {
                $no_induk = User::firstUserNoInduk($author['no_induk']);
                if ($author['no_induk'] != NULL) {
                    if ($no_induk != NULL) {
                        return back()->with('danger', 'No Induk telah ada');
                    }
                }
                
                $email = User::firstUserEmail($author['email']);
                if ($email != NULL) {
                    return back()->with('danger', 'Email telah ada');
                }
            }
        }
        // Update Author
        AuthorSubmission::deleteAuthorSubmissionBySubmission($submission->id);
        foreach ($request->author as $key => $author) {
            if (array_key_exists('author', $author)) {
                AuthorSubmission::storeAuthorSubmission($submission->id, $author['author']);
            } else {
                $no_induk = NULL;
                $password = 123456;
                if ($author['no_induk'] != NULL) {
                    $no_induk = $author['no_induk'];
                    $password = $author['no_induk'];
                }
    
                $user = User::storeUserAuthor($author['name'], $no_induk, $author['email'], $password);
                ListRole::storeListRoleAuthor($user->id, 3);
                DataUser::storeDataUserAuthor($user->id, $author['kewarganegaraan'], $author['jenis_kelamin']);
    
                AuthorSubmission::storeAuthorSubmission($submission->id, $user->id);
            }
        }

        // Update Kata Kunci Abstrak
        KataKunciAbstrak::deleteKataKunciAbstrakBySubmission($submission->id);
        $kata_kunci = explode(';', $request->kata_kunci);
        foreach ($kata_kunci as $key => $value) {
            KataKunciAbstrak::storeKataKunciAbstrak($submission->id, $value, 2);
        }
        $kata_kunciindo = explode(';', $request->kata_kunciindo);
        foreach ($kata_kunciindo as $key => $value) {
            KataKunciAbstrak::storeKataKunciAbstrak($submission->id, $value, 1);
        }

        // Update Status Submission
        Submission::updateSubmissionStatus($submission->id, $step);

        // Store Riwayat Submission
        RiwayatSubmission::storeRiwayatSubmission($submission->id, $step);

        return back()->with('success', 'Upload Full Paper berhasil');
    }

    public function step4($request, $submission)
    {
        $validator = Validator::make($request->all(), [
            'file'      => 'max:5120'
        ]);
        if ($validator->fails()) {
            return back()->with('danger', $validator->errors()->first());
        }

        $count = ReviewSubmission::getReviewSubmissionBySubmissionCount($submission->id);

        $image = $request->file('file');
        $file = sprintf("%'04d", $submission->id) . '-'. sprintf("%'02d", $count + 1) . '.' . $image->getClientOriginalExtension();
        $image->move('files/submission/full-paper/', $file);

        ReviewSubmission::storeReviewSubmission(Auth::id(), $submission->id, $request->pesan, $file, 2);

        return back()->with('success', 'Artikel berhasil direvisi');
    }

    public function step6($request, $submission, $step)
    {
        $validator = Validator::make($request->all(), [
            'file'      => 'max:5120'
        ]);
        if ($validator->fails()) {
            return back()->with('danger', $validator->errors()->first());
        }

        // Update Full Paper
        $image = $request->file('file');
        $file = sprintf("%'04d", $submission->id) . '.' . $image->getClientOriginalExtension();
        $image->move('files/submission/full-paper/', $file);
        Submission::updateSubmissionFullPaperFile($submission->id, $file);

        // Update Status Submission
        Submission::updateSubmissionStatus($submission->id, $step);
    
        // Store Riwayat Submission
        RiwayatSubmission::storeRiwayatSubmission($submission->id, $step);

        return back()->with('success', 'Proses Submission berhasil');
    }

    public function loa($id)
    {
        $data = Submission::firstSubmission($id);
        $author_data = AuthorSubmission::getAuthorSubmissionBySubmission($id);
        
        $loa = Loa::where('conference_id', $data->conference_id)->first();
        if($loa == NULL) return abort(404);
        
        $tanda_tangan = LoaTandaTangan::where('loa_id', $loa->id)->get();
        $valid = false;

        foreach ($author_data as $key => $value) {
            if ($value->id == Auth::id()) {
                $valid = true;
                break;
            }
        }

        if ($data->submission_status < 4  || !$valid || $loa == NULL) {
            return abort(404);
        }

        $judul = $data->submission_judul;
        $author = "";
        
        if (Cookie::get('lang') != NULL) {
            $waktu = Carbon::now()->locale(Cookie::get('lang'))->isoFormat('dddd, D MMMM, YYYY');
        } else {
            $waktu = Carbon::now()->locale('id')->isoFormat('dddd, D MMMM, YYYY');
        }

        foreach ($author_data as $key => $value) {
            if ($key == 0) {
                $author .= $value->nama;
            } else {
                $author .= ", ". $value->nama;
            }
        }

        $hasil_kop = str_replace(['ql-align'], ['text'], $loa->kop);
        $hasil_isi = str_replace(['#judul', '#author', '#waktu', 'ql-align'], [$judul, $author, $waktu, 'text'], $loa->isi);

        return view('pages.pdf.letter-of-acceptance', compact('loa', 'hasil_isi', 'tanda_tangan', 'judul', 'hasil_kop'));
    }

    public function destroy(Submission $submission)
    {
        Submission::deleteDataByQuery(["id" => $submission->id]);
        AuthorSubmission::deleteDataByQuery(["submission_id" => $submission->id]);

        return back()->with('success', 'Submission berhasil dihapus');
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }

    public function update(Request $request, Submission $submission)
    {
        if ($request->author != NULL) {
            foreach ($request->author as $key => $author) {
                if (array_key_exists('email', $author)) {
                    $no_induk = User::firstUserNoInduk($author['no_induk']);
                    if ($author['no_induk'] != NULL) {
                        if ($no_induk != NULL) {
                            return back()->with('danger', 'No Induk telah ada');
                        }
                    }
                    
                    $email = User::firstUserEmail($author['email']);
                    if ($email != NULL) {
                        return back()->with('danger', 'Email telah ada');
                    }
                }
            }
        }

        Submission::updateDataByQuery($submission->id, 
            [
                'submission_judul' => $request->judul, 
                'submission_abstrak' => $request->abstrak_inggris, 
                'submission_abstrakindo' => $request->abstrak_indonesia,
            ]
        );

        AuthorSubmission::deleteDataByQuery(['submission_id' => $submission->id]);
        AuthorSubmission::storeAuthorSubmission($submission->id, Auth::id());

        KataKunciAbstrak::deleteDataByQuery(['submission_id' => $submission->id]);
        $kata_kunci_inggris = explode(';', $request->keyword_inggris);
        foreach ($kata_kunci_inggris as $key => $value) {
            KataKunciAbstrak::storeKataKunciAbstrak($submission->id, $value, 2);
        }

        if (isset($request->keyword_indonesia)) {
            $kata_kunci_indonesia = explode(';', $request->keyword_indonesia);
            foreach ($kata_kunci_indonesia as $key => $value) {
                KataKunciAbstrak::storeKataKunciAbstrak($submission->id, $value, 1);
            }
        }

        if ($request->author != NULL) {
            foreach ($request->author as $key => $author) {
                if (array_key_exists('author', $author)) {
                    AuthorSubmission::storeAuthorSubmission($submission->id, $author['author']);
                } else {
                    $no_induk = NULL;
                    $password = 123456;
                    if ($author['no_induk'] != NULL) {
                        $no_induk = $author['no_induk'];
                        $password = $author['no_induk'];
                    }
        
                    $user = User::storeUserAuthor($author['name'], $no_induk, $author['email'], $password);
                    ListRole::storeListRoleAuthor($user->id, 3);
                    DataUser::storeDataUserAuthor($user->id, $author['kewarganegaraan'], $author['jenis_kelamin']);
        
                    AuthorSubmission::storeAuthorSubmission($submission->id, $user->id);
                }
            }
        }
        
        return back()->with('success', 'Submission berhasil diubah');
    }
}
