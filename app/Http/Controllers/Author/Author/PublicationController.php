<?php

namespace App\Http\Controllers\Author\Author;

use App\Http\Controllers\AdminConference\Publication\SubmissionController;

use Auth;
use Illuminate\Http\Request;

use App\Models\AuthorSubmission;
use App\Models\Submission;

class PublicationController extends SubmissionController
{
    public function submission_show(Submission $submission)
    {
        $author = $submission->author_submission;
        if (count($author) > 0) {
            foreach ($author as $value) {
                if (Auth::id() === $value->user_id) {
                    return $this->show($submission);
                }
            }
        }

        return abort(404);
    }

    public function submission_show_author(Submission $submission)
    {
        return $this->show_author($submission);
    }

    public function submission_update_keyword(Request $request, Submission $submission)
    {
        return $this->update_keyword($request, $submission);
    }

    public function submission_update_galley(Request $request, Submission $submission)
    {
        return $this->update_galley($request, $submission);
    }

    public function submission_update_abstract(Request $request, Submission $submission)
    {
        return $this->update_abstract($request, $submission);
    }

    public function submission_users(Request $request)
    {
        return $this->users($request);
    }

    public function submission_update_author(Request $request, Submission $submission)
    {
        return $this->update_author($request, $submission);
    }

    public function submission_delete_author(AuthorSubmission $submission)
    {
        return $this->delete_author($submission);
    }
}
