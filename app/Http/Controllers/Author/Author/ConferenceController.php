<?php

namespace App\Http\Controllers\Author\Author;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Conference;
use App\Models\User;
use App\Models\Submission;
use App\Models\KataKunciAbstrak;
use App\Models\RiwayatSubmission;
use App\Models\AuthorSubmission;
use App\Models\ListRole;
use App\Models\DataUser;
use App\Models\Kewarganegaraan;
use Auth;

class ConferenceController extends Controller
{
    public function index()
    {
        $data = Conference::getConferenceIdNama(0);

        return view('pages.author.author.conference', compact('data'));
    }

    public function store(Request $request)
    {
        if ($request->author != NULL) {
            foreach ($request->author as $key => $author) {
                if (array_key_exists('email', $author)) {
                    $no_induk = User::firstUserNoInduk($author['no_induk']);
                    if ($author['no_induk'] != NULL) {
                        if ($no_induk != NULL) {
                            return $this->status(0, 'No Induk telah ada');
                        }
                    }
                    
                    $email = User::firstUserEmail($author['email']);
                    if ($email != NULL) {
                        return $this->status(0, 'Email telah ada');
                    }
                }
            }
        }

        $submission = Submission::storeSubmissionStep1($request->conference, $request->judul, $request->abstrak_inggris, $request->abstrak_indonesia,1);

        AuthorSubmission::storeAuthorSubmission($submission->id, Auth::id());
        
        RiwayatSubmission::storeRiwayatSubmission($submission->id, $submission->submission_status);

        $kata_kunci_inggris = explode(';', $request->keyword_inggris);
        foreach ($kata_kunci_inggris as $key => $value) {
            KataKunciAbstrak::storeKataKunciAbstrak($submission->id, $value, 2);
        }

        if (isset($request->keyword_indonesia)) {
            $kata_kunci_indonesia = explode(';', $request->keyword_indonesia);
            foreach ($kata_kunci_indonesia as $key => $value) {
                KataKunciAbstrak::storeKataKunciAbstrak($submission->id, $value, 1);
            }
        }

        if ($request->author != NULL) {
            foreach ($request->author as $key => $author) {
                if (array_key_exists('author', $author)) {
                    AuthorSubmission::storeAuthorSubmission($submission->id, $author['author']);
                } else {
                    $no_induk = NULL;
                    $password = 123456;
                    if ($author['no_induk'] != NULL) {
                        $no_induk = $author['no_induk'];
                        $password = $author['no_induk'];
                    }
        
                    $user = User::storeUserAuthor($author['name'], $no_induk, $author['email'], $password);
                    ListRole::storeListRoleAuthor($user->id, 3);
                    DataUser::storeDataUserAuthor($user->id, $author['kewarganegaraan'], $author['jenis_kelamin']);
        
                    AuthorSubmission::storeAuthorSubmission($submission->id, $user->id);
                }
            }
        }
        
        return $this->status(1, 'Submission berhasil ditambah');
    }

    public function data(Request $request)
    {
        $data = User::getDropdownUserByRole(3, $request->search);
        foreach ($data as $key => $value) {
            $abc['results'][$key]['id'] = $value->id;
            $abc['results'][$key]['text'] = $value->name;
        }

        if ($data->nextPageUrl() != NULL) {
            $abc['pagination'] = ['more' => true];
        } else {
            $abc['pagination'] = ['more' => false];
        }

        return $abc;
    }

    public function kewarganegaraan(Request $request)
    {
        $data = Kewarganegaraan::getDropdown($request->search);
        foreach ($data as $key => $value) {
            $abc['results'][$key]['id'] = $value->id;
            $abc['results'][$key]['text'] = $value->ke_nama;
        }

        if ($data->nextPageUrl() != NULL) {
            $abc['pagination'] = ['more' => true];
        } else {
            $abc['pagination'] = ['more' => false];
        }

        return $abc;
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }
}
