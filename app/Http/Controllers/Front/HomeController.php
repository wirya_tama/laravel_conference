<?php

namespace App\Http\Controllers\Front;

use App\Models\Portal;
use App\Models\FAQ;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;

class HomeController extends Controller
{
    public function index()
    {
        $portal = Portal::firstPortal(0);
        $faq = FAQ::getAll();

        return view('front.portal.home', compact('portal', 'faq'));
    }

    public function conferences()
    {
        return view('front.portal.conferences');
    }

    public function lang($lang)
    {
        if (!in_array($lang, ['id', 'en'])) {
            $lang = 'id';
        }

        Cookie::queue(Cookie::forever('lang', $lang));
        return back();
    }
}
