<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;

use App\Models\Conference;
use App\Models\KeynoteSpeaker;
use App\Models\TanggalPenting;
use App\Models\Organizer;
use App\Models\Page;
use App\Models\Pengumuman;
use App\Models\User;

class ConferenceController extends Controller
{
    public function index($id)
    {
        $data = Conference::singleTanggalPentingBySlug($id);
        if ($data == null) {
            return abort(404);
        }

        $speaker = KeynoteSpeaker::getByConference($data->id);
        $tanggal = TanggalPenting::getByConference($data->id);
        $pengumuman = Pengumuman::getByConference($data->id);
        $organizer = Organizer::getByConference($data->id);

        $isAuthor = false;
        if (Auth::user()) {
            $user = User::firstUserDataUser(Auth::user()->id);
            foreach ($user['roles'] as $key => $roles) {
                if ($roles['role_id'] == 3) {
                    $isAuthor = true;
                    break;
                }
            }
        }

        return view('front.conference.home', compact('data', 'isAuthor', 'speaker', 'tanggal', 'pengumuman', 'organizer'));
    }

    public function speaker($id)
    {
        $data = Conference::singleBySlug($id);
        $speaker = KeynoteSpeaker::getByConference($data->id);

        return view('front.conference.speaker', compact('data', 'speaker'));
    }

    public function schedule($id)
    {
        $data = Conference::singleBySlug($id);
        $tanggal = TanggalPenting::getByConference($data->id);

        return view('front.conference.schedule', compact('data', 'tanggal'));
    }

    public function announcements($id)
    {
        $data = Conference::singleBySlug($id);
        $pengumuman = Pengumuman::getByConference($data->id);

        return view('front.conference.announcement.index', compact('data', 'pengumuman'));
    }

    public function announcements_show($id, Pengumuman $pengumuman)
    {
        $data = Conference::singleBySlug($id);

        return view('front.conference.announcement.show', compact('data', 'pengumuman'));
    }

    public function register()
    {
        $user = User::firstUserDataUser(Auth::user()->id);
        $isAuthor = false;

        foreach ($user['roles'] as $key => $roles) {
            if ($roles['role_id'] == 3) {
                $isAuthor = true;
                break;
            }
        }

        if ($isAuthor) {
            DB::table('role_user')
                ->where('user_id', Auth::user()->id)
                ->update(['role_id' => 3]);

            return redirect()->route('author.author.conference.index');
        } else {
            return back();
        }
    }

    public function download($id)
    {
        $data = Conference::singleBySlug($id);

        return view('front.conference.download', compact('data'));
    }

    public function indexPage($id)
    {
        $data = Conference::singleBySlug($id);
        $pages = Page::getData([['conference_id', $data->id]]);

        return view('front.conference.static-page.index', compact('data', 'pages'));
    }

    public function showPage($id, $slug)
    {
        $data = Conference::singleBySlug($id);
        $page = Page::firstData([['conference_id', $data->id], ['slug', $slug]]);

        return view('front.conference.static-page.show', compact('data', 'page'));
    }
}
