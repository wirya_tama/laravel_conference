<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Conference;
use App\Models\Proceeding;
use App\Models\ProceedingDetail;

class ProceedingController extends Controller
{
    public function index($id)
    {
        $conference = Conference::query()
            ->where('conference_slug', $id)
            ->first();

        return view('front.conference.proceeding.index', compact('conference', 'id'));
    }

    public function show($id, $slug)
    {
        $data = Proceeding::where('slug', $slug)->first();

        return view('front.conference.proceeding.info', compact('data', 'id', 'slug'));
    }

    public function search($id, $slug, Request $request)
    {
        $data = Proceeding::where('slug', $slug)->first();
        
        $conference = Conference::query()
            ->where('conference_slug', $id)
            ->first();

        $proceeding = ProceedingDetail::query()
            ->join('submission', 'proceeding_details.submission_id', 'submission.id')
            ->select('proceeding_details.*', 'submission_judul')
            ->where('proceeding_id', $data->id)
            ->where('submission_judul', 'like', "%". $request->q ."%")
            ->with('submission.author_submission.user')
            ->orderBy('proceeding_details.id')
            ->get();

        return view('front.conference.proceeding.show', compact('proceeding', 'conference', 'id', 'slug', 'request'));
    }

    public function submission($id, $slug, $code)
    {
        $data = Proceeding::where('slug', $slug)->first();
        
        $conference = Conference::query()
            ->where('conference_slug', $id)
            ->first();

        $proceeding = ProceedingDetail::where('kode', $code)->first();

        $kata_kunci_abstrak = $proceeding->submission->kata_kunci_abstrak;

        return view('front.conference.proceeding.submission', compact('proceeding', 'conference', 'data', 'id', 'slug', 'kata_kunci_abstrak'));
    }
}
