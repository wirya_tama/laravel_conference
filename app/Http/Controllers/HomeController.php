<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\ListRole;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (\Auth::user()->hasRole('admin')) {
            return redirect('/admin/master/user');
        } elseif (\Auth::user()->hasRole('admin_conference')) {
            return redirect('/admin-conference');
        } elseif (\Auth::user()->hasRole('author')) {
            return redirect('/author/author/profil');
        } else {
            $data = ListRole::firstDataByQuery(['user_id' => \Auth::id()]);

            if ($data == NULL) {
                return abort(401);
            }

            \DB::table('role_user')
                ->insert([
                    'role_id' => $data->role_id,
                    'user_id' => \Auth::id()
                ]);

            return redirect('/panel');
        }
    }

    public function role($name)
    {
        $user = User::firstUserDataUser(\Auth::user()->id);
        $hasRole = false;
        $roleId = 3;

        foreach ($user['roles'] as $key => $roles) {
            if ($roles['role_name'] == $name) {
                $hasRole = true;
                $roleId = $roles['role_id'];
                break;
            }
        }

        if ($hasRole) {
            \DB::table('role_user')
                ->where('user_id', \Auth::user()->id)
                ->update(['role_id' => $roleId]);
        }

        return redirect('panel');
    }
}
