<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Models\ListRole;
use App\Models\DataUser;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed|min:8',
            'afiliasi' => 'required',
        ]);

        $user = User::storeUserRegister($request->name, $request->email, $request->password);
        ListRole::storeListRoleRegister($user->id, 3);
        DataUser::storeDataByQuery([
            'user_id'           => $user->id,
            'ke_id'             => 1,
            'du_jenis_kelamin'  => 1,
            'du_afiliasi'       => $request->afiliasi
        ]);

        event(new Registered($user));
        
        auth()->login($user);

        return redirect(RouteServiceProvider::PANEL);
    }
}
