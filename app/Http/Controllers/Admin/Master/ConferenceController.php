<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\Controller;
use App\Models\Conference;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

use Yajra\DataTables\Facades\DataTables;
use App\Models\Portal;
use App\Models\User;
use App\Models\TanggalPenting;
use App\Models\Loa;
use Str;

class ConferenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portal = Portal::getPortalIdNama();
        $admin = User::getUserByRole(2);

        return view('pages.admin.master.conference', compact('portal', 'admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $conference = Conference::storeConference($request, Str::slug($request->nama).'-'.Str::random(3));

        Loa::create([
            'conference_id' => $conference->id
        ]);

        $this->storeTanggalPenting($conference->id, $request->tanggal);
        
        return $this->status(1, 'Conference berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Conference  $conference
     * @return \Illuminate\Http\Response
     */
    public function show(Conference $conference)
    {
        return json_encode(Conference::singleConferenceTanggalPenting($conference->id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Conference  $conference
     * @return \Illuminate\Http\Response
     */
    public function edit(Conference $conference)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Conference  $conference
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Conference $conference)
    {
        Conference::updateAdminConference($conference->id, $request);
        
        $this->storeTanggalPenting($conference->id, $request->tanggal);
        
        return $this->status(1, 'Conference berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Conference  $conference
     * @return \Illuminate\Http\Response
     */
    public function destroy(Conference $conference)
    {
        Conference::deleteConference($conference->id);
        
        return $this->status(1, 'Conference berhasil dihapus');
    }

    public function data()
    {
        $conference = Conference::join('users', 'conference.user_id', 'users.id')
                        ->join('portal', 'conference.portal_id', 'portal.id')
                        ->select(['conference.id', 'conference.conference_nama', 'users.name', 'portal.portal_nama', 'conference.conference_status']);
        
        return DataTables::of($conference)
            ->addColumn('status', function ($item) {
                if (App::isLocale('en')) {
                    if ($item->conference_status == 1) {
                        return '<div disabled class="badge badge-md badge-success">Active</div>';
                    } else if ($item->conference_status == 2) {
                        return '<div disabled class="badge badge-md badge-warning">Draft</div>';
                    } else {
                        return '<div disabled class="badge badge-md badge-danger">Inactive</div>';
                    }
                } else {
                    if ($item->conference_status == 1) {
                        return '<div disabled class="badge badge-md badge-success">Aktif</div>';
                    } else if ($item->conference_status == 2) {
                        return '<div disabled class="badge badge-md badge-warning">Draf</div>';
                    } else {
                        return '<div disabled class="badge badge-md badge-danger">Tidak Aktif</div>';
                    }
                }
            })
            ->addColumn('aksi', function ($item) {
                return '<form class="text-center" method="POST"><div class="btn-group"><button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button><button type="button" class="btn btn-danger hapus" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button></div></form>';
            })
            ->rawColumns(['status', 'aksi'])
            ->removeColumn('conference_status', 'id')
            ->make(true);
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }

    public function storeTanggalPenting($id, $tanggal)
    {
        TanggalPenting::deleteTanggalPentingByConference($id);
        
        TanggalPenting::storeTanggalPentingDefault($id, "Upload Abstrak", date('Y-m-d', strtotime($tanggal . "- 2 days")), 0);
        TanggalPenting::storeTanggalPentingDefault($id, "Pembayaran", date('Y-m-d', strtotime($tanggal . "- 1 days")), 0);
        TanggalPenting::storeTanggalPentingDefault($id, "Conference", $tanggal, 1);
        TanggalPenting::storeTanggalPentingDefault($id, "Upload Full Paper", date('Y-m-d', strtotime($tanggal . "+ 1 days")), 0);
        TanggalPenting::storeTanggalPentingDefault($id, "Review", date('Y-m-d', strtotime($tanggal . "+ 2 days")), 0);
        TanggalPenting::storeTanggalPentingDefault($id, "Pembayaran Publish", date('Y-m-d', strtotime($tanggal . "+ 3 days")), 0);
        TanggalPenting::storeTanggalPentingDefault($id, "Publish Artikel", date('Y-m-d', strtotime($tanggal . "+ 4 days")), 0);
    }
}
