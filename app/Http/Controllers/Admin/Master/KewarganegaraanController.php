<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\Controller;
use App\Models\Kewarganegaraan;
use Illuminate\Http\Request;

use Yajra\DataTables\Facades\DataTables;

class KewarganegaraanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Kewarganegaraan::getKewarganegaraan();

        return view('pages.admin.master.kewarganegaraan', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = Kewarganegaraan::firstKewarganegaraanIdNama(0, $request->nama);
        if (isset($data)) {
            return $this->status(0, 'Data sudah ada');
        } else {
            Kewarganegaraan::storeKewarganegaraan($request);
            return $this->status(1, 'Data berhasil ditambah');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kewarganegaraan  $kewarganegaraan
     * @return \Illuminate\Http\Response
     */
    public function show(Kewarganegaraan $kewarganegaraan)
    {
        return json_encode($kewarganegaraan);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kewarganegaraan  $kewarganegaraan
     * @return \Illuminate\Http\Response
     */
    public function edit(Kewarganegaraan $kewarganegaraan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kewarganegaraan  $kewarganegaraan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kewarganegaraan $kewarganegaraan)
    {
        $data = Kewarganegaraan::firstKewarganegaraanIdNama($kewarganegaraan->id, $request->nama);
        if (isset($data)) {
            return $this->status(0, 'Data sudah ada');
        } else {
            Kewarganegaraan::updateKewarganegaraan($kewarganegaraan->id, $request);
            return $this->status(1, 'Data berhasil diubah');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kewarganegaraan  $kewarganegaraan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kewarganegaraan $kewarganegaraan)
    {
        Kewarganegaraan::deleteKewarganegaraan($kewarganegaraan->id);
        return $this->status(1, 'Data berhasil dihapus');
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }

    public function data()
    {
        $kewarganegaraan = Kewarganegaraan::select(['id', 'ke_nama']);
        
        return DataTables::of($kewarganegaraan)
            ->addColumn('aksi', function ($item) {
                return '<form action="/admin/master/kewarganegaraan/destroy/'. $item->id .'" class="text-center" method="POST"><div class="btn-group"><button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'" data-nama="'. $item->ke_nama .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button><button type="button" class="btn btn-danger hapus" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button></div></form>';
            })
            ->rawColumns(['aksi'])
            ->removeColumn('id')
            ->make(true);
    }
}
