<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\Controller;
use App\Models\Portal;
use Illuminate\Http\Request;

use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class PortalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.admin.master.portal');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->restore != NULL) {
            if (count(Portal::getPortalIdNama()) > 0) {
                return $this->status(0, 'Saat ini Portal tidak boleh lebih dari 1');
            } else {
                Portal::restore($request->restore);
    
                return $this->status(1, 'Portal berhasil dipulihkan');
            }
        }

        $validator = Validator::make($request->all(), [
            'latitude'      => 'max:20',
            'longitude'     => 'max:20',
            'facebook'      => 'nullable|url',
            'instagram'     => 'nullable|url',
            'twitter'       => 'nullable|url',
            'youtube'       => 'nullable|url',
            'kontak_hp1'    => 'max:15',
            'kontak_hp2'    => 'nullable|max:15',
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }

        if (count(Portal::getPortalIdNama()) > 0) {
            return $this->status(0, 'Saat ini Portal tidak boleh lebih dari 1');
        }
        
        Portal::storePortal($request);
        
        return $this->status(1, 'Portal berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Portal  $portal
     * @return \Illuminate\Http\Response
     */
    public function show(Portal $portal)
    {
        return json_encode($portal);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Portal  $portal
     * @return \Illuminate\Http\Response
     */
    public function edit(Portal $portal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Portal  $portal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Portal $portal)
    {
        if ($request->logo != NULL) {
            return $this->uploadFile($portal, $request);
        }

        $validator = Validator::make($request->all(), [
            'latitude'      => 'max:20',
            'longitude'     => 'max:20',
            'facebook'      => 'nullable|url',
            'instagram'     => 'nullable|url',
            'twitter'       => 'nullable|url',
            'youtube'       => 'nullable|url',
            'kontak_hp1'    => 'max:15',
            'kontak_hp2'    => 'nullable|max:15',
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        
        Portal::updatePortal($portal->id, $request);
        
        return $this->status(1, 'Portal berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Portal  $portal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Portal $portal)
    {
        Portal::deletePortal($portal->id);

        return $this->status(1, 'Portal berhasil dihapus');
    }

    public function data()
    {
        $portal = Portal::select(['id', 'portal_nama', 'portal_deskripsi']);
        
        return DataTables::of($portal)
            ->addColumn('aksi', function ($item) {
                return '<form class="text-center" method="POST"><div class="btn-group"><button type="button" class="btn btn-primary file" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg></button><button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button><button type="button" class="btn btn-danger" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button></div></form>';
            })
            ->rawColumns(['aksi'])
            ->removeColumn('id')
            ->make(true);
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }

    public function uploadFile($portal, $request)
    {
        $validator = Validator::make($request->all(), [
            'logo'      => 'max:1024'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }

        $image = $request->file('logo');
        $logo = rand() . '.' . $image->getClientOriginalExtension();
        $image->move('images/portal/', $logo);

        Portal::updateLogoPortal($portal->id, $logo);

        return $this->status(1, 'File Portal berhasil diubah');
    }

    public function dataTrashed()
    {
        return json_encode(Portal::getTrashed());
    }
}
