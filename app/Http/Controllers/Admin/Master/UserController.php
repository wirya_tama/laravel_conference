<?php

namespace App\Http\Controllers\Admin\Master;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

use App\Models\ListRole;
use App\Models\DataUser;

use Auth;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.admin.master.user');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'no_induk'  => 'unique:App\Models\User,no_induk|max:15',
            'email'     => 'unique:App\Models\User,email'
        ]);
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        
        $user = User::storeUser($request);
        ListRole::storeListRole($user->id, $request->role);
        DataUser::storeDataUser($user->id, $request);
        
        return $this->status(1, 'Data berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return json_encode(User::firstUserDataUser($user->id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'email'     => Rule::unique('users', 'email')->ignore($user->id),
        ]);
        if ($request->no_induk != NULL) {
            $validator = Validator::make($request->all(), [
                'no_induk'  => [Rule::unique('users', 'no_induk')->ignore($user->id), 'max:15']
            ]);
        }
        if ($validator->fails()) {
            return $this->status(0, $validator->errors()->first());
        }
        
        User::updateUser($user->id, $request);
        ListRole::storeListRole($user->id, $request->role);
        DataUser::updateDataUser($user->id, $request);
        
        return $this->status(1, 'Data berhasil ditambah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (Auth::id() == $user->id) {
            return $this->status(0, 'User tidak dapat dihapus');
        }

        User::query()
            ->whereId($user->id)
            ->update([
                'email' => rand(10000000,99999999) .'@mail.com'
            ]);

        User::deleteUser($user->id);
        return $this->status(1, 'User berhasil dihapus');
    }

    public function status($id, $keterangan)
    {
        if ($id == 1) {
            $status = 'Berhasil';
        } else {
            $status = 'Gagal';
        }

        return [
            'id'            => $id,
            'status'        => $status,
            'keterangan'    => $keterangan
        ];
    }

    public function data()
    {
        $user = User::select(['id', 'no_induk', 'name']);
        
        return DataTables::of($user)
            ->addColumn('role', function ($item) {
                if (App::isLocale('en')) {
                    $role = ListRole::selectRaw('(CASE role_id WHEN 1 THEN "Admin" WHEN 2 THEN "Conference Admin" WHEN 3 THEN "Author" ELSE "Reviewer" END) as role')->where('user_id', $item->id)->get();
                } else {
                    $role = ListRole::selectRaw('(CASE role_id WHEN 1 THEN "Admin" WHEN 2 THEN "Admin Konferensi" WHEN 3 THEN "Penulis" ELSE "Peninjau" END) as role')->where('user_id', $item->id)->get();
                }
                
                $roles = "";
                foreach ($role as $key => $value) {
                    $roles .= '<li class="list-group-item">'.$value->role.'</li>';
                }
                return '<ul class="list-group">'.$roles.'</ul>';
            })
            ->addColumn('aksi', function ($item) {
                return '<form action="/admin/master/user/destroy/'. $item->id .'" class="text-center" method="POST"><div class="btn-group"><button type="button" class="btn btn-warning ubah" data-value="'. $item->id .'"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2"><path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path></svg></button><button type="button" class="btn btn-danger hapus" onclick="hapus('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button><button type="button" class="btn btn-primary" onclick="aksi('. $item->id .')"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-lock"><rect x="3" y="11" width="18" height="11" rx="2" ry="2"></rect><path d="M7 11V7a5 5 0 0 1 10 0v4"></path></svg></button></div></form>';
            })
            ->rawColumns(['role', 'aksi'])
            ->removeColumn('id')
            ->make(true);
    }

    public function author()
    {
        $data = User::getUserByRole(3);
        
        return view('pages.admin.master.author', compact('data'));
    }

    public function reviewer()
    {
        $data = User::getUserByRole(4);
        
        return view('pages.admin.master.reviewer', compact('data'));
    }

    public function verified($id)
    {
        User::verifiedUser($id);
        
        return $this->status(1, 'Email berhasil divalidasi');
    }

    public function action($action, User $user)
    {
        $password = "";

        if ($user->no_induk == NULL) {
            $password = "123456";
        } else {
            $password = $user->no_induk;
        };

        User::updateDataByQuery(
            ['id' => $user->id],
            ['password' => Hash::make($password)]
        );

        return $this->status(1, 'Password berhasil direset menjadi '. $password);
    }
}