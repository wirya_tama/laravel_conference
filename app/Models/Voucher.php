<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Str;

class Voucher extends Model
{
    use HasFactory;

    protected $table = 'voucher';
    protected $fillable = ['conference_id', 'voucher_kode', 'voucher_status'];

    static function storeVoucher($request)
    {
        Voucher::create([
            'conference_id' => $request->conference,
            'voucher_kode'  => Str::random(15)
        ]);
    }

    static function updateVoucher($id, $request)
    {
        Voucher::whereId($id)->update([
            'conference_id'     => $request->conference,
            'voucher_status'    => $request->status
        ]);
    }

    static function deleteVoucher($id)
    {
        Voucher::whereId($id)->delete();
    }

    static function firstVoucher($id)
    {
        return Voucher::where('voucher_kode', $id)
                    ->where('voucher_status', 1)
                    ->first();
    }

    static function updateVoucherStatus($kode)
    {
        Voucher::where('voucher_kode', $kode)->update([
            'voucher_status' => 0
        ]);
    }
}
