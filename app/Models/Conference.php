<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conference extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'conference';
    protected $fillable = [
        'user_id', 'portal_id', 'conference_nama', 'conference_bayar_publikasi',
        'conference_tema', 'conference_tentang', 'conference_slug',
        'conference_lokasi', 'conference_latitude', 'conference_longitude',
        'conference_kontak_nama1', 'conference_kontak_hp1', 'conference_kontak_nama2',
        'conference_kontak_hp2', 'conference_logo', 'conference_pamflet',
        'conference_instagram', 'conference_email', 'conference_level',
        'conference_facebook', 'conference_twitter', 'conference_youtube',
        'conference_status', 'conference_template', 'conference_bayar_abstrak',
        'conference_pembayaran', 'conference_google_maps', 'conference_nama_status',
        'conference_afiliasi'
    ];

    public function proceeding()
    {
        return $this->hasMany(Proceeding::class, 'conference_id', 'id')->orderBy('title');
    }

    static function storeConference($request, $slug)
    {
        return Conference::create([
            'conference_nama'   => $request->nama,
            'portal_id'         => $request->portal,
            'user_id'           => $request->admin,
            'conference_slug'   => $slug,
        ]);
    }

    static function updateAdminConference($id, $request)
    {
        Conference::whereId($id)->update([
            'conference_nama'   => $request->nama,
            'portal_id'         => $request->portal,
            'user_id'           => $request->admin
        ]);
    }

    static function updateConference($id, $request, $slug)
    {
        Conference::whereId($id)->update([
            'conference_nama'           => $request->nama,
            'conference_tema'           => $request->tema,
            'conference_tentang'        => $request->tentang,
            'conference_lokasi'         => $request->lokasi,
            'conference_latitude'       => $request->latitude,
            'conference_longitude'      => $request->longitude,
            'conference_google_maps'    => $request->google_maps,
            'conference_kontak_nama1'   => $request->kontak_nama1,
            'conference_kontak_hp1'     => $request->kontak_hp1,
            'conference_kontak_nama2'   => $request->kontak_nama2,
            'conference_kontak_hp2'     => $request->kontak_hp2,
            'conference_email'          => $request->email,
            'conference_instagram'      => $request->instagram,
            'conference_facebook'       => $request->facebook,
            'conference_twitter'        => $request->twitter,
            'conference_youtube'        => $request->youtube,
            'conference_level'          => $request->level,
            'conference_status'         => $request->status,
            'conference_bayar_abstrak'  => $request->bayar_abstrak,
            'conference_bayar_publikasi'=> $request->bayar_publikasi,
            'conference_pembayaran'     => $request->pembayaran,
            'conference_nama_status'    => $request->nama_status,
            'conference_afiliasi'       => $request->afiliasi,
            'conference_slug'           => $slug,
        ]);
    }

    static function updateFileConference($id, $request)
    {
        Conference::whereId($id)->update([
            'conference_logo'       => $request['logo'],
            'conference_pamflet'    => $request['pamflet']
        ]);
    }

    static function updateFileBannerConference($id, $banner)
    {
        Conference::whereId($id)->update([
            'conference_banner' => $banner
        ]);
    }

    static function updateSingleByQuery($id, $query, $value)
    {
        Conference::whereId($id)->update([
            $query => $value
        ]);
    }

    static function deleteConference($id)
    {
        Conference::whereId($id)->delete();
    }

    static function firstConferencebySubmission($id)
    {
        return Conference::select('conference_nama as nama', 'conference_bayar_publikasi as bayar_publikasi', 'conference_level as level',
                                'conference_bayar_abstrak as bayar_abstrak', 'conference_pembayaran as pembayaran')
                        ->whereId($id)
                        ->withTrashed()
                        ->first();
    }

    static function getConferencebyUser($id)
    {
        return Conference::select('conference_nama as nama', 'id', 'user_id')
                        ->where('user_id', $id)
                        ->get();
    }

    static function getConferenceDatabyUser($id)
    {
        return Conference::join('portal', 'conference.portal_id', 'portal.id')
                        ->leftJoin('tanggal_penting', 'conference.id', 'tanggal_penting.conference_id')
                        ->select('portal_nama as portal', 'conference.id', 'conference_nama as conference', 'tanggal_penting.tp_tanggal_awal as tanggal')
                        ->where('tp_conference', 1)
                        ->where('user_id', $id)
                        ->orderBy('tp_tanggal_awal', 'desc')
                        ->get();
    }

    static function getConferenceIdNama($id)
    {
        if ($id != 0) {
            return Conference::join('portal', 'conference.portal_id', 'portal.id')
                            ->select('conference.id', 'conference_nama as nama', 'portal_nama as portal', 'conference_level as level')
                            ->where('user_id', $id)
                            ->where('conference_status', 1)
                            ->get();
        } else {
            return Conference::join('portal', 'conference.portal_id', 'portal.id')
                            ->select('conference.id', 'conference_nama as nama', 'conference_level as level')
                            ->where('conference_status', 1)
                            ->get();
        }
    }

    static function singleConferenceTanggalPenting($id)
    {
        return Conference::join('tanggal_penting', 'conference.id', 'tanggal_penting.conference_id')
                            ->select('conference.id', 'conference_nama as nama', 'portal_id as portal', 'user_id as admin', 'tp_tanggal_awal as tanggal')
                            ->where('conference.id', $id)
                            ->where('tp_conference', 1)
                            ->first();
    }

    static function getConferenceTanggalPentingByPortal($id)
    {
        $conference = Conference::join('tanggal_penting', 'conference.id', 'tanggal_penting.conference_id')
                                    ->select('conference.id', 'conference_nama as nama', 'portal_id as portal', 'user_id as admin', 'conference_slug as slug', 'conference_tentang as tentang', 'conference_afiliasi')
                                    ->selectraw('min(tp_tanggal_awal) as tanggal_awal, max(tp_tanggal_awal) as tanggal_akhir')
                                    ->where('portal_id', $id)
                                    ->groupBy('conference.id')
                                    ->get();

        $now = 0;
        $future = 0;
        $past = 0;
        foreach ($conference as $key => $value) {
            $tanggal = Conference::join('tanggal_penting', 'conference.id', 'tanggal_penting.conference_id')
                                    ->select('tp_tanggal_awal')
                                    ->where('tp_conference', 1)
                                    ->where('conference.id', $value->id)
                                    ->first();

            if (now() >= $value->tanggal_awal) {
                if (now() > $value->tanggal_akhir) {
                    $data['past'][$past] = $value;
                    $data['past'][$past]->tanggal = $tanggal->tp_tanggal_awal;
                    $past++;
                }
                else {
                    $data['now'][$now] = $value;
                    $data['now'][$now]->tanggal = $tanggal->tp_tanggal_awal;
                    $now++;
                }
            } else {
                $data['future'][$future] = $value;
                $data['future'][$future]->tanggal = $tanggal->tp_tanggal_awal;
                $future++;
            }
        }
        $data['tentang'] = $conference[0]->tentang;

        return $data;
    }

    static function singleTanggalPentingBySlug($slug)
    {
        return Conference::join('tanggal_penting', 'conference.id', 'tanggal_penting.conference_id')
                        ->select('conference.*', 'tp_tanggal_awal as tanggal_awal', 'tp_tanggal_akhir as tanggal_akhir')
                        ->where('conference_slug', $slug)
                        ->where('tp_conference', 1)
                        ->first();
    }

    static function singleBySlug($slug)
    {
        return Conference::where('conference_slug', $slug)->first();
    }

    static function checkSlug($slug, $id)
    {
        return Conference::where('conference_slug', $slug)
                        ->where('id', '!=', $id)
                        ->first();
    }
}
