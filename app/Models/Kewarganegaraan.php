<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use DB;

class Kewarganegaraan extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'kewarganegaraan';
    protected $fillable = ['ke_nama'];

    static function getKewarganegaraan()
    {
        return Kewarganegaraan::get();
    }

    static function firstKewarganegaraanIdNama($id, $nama)
    {
        return Kewarganegaraan::where('id', '!=', $id)
                                    ->where('ke_nama', $nama)
                                    ->first();
    }

    static function storeKewarganegaraan($request)
    {
        return Kewarganegaraan::create([
            'ke_nama' => $request->nama
        ]);
    }

    static function updateKewarganegaraan($id, $request)
    {
        return Kewarganegaraan::whereId($id)->update([
            'ke_nama' => $request->nama
        ]);
    }

    static function deleteKewarganegaraan($id)
    {
        return Kewarganegaraan::whereId($id)->delete();
    }

    static function getDropdown($search)
    {
        return Kewarganegaraan::query()
            ->where('ke_nama','like',"%".$search."%")
            ->orderBy('ke_nama')
            ->paginate(20);
    }
}
