<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Publisher extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'publisher';
    protected $fillable = ['pu_nama', 'pu_level'];

    static function storePublisher($request, $id)
    {
        Publisher::updateOrCreate([
                'id' => $id,
            ],
            [
                'pu_nama' => $request->nama,
                'pu_level' => $request->level
            ]
        );
    }

    static function deletePublisher($id)
    {
        Publisher::find($id)->delete();
    }

    static function getPublisherbyLevel($id)
    {
        return Publisher::where('pu_level', $id)->get();
    }
}
