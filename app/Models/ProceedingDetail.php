<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProceedingDetail extends Model
{
    use HasFactory;

    protected $fillable = ['proceeding_id', 'submission_id', 'kode'];

    public function proceeding()
    {
        return $this->belongsTo(Proceeding::class);
    }

    public function submission()
    {
        return $this->belongsTo(Submission::class);
    }
}
