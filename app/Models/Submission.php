<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

use App\Models\KataKunciAbstrak;
use App\Models\AuthorSubmission;

class Submission extends Model
{
    use HasFactory, softDeletes;

    protected $table = 'submission';
    protected $fillable = [
        'conference_id', 'submission_judul', 'submission_abstrak', 'submission_abstrakindo',
        'submission_submitter', 'submission_tanggal_submit', 'submission_abstrak_file',
        'submission_status', 'submission_full_paper', 'submission_pembayaran_publikasi_status',
        'submission_pembayaran_abstrak_voucher', 'submission_pembayaran_abstrak_file',
        'submission_pembayaran_publikasi_voucher', 'submission_pembayaran_publikasi_file',
        'submission_pembayaran_abstrak_status', 'submission_pembayaran_abstrak_tanggal',
        'submission_pembayaran_abstrak_nominal', 'submission_pembayaran_abstrak_bank',
        'submission_pembayaran_abstrak_rekening', 'submission_pembayaran_abstrak_invoice',
        'submission_pembayaran_abstrak_pesan', 'submission_pembayaran_publikasi_tanggal',
        'submission_pembayaran_publikasi_nominal', 'submission_pembayaran_publikasi_bank',
        'submission_pembayaran_publikasi_rekening', 'submission_pembayaran_publikasi_invoice',
        'submission_pembayaran_publikasi_pesan'
    ];

    public function kata_kunci_abstrak()
    {
        return $this->hasMany(KataKunciAbstrak::class, 'submission_id', 'id')->select('id', 'submission_id', 'kka_kata_kunci', 'kka_level');
    }
    
    public function conference()
    {
        return $this->belongsTo(Conference::class, 'conference_id', 'id');
    }
    
    public function author_submission()
    {
        return $this->hasMany(AuthorSubmission::class, 'submission_id', 'id')->select('id', 'submission_id', 'user_id');
    }

    public function proceeding_detail()
    {
        return $this->hasOne(ProceedingDetail::class, 'submission_id', 'id');
    }

    // Umum
    static function updateDataByQuery($id, $query)
    {
        Submission::whereId($id)->update($query);
    }

    static function deleteDataByQuery($query)
    {
        Submission::where($query)->delete();
    }
    // /Umum

    // Route ['admin-conference.submission.pembayaran.show']
    static function firstSubmissionWithConferenceByQuerySelect($query, $select)
    {
        return Submission::join('conference', 'submission.conference_id', 'conference.id')
                        ->select($select)
                        ->where($query)
                        ->first();
    }

    static function getSubmissionByUserOnSubmission($id)
    {
        return Submission::join('conference', 'submission.conference_id', 'conference.id')
                    ->join('author_submission', 'author_submission.submission_id', 'submission.id')
                    ->select('submission.id', 'submission_judul as judul', 'conference_nama as nama', 'submission_pembayaran_abstrak_status as abstrak_status', 'submission_pembayaran_publikasi_status as publikasi_status', 'submission_status')
                    ->selectRaw('(CASE submission_status WHEN 1 THEN "Tahap 1 : Submit Abstrak" WHEN 2 THEN "Tahap 2 : Pembayaran Abstrak" WHEN 3 THEN "Tahap 3 : Upload Full Paper" WHEN 4 THEN "Tahap 4 : Review" WHEN 5 THEN "Tahap 5 : Pembayaran Publikasi" WHEN 6 THEN "Tahap 6 : Publikasi" ELSE "Selesai" END) as status')
                    ->where('author_submission.user_id', $id)
                    ->get();
    }

    static function firstSubmissionByUser($id)
    {
        return Submission::join('conference', 'submission.conference_id', 'conference.id')
                    ->join('author_submission', 'author_submission.submission_id', 'submission.id')
                    ->select('submission.*', 'author_submission.user_id')
                    ->where('author_submission.user_id', $id)
                    ->first();
    }

    static function storeSubmissionStep1($conference, $judul, $abstrak_inggris, $abstrak_indonesia, $step)
    {
        return Submission::create([
            'conference_id'             => $conference,
            'submission_judul'          => $judul,
            'submission_abstrak'        => $abstrak_inggris,
            'submission_abstrakindo'    => $abstrak_indonesia,
            'submission_status'         => $step
        ]);
    }

    static function firstSubmission($id)
    {
        return Submission::whereId($id)->first();
    }

    static function updateSubmissionAbstrakFile($id, $file)
    {
        Submission::whereId($id)->update([
            'submission_pembayaran_abstrak_file' => $file
        ]);
    }

    static function updateSubmissionPublikasiFile($id, $file)
    {
        Submission::whereId($id)->update([
            'submission_pembayaran_publikasi_file' => $file
        ]);
    }

    static function updateSubmissionAbstrakVoucher($id, $voucher, $step)
    {
        Submission::whereId($id)->update([
            'submission_pembayaran_abstrak_voucher' => $voucher,
            'submission_pembayaran_abstrak_status'  => 1,
            'submission_status'                     => $step
        ]);
    }

    static function updateSubmissionPublikasiVoucher($id, $voucher, $step)
    {
        Submission::whereId($id)->update([
            'submission_pembayaran_Publikasi_voucher' => $voucher,
            'submission_pembayaran_Publikasi_status'  => 1,
            'submission_status'                     => $step
        ]);
    }

    static function updateSubmissionJudulAbstrak($id, $judul, $abstrak, $abstrakindo)
    {
        Submission::whereId($id)->update([
            'submission_judul'          => $judul,
            'submission_abstrak'        => $abstrak,
            'submission_abstrakindo'    => $abstrakindo
        ]);
    }

    static function updateSubmissionStatus($id, $step)
    {
        Submission::whereId($id)->update([
            'submission_status' => $step
        ]);
    }

    static function updateSubmissionValidasiPembayaran($id, $step)
    {
        if ($step == 2) {
            Submission::whereId($id)->update([
                'submission_pembayaran_abstrak_status'  => 1,
                'submission_status'                     => $step
            ]);
        } else {
            Submission::whereId($id)->update([
                'submission_pembayaran_publikasi_status'    => 1,
                'submission_status'                         => $step
            ]);
        }
    }

    static function firstSubmissionPembayaran($id)
    {
        return Submission::join('conference', 'submission.conference_id', 'conference.id')
                        ->select('submission_judul as judul', 'conference_bayar_abstrak as abstrak_bayar', 'conference_bayar_publikasi as publikasi_bayar', 'submission_pembayaran_abstrak_file as abstrak_file', 'submission_pembayaran_publikasi_file as publikasi_file')
                        ->where('submission.id', $id)
                        ->first();
    }

    static function updateSubmissionFullPaperFile($id, $file)
    {
        Submission::whereId($id)->update([
            'submission_full_paper' => $file
        ]);
    }

    static function getSubmissionReviewByConference($id)
    {
        return Submission::leftJoin('author_submission', 'submission.id', 'author_submission.submission_id')
                        ->join('conference', 'submission.conference_id', 'conference.id')
                        ->join('users', 'author_submission.user_id', 'users.id')
                        ->select('submission.id', 'submission_judul as submission', 'users.name as nama')
                        ->where('submission_status', 3)
                        ->where('conference.user_id', $id)
                        ->groupBy('submission.id')
                        ->get();
    }

    static function getSubmissionBookOfAbstract($id)
    {
        $submission = Submission::join('conference', 'submission.conference_id', 'conference.id')
                                ->select('submission.id', 'conference_level')
                                ->where('conference.id', $id)
                                ->where('submission_status', '>', 1)
                                ->get();

        if (count($submission) > 0) {
            foreach ($submission as $key => $value) {
                $data['data'][$key] = Submission::select('submission_judul as judul', 'submission_abstrak as abstrak_inggris', 
                                                'submission_abstrakindo as abstrak_indonesia')
                                        ->whereId($value->id)
                                        ->first();
                $data['data'][$key]['author'] = AuthorSubmission::getAuthorSubmissionBySubmission($value->id);
                $data['data'][$key]['kata_kunci_inggris'] = KataKunciAbstrak::getKataKunciAbstrakBySubmissionLevel($value->id, 2);
                if ($value->conference_level == 1) {
                    $data['data'][$key]['kata_kunci_indonesia'] = KataKunciAbstrak::getKataKunciAbstrakBySubmissionLevel($value->id, 1);
                }
            }

            $data['status'] = [
                'kode' => 1,
                'status' => "Berhasil"
            ];

            $data['level'] = $submission[0]->conference_level;

            return $data;
        } else {
            return $data['status'] = [
                'kode' => 0,
                'status' => "Data Kosong"
            ];
        }
    }

    static function getSubmissionFullPaper($id)
    {
        return Submission::join('conference', 'submission.conference_id', 'conference.id')
                        ->select('submission.id', 'conference_nama as conference', 'submission_judul as submission', 'submission_full_paper as file')
                        ->where('conference.user_id', $id)
                        ->where('submission_status', 6)
                        ->get();
    }
}
