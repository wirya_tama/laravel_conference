<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KeynoteSpeaker extends Model
{
    use HasFactory;

    protected $table = 'keynote_speaker';
    protected $fillable = [
        'conference_id', 'ks_nama', 'ks_afiliasi',
        'ks_deskripsi', 'ks_foto'
    ];

    static function storeKeynoteSpeaker($request)
    {
        KeynoteSpeaker::create([
            'conference_id' => $request->conference,
            'ks_nama'       => $request->nama,
            'ks_afiliasi'   => $request->afiliasi,
            'ks_deskripsi'  => $request->deskripsi,
        ]);
    }

    static function updateKeynoteSpeaker($id, $request)
    {
        KeynoteSpeaker::whereId($id)->update([
            'conference_id' => $request->conference,
            'ks_nama'       => $request->nama,
            'ks_afiliasi'   => $request->afiliasi,
            'ks_deskripsi'  => $request->deskripsi,
        ]);
    }

    static function deleteKeynoteSpeaker($id)
    {
        KeynoteSpeaker::whereId($id)->delete();
    }

    static function updateFileKeynoteSpeaker($id, $foto)
    {
        KeynoteSpeaker::whereId($id)->update([
            'ks_foto' => $foto
        ]);
    }

    static function getByConference($id)
    {
        return KeynoteSpeaker::where('conference_id', $id)->get();
    }
}
