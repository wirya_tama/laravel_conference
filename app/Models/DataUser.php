<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataUser extends Model
{
    use HasFactory;

    protected $table = 'data_user';
    protected $fillable = ['user_id', 'ke_id', 'du_alamat', 'du_afiliasi', 'du_telepon', 'du_jenis_kelamin'];
    protected $hidden = ['created_at', 'updated_at'];

    static function firstDataUser($id)
    {
        return DataUser::where('user_id', $id)->first();
    }

    static function storeDataUser($id, $request)
    {
        DataUser::create([
            'user_id'           => $id,
            'ke_id'             => $request->kewarganegaraan,
            'du_jenis_kelamin'  => $request->jenis_kelamin
        ]);
    }

    static function storeDataByQuery($query)
    {
        DataUser::create($query);
    }

    static function storeDataUserRegister($id, $kewarganegaraan, $jenis_kelamin)
    {
        DataUser::create([
            'user_id'           => $id,
            'ke_id'             => $kewarganegaraan,
            'du_jenis_kelamin'  => $jenis_kelamin
        ]);
    }

    static function storeDataUserAuthor($id, $kewarganegaraan, $jenis_kelamin)
    {
        DataUser::create([
            'user_id'           => $id,
            'ke_id'             => $kewarganegaraan,
            'du_jenis_kelamin'  => $jenis_kelamin
        ]);
    }

    static function updateDataUser($id, $request)
    {
        DataUser::where('user_id', $id)->update([
            'ke_id'             => $request->kewarganegaraan,
            'du_jenis_kelamin'  => $request->jenis_kelamin
        ]);
    }

    static function updateData($id, $kewarganegaraan, $alamat, $afiliasi, $telepon, $jenis_kelamin)
    {
        DataUser::where('user_id', $id)->update([
            'ke_id'             => $kewarganegaraan,
            'du_alamat'         => $alamat,
            'du_afiliasi'       => $afiliasi,
            'du_telepon'        => $telepon,
            'du_jenis_kelamin'  => $jenis_kelamin
        ]);
    }
}
