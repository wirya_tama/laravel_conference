<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ReviewSubmission extends Model
{
    use HasFactory;

    protected $table = 'review_submission';
    protected $fillable = ['user_id', 'submission_id', 'res_pesan', 'res_file', 'res_status'];

    static function storeReviewSubmission($user_id, $submission_id, $pesan, $file, $status)
    {
        ReviewSubmission::create([
            'user_id'       => $user_id,
            'submission_id' => $submission_id,
            'res_pesan'     => $pesan,
            'res_file'      => $file,
            'res_status'    => $status
        ]);
    }

    static function getReviewSubmissionBySubmission($id)
    {
        return ReviewSubmission::select('user_id', 'submission_id', 'res_pesan as pesan', 'res_file as file', 'res_status as status', 'created_at as tanggal')
                                ->where('submission_id', $id)
                                ->orderBy('id', 'DESC')
                                ->get();
    }

    static function getReviewSubmissionBySubmissionCount($id)
    {
        return ReviewSubmission::select('id')->where('submission_id', $id)->count();
    }
}
