<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'gambar',
        'password',
        'no_induk'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
    * @param string|array $roles
    */
    public function authorizeRoles($roles)
    {
        if (is_array($roles)) {
            return $this->hasAnyRole($roles) || 
            abort(401, 'This action is unauthorized.');
        }
        return $this->hasRole($roles) || 
        abort(401, 'This action is unauthorized.');
    }

    /**
    * Check multiple roles
    * @param array $roles
    */
    public function hasAnyRole($roles)
    {
        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
    * Check one role
    * @param string $role
    */
    public function hasRole($role)
    {
        return null !== $this->roles()->where('name', $role)->first();
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    // Umum
    static function updateDataByQuery($query, $data)
    {
        User::where($query)->update($data);
    }

    static function getDropdownUserByRole($id, $search)
    {
        return User::query()
            ->join('list_roles', 'users.id', 'list_roles.user_id')
            ->select('users.id', 'users.name')
            ->where('list_roles.role_id', $id)
            ->where('users.name','like',"%".$search."%")
            ->orderBy('users.name')
            ->paginate(20);
    }

    static function firstUserDataUser($id)
    {
        $user = User::whereId($id)->first();
        $user['roles'] = ListRole::getRole($id);
        $user['data_user'] = DataUser::firstDataUser($id);

        return $user;
    }

    static function storeUser($request)
    {
        return User::create([
            'name'      => $request->name,
            'email'     => $request->email,
            'no_induk'  => $request->no_induk,
            'password'  => Hash::make($request->no_induk),
        ]);
    }

    static function verifiedUser($id)
    {
        User::whereId($id)->update([
            'email_verified_at' => now()
        ]);
    }

    static function storeUserRegister($name, $email, $password)
    {
        return User::create([
            'name'      => $name,
            'email'     => $email,
            'password'  => Hash::make($password),
        ]);
    }

    static function storeUserAuthor($name, $no_induk, $email, $password)
    {
        return User::create([
            'name'      => $name,
            'email'     => $email,
            'no_induk'  => $no_induk,
            'password'  => Hash::make($no_induk),
        ]);
    }

    static function updateUser($id, $request)
    {
        User::whereId($id)->update([
            'name'      => $request->name,
            'email'     => $request->email,
            'no_induk'  => $request->no_induk,
        ]);
    }

    static function updateData($id, $name, $email, $no_induk)
    {
        User::whereId($id)->update([
            'name'      => $name,
            'email'     => $email,
            'no_induk'  => $no_induk,
        ]);
    }

    static function deleteUser($id)
    {
        User::whereId($id)->delete();
    }

    static function getUserByRole($id)
    {
        return User::join('list_roles', 'users.id', 'list_roles.user_id')
                    ->select('users.id', 'users.name', 'users.no_induk')
                    ->where('list_roles.role_id', $id)
                    ->get();
    }

    static function firstUserEmail($email)
    {
        return User::where('email', $email)->withTrashed()->first();
    }

    static function firstUserNoInduk($no_induk)
    {
        return User::where('no_induk', $no_induk)->withTrashed()->first();
    }

    static function firstUserDataUserKewarganegaraan($id)
    {
        return User::join('data_user', 'users.id', 'data_user.user_id')
                    ->join('kewarganegaraan', 'data_user.ke_id', 'kewarganegaraan.id')
                    ->select('name as nama', 'du_alamat as alamat', 'du_afiliasi as afiliasi', 'ke_nama as kewarganegaraan', 'du_telepon as hp', 'email', 'gambar')
                    ->selectRaw('(CASE du_jenis_kelamin WHEN 1 THEN "Laki-laki" ELSE "Perempuan" END) as jenis_kelamin')
                    ->where('users.id', $id)
                    ->first();
    }

    static function updateGambar($gambar, $id)
    {
        User::whereId($id)->update([
            'gambar' => $gambar
        ]);
    }

    static function updatePassword($id, $password)
    {
        User::whereId($id)->update([
            'password'  => Hash::make($password),
        ]);
    }
}
