<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TanggalPenting extends Model
{
    use HasFactory;

    protected $table = 'tanggal_penting';
    protected $fillable = ['conference_id', 'tp_judul', 'tp_tanggal_awal', 'tp_tanggal_akhir', 'tp_conference'];

    static function deleteTanggalPentingByConference($id)
    {
        TanggalPenting::where('conference_id', $id)->delete();
    }

    static function storeTanggalPentingDefault($conference_id, $judul, $tanggal, $conference)
    {
        TanggalPenting::create([
            'conference_id'     => $conference_id,
            'tp_judul'          => $judul,
            'tp_tanggal_awal'   => $tanggal,
            'tp_tanggal_akhir'  => $tanggal,
            'tp_conference'     => $conference
        ]);
    }

    static function storeTanggalPenting($conference_id, $judul, $tanggal_awal, $tanggal_akhir, $conference)
    {
        TanggalPenting::create([
            'conference_id'     => $conference_id,
            'tp_judul'          => $judul,
            'tp_tanggal_awal'   => $tanggal_awal,
            'tp_tanggal_akhir'  => $tanggal_akhir,
            'tp_conference'     => $conference
        ]);
    }

    static function updateTanggalPenting($tanggal_penting, $request)
    {
        TanggalPenting::whereId($tanggal_penting->id)->update([
            'conference_id'     => $request->conference,
            'tp_judul'          => $request->judul,
            'tp_tanggal_awal'   => $request->tanggal_awal,
            'tp_tanggal_akhir'  => $request->tanggal_akhir,
            'tp_conference'     => $tanggal_penting->tp_conference
        ]);
    }

    static function getTanggalPenting($id)
    {
        return TanggalPenting::select('tp_judul as judul', 'tp_tanggal_awal as tanggal_awal', 'tp_tanggal_akhir as tanggal_akhir', 'tp_conference as conference')
                            ->where('conference_id', $id)
                            ->orderBy('tp_conference', 'desc')
                            ->orderBy('tanggal_penting.id', 'asc')
                            ->get();
    }

    static function getByConference($id)
    {
        return TanggalPenting::select('tp_judul as judul', 'tp_tanggal_awal as tanggal_awal', 'tp_tanggal_akhir as tanggal_akhir', 'tp_conference as conference')
                            ->where('conference_id', $id)
                            ->orderBy('tanggal_penting.tp_tanggal_awal', 'asc')
                            ->get();
    }

    static function getTanggalPentingAppCalendar($auth)
    {
        $tanggal_penting = TanggalPenting::join('conference', 'tanggal_penting.conference_id', 'conference.id')
                                        ->select('tanggal_penting.id')
                                        ->where('conference.user_id', $auth)
                                        ->get();

        if (count($tanggal_penting) > 0) {
            foreach ($tanggal_penting as $key => $value) {
                $id = $value->id;

                $data[$key] = TanggalPenting::join('conference', 'tanggal_penting.conference_id', 'conference.id')
                                            ->select('tanggal_penting.id', 'tp_tanggal_awal as start', 'tp_tanggal_akhir as end')
                                            ->selectRaw('CONCAT(conference_nama, " | ", tp_judul) as title')
                                            // ->select('tanggal_penting.id')
                                            // ->selectRaw('CONCAT(conference_nama, " | ", tp_judul) as title, CONCAT(tp_tanggal_awal, " 00:00:00") as start, CONCAT(tp_tanggal_akhir, " 23:59:59") as end')
                                            ->where('tanggal_penting.id', $id)
                                            ->first();
                $data[$key]['extendedProps'] = TanggalPenting::select('tp_judul as judul', 'conference_id as conference')
                                                            ->where('tanggal_penting.id', $id)
                                                            ->first();
            }
        }

        return $data;
    }

    static function deleteTanggalPenting($id)
    {
        TanggalPenting::whereId($id)->delete();
    }
}
