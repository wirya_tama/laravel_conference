<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PublisherConference extends Model
{
    use HasFactory;

    protected $table = 'publisher_conference';
    protected $fillable = ['conference_id', 'publisher_id'];

    static function getPublisherByConference($id)
    {
        return PublisherConference::join('publisher', 'publisher_conference.publisher_id', 'publisher.id')
                                    ->select('publisher.id', 'pu_nama as publisher')
                                    ->where('publisher_conference.conference_id', $id)
                                    ->get();
    }

    static function deletePublisherConferenceByConference($id)
    {
        return PublisherConference::where('conference_id', $id)->delete();
    }

    static function storePublisherConference($conference, $publisher)
    {
        PublisherConference::updateOrCreate(
            [
                'conference_id' => $conference, 
                'publisher_id'  => $publisher
            ],
            [
                'conference_id' => $conference, 
                'publisher_id'  => $publisher
            ],
        );
    }
}
