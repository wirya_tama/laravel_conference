<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;
    protected $table = 'page';
    protected $fillable = ['conference_id', 'judul', 'slug', 'gambar', 'isi'];

    static function firstData($params)
    {
        return Page::query()
            ->where($params)
            ->first();
    }

    static function getData($params)
    {
        return Page::query()
            ->join('conference', 'page.conference_id', 'conference.id')
            ->select('page.id', 'judul', 'conference.conference_nama', 'page.slug', 'page.gambar')
            ->where($params)
            ->get();
    }

    static function getDataByUser($id)
    {
        return Page::query()
            ->join('conference', 'page.conference_id', 'conference.id')
            ->select('page.id', 'judul', 'conference.conference_nama')
            ->where('user_id', $id)
            ->get();
    }

    static function storeAllData($request)
    {
        return Page::create([
            'conference_id' => $request->conference_id,
            'judul'         => $request->judul,
            'slug'          => $request->slug,
            'isi'           => $request->isi
        ]);
    }

    static function updateSingleDataByQuery($id, $query, $value)
    {
        Page::whereId($id)->update([
            $query => $value
        ]);
    }

    static function updateAllData($id, $request)
    {
        Page::whereId($id)->update([
            'conference_id' => $request->conference_id,
            'judul'         => $request->judul,
            'slug'          => $request->slug,
            'isi'           => $request->isi
        ]);
    }

    static function deleteDataByQuery($query, $value)
    {
        Page::where($query, $value)->delete();
    }
}
