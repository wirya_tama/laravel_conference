<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class FAQ extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'faq';
    protected $fillable = ['faq_pertanyaan', 'faq_jawaban'];

    static function storeFAQ($request)
    {
        FAQ::create([
            'faq_pertanyaan'    => $request->pertanyaan,
            'faq_jawaban'       => $request->jawaban
        ]);
    }

    static function updateFAQ($id, $request)
    {
        FAQ::whereId($id)->update([
            'faq_pertanyaan'    => $request->pertanyaan,
            'faq_jawaban'       => $request->jawaban
        ]);
    }

    static function deleteFAQ($id)
    {
        FAQ::whereId($id)->delete();
    }

    static function getAll()
    {
        return FAQ::select('id', 'faq_pertanyaan as pertanyaan', 'faq_jawaban as jawaban')->get();
    }
}
