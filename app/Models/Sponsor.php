<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sponsor extends Model
{
    use HasFactory;

    protected $table = 'sponsor';
    protected $fillable = ['conference_id', 'sponsor_nama', 'sponsor_logo'];

    static function storeSponsor($request)
    {
        Sponsor::create([
            'conference_id' => $request->conference,
            'sponsor_nama'  => $request->nama
        ]);
    }

    static function updateSponsor($id, $request)
    {
        Sponsor::whereId($id)->update([
            'conference_id' => $request->conference,
            'sponsor_nama'  => $request->nama
        ]);
    }

    static function updateFileSponsor($id, $logo)
    {
        Sponsor::whereId($id)->update([
            'sponsor_logo'  => $logo
        ]);
    }

    static function deleteSponsor($id)
    {
        Sponsor::whereId($id)->delete();
    }
}
