<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Organizer extends Model
{
    use HasFactory;
    protected $table = 'organizer';
    protected $fillable = ['conference_id', 'nama', 'link', 'gambar'];

    static function getDataByUser($id)
    {
        return Organizer::query()
            ->join('conference', 'organizer.conference_id', 'conference.id')
            ->select('organizer.*', 'conference_nama as conference')
            ->where('user_id', $id)
            ->get();
    }

    static function getByConference($id)
    {
        return Organizer::query()
            ->where('conference_id', $id)
            ->get();
    }

    static function storeAllData($request)
    {
        return Organizer::create([
            'conference_id' => $request->conference_id,
            'nama'          => $request->nama,
            'link'          => $request->link
        ]);
    }

    static function updateSingleDataByQuery($id, $query, $value)
    {
        Organizer::whereId($id)->update([
            $query => $value
        ]);
    }

    static function updateAllData($id, $request)
    {
        Organizer::whereId($id)->update([
            'conference_id' => $request->conference_id,
            'nama'          => $request->nama,
            'link'          => $request->link
        ]);
    }

    static function deleteDataById($id)
    {
        Organizer::whereId($id)->delete();
    }
}
