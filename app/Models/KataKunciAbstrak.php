<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KataKunciAbstrak extends Model
{
    use HasFactory;

    protected $table = 'kata_kunci_abstrak';
    protected $fillable = ['submission_id', 'kka_kata_kunci', 'kka_level'];

    // Umum
    static function deleteDataByQuery($query)
    {
        KataKunciAbstrak::where($query)->delete();
    }

    static function storeKataKunciAbstrak($id, $kata_kunci, $level)
    {
        KataKunciAbstrak::create([
            'submission_id'     => $id,
            'kka_kata_kunci'    => $kata_kunci,
            'kka_level'         => $level
        ]);
    }

    static function getKataKunciAbstrakBySubmission($id)
    {
        return KataKunciAbstrak::select('kka_kata_kunci as kata_kunci')->where('submission_id', $id)->get();
    }

    static function getKataKunciAbstrakBySubmissionLevel($id, $level)
    {
        return KataKunciAbstrak::select('kka_kata_kunci as kata_kunci')
                                ->where('submission_id', $id)
                                ->where('kka_level', $level)
                                ->get();
    }

    static function deleteKataKunciAbstrakBySubmission($id)
    {
        KataKunciAbstrak::where('submission_id', $id)->delete();
    }
}