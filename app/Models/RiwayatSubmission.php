<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RiwayatSubmission extends Model
{
    use HasFactory;

    protected $table = 'riwayat_submission';
    protected $fillable = ['submission_id', 'rs_status'];

    static function storeRiwayatSubmission($id, $status)
    {
        RiwayatSubmission::create([
            'submission_id' => $id,
            'rs_status'     => $status
        ]);
    }
}