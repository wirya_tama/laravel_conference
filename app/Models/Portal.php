<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Portal extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'portal';
    protected $fillable = [
        'portal_nama', 'portal_logo', 'portal_deskripsi', 'portal_tentang',
        'portal_latitude', 'portal_longitude', 'portal_kontak_nama1', 'portal_kontak_hp1',
        'portal_kontak_nama2', 'portal_kontak_hp2', 'portal_email', 'portal_instagram', 
        'portal_facebook', 'portal_twitter', 'portal_youtube'
    ];

    static function storePortal($request)
    {
        Portal::create([
            'portal_nama'           => $request->nama,
            'portal_deskripsi'      => $request->deskripsi,
            'portal_tentang'        => $request->tentang,
            'portal_latitude'       => $request->latitude,
            'portal_longitude'      => $request->longitude,
            'portal_kontak_nama1'   => $request->kontak_nama1,
            'portal_kontak_hp1'     => $request->kontak_hp1,
            'portal_kontak_nama2'   => $request->kontak_nama2,
            'portal_kontak_hp2'     => $request->kontak_hp2,
            'portal_email'          => $request->email,
            'portal_instagram'      => $request->instagram,
            'portal_facebook'       => $request->facebook,
            'portal_twitter'        => $request->twitter,
            'portal_youtube'        => $request->youtube,
        ]);
    }

    static function updatePortal($id, $request)
    {
        Portal::whereId($id)->update([
            'portal_nama'           => $request->nama,
            'portal_deskripsi'      => $request->deskripsi,
            'portal_tentang'        => $request->tentang,
            'portal_latitude'       => $request->latitude,
            'portal_longitude'      => $request->longitude,
            'portal_kontak_nama1'   => $request->kontak_nama1,
            'portal_kontak_hp1'     => $request->kontak_hp1,
            'portal_kontak_nama2'   => $request->kontak_nama2,
            'portal_kontak_hp2'     => $request->kontak_hp2,
            'portal_email'          => $request->email,
            'portal_instagram'      => $request->instagram,
            'portal_facebook'       => $request->facebook,
            'portal_twitter'        => $request->twitter,
            'portal_youtube'        => $request->youtube,
        ]);
    }

    static function updateLogoPortal($id, $logo)
    {
        Portal::whereId($id)->update([
            'portal_logo'           => $logo,
        ]);
    }

    static function deletePortal($id)
    {
        Portal::whereId($id)->delete();
    }

    static function getPortalIdNama()
    {
        return Portal::select('id', 'portal_nama as nama')->get();
    }
    
    static function firstPortal($id)
    {
        if ($id == 0) {
            return Portal::first();
        }
    }

    static function getTrashed()
    {
        return Portal::select('id', 'portal_nama as nama')
                    ->onlyTrashed()
                    ->get();
    }

    static function restore($id)
    {
        Portal::withTrashed()->whereId($id)->restore();
    }
}
