<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AuthorSubmission extends Model
{
    use HasFactory;

    protected $table = 'author_submission';
    protected $fillable = ['submission_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Umum
    static function deleteDataByQuery($query)
    {
        AuthorSubmission::where($query)->delete();
    }

    static function storeAuthorSubmission($submission_id, $user_id)
    {
        AuthorSubmission::updateOrCreate([
            'submission_id' => $submission_id,
            'user_id'       => $user_id
        ], [
            'submission_id' => $submission_id,
            'user_id'       => $user_id
        ]);
    }

    static function getAuthorSubmissionBySubmission($id)
    {
        return AuthorSubmission::join('users', 'author_submission.user_id', 'users.id')
                                ->select('users.name as nama', 'users.id as id')
                                ->where('submission_id', $id)
                                ->get();
    }

    static function deleteAuthorSubmissionBySubmission($id)
    {
        AuthorSubmission::where('submission_id', $id)->delete();
    }
}
