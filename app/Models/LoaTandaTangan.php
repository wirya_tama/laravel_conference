<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoaTandaTangan extends Model
{
    use HasFactory;

    protected $fillable = ['loa_id', 'title', 'gambar', 'keterangan'];
}
