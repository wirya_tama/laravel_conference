<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Topik extends Model
{
    use HasFactory;

    protected $table = 'topik';
    protected $fillable = ['conference_id', 'topik_nama'];

    static function storeTopik($request)
    {
        Topik::create([
            'conference_id' => $request->conference,
            'topik_nama'    => $request->nama
        ]);
    }

    static function updateTopik($id, $request)
    {
        Topik::whereId($id)->update([
            'conference_id' => $request->conference,
            'topik_nama'    => $request->nama
        ]);
    }

    static function deleteTopik($id)
    {
        Topik::whereId($id)->delete();
    }
}
