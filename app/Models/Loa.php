<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Loa extends Model
{
    use HasFactory;

    protected $fillable = ['conference_id', 'gambar1', 'gambar2', 'kop', 'isi'];
}
