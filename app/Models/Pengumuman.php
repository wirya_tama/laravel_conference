<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengumuman extends Model
{
    use HasFactory;

    protected $table = 'pengumuman';
    protected $fillable = ['conference_id', 'pengumuman_judul', 'pengumuman_jumlah', 'pengumuman_gambar', 'pengumuman_deskripsi'];

    static function storePengumuman($request)
    {
        Pengumuman::create([
            'conference_id'         => $request->conference,
            'pengumuman_judul'      => $request->judul,
            'pengumuman_deskripsi'  => $request->deskripsi
        ]);
    }

    static function updatePengumuman($id, $request)
    {
        Pengumuman::whereId($id)->update([
            'conference_id'         => $request->conference,
            'pengumuman_judul'      => $request->judul,
            'pengumuman_deskripsi'  => $request->deskripsi
        ]);
    }

    static function updateFilePengumuman($id, $gambar)
    {
        Pengumuman::whereId($id)->update([
            'pengumuman_gambar' => $gambar
        ]);
    }

    static function deletePengumuman($id)
    {
        Pengumuman::whereId($id)->delete();
    }

    static function getByConference($id)
    {
        return Pengumuman::select('pengumuman.id', 'pengumuman_judul as judul', 'pengumuman.created_at as tanggal', 'pengumuman_gambar as gambar', 'pengumuman_deskripsi as deskripsi')
                        ->where('conference_id', $id)
                        ->orderBy('id', 'DESC')
                        ->get();
    }
}
