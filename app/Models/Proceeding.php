<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proceeding extends Model
{
    use HasFactory;

    public $fillable = ['conference_id', 'title', 'slug', 'editor', 'volume', 'issn', 'isbn', 'tanggal'];

    public function conference()
    {
        return $this->belongsTo(Conference::class, 'conference_id', 'id')->select('id', 'conference_nama as nama');
    }

    /**
     * The submissions that belong to the Proceeding
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function submissions()
    {
        return $this->belongsToMany(Submission::class, 'proceeding_details', 'proceeding_id', 'submission_id');
    }

    public function proceeding_details()
    {
        return $this->hasMany(ProceedingDetail::class, 'proceeding_id', 'id');
    }
}
