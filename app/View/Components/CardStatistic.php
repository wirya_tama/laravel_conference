<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CardStatistic extends Component
{
    public $title;
    public $subtitle;
    public $icon;
    public $color;

    public function __construct($title, $subtitle, $icon, $color)
    {
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->icon = $icon;
        $this->color = $color;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.card-statistic');
    }
}
