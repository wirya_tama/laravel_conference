<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Modal extends Component
{
    public $class;
    public $type;
    public $id;
    public $title;

    public function __construct($class, $type, $id, $title)
    {
        $this->class = $class;
        $this->type = $type;
        $this->id = $id;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.modal');
    }
}
