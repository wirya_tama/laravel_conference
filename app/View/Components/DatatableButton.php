<?php

namespace App\View\Components;

use Illuminate\View\Component;

class DatatableButton extends Component
{
    public $title;
    public $buttonTitle;
    public $id;

    public function __construct($title, $buttonTitle, $id)
    {
        $this->title = $title;
        $this->buttonTitle = $buttonTitle;
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.datatable-button');
    }
}
