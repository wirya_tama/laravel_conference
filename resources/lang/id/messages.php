<?php

return [
    // Choose
    'choose' => [
        'conference' => 'Pilih Konferensi',
        'user' => 'Pilih Pengguna'
    ],

    // Data
    'data' => [
        'proceeding' => 'data prosiding'
    ],
    
    // Message
    'Message' => [
        'copyright holder' => 'Copyright will be assigned automatically to :title when this is published.',
        'copyright year' => 'The Copyright Year will be set automatically when this is published in an issue.'
    ],

    // A
    'about' => 'Tentang',
    'about portal' => 'Tentang Portal',
    'about us' => 'Tentang Kami',
    'abstract' => 'Abstrak',
    'add' => 'Tambah',
    'add country' => 'Tambah Negara',
    'admin' => 'Admin',
    'affiliation' => 'Afiliasi',
    'already registered?' => 'Sudah mendaftar?',
    'announcement' => 'Pengumuman',
    'announcements' => 'Pengumuman',
    'answer' => 'Jawaban',
    'are you sure' => 'Apakah Anda Yakin',
    'article' => 'Artikel',
    'article details' => 'Detail Artikel',
    'article template' => 'Templat Artikel',
    'author' => 'Penulis',
    'author master data' => 'Data Master Penulis',
    'author payment' => 'Pembayaran Penulis',
    
    // B
    'book of abstract' => 'Buku Abstrak',
    
    // C
    'cancel' => 'Batal',
    'change' => 'Ubah',
    'change logo' => 'Ubah Logo',
    'conference' => 'Konferensi',
    'conference admin' => 'Admin Konferensi',
    'conference audience' => 'Peserta Konferensi',
    'conference data' => 'Data Konferensi',
    'conference date' => 'Tanggal Konferensi',
    'conference master data' => 'Data Master Konferensi',
    'conference title' => 'Judul Konferensi',
    'conferences' => 'Konferensi',
    'contact person' => 'Narahubung',
    'contact person name' => 'Nama Narahubung',
    'contact person phone' => 'No. HP Narahubung',
    'country' => 'Negara',
    'create' => 'Tambah',
    'create conference' => 'Tambah Konferensi',
    'create faq' => 'Tambah Pertanyaan Umum',
    'create portal' => 'Tambah Portal',
    'create publisher' => 'Tambah Penerbit',
    'create user' => 'Tambah Pengguna',
    
    // D
    'date' => 'Tanggal',
    'delete' => 'Hapus',
    'deleted portal' => 'Portal Terhapus',
    'description' => 'Deskripsi',
    'detail' => 'Detail',
    "don't have an account?" => 'Belum memiliki akun?',
    'download' => 'Unduh',
    'download article' => 'Unduh Artikel',
    
    // E
    'editor' => 'Editor',
    'edit' => 'Ubah',
    'email' => 'Email',
    'error' => 'Error',
    'event schedule' => 'Agenda Acara',
    
    // F
    'failed' => 'Gagal',
    'faq' => 'Pertanyaan Umum',
    'faq data' => 'Data Pertanyaan Umum',
    'faq master data' => 'Data Master Pertanyaan Umum',
    'female' => 'Perempuan',
    'flash' => [
        'created' => ':data berhasil tersimpan',
        'deleted' => ':data berhasil terhapus',
        'imported' => ':data berhasil terimpor',
        'restored' => ':data berhasil terpulihkan',
        'retrieved' => ':data berhasil termuat',
        'updated' => ':data berhasil terubah',
    ],
    'error flash' => [
        'created' => ':data gagal tersimpan',
        'deleted' => ':data gagal terhapus',
        'imported' => ':data gagal terimpor',
        'restored' => ':data gagal terpulihkan',
        'retrieved' => ':data gagal termuat',
        'updated' => ':data gagal terubah',
    ],
    'frequently asked questions' => 'Pertanyaan Umum',
    'file' => 'Berkas',
    'files' => 'Berkas',
    'fill this field with numbers, dot, or minus' => 'Hanya boleh diisi dengan angka(0-9), titik(.), atau kurang(-)',
    'full paper' => 'Artikel Lengkap',
    'forget your password?' => 'Lupa password?',
    'future' => 'Mendatang',
    'future conference' => 'Konferensi Mendatang',
    'future conferences' => 'Konferensi Mendatang',
    
    // G
    'galley' => 'Galley',
    'gender' => 'Jenis Kelamin',

    // H
    'home' => 'Beranda',
    
    // I
    'info' => 'Info',
    'information' => 'Informasi',
    'informations' => 'Informasi',
    'international' => 'Internasional',
    
    // J
    'join conference' => 'Daftar Konferensi',
    
    // K
    'keynote speaker' => 'Pembicara',
    'keyword' => 'Kata Kunci',
    
    // L
    'letter of acceptance' => 'LOA',
    'letter of acceptance data' => 'Data LOA',
    'latest' => 'Terbaru',
    'latitude' => 'Garis Lintang',
    'level' => 'Level',
    'loading' => 'Memuat',
    'longitude' => 'Garis Bujur',
    'login' => 'Masuk',
    
    // M
    'male' => 'Laki-laki',
    'master' => 'Master',
    'metadata' => 'Metadata',
    'more info' => 'Info Lainnya',
    'must be at least 1' => 'Wajib minimal 1',
    
    // N
    'name' => 'Nama',
    'nation' => 'Kewarganegaraan',
    'nation master data' => 'Data Master Kewarganegaraan',
    'nation data' => 'Data Kewarganegaraan',
    'national' => 'Nasional',
    'no info available' => 'Tidak Ada Info',
    'no announcement available' => 'Tidak Ada Pengumuman',
    'no conference administrator available' => 'Tidak Ada Administrator Konferensi',
    'no conference available' => 'Tidak Ada Konferensi',
    'no portal available' => 'Tidak Ada Portal',
    'not required' => 'Tidak Wajib Diisi',
    
    // O
    'ongoing conferences' => 'Konferensi Terkini',
    'organizer' => 'Penyelenggara',
    'organized by' => 'Diselenggarakan oleh',
    
    // P
    'password' => 'Kata sandi',
    'password confirmation' => 'Konfirmasi Password',
    'past' => 'Lampau',
    'past conference' => 'Konferensi Lampau',
    'payment' => 'Pembayaran',
    'place' => 'Lokasi',
    'portal' => 'Portal',
    'portal data' => 'Data Portal',
    'portal description' => 'Deskripsi Portal',
    'portal email' => 'Email Portal',
    'portal facebook' => 'Facebook Portal',
    'portal instagram' => 'Instagram Portal',
    'portal latitude' => 'Garis Lintang Portal',
    'portal longitude' => 'Garis Bujur Portal',
    'portal master data' => 'Data Master Portal',
    'portal title' => 'Judul Portal',
    'portal twitter' => 'Twitter Portal',
    'portal youtube' => 'YouTube Portal',
    'present' => 'Terkini',
    'present conference' => 'Konferensi Terkini',
    'profile' => 'Profil',
    'proceeding' => 'Prosiding',
    'proceeding not empty' => 'Prosiding tidak kosong',
    'proceedings' => 'Prosiding',
    'proceedings article' => 'Artikel Prosiding',
    'proceeding detail' => 'Detail Prosiding',
    'published article' => 'Artikel Yang Diterbitkan',
    'published date' => 'Tanggal Terbit',
    'publisher' => 'Penerbit',
    'publisher data' => 'Data Penerbit',
    'publisher master data' => 'Data Master Penerbit',
    'publisher name' => 'Nama Penerbit',
    'publishing info' => 'Info Publikasi',
    'publication' => 'Publikasi',

    // Q
    'question' => 'Pertanyaan',
    
    // R
    'read more' => 'Selengkapnya',
    'read less' => 'Tutup',
    'register' => 'Daftar',
    'registration number' => 'No Induk',
    'report' => 'Laporan',
    'review' => 'Tinjauan',
    'reviewer data' => 'Data Peninjau',
    'reviewer master data' => 'Data Master Peninjau',
    'role' => 'Hak Akses',
    
    // S
    'save' => 'Simpan',
    'schedule' => 'Agenda',
    'schedules' => 'Agenda',
    'search' => 'Pencarian',
    'see conferences' => 'Lihat Konferensi',
    'see all conferences' => 'Lihat Semua Konferensi',
    'see all announcements' => 'Lihat Semua Pengumuman',
    'select conference administrator' => 'Pilih Administrator Konferensi',
    'select conference portal' => 'Pilih Portal Konferensi',
    'select nation' => 'Pilih Kewarganegaraan',
    'select publisher level' => 'Pilih Level Penerbit',
    'settings' => 'Pengaturan',
    'sign out' => 'Keluar',
    'slug' => 'Slug',
    'speaker' => 'Narasumber',
    'speakers' => 'Narasumber',
    'the speakers' => 'Narasumber',
    'sponsor' => 'Sponsor',
    'static page' => 'Halaman Statis',
    'status' => 'Status',
    'submission' => 'Pengajuan',
    'success' => 'Sukses',
    'system' => 'Sistem',
    
    // T
    'title' => 'Judul',
    'title & abstract' => 'Judul & Abstrak',
    'topic' => 'Topik',
    
    // U
    'update' => 'Ubah',
    'update conference' => 'Ubah Konferensi',
    'update country' => 'Ubah Negara',
    'update faq' => 'Ubah Pertanyaan Umum',
    'update portal' => 'Ubah Portal',
    'update publisher' => 'Ubah Penerbit',
    'update user' => 'Ubah Pengguna',
    'user' => 'Pengguna',
    'user data' => 'Data Pengguna',
    'user email' => 'Email Pengguna',
    'user master data' => 'Data Master Pengguna',
    
    // V
    'verify email' => 'Verifikasi Email',
    'volume' => 'Volume',
    'voucher' => 'Voucher',
    
    // W
    'welcome' => 'Halo',
    'welcome messages' => 'Selamat Datang di Konferensi Ilmiah Universitas Warmadewa',
    'where when' => 'Waktu & Tempat',
    
    // Y
    'yes' => 'Ya',
    'your email address' => 'Alamat email...',
    'your name' => 'Nama...',
    'your password' => 'Password...',

];
