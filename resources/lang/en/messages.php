<?php

return [
    // Choose
    'choose' => [
        'conference' => 'Choose Conference',
        'user' => 'Choose User'
    ],
    
    // Data
    'data' => [
        'proceeding' => 'proceeding data'
    ],
    
    // Message
    'Message' => [
        'copyright holder' => 'Copyright will be assigned automatically to :title when this is published.',
        'copyright year' => 'The Copyright Year will be set automatically when this is published in an issue.'
    ],

    // A
    'about' => 'About',
    'about portal' => 'About Portal',
    'about us' => 'About Us',
    'abstract' => 'Abstract',
    'add' => 'Add',
    'add country' => 'Add Country',
    'admin' => 'Admin',
    'affiliation' => 'Affiliation',
    'already registered?' => 'Already Registered?',
    'announcement' => 'Announcement',
    'announcements' => 'Announcements',
    'answer' => 'Answer',
    'are you sure' => 'Are You Sure',
    'article' => 'Article',
    'article details' => 'Article Details',
    'article template' => 'Article Template',
    'author' => 'Author',
    'author master data' => 'Author Master Data',
    'author payment' => 'Author Payment',

    // B
    'book of abstract' => 'Book of Abstract',
    
    // C
    'cancel' => 'cancel',
    'change' => 'Change',
    'change logo' => 'Change Logo',
    'conference' => 'Conference',
    'conference admin' => 'Conference Admin',
    'conference audience' => 'Conference Audience',
    'conference data' => 'Conference Data',
    'conference date' => 'Conference Date',
    'conference master data' => 'Conference Master Data',
    'conference title' => 'Conference Title',
    'conferences' => 'Conferences',
    'contact person' => 'Contact Person',
    'contact person name' => 'Contact Person Name',
    'contact person phone' => 'Contact Person Phone',
    'country' => 'Country',
    'create' => 'Create',
    'create conference' => 'Create Conference',
    'create faq' => 'Create FAQ',
    'create portal' => 'Create Portal',
    'create publisher' => 'Create Publisher',
    'create user' => 'Create User',
    
    // D
    'date' => 'Date',
    'delete' => 'Delete',
    'deleted portal' => 'Deleted Portal',
    'description' => 'Description',
    'detail' => 'Detail',
    "don't have an account?" => "Don't have an account?",
    'download' => 'Download',
    'download article' => 'Download Article',
    
    // E
    'editor' => 'Editor',
    'edit' => 'Edit',
    'email' => 'Email',
    'error' => 'Error',
    'event schedule' => 'Event Schedule',
    
    // F
    'failed' => 'Failed',
    'faq' => 'FAQ',
    'faq data' => 'FAQ Data',
    'faq master data' => 'FAQ Master Data',
    'female' => 'Female',
    'flash' => [
        'created' => ':data created successfully',
        'deleted' => ':data deleted successfully',
        'imported' => ':data imported successfully',
        'restored' => ':data restored successfully',
        'retrieved' => ':data retrieved successfully',
        'updated' => ':data updated successfully',
    ],
    'error flash' => [
        'created' => 'create :data failed',
        'deleted' => 'delete :data failed',
        'imported' => 'import :data failed',
        'restored' => 'restore :data failed',
        'retrieved' => 'retrieve :data failed',
        'updated' => 'update :data failed',
    ],
    'frequently asked questions' => 'Frequently Asked Questions',
    'file' => 'File',
    'files' => 'Files',
    'fill this field with numbers, dot, or minus' => 'Fill this field with numbers(0-9), dot(.), or minus(-)',
    'forget your password?' => 'Forget your password?',
    'full paper' => 'Full Paper',
    'future' => 'Future',
    'future conference' => 'Future Conference',
    'future conferences' => 'Future Conferences',
    
    // G
    'galley' => 'Galley',
    'gender' => 'Gender',
    
    // H
    'home' => 'Home',
    
    // I
    'info' => 'Info',
    'information' => 'Information',
    'informations' => 'Informations',
    'international' => 'International',
    
    // J
    'join conference' => 'Join Conference',
    
    // K
    'keynote speaker' => 'Keynote Speaker',
    'keyword' => 'Keyword',
    
    // L
    'letter of acceptance' => 'Letter of Acceptance',
    'letter of acceptance data' => 'Letter of Acceptance Data',
    'latest' => 'Latest',
    'latitude' => 'Latitude',
    'level' => 'Level',
    'loading' => 'Loading',
    'longitude' => 'Longitude',
    'login' => 'login',
    
    // M
    'male' => 'Male',
    'master' => 'Master',
    'metadata' => 'Metadata',
    'more info' => 'More Info',
    'must be at least 1' => 'Must be at least 1',
    
    // N
    'name' => 'Name',
    'nation' => 'Nation',
    'nation master data' => 'Nation Master Data',
    'nation data' => 'Nation Data',
    'national' => 'National',
    'no info available' => 'No Info Available',
    'no announcement available' => 'No Announcement Available',
    'no conference administrator available' => 'No Conference Administrator Available',
    'no conference available' => 'No Conference Available',
    'no portal available' => 'No Portal Available',
    'not required' => 'Not Required',
    
    // O
    'ongoing conferences' => 'Ongoing Conferences',
    'organizer' => 'Organizer',
    'organized by' => 'Organized By',
    
    // P
    'password' => 'Password',
    'password confirmation' => 'Confirm Password',
    'past' => 'Past',
    'past conference' => 'Past Conference',
    'payment' => 'Payment',
    'place' => 'Place',
    'portal' => 'Portal',
    'portal data' => 'Portal Data',
    'portal description' => 'Portal Description',
    'portal email' => 'Portal Email',
    'portal facebook' => 'Portal Facebook',
    'portal instagram' => 'Portal Instagram',
    'portal latitude' => 'Portal Latitude',
    'portal longitude' => 'Portal Longitude',
    'portal master data' => 'Portal Master Data',
    'portal title' => 'Portal Title',
    'portal twitter' => 'Portal Twitter',
    'portal youtube' => 'Portal YouTube',
    'present' => 'Present',
    'present conference' => 'Present Conference',
    'profile' => 'Profile',
    'proceeding' => 'Proceeding',
    'proceeding not empty' => 'Proceeding is not empty',
    'proceedings' => 'Proceedings',
    'proceedings article' => 'Proceedings Article',
    'proceeding detail' => 'Proceeding Detail',
    'published article' => 'Published Article',
    'published date' => 'Published Date',
    'publisher' => 'Publisher',
    'publisher master data' => 'Publisher Master Data',
    'publisher name' => 'Publisher Name',
    'publishing info' => 'Publishing Info',
    'publication' => 'Publication',
    
    // Q
    'question' => 'Question',

    // R
    'read more' => 'Read More',
    'read less' => 'Read Less',
    'register' => 'Register',
    'registration number' => 'Reg Number',
    'report' => 'Report',
    'review' => 'Review',
    'reviewer data' => 'Reviewer Data',
    'reviewer master data' => 'Reviewer Master Data',
    'role' => 'Role',
    
    // S
    'save' => 'Save',
    'schedule' => 'Schedule',
    'schedules' => 'Schedules',
    'search' => 'Search',
    'see conferences' => 'See Conferences',
    'see all conferences' => 'See All Conferences',
    'see all announcements' => 'See All Announcements',
    'select conference administrator' => 'Select Conference Administrator',
    'select conference portal' => 'Select Conference Portal',
    'select nation' => 'Select Nation',
    'select publisher level' => 'Select Publisher Level',
    'settings' => 'Settings',
    'sign out' => 'Sign Out',
    'slug' => 'Slug',
    'speaker' => 'Speaker',
    'speakers' => 'Speakers',
    'the speakers' => 'The Speakers',
    'sponsor' => 'Sponsor',
    'static page' => 'Static Page',
    'status' => 'Status',
    'submission' => 'Submission',
    'success' => 'Success',
    'system' => 'System',
    
    // T
    'title' => 'Title',
    'title & abstract' => 'Title & Abstract',
    'topic' => 'Topic',
    
    // U
    'update' => 'Update',
    'update conference' => 'Update Conference',
    'update country' => 'Update Country',
    'update faq' => 'Update FAQ',
    'update portal' => 'Update Portal',
    'update publisher' => 'Update Publisher',
    'update user' => 'Update User',
    'user' => 'User',
    'user data' => 'User Data',
    'user email' => 'User Email',
    'user master data' => 'User Master Data',
    
    // V
    'verify email' => 'Verify Email',
    'volume' => 'Volume',
    'voucher' => 'Voucher',
    
    // W
    'welcome' => 'Welcome',
    'welcome messages' => 'Welcome to the Biggest Warmadewa University Conference',
    'where when' => 'Waktu & Tempat',
    
    // Y
    'yes' => 'Yes',
    'your email address' => 'Your email address...',
    'your name' => 'Your name...',
    'your password' => 'Your password...',

];
