
@if (request()->is('admin') || request()->is('admin/*'))
    {{-- Home --}}
    <li class="nav-item {{ (request()->routeIs('admin.home')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="#"><i data-feather="home"></i><span class="menu-title text-truncate">{{ __('messages.home') }}</span></a></li>
    
    {{-- Data Master--}}
    <li class="navigation-header"><span>{{ __('messages.master') }}</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.user.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin/master/user') }}"><i data-feather="user"></i><span class="menu-title text-truncate">{{ __('messages.user') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.author.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin/master/author') }}"><i data-feather="user-plus"></i><span class="menu-title text-truncate">{{ __('messages.author') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.kewarganegaraan.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin/master/kewarganegaraan') }}"><i data-feather="flag"></i><span class="menu-title text-truncate">{{ __('messages.nation') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.faq.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin/master/faq') }}"><i data-feather="help-circle"></i><span class="menu-title text-truncate">{{ __('messages.faq') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.portal.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin/master/portal') }}"><i data-feather="aperture"></i><span class="menu-title text-truncate">{{ __('messages.portal') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.conference.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin/master/conference') }}"><i data-feather="cloud"></i><span class="menu-title text-truncate">{{ __('messages.conference') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin.master.publisher.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin/master/publisher') }}"><i data-feather="book"></i><span class="menu-title text-truncate">{{ __('messages.publisher') }}</span></a></li>

@elseif (request()->is('admin-conference*'))
    {{-- Home --}}
    <li class="nav-item {{ (request()->routeIs('admin-conference.home')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="/admin-conference"><i data-feather="home"></i><span class="menu-title text-truncate">{{ __('messages.home') }}</span></a></li>
    
    {{-- Data Conference --}}
    <li class="navigation-header"><span>{{ __('messages.conference') }}</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.conference.conference.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/conference/conference') }}"><i data-feather="cloud"></i><span class="menu-title text-truncate">{{ __('messages.conference') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.conference.topik.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/conference/topik') }}"><i data-feather="activity"></i><span class="menu-title text-truncate">{{ __('messages.topic') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.conference.keynote.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/conference/keynote') }}"><i data-feather="key"></i><span class="menu-title text-truncate">{{ __('messages.keynote speaker') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.conference.tanggal-penting.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/conference/tanggal-penting') }}"><i data-feather="calendar"></i><span class="menu-title text-truncate">{{ __('messages.event schedule') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.conference.pengumuman.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/conference/pengumuman') }}"><i data-feather="info"></i><span class="menu-title text-truncate">{{ __('messages.announcement') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.conference.sponsor.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/conference/sponsor') }}"><i data-feather="dollar-sign"></i><span class="menu-title text-truncate">{{ __('messages.sponsor') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.conference.voucher.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/conference/voucher') }}"><i data-feather="credit-card"></i><span class="menu-title text-truncate">{{ __('messages.voucher') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.conference.organizer.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/conference/organizer') }}"><i data-feather="users"></i><span class="menu-title text-truncate">{{ __('messages.organizer') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.conference.page.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/conference/page') }}"><i data-feather="file-text"></i><span class="menu-title text-truncate">{{ __('messages.static page') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.conference.loa.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/conference/loa') }}"><i data-feather="file-text"></i><span class="menu-title text-truncate">{{ __('messages.letter of acceptance') }}</span></a></li>

    {{-- Data Submission --}}
    <li class="navigation-header"><span>{{ __('messages.submission') }}</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference/submission"><i data-feather="file-text"></i><span class="menu-title text-truncate">{{ __('messages.submission') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.submission.pembayaran.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/submission/pembayaran') }}"><i data-feather="dollar-sign"></i><span class="menu-title text-truncate">{{ __('messages.payment') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.submission.review.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/submission/review') }}"><i data-feather="users"></i><span class="menu-title text-truncate">{{ __('messages.review') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.submission.abstract.index')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/submission/abstract') }}"><i data-feather="book-open"></i><span class="menu-title text-truncate">{{ __('messages.book of abstract') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.submission.abstract.full-paper')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/submission/full-paper') }}"><i data-feather="book"></i><span class="menu-title text-truncate">{{ __('messages.full paper') }}</span></a></li>

    {{-- Data Publikasi --}}
    <li class="navigation-header"><span>{{ __('messages.publication') }}</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="{{ route('admin-conference.publication.proceeding.index') }}"><i data-feather="file-text"></i><span class="menu-title text-truncate">{{ __('messages.proceeding') }}</span></a></li>

    {{-- Data Laporan --}}
    <li class="navigation-header"><span>{{ __('messages.report') }}</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.laporan.peserta.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/laporan/peserta-conference') }}"><i data-feather="users"></i><span class="menu-title text-truncate">{{ __('messages.conference audience') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('admin-conference.laporan.pembayaran.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('admin-conference/laporan/pembayaran') }}"><i data-feather="shopping-bag"></i><span class="menu-title text-truncate">{{ __('messages.author payment') }}</span></a></li>
    {{-- <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference/laporan/submission"><i data-feather="file-text"></i><span class="menu-title text-truncate">{{ __('messages.submission') }}</span></a></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="/admin-conference/laporan/published"><i data-feather="book"></i><span class="menu-title text-truncate">{{ __('messages.published article') }}</span></a></li> --}}

@elseif (request()->is('author*'))
    {{-- Home --}}
    <li class="nav-item {{ (request()->routeIs('author.home')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="#"><i data-feather="home"></i><span class="menu-title text-truncate">{{ __('messages.home') }}</span></a></li>
    
    {{-- Data Conference --}}
    <li class="navigation-header"><span>{{ __('messages.author') }}</span><i data-feather="more-horizontal"></i></li>
    <li class="nav-item {{ (request()->routeIs('author.author.profil.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('author/author/profil') }}"><i data-feather="user"></i><span class="menu-title text-truncate">{{ __('messages.profile') }}</span></a></li>
    <li class="nav-item {{ (request()->routeIs('author.author.submission.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('author/author/submission') }}"><i data-feather="file-text"></i><span class="menu-title text-truncate">{{ __('messages.submission') }}</span></a></li>
    <li class="nav-item"><a class="d-flex align-items-center" href="#"><i data-feather="book-open"></i><span class="menu-title text-truncate">Book of Abstract</span></a></li>
    <li class="nav-item {{ (request()->routeIs('author.author.conference.*')) ? 'active' : '' }}"><a class="d-flex align-items-center" href="{{ url('author/author/conference') }}"><i data-feather="cloud"></i><span class="menu-title text-truncate">{{ __('messages.join conference') }}</span></a></li>
@endif

<li class="navigation-header"><span>{{ __('messages.system') }}</span><i data-feather="more-horizontal"></i></li>
<li class="nav-item"><a class="d-flex align-items-center" href="{{ route('portal.home') }}"><i data-feather="log-out"></i><span class="menu-title text-truncate">{{ __('messages.sign out') }}</span></a></li>