<div id="sidebar" class="col-md-4">

    @if ($abc['tentang'] != NULL)
        <div class="widget widget-text">
            <h4>{{ __('messages.about us') }}</h4>
            <div class="small-border"></div>
            
            <style>
                .content {
                    height:90px;
                    overflow:hidden;
                    margin-bottom:10px;
                }
                
                .content.visible {
                    height:auto;
                    overflow:visible;
                }
            </style>
            <div class="content">
                {{ $abc['tentang'] }}
            </div>

            <div class="text-center mt-3">
                <button class="show_hide btn btn-primary text-white font-weight-bold" data-content="toggle-text" onclick="read_toggle(1)">Read More</button>
            </div>
        </div>
    @endif

    <div class="widget widget-post">
        <h4>{{ __('messages.ongoing conferences') }}</h4>
        <div class="small-border"></div>
        @if (isset($abc['now']))
            @for ($i = 0; $i < count($abc['now']); $i++)
                @if ($i == count($abc['now']) - 1 || $i == 3)
                    <a href="/{{ $abc['now'][$i]->slug }}">
                        <div class="schedule-listing">
                            <div class="schedule-item pb-0" style="border-bottom: 0">
                                <div class="sc-date">
                                    {{ date('d', strtotime($abc['now'][$i]['tanggal'])) }}<br><span>{{ date('M', strtotime($abc['now'][$i]['tanggal'])) }}</span>
                                </div>
                                <div class="sc-info">
                                    <h3>{{ $abc['now'][$i]['nama'] }}</h3>
                                    <p>{{ $abc['now'][$i]['conference_afiliasi'] }}</p>
                                </div>
                                {{-- <div class="clearfix"></div> --}}
                            </div>
                        </div>
                    </a>
                    @break
                @else
                    <a href="/{{ $abc['now'][$i]->slug }}">
                        <div class="schedule-listing">
                            <div class="schedule-item">
                                <div class="sc-date">
                                    {{ date('d', strtotime($abc['now'][$i]['tanggal'])) }}<br><span>{{ date('M', strtotime($abc['now'][$i]['tanggal'])) }}</span>
                                </div>
                                <div class="sc-info">
                                    <h3>{{ $abc['now'][$i]['nama'] }}</h3>
                                    <p>{{ $abc['now'][$i]['conference_afiliasi'] }}</p>
                                </div>
                                {{-- <div class="clearfix"></div> --}}
                            </div>
                        </div>
                    </a>
                @endif
            @endfor
        @else
            <h6 class="text-center my-5 text-uppercase">Tidak Ada Conference</h6>
        @endif
    </div>

    <div class="widget widget-post">
        <h4>{{ __('messages.future conferences') }}</h4>
        <div class="small-border"></div>
        @if (isset($abc['future']))
            @for ($i = 0; $i < count($abc['future']); $i++)
                @if ($i == count($abc['future']) - 1 || $i == 3)
                    <a href="/{{ $abc['future'][$i]->slug }}">
                        <div class="schedule-listing">
                            <div class="schedule-item pb-0" style="border-bottom: 0">
                                <div class="sc-date">
                                    {{ date('d', strtotime($abc['future'][$i]['tanggal'])) }}<br><span>{{ date('M', strtotime($abc['future'][$i]['tanggal'])) }}</span>
                                </div>
                                <div class="sc-info">
                                    <h3>{{ $abc['future'][$i]['nama'] }}</h3>
                                    <p>{{ $abc['future'][$i]['conference_afiliasi'] }}</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </a>
                    @break
                @else
                    <a href="/{{ $abc['future'][$i]->slug }}">
                        <div class="schedule-listing">
                            <div class="schedule-item">
                                <div class="sc-date">
                                    {{ date('d', strtotime($abc['future'][$i]['tanggal'])) }}<br><span>{{ date('M', strtotime($abc['future'][$i]['tanggal'])) }}</span>
                                </div>
                                <div class="sc-info">
                                    <h3>{{ $abc['future'][$i]['nama'] }}</h3>
                                    <p>{{ $abc['future'][$i]['conference_afiliasi'] }}</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </a>
                @endif
            @endfor
        @else
            <h6 class="text-center my-5 text-uppercase">Tidak Ada Conference</h6>
        @endif
    </div>
</div>

<script>
    function read_toggle(data) {     
        $(".show_hide").text(data == 0 ? '{{ __("messages.read more") }}' : '{{ __("messages.read less") }}');
        $(".show_hide").attr('onclick', data == 0 ? 'read_toggle(1)' : 'read_toggle(0)');
        $(".content").toggleClass("visible");
    };
</script>
