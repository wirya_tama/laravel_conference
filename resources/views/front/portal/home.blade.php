@extends('front.layouts.app')

@section('title')
    {{ $portal->portal_nama }}
@endsection

@section('css')
    <!-- RS5.0 Stylesheet -->
    <link rel="stylesheet" href="{{ asset('front-assets/revolution/css/settings.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/revolution/css/layers.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/revolution/css/navigation.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/css/rev-settings.css') }}" type="text/css">
    <style>
        .faq p {
            margin-bottom: 0%;
        }
    </style>
@endsection

@section('content')
    <!-- parallax section -->
    <section id="section-hero" class="fullwidthbanner-container" aria-label="section-slider" data-bgcolor="#101010">
        <div id="slider-revolution">
            <ul>
                <li data-transition="random-premium" data-slotamount="10" data-masterspeed="200" data-thumb="">
                    <!--  BACKGROUND IMAGE -->
                    <img alt="" class="rev-slidebg" data-bgparallax="0" src="{{ asset('images/portal/pexels-icsa-1708936.jpg') }}">
                </li>

                <li data-transition="random-premium" data-slotamount="10" data-masterspeed="200" data-thumb="">
                    <!--  BACKGROUND IMAGE -->
                    <img alt="" class="rev-slidebg" data-bgparallax="0" src="{{ asset('images/portal/pexels-luis-quintero-2833037.jpg') }}">
                </li>

            </ul>

            <div style="" class="tp-static-layers">
                    <div
                        class="text-center tp-caption size-84 font-weight-bold text-white tp-static-layer"
                        data-x="['center','center','center','center']"
                        data-hoffset="['0']"
                        data-y="['center','center','center','center']"
                        data-lineheight="['90','90','90','90']"
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-type="text"
                        data-responsive_offset="on"
                        data-startslide="0"
                        data-endslide="3"
                        data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                        data-textAlign="['center','center','center','center']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                    >

                    @php
                        $data = explode(' ', $portal->portal_nama);
                        $judul = '';
                        $br = 0;

                        for ($i=0; $i < count($data); $i++) {
                            $judul .= $data[$i].' ';
                            $br++;

                            if ($br == 3) {
                                $judul .= '<br>';
                                $br = 0;
                            }
                        }
                    @endphp
                        {!! $judul !!}
                    </div>

                    <div
                        class="tp-caption tp-static-layer"
                        data-x="['center','center','center','center']"
                        data-hoffset="['0']"
                        data-y="['top','top','top','top']"
                        data-voffset="['506']"
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-type="button"
                        data-responsive_offset="on"
                        data-responsive="off"
                        data-startslide="0"
                        data-endslide="3"
                        data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(255, 255, 255, 0.15);bw:2px 2px 2px 2px;"}]'
                        data-textAlign="['center','center','center','center']"
                        data-paddingtop="[12,12,12,12]"
                        data-paddingright="[20,20,20,20]"
                        data-paddingbottom="[12,12,12,12]"
                        data-paddingleft="[20,20,20,20]"
                    >
                        <a href="#section-conference" class="btn-custom text-white scroll-to">{{ __('messages.see conferences') }}</a>
                    </div>

                    <div
                        class="tp-caption tp-static-layer"
                        data-x="['center','center','center','center']"
                        data-hoffset="['0']"
                        data-y="['top','top','top','top']"
                        data-voffset="['506']"
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-type="button"
                        data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                        data-responsive_offset="on"
                        data-responsive="off"
                        data-startslide="0"
                        data-endslide="3"
                        data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(255, 255, 255, 0.15);bw:2px 2px 2px 2px;"}]'
                        data-textAlign="['center','center','center','center']"
                        data-paddingtop="[12,12,12,12]"
                        data-paddingright="[20,20,20,20]"
                        data-paddingbottom="[12,12,12,12]"
                        data-paddingleft="[20,20,20,20]"
                    >
                    </div>
                </div>
        </div>
    </section>
    <!-- section close -->

    <!-- section begin -->
    <section id="section-about" data-bgimage="url({{ asset('front-assets/images-event/bg/4.jpg') }}) top left no-repeat">
        <div class="wm wm-border light wow fadeInDown">{{ strtolower(__('messages.welcome')) }}</div>
        <div class="container">
            <div class="row align-items-center">

                <div class="col-lg-6 wow fadeInLeft" data-wow-delay="0s">
                    <h2>{{ __('messages.welcome messages') }}</h2>
                    <p>{{ $portal->portal_deskripsi }}</p>
                </div>

                <div class="col-lg-6 mb-sm-30 text-center wow fadeInRight">
                    <div class="de-images">
                        <img class="di-small-2" src="{{ asset('front-assets/images-event/misc/3.jpg') }}" alt="" />
                        <img class="img-fluid wow fadeInRight" data-wow-delay=".25s" src="{{ asset('front-assets/images-event/misc/1.jpg') }}" alt="" />
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- section close -->

    <!-- section begin -->
    <section id="section-conference" aria-label="section-services-tab" data-bgimage="url({{ asset('front-assets/images-event/bg/6.jpg') }}) top right no-repeat">
        <div class="wm wm-border light wow fadeInDown ">{{ strtolower(__('messages.conference')) }}</div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                    <h1>{{ __('messages.conferences') }}</h1>
                    <div class="separator"><span><i class="fa fa-square"></i></span></div>
                    <div class="spacer-single"></div>
                </div>

                <div class="col-md-12 wow fadeInUp">
                    <div class="de_tab tab_style_4 text-center">
                        <ul class="de_nav de_nav_dark">
                            <li data-link="#section-services-tab">
                                <h3>Past</h3>
                                <h4>Conference</h4>
                            </li>
                            <li class="active" data-link="#section-services-tab">
                                <h3>Present</h3>
                                <h4>Conference</h4>
                            </li>
                            <li data-link="#section-services-tab">
                                <h3>Future</h3>
                                <h4>Conference</h4>
                            </li>
                        </ul>

                        <div class="de_tab_content text-left">

                            <div id="tab1" class="tab_single_content">
                                @if (isset($abc['past']))
                                    <div class="row">
                                        @foreach ($abc['past'] as $item)
                                            <div class="col-md-6">
                                                <a href="{{ $item->slug }}">
                                                    <div class="schedule-listing">
                                                        <div class="schedule-item">
                                                            <div class="sc-date">
                                                                {{ date('d', strtotime($item['tanggal'])) }}<br><span>{{ date('M', strtotime($item['tanggal'])) }}</span>
                                                            </div>
                                                            <div class="sc-info">
                                                                <h3>{{ $item['nama'] }}</h3>
                                                                <p>{{ $item['conference_afiliasi'] }}</p>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <h6 class="text-center my-5 text-uppercase">{{ __('messages.no conference available') }}</h6>
                                @endif
                            </div>

                            <div id="tab2" class="tab_single_content">
                                @if (isset($abc['now']))
                                    <div class="row">
                                        @foreach ($abc['now'] as $item)
                                            <div class="col-md-6">
                                                <a href="{{ $item->slug }}">
                                                    <div class="schedule-listing">
                                                        <div class="schedule-item">
                                                            <div class="sc-date">
                                                                {{ date('d', strtotime($item['tanggal'])) }}<br><span>{{ date('M', strtotime($item['tanggal'])) }}</span>
                                                            </div>
                                                            <div class="sc-info">
                                                                <h3>{{ $item['nama'] }}</h3>
                                                                <p>{{ $item['conference_afiliasi'] }}</p>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <h6 class="text-center my-5 text-uppercase">{{ __('messages.no conference available') }}</h6>
                                @endif
                            </div>

                            <div id="tab3" class="tab_single_content">
                                @if (isset($abc['future']))
                                    <div class="row">
                                        @foreach ($abc['future'] as $item)
                                            <div class="col-md-6">
                                                <a href="{{ $item->slug }}">
                                                    <div class="schedule-listing">
                                                        <div class="schedule-item">
                                                            <div class="sc-date">
                                                                {{ date('d', strtotime($item['tanggal'])) }}<br><span>{{ date('M', strtotime($item['tanggal'])) }}</span>
                                                            </div>
                                                            <div class="sc-info">
                                                                <h3>{{ $item['nama'] }}</h3>
                                                                <p>{{ $item['conference_afiliasi'] }}</p>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <h6 class="text-center my-5 text-uppercase">{{ __('messages.no conference available') }}</h6>
                                @endif
                            </div>

                        </div>

                        <a href="{{ route('portal.conferences') }}" class="btn-custom text-white mt-5">{{ __('messages.see all conferences') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->

    @if (count($faq) > 0)
        <!-- section begin -->
        <section id="section-faq" data-bgcolor="#101010">
            <div class="wm wm-border dark wow fadeInDown">{{ strtolower(__('messages.faq')) }}</div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                        <h1 class="text-light">{{ __('messages.frequently asked questions') }}</h1>
                        <div class="separator"><span><i class="fa fa-square"></i></span></div>
                        <div class="spacer-single"></div>
                    </div>

                    <div class="col-md-8 offset-md-2 wow fadeInUp">
                        <div class="accordion" id="accordionExample">
                            @php $key = 0 @endphp
                            @foreach ($faq as $item)
                                <div class="card">
                                    <div class="card-header" id="heading{{ $item->id }}">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $item->id }}" aria-expanded="true" aria-controls="collapse{{ $item->id }}">
                                                <div class="faq text-uppercase">
                                                    {!! $item->pertanyaan !!}
                                                </div>
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="collapse{{ $item->id }}" class="collapse {{ $key == 0 ? 'show' : '' }}" aria-labelledby="heading{{ $item->id }}" data-parent="#accordionExample">
                                        <div class="card-body text-dark">
                                            <div class="faq">
                                                {!! $item->jawaban !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @php $key++ @endphp
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section close -->
    @endif
@endsection

@section('js')
    <!-- RS5.0 Core JS Files -->
    <script src="{{ asset('front-assets/revolution/js/jquery.themepunch.tools.min.js?rev=5.0') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0') }}"></script>

    <!-- RS5.0 Extensions Files -->
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>

    <script>
        jQuery(document).ready(function() {
            // revolution slider
            jQuery("#slider-revolution").revolution({
                sliderType: "standard",
                sliderLayout: "fullwidth",
                delay: 5000,
                navigation: {
                    arrows: {
                        enable: true
                    },
                    bullets: {
                        enable: false,
                        style: 'hermes'
                    },

                },
                parallax: {
                    type: "mouse",
                    origo: "slidercenter",
                    speed: 2000,
                    levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
                },
                spinner: "off",
                gridwidth: 1140,
                gridheight: 700,
                disableProgressBar: "on"
            });
        });
    </script>
@endsection
