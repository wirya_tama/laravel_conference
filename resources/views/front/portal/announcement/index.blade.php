@extends('front.layouts.app')

@section('content')
    <!-- parallax section -->
    <section id="section-hero" class="text-light" data-bgimage="url({{ asset('front-assets/images-event/bg/10.jpg') }}) fixed top center"  data-stellar-background-ratio=".2">
        <div class="wm wm-border dark wow fadeInDown">latest</div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-2 mt40 mb40">
                        Announcements
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->

    <section id="section-news" data-bgimage="url({{ asset('front-assets/images-event/bg/3.png') }}) fixed center no-repeat">
        <div class="container">
            <div class="row justify-content-center">
                @if (count($pengumuman) > 0)
                    @foreach ($pengumuman as $item)
                        <div class="col-lg-4 col-md-6 mb30">
                            <div class="bloglist item">
                                <div class="post-content">
                                    <div class="picframe rounded mb30">
                                        <a class="pf-click" href="{{ $item->id }}">
                                            <span class="overlay-v">
                                            </span>
                                            <img src="{{ asset('images/pengumuman/'. $item->gambar) }}" alt="{{ $item->judul }}">
                                        </a>
                                    </div>
                                    <div class="post-text">
                                        <div class="post-info">
                                            <span class="post-date text-dark">{{ date('d/m/Y', strtotime($item->tanggal)) }}</span>
                                        </div>
                                        <a href="{{ route('portal.announcements.show') }}"><h3 class="text-dark">{{ $item->judul }}</h3></a>
                                            <p class="text-black-50">{{ $item->deskripsi }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <h6 class="text-center my-5 text-uppercase">Tidak Ada Pengumuman</h6>
                @endif
            </div>
        </div>
    </section>

    <div class="height1 gradient-to-right"></div>
@endsection
