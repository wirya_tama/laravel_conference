@extends('front.layouts.app')

@section('content')
    <!-- parallax section -->
    <section id="section-hero" class="text-light" data-bgimage="url({{ asset('front-assets/images-event/bg/10.jpg') }}) fixed top center"  data-stellar-background-ratio=".2">
        <div class="wm wm-border dark wow fadeInDown">announcement</div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h1 class="title-2 mt40 mb10">
                        {{ $pengumuman->pengumuman_judul }}
                    </h1>
                    <span>{{ date('d/m/Y', strtotime($pengumuman->created_at)) }}</span>
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->

    <!-- section begin -->
    <section data-bgimage="url({{ asset('front-assets/images-event/bg/3.png') }}) fixed top center">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="blog-read">
                        <div class="image-with-caption">
                            <img src="{{ asset('images/pengumuman/'. $pengumuman->pengumuman_gambar) }}" alt="{{ $pengumuman->judul }}" class="img-fluid rounded mb30">
                        </div>

                        <div class="post-text">
                            <p>{{ $pengumuman->pengumuman_deskripsi }}</p>
                        </div>
                    </div>
                    <div class="spacer-single"></div>
                </div>
                @include('front.portal.sidebar')
            </div>
        </div>
    </section>

    <div class="height1 gradient-to-right"></div>
@endsection
