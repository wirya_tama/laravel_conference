@extends('front.layouts.app')

@section('content')
    <!-- parallax section -->
    <section id="section-hero" class="text-light" data-bgimage="url({{ asset('front-assets/images-event/bg/10.jpg') }}) fixed top center"  data-stellar-background-ratio=".2">
        <div class="wm wm-border dark wow fadeInDown">{{ strtolower(__('messages.conference')) }}</div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-2 mt40 mb40">
                        {{ __('messages.conferences') }}
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->

    <!-- section begin -->
    <section data-bgimage="url({{ asset('front-assets/images-event/bg/3.png') }}) fixed top center">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="de_tab tab_style_4 text-center">
                        <ul class="de_nav de_nav_dark">
                            <li data-link="#section-services-tab">
                                <h3>Past</h3>
                                <h4>Conference</h4>
                            </li>
                            <li class="active" data-link="#section-services-tab">
                                <h3>Present</h3>
                                <h4>Conference</h4>
                            </li>
                            <li data-link="#section-services-tab">
                                <h3>Future</h3>
                                <h4>Conference</h4>
                            </li>
                        </ul>

                        <div class="de_tab_content text-left">
                            <div id="tab1" class="tab_single_content">
                                @if (isset($abc['past']))
                                    @foreach ($abc['past'] as $item)
                                        <a href="{{ $item->slug }}">
                                            <div class="schedule-listing">
                                                <div class="schedule-item">
                                                    <div class="sc-date">
                                                        {{ date('d', strtotime($item['tanggal'])) }}<br><span>{{ date('M', strtotime($item['tanggal'])) }}</span>
                                                    </div>
                                                    <div class="sc-info">
                                                        <h3>{{ $item['nama'] }}</h3>
                                                        <p>{{ $item['conference_afiliasi'] }}</p>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </a>
                                    @endforeach
                                @else
                                    <h6 class="text-center my-5 text-uppercase">{{ __('messages.no conference available') }}</h6>
                                @endif
                            </div>

                            <div id="tab2" class="tab_single_content">
                                @if (isset($abc['now']))
                                    @foreach ($abc['now'] as $item)
                                        <a href="{{ $item->slug }}">
                                            <div class="schedule-listing">
                                                <div class="schedule-item">
                                                    <div class="sc-date">
                                                        {{ date('d', strtotime($item['tanggal'])) }}<br><span>{{ date('M', strtotime($item['tanggal'])) }}</span>
                                                    </div>
                                                    <div class="sc-info">
                                                        <h3>{{ $item['nama'] }}</h3>
                                                        <p>{{ $item['conference_afiliasi'] }}</p>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </a>
                                    @endforeach
                                @else
                                    <h6 class="text-center my-5 text-uppercase">{{ __('messages.no conference available') }}</h6>
                                @endif
                            </div>

                            <div id="tab3" class="tab_single_content">
                                @if (isset($abc['future']))
                                    @foreach ($abc['future'] as $item)
                                        <a href="{{ $item->slug }}">
                                            <div class="schedule-listing">
                                                <div class="schedule-item">
                                                    <div class="sc-date">
                                                        {{ date('d', strtotime($item['tanggal'])) }}<br><span>{{ date('M', strtotime($item['tanggal'])) }}</span>
                                                    </div>
                                                    <div class="sc-info">
                                                        <h3>{{ $item['nama'] }}</h3>
                                                        <p>{{ $item['conference_afiliasi'] }}</p>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </a>
                                    @endforeach
                                @else
                                    <h6 class="text-center my-5 text-uppercase">{{ __('messages.no conference available') }}</h6>
                                @endif
                            </div>

                        </div>

                    </div>

                    <div class="spacer-single"></div>

                </div>

                @include('front.portal.sidebar')

            </div>
        </div>
    </section>

    <div class="height1 gradient-to-right"></div>
@endsection
