<footer class="style-2">
    <div class="container">
        <div class="row align-items-middle">
            <div class="col-md-3">
                <img class="logo" src="{{ asset('images/logo.png') }}" width="70px" alt=""><br>
            </div>

            <div class="col-md-6">
                &copy; Copyright 2020
            </div>

            <div class="col-md-3 text-right">
                <div class="social-icons">
                    <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                    <a href="#"><i class="fa fa-twitter fa-lg"></i></a>
                    <a href="#"><i class="fa fa-rss fa-lg"></i></a>
                    <a href="#"><i class="fa fa-google-plus fa-lg"></i></a>
                    <a href="#"><i class="fa fa-skype fa-lg"></i></a>
                    <a href="#"><i class="fa fa-dribbble fa-lg"></i></a>
                </div>
            </div>
        </div>
    </div>


    <a href="#" id="back-to-top" class="custom-1"></a>
</footer>
