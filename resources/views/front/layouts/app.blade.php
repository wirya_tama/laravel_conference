<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>@yield('title', 'Conference') - Warmadewa Conference System</title>
    <link rel="apple-touch-icon" href="{{ asset('images/portal/' . $logo) }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/portal/' . $logo) }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Warmadewa Conference System">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="PT. ARCOM Bali">


    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('front-assets/css/bootstrap.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/css/jpreloader.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/css/animate.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/css/plugin.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/css/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/css/owl.theme.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/css/owl.transitions.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/css/magnific-popup.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/css/jquery.countdown.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/css/style.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ asset('front-assets/css/twentytwenty.css') }}" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="{{ asset('front-assets/css/bg.css') }}" type="text/css">

    <!-- color scheme -->
	<link rel="stylesheet" href="{{ asset('front-assets/css/colors/blue-2.css') }}" type="text/css" id="colors">
    <link rel="stylesheet" href="{{ asset('front-assets/css/color.css') }}" type="text/css">

    <!-- load fonts -->
    <link rel="stylesheet" href="{{ asset('front-assets/fonts/font-awesome/css/font-awesome.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/fonts/elegant_font/HTML_CSS/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/fonts/et-line-font/style.css') }}" type="text/css">

	<!-- custom font -->
	<link rel="stylesheet" href="{{ asset('front-assets/css/font-style.css') }}" type="text/css">

    {{-- flag icons --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.5.0/css/flag-icon.min.css" integrity="sha512-Cv93isQdFwaKBV+Z4X8kaVBYWHST58Xb/jVOcV9aRsGSArZsgAnFIhMpDoMDcFNoUtday1hdjn0nGp3+KZyyFw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    @yield('css')
</head>

<body id="homepage" class="de_light">

    <div id="wrapper">

        <!-- header -->
        @include('front.layouts.header')


        <!-- content begin -->
        <div id="content" class="no-bottom no-top">

            <!-- content -->
			@yield('content')

            <!-- footer -->
            @include('front.layouts.footer')

        </div>
    </div>

    <!-- sidebar -->
    @yield('sidebar')

    {{-- @guest
        @include('front.register')
    @endguest --}}

    <!-- Modal -->
    <div class="modal fade" id="lang" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Change Language/Ganti Bahasa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm text-center">
                            <a href="{{ route('lang', 'en') }}" class="btn btn-lg display-1" title="English">
                                <h1 class="display-1 ">
                                    <span class="img-thumbnail flag-icon flag-icon-us"></span>
                                </h1>
                            </a>
                        </div>
                        <div class="col-sm text-center">
                            <a href="{{ route('lang', 'id') }}" class="btn btn-lg display-1" title="Bahasa Indonesia">
                                <h1 class="display-1 ">
                                    <span class="img-thumbnail flag-icon flag-icon-id"></span>
                                </h1>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Javascript Files
    ================================================== -->
    <script src="{{ asset('front-assets/js/jquery.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="{{ asset('front-assets/js/jpreLoader.js') }}"></script>
    <script src="{{ asset('front-assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('front-assets/js/jquery.isotope.min.js') }}"></script>
    <script src="{{ asset('front-assets/js/easing.js') }}"></script>
    <script src="{{ asset('front-assets/js/jquery.flexslider-min.js') }}"></script>
    <script src="{{ asset('front-assets/js/jquery.scrollto.js') }}"></script>
    <script src="{{ asset('front-assets/js/owl.carousel.js') }}"></script>
    <script src="{{ asset('front-assets/js/jquery.countTo.js') }}"></script>
    <script src="{{ asset('front-assets/js/video.resize.js') }}"></script>
    <script src="{{ asset('front-assets/js/validation.js') }}"></script>
    <script src="{{ asset('front-assets/js/wow.min.js') }}"></script>
    <script src="{{ asset('front-assets/js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('front-assets/js/jquery.stellar.min.js') }}"></script>
	<script src="{{ asset('front-assets/js/enquire.min.js') }}"></script>
    <script src="{{ asset('front-assets/js/designesia.js') }}"></script>
	<script src="{{ asset('front-assets/js/jquery.event.move.js') }}"></script>
	<script src="{{ asset('front-assets/js/jquery.plugin.js') }}"></script>
    <script src="{{ asset('front-assets/js/jquery.countdown.js') }}"></script>
    <script src="{{ asset('front-assets/js/countdown-custom.js') }}"></script>
    <script src="{{ asset('front-assets/js/jquery.twentytwenty.js') }}"></script>

	<script>
        $(window).on("load", function () {
            $(".twentytwenty-container[data-orientation!='vertical']").twentytwenty({default_offset_pct: 0.7});
            $(".twentytwenty-container[data-orientation='vertical']").twentytwenty({default_offset_pct: 0.3, orientation: 'vertical'});
        });
    </script>

    @yield('js')

</body>
</html>
