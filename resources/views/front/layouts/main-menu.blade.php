@if (request()->is('/'))
    <li><a href="#section-hero">{{ __('messages.home') }}<span></span></a></li>
    <li><a href="#section-conference">{{ __('messages.conference') }}<span></span></a></li>
    {{-- <li><a href="#section-announcement">Announcement<span></span></a></li> --}}
    <li><a href="#section-faq">{{ __('messages.faq') }}<span></span></a></li>
@elseif (request()->is('conferences') || request()->is('announcements') || request()->is('announcements/*'))
    <li><a href="{{ route('portal.home') }}">{{ __('messages.home') }}<span></span></a></li>
    <li><a href="{{ route('portal.conferences') }}">{{ __('messages.conference') }}<span></span></a></li>
    {{-- <li><a href="{{ route('portal.announcements.index') }}">Announcement<span></span></a></li> --}}
    {{-- <li><a href="{{ url('#section-faq') }}">FAQ<span></span></a></li> --}}
@else
    <li><a href="{{ route('conference.home', request()->segment('1')) }}">{{ __('messages.home') }}<span></span></a></li>
    <li><a href="{{ route('conference.speaker', request()->segment('1')) }}">{{ __('messages.speaker') }}<span></span></a></li>
    <li><a href="{{ route('conference.schedule', request()->segment('1')) }}">{{ __('messages.schedule') }}<span></span></a></li>
    <li><a href="{{ route('conference.announcements.index', request()->segment('1')) }}">{{ __('messages.announcement') }}<span></span></a></li>
    <li><a href="{{ route('conference.proceeding.index', request()->segment('1')) }}">{{ __('messages.proceeding') }}<span></span></a></li>
    <li><a href="{{ route('conference.download', request()->segment('1')) }}">{{ __('messages.download') }}<span></span></a></li>
    {{-- <li><a href="{{ route('conference.gallery', request()->segment('1')) }}">Gallery<span></span></a></li> --}}
    {{-- <li><a href="{{ route('conference.page.index', request()->segment('1')) }}">{{ __('messages.more info') }}<span></span></a></li> --}}
@endif

@auth
    <li><a href="{{ url('panel') }}"><i class="fa fa-user" aria-hidden="true"></i> {{ Illuminate\Support\Str::words(Auth::user()->name, 2, '') }}</a></li>
@endauth

@guest
    <li><a href="{{ route('login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> Login<span></span></a></li>
@endguest

<li><a href="#lang" data-toggle="modal"><i class="flag-icon
    @if (App::isLocale('en'))
        flag-icon-us
    @else
        flag-icon-id
    @endif
"></i></a></li>
