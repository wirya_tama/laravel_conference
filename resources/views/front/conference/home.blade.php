@extends('front.layouts.app')

@section('title')
    {{ $data->conference_nama }}
@endsection

@section('css')
    <!-- Timeline -->
    <link rel="stylesheet" href="{{ asset('front-assets/css/timeline.css') }}" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css">

    <!-- RS5.0 Stylesheet -->
    <link rel="stylesheet" href="{{ asset('front-assets/revolution/css/settings.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/revolution/css/layers.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/revolution/css/navigation.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('front-assets/css/rev-settings.css') }}" type="text/css">
@endsection

@section('content')
    <!-- parallax section -->
    <section id="section-hero" class="fullwidthbanner-container" aria-label="section-slider" data-bgcolor="#101010">
        <div id="slider-revolution">
            <ul>
                <li data-transition="random-premium" data-slotamount="10" data-masterspeed="0" data-thumb="">
                    <!--  BACKGROUND IMAGE -->
                    <img alt="" class="w-100" height="400" style="object-fit: cover" data-bgparallax="0" src="{{ asset('images/conference/banner/'.$data->conference_banner) }}">
                </li>
            </ul>

            <div style="" class="tp-static-layers">
                @if ($data->conference_nama_status == 1)
                    <div
                        class="text-center tp-caption size-20 font-weight-bold text-white tp-static-layer"
                        data-x="['center','center','center','center']"
                        data-hoffset="['0']"
                        data-y="['top','top','top','top']"
                        data-voffset="['185']"
                        data-lineheight="['90','90','90','90']"
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-type="text"
                        data-responsive_offset="on"
                        data-startslide="0"
                        data-endslide="3"
                        data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                        data-textAlign="['center','center','center','center']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                    >
                        <span class="id-color-2">{{ Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y') }}</span>
                    </div>

                    <div
                        class="text-center tp-caption size-48 font-weight-bold text-white tp-static-layer"
                        data-x="['center','center','center','center']"
                        data-hoffset="['0']"
                        data-y="['center','center','center','center']"
                        data-lineheight="['50','0','50','0']"
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-type="text"
                        data-responsive_offset="on"
                        data-startslide="0"
                        data-endslide="3"
                        data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                        data-textAlign="['center','center','center','center']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                    >
                        @php
                            $datas = explode(' ', $data->conference_nama);
                            $judul = '';
                            $br = 0;

                            for ($i=0; $i < count($datas); $i++) {
                                $judul .= $datas[$i].' ';
                                $br++;

                                if ($br == 3) {
                                    $judul .= '<br>';
                                    $br = 0;
                                }
                            }
                        @endphp
                        {!! $judul !!}
                    </div>

                    <div
                        class="text-center tp-caption size-20 text-white tp-static-layer"
                        data-x="['center','center','center','center']"
                        data-hoffset="['0']"
                        data-y="['top','top','top','top']"
                        data-voffset="['430']"
                        data-lineheight="['90','90','90','90']"
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-type="text"
                        data-responsive_offset="on"
                        data-startslide="0"
                        data-endslide="3"
                        data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                        data-textAlign="['center','center','center','center']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"
                    >
                        <div class="spacer-single"></div>
                            <div class="d-lg-flex justify-content-center sm-hide">
                                <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-calendar-check-o id-color"></i>{{ $data->tanggal_awal == $data->tanggal_akhir ? Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y') : Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y').' - '.Carbon\Carbon::parse($data->tanggal_akhir)->formatLocalized('%d %B %Y') }}</div>
                                <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-user id-color"></i>{{ count($speaker) }} {{ count($speaker) == 1 ? 'Speaker' : 'Speakers' }}</div>
                            </div>
                    </div>

                @endif

                <div
                    class="tp-caption tp-static-layer"
                    data-x="['center','center','center','center']"
                    data-hoffset="['0']"
                    data-y="['top','top','top','top']"
                    data-voffset="['506']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-type="button"
                    data-actions='[{"event":"click","offset":"px","delay":""}]'
                    data-responsive_offset="on"
                    data-responsive="off"
                    data-startslide="0"
                    data-endslide="3"
                    data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(255, 255, 255, 0.15);bw:2px 2px 2px 2px;"}]'
                    data-textAlign="['center','center','center','center']"
                    data-paddingtop="[12,12,12,12]"
                    data-paddingright="[20,20,20,20]"
                    data-paddingbottom="[12,12,12,12]"
                    data-paddingleft="[20,20,20,20]"
                >
                    @if ($isAuthor == true)
                        <a href="{{ route('conference.register', request()->segment('1')) }}" class="btn-custom text-white">Register</a>
                    @elseif (Auth::guest())
                        <a href="{{ route('login') }}" class="btn-custom text-white">
                            <i class="fa fa-sign-in" aria-hidden="true"></i> Submission
                        </a>
                    @endif
                </div>

                <div
                    class="tp-caption tp-static-layer"
                    data-x="['center','center','center','center']"
                    data-hoffset="['0']"
                    data-y="['top','top','top','top']"
                    data-voffset="['506']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-type="button"
                    data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                    data-responsive_offset="on"
                    data-responsive="off"
                    data-startslide="0"
                    data-endslide="3"
                    data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(255, 255, 255, 0.15);bw:2px 2px 2px 2px;"}]'
                    data-textAlign="['center','center','center','center']"
                    data-paddingtop="[12,12,12,12]"
                    data-paddingright="[20,20,20,20]"
                    data-paddingbottom="[12,12,12,12]"
                    data-paddingleft="[20,20,20,20]"
                >
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->

    <!-- section begin -->
    {{-- Source: https://www.bootdey.com/snippets/view/bs4-timeline-widget --}}
    @if ($data->tentang != NULL)
        <section id="section-about" aria-label="section-about-tab" data-bgimage="url({{ asset('front-assets/images-event/bg/6.jpg') }}) top right no-repeat">
            <div class="wm wm-border light wow fadeInDown ">about</div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                        <h1>About Conference</h1>
                        <div class="separator"><span><i class="fa fa-square"></i></span></div>
                        <div class="spacer-single"></div>
                    </div>

                    <style>
                        .content {
                            height:90px;
                            overflow:hidden;
                            margin-bottom:10px;
                        }
                        
                        .content.visible {
                            height:auto;
                            overflow:visible;
                        }
                    </style>

                    {{-- Teks Tentang --}}
                    @php
                        $count = (strlen($data->conference_tentang));

                        if ($count % 2 != 0) {
                            $divide = $count/2 + 0.5;
                        } else {
                            $divide = $count/2;
                        }
                        
                        $tentang = (str_split($data->conference_tentang, $divide));
                    @endphp
                    <div class="col-md-6 d-none d-md-block">
                        <div class="content">
                            <div class="text-justify" style="font-size: 16px">
                                {{ $tentang[0] ?? "" }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 d-none d-md-block">
                        <div class="content">
                            <div class="text-justify" style="font-size: 16px">
                                {{ $tentang[1] ?? "" }}
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-12 d-block d-md-none">
                        <div class="content">
                            <div class="text-justify" style="font-size: 16px">
                                {{ $data->conference_tentang }}
                            </div>
                        </div>
                    </div>
                </div>
                {{-- /Teks Tentang --}}
                
                <div class="text-center">
                    <button class="show_hide btn btn-primary text-white font-weight-bold" data-content="toggle-text" onclick="read_toggle(1)">Read More</button>
                </div>
            </div>
        </section>
    @endif
    <!-- section close -->

    <!-- section begin -->
    <section id="section-features" class="text-light" data-bgcolor="#101010">
        <div class="container">
            <div class="row">

                <div class="col-lg-6 wow fadeIn" data-wow-delay="0s">
                    <div class="box-number square">
                       <i class="bg-color hover-color-2 fa fa-calendar text-light"></i>
                        <div class="text">
                            <h3><span>Date</span></h3>
                            <p>{{ $data->tanggal_awal == $data->tanggal_akhir ? Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y') : Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y').' - '.Carbon\Carbon::parse($data->tanggal_akhir)->formatLocalized('%d %B %Y') }}</p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 wow fadeIn mt-5 mt-lg-0" data-wow-delay=".25s">
                    @if ($data->conference_google_maps != NULL)
                        <a href="{{ $data->conference_google_maps }}" target="_blank">
                            <div class="box-number square">
                                <i class="bg-color hover-color-2 fa fa-map-marker text-light"></i>
                                <div class="text">
                                    <h3><span>Place</span></h3>
                                    <p>{{ $data->conference_lokasi ?? '-' }}</p>
                                </div>
                            </div>
                        </a>
                    @else
                        <div class="box-number square">
                            <i class="bg-color hover-color-2 fa fa-map-marker text-light"></i>
                            <div class="text">
                                <h3><span>Place</span></h3>
                                <p>{{ $data->conference_lokasi ?? '-' }}</p>
                            </div>
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </section>
    <!-- section close -->

    <!-- section begin -->
    <section id="section-speakers" class="text-light" data-bgimage="url('https://images.unsplash.com/photo-1538449327350-43b4fcfd35ac?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1353&q=80') fixed top center" data-stellar-background-ratio=".2">
        <div class="wm wm-border dark wow fadeInDown">speakers</div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 text-center wow fadeInUp">
                    <h1>The Speakers</h1>
                    <div class="separator"><span><i class="fa fa-square"></i></span></div>
                    <div class="spacer-single"></div>
                </div>

                <div class="clearfix"></div>

                @if (count($speaker) > 0)
                    @foreach ($speaker as $item)
                        <div class="col-xl-3 col-lg-4 mb30 wow fadeInUp">
                            <!-- team member -->
                            <a href="#speakerModal" class="speaker" data-toggle="modal" data-nama="{{ $item->ks_nama }}" data-afiliasi="{{ $item->ks_afiliasi }}" data-deskripsi="{{ $item->ks_deskripsi }}" data-foto="{{ $item->ks_foto }}">
                                <div class="card-dark text-center">
                                    <div class="card-body">
                                        <div class="circular--portrait mb-4">
                                            <img src="{{ asset('images/keynote_speaker/'. $item->ks_foto) }}" alt="{{ $item->ks_nama }}">
                                        </div>
                                        <h3 class="pb-3 text-dark">{{ $item->ks_nama }}</h3>
                                        <h6 class="mb-2 text-primary border-secondary border-top border-bottom py-3">{{ $item->ks_afiliasi }}</h6>
                                    </div>
                                </div>
                            </a>
                            <!-- team close -->
                        </div>
                    @endforeach
                @else
                    <h6 class="text-center my-5 text-uppercase text-white">Tidak Ada Speaker</h6>
                @endif

                <div class="clearfix"></div>

            </div>
        </div>
    </section>
    <!-- section close -->

    <!-- section begin -->
    {{-- Source: https://www.bootdey.com/snippets/view/bs4-timeline-widget --}}
    <section id="section-schedule" aria-label="section-services-tab" data-bgimage="url({{ asset('front-assets/images-event/bg/6.jpg') }}) top right no-repeat">
        <div class="wm wm-border light wow fadeInDown ">{{ strtolower(__('messages.schedule')) }}</div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                    <h1>{{ __('messages.event schedule') }}</h1>
                    <div class="separator"><span><i class="fa fa-square"></i></span></div>
                    <div class="spacer-single"></div>
                </div>

                <div class="col-xl-10 col-12 mx-auto fadeInUp">
                    <div class="timeline">
                        <div class="date-title">
                            <span>{{ date('Y', strtotime($tanggal[0]->tanggal_awal)) == date('Y', strtotime($tanggal[count($tanggal) - 1]->tanggal_akhir)) ? date('Y', strtotime($tanggal[0]->tanggal_awal)) : date('Y', strtotime($tanggal[0]->tanggal_awal)).' - '.date('Y', strtotime($tanggal[count($tanggal) - 1]->tanggal_akhir)) }}</span>
                        </div>
                        <div class="row">
                            @foreach ($tanggal as $item)
                                @php
                                    if ($item->tanggal_awal == $item->tanggal_akhir) {
                                        $data_tanggal = '<p>'.date('d', strtotime($item->tanggal_awal)).'</p><small>'.date('M', strtotime($item->tanggal_awal)).'</small>';
                                    } else {
                                        $data_tanggal = '<p>'.date('d', strtotime($item->tanggal_awal)).'</p><small>'.date('M', strtotime($item->tanggal_awal)).'</small><br><small>-</small><p>'.date('d', strtotime($item->tanggal_akhir)).'</p><small>'.date('M', strtotime($item->tanggal_akhir)).'</small>';
                                    }
                                @endphp
                                <div class="col-sm-6 news-item {{ $loop->even ? 'right' : '' }}">
                                    <div class="news-content">
                                        <div class="date">
                                            {!! $data_tanggal !!}
                                        </div>
                                        <h2 class="news-title">{{ $item->judul }}</h2>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->

    <!-- section begin -->
    <section id="section-announcement" class="text-light" data-bgimage="url('https://images.unsplash.com/photo-1477281765962-ef34e8bb0967?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1532&q=80') fixed top center" data-stellar-background-ratio=".2">
        <div class="wm wm-border dark wow fadeInDown">{{ strtolower(__('messages.announcement')) }}</div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6 text-center wow fadeInUp">
                    <h1>{{ __('messages.announcements') }}</h1>
                    <div class="separator"><span><i class="fa fa-square"></i></span></div>
                    <div class="spacer-single"></div>
                </div>

                <div class="clearfix"></div>

                @if (count($pengumuman) > 0)
                    @foreach ($pengumuman as $item)
                        <div class="col-lg-4 col-md-6 mb30" style="background-size: cover;">
                            <div class="card">
                                <div class="bloglist item" style="background-size: cover;">
                                    <div class="post-content" style="background-size: cover;">
                                        <div class="picframe rounded mb30" style="background-size: cover;">
                                            @if ($item->gambar != 'blank.png')
                                                <a class="pf-click" href="{{ route('conference.announcements.show', [$data->conference_slug, $item->id] ) }}">
                                                    <span class="overlay-v">
                                                    </span>
                                                    <img src="{{ asset('images/pengumuman/'. $item->gambar) }}" alt="{{ $item->judul }}">
                                                </a>
                                            @endif
                                        </div>
                                        <div class="post-text px-3" style="background-size: cover;">
                                            <div class="post-info" style="background-size: cover;">
                                                <span class="post-date text-dark">{{ date('d/m/Y', strtotime($item->tanggal)) }}</span>
                                            </div>
                                            <a href="{{ route('conference.announcements.show', [$data->conference_slug, $item->id] ) }}"><h3 class="text-dark">{{ $item->judul }}</h3></a>
                                            @php
                                                $deskripsi = strip_tags($item->deskripsi);
                                            @endphp
                                            <p class="text-black-50">{{ (Str::words($deskripsi, 20)) }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @break($loop->index == 2)
                    @endforeach
                @else
                    <h6 class="text-center my-5 text-uppercase text-white">{{ __('messages.no announcement available') }}</h6>
                @endif

                @if (count($pengumuman) > 3)
                    <div class="col-md-12 mt-5 text-center">
                        <a href="#" class="btn-custom text-white scroll-to">{{ __('messages.see all announcements') }}</a>
                    </div>
                @endif

                <div class="clearfix"></div>

            </div>
        </div>
    </section>
    <!-- section close -->

    @if (count($organizer) > 0)
        <!-- section begin -->
        <section id="section-organizer" aria-label="section-services-tab" data-bgimage="url({{ asset('front-assets/images-event/bg/6.jpg') }}) top right no-repeat">
            <div class="wm wm-border light wow fadeInDown ">{{ strtolower(__('messages.organizer')) }}</div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 offset-md-3 text-center wow fadeInUp">
                        <h1>{{ __('messages.organized by') }}</h1>
                        <div class="separator"><span><i class="fa fa-square"></i></span></div>
                        <div class="spacer-single"></div>
                    </div>

                    <div class="container">
                        <div class="d-flex justify-content-center flex-wrap">
                            @foreach ($organizer as $item)
                                <a href="{{ $item->link }}" title="{{ $item->nama }}">
                                    <img class="img-fluid m-2" style="max-height: 100px" src="{{ asset('images/organizer/' . $item->gambar) }}" alt="{{ $item->nama }}">
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section close -->
    @endif

    <!-- Modal -->
    <div class="modal fade" id="speakerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content p-0">
                <div class="modal-body" data-bgcolor="#101010">
                    <div class="text-light row">
                        <div class="col-md-4" id="foto">
                            <img src="{{ asset('front-assets/images-event/team/1.jpg') }}" class="img-responsive" />
                        </div>
                        <div class="col-md-8">
                            <h2 class="mb-0" id="nama">John Smith</h2>
                            <p class="text-primary font-weight-bold" id="afiliasi">Expert Designer</p>
                        </div>
                        <div class="col-10 mx-auto small-border mx-0" style="background-size: auto; margin: 50px 0;"></div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                        </div>
                        <div class="text-justify">
                            <p id="deskripsi">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- RS5.0 Core JS Files -->
    <script src="{{ asset('front-assets/revolution/js/jquery.themepunch.tools.min.js?rev=5.0') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0') }}"></script>

    <!-- RS5.0 Extensions Files -->
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ asset('front-assets/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>

    <script>
        function read_toggle(data) {     
            $(".show_hide").text(data == 0 ? 'Read More' : 'Read Less');
            $(".show_hide").attr('onclick', data == 0 ? 'read_toggle(1)' : 'read_toggle(0)');
            $(".content").toggleClass("visible");
        };
    </script>

    <script>
        jQuery(document).ready(function() {
            // revolution slider
            jQuery("#slider-revolution").revolution({
                sliderType: "standard",
                sliderLayout: "fullwidth",
                delay: 5000,
                navigation: {
                    arrows: {
                        enable: false
                    },
                    bullets: {
                        enable: false,
                        style: 'hermes'
                    },

                },
                parallax: {
                    type: "mouse",
                    origo: "slidercenter",
                    speed: 2000,
                    levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
                },
                spinner: "off",
                gridwidth: 1140,
                gridheight: 700,
                disableProgressBar: "on"
            });

            // Modal Speaker
            jQuery(".speaker").mouseenter(function() {
                jQuery('#foto').html(`<img src="{{ url('images/keynote_speaker') }}/`+ jQuery(this).attr('data-foto') +`" class="img-responsive" />`);
                jQuery('#nama').text(jQuery(this).attr('data-nama'));
                jQuery('#afiliasi').text(jQuery(this).attr('data-afiliasi'));
                jQuery('#deskripsi').text(jQuery(this).attr('data-deskripsi'));
            });
        });
    </script>
@endsection

@section('sidebar')
    <div id="de-extra-wrap" class="de_light overflow-auto">
        <span id="b-menu-close">
            <span></span>
            <span></span>
        </span>
        <div class="de-extra-content">
            <h3>{{ __('messages.information') }}</h3>
            <img src="{{ asset('images/conference/pamflet/'.$data->conference_pamflet) }}" alt="{{ $data->conference_nama }}" class="img-fluid">

            <div class="spacer-single"></div>

            <h3>{{ __('messages.where when') }}</h3>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-calendar-check-o id-color"></i>{{ $data->tanggal_awal == $data->tanggal_akhir ? Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y') : Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y').' - '.Carbon\Carbon::parse($data->tanggal_akhir)->formatLocalized('%d %B %Y') }}</div>
            <div class="h6 padding10 pt0 pb0">
                @if ($data->conference_google_maps != NULL)
                    <a href="{{ $data->conference_google_maps }}" target="_blank">
                        <i class="i_h3 fa fa-map-marker id-color"></i>{{ $data->conference_lokasi ?? '-' }}
                    </a>
                @else
                    <i class="i_h3 fa fa-map-marker id-color"></i>{{ $data->conference_lokasi ?? '-' }}
                @endif
            </div>

            <div class="spacer-single"></div>

            <h3>{{ __('messages.contact person') }}</h3>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-user id-color"></i>{{ $data->conference_kontak_nama1 }}</div>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-phone id-color"></i>{{ $data->conference_kontak_hp1 }}</div>
        </div>
    </div>
    <div id="de-overlay"></div>
@endsection
