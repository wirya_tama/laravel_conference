@extends('front.layouts.app')

@section('title')
    {{ __('messages.speaker') }}
@endsection

@section('content')
    <!-- parallax section -->
    <section id="section-hero" class="text-light" data-bgimage="url({{ asset('front-assets/images-event/bg/10.jpg') }}) fixed top center"  data-stellar-background-ratio=".2">
        <div class="wm wm-border dark wow fadeInDown">{{ strtolower(__('messages.speaker')) }}</div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-2 mt40 mb40">
                        {{ __('messages.speakers') }}
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->

    <!-- section begin -->
    <section data-bgimage="url({{ asset('front-assets/images-event/bg/3.png') }}) fixed top center">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    @if (count($speaker) > 0)
                        @foreach ($speaker as $item)
                            <div class="mb30 wow fadeInUp">
                                <!-- team member -->
                                <a href="#speakerModal" class="speaker" data-toggle="modal" data-nama="{{ $item->ks_nama }}" data-afiliasi="{{ $item->ks_afiliasi }}" data-deskripsi="{{ $item->ks_deskripsi }}" data-foto="{{ $item->ks_foto }}">
                                    <div class="row">
                                        <div class="col-4">
                                            <img src="{{ asset('images/keynote_speaker/'. $item->ks_foto) }}" class="img-responsive" alt="{{ $item->ks_nama }}" />
                                        </div>
                                        <div class="col-8">
                                            <h2 class="mb-0">{{ $item->ks_nama }}</h2>
                                            <p class="text-primary font-weight-bold">{{ $item->ks_afiliasi }}</p>
                                            <p>{{ strlen($item->ks_deskripsi) > 15 ? substr($item->ks_deskripsi, 0, 15).'...' : $item->ks_deskripsi }}</p>
                                        </div>
                                    </div>
                                </a>
                                <!-- team close -->
                            </div>

                            <div class="spacer-single"></div>
                        @endforeach
                    @else
                        <h6 class="text-center my-5 text-uppercase">Tidak Ada Speaker</h6>
                    @endif
                </div>

                @include('front.portal.sidebar')
            </div>
        </div>
    </section>

    <div class="height1 gradient-to-right"></div>

    <!-- Modal -->
    <div class="modal fade" id="speakerModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body" data-bgcolor="#101010">
                    <div class="text-light row">
                        <div class="col-md-4" id="foto">
                            <img src="{{ asset('front-assets/images-event/team/1.jpg') }}" class="img-responsive" />
                        </div>
                        <div class="col-md-8">
                            <h2 class="mb-0" id="nama">John Smith</h2>
                            <p class="text-primary font-weight-bold" id="afiliasi">Expert Designer</p>
                        </div>
                        <div class="col-10 mx-auto small-border mx-0" style="background-size: auto; margin: 50px 0;"></div>
                    </div>
                    <div class="container-fluid">
                        <div class="row">
                        </div>
                        <div class="text-justify">
                            <p id="deskripsi">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('sidebar')
    <div id="de-extra-wrap" class="de_light">
        <span id="b-menu-close">
            <span></span>
            <span></span>
        </span>
        <div class="de-extra-content">
            <h3>{{ __('messages.information') }}</h3>
            <img src="{{ asset('images/conference/pamflet/'.$data->conference_pamflet) }}" alt="{{ $data->conference_nama }}" class="img-fluid">

            <div class="spacer-single"></div>

            <h3>{{ __('messages.where when') }}</h3>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-calendar-check-o id-color"></i>{{ $data->tanggal_awal == $data->tanggal_akhir ? Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y') : Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y').' - '.Carbon\Carbon::parse($data->tanggal_akhir)->formatLocalized('%d %B %Y') }}</div>
            <div class="h6 padding10 pt0 pb0">
                @if ($data->conference_google_maps != NULL)
                    <a href="{{ $data->conference_google_maps }}" target="_blank">
                        <i class="i_h3 fa fa-map-marker id-color"></i>{{ $data->conference_lokasi ?? '-' }}
                    </a>
                @else
                    <i class="i_h3 fa fa-map-marker id-color"></i>{{ $data->conference_lokasi ?? '-' }}
                @endif
            </div>

            <div class="spacer-single"></div>

            <h3>{{ __('messages.contact person') }}</h3>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-user id-color"></i>{{ $data->conference_kontak_nama1 }}</div>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-phone id-color"></i>{{ $data->conference_kontak_hp1 }}</div>
        </div>
    </div>
    <div id="de-overlay"></div>
@endsection

@section('js')
    <script>
        // Modal Speaker
        jQuery(".speaker").mouseenter(function() {
            jQuery('#foto').html(`<img src="{{ url('images/keynote_speaker') }}/`+ jQuery(this).attr('data-foto') +`" class="img-responsive" />`);
            jQuery('#nama').text(jQuery(this).attr('data-nama'));
            jQuery('#afiliasi').text(jQuery(this).attr('data-afiliasi'));
            jQuery('#deskripsi').text(jQuery(this).attr('data-deskripsi'));
        });
    </script>
@endsection
