@extends('front.layouts.app')

@section('title')
    Schedule
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('front-assets/css/timeline.css') }}" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css">
@endsection

@section('content')
    <!-- parallax section -->
    <section id="section-hero" class="text-light" data-bgimage="url({{ asset('front-assets/images-event/bg/10.jpg') }}) fixed top center"  data-stellar-background-ratio=".2">
        <div class="wm wm-border dark wow fadeInDown">{{ strtolower(__('messages.schedule')) }}</div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-2 mt40 mb40">
                        {{ __('messages.event schedule') }}
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->

    <!-- section begin -->
    <section data-bgimage="url({{ asset('front-assets/images-event/bg/3.png') }}) fixed top center">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="fadeInUp">
                        <div class="timeline">
                            <div class="date-title">
                                <span>{{ date('Y', strtotime($tanggal[0]->tanggal_awal)) == date('Y', strtotime($tanggal[count($tanggal) - 1]->tanggal_akhir)) ? date('Y', strtotime($tanggal[0]->tanggal_awal)) : date('Y', strtotime($tanggal[0]->tanggal_awal)).' - '.date('Y', strtotime($tanggal[count($tanggal) - 1]->tanggal_akhir)) }}</span>
                            </div>
                            <div class="row">
                                @foreach ($tanggal as $item)
                                    @php
                                        if ($item->tanggal_awal == $item->tanggal_akhir) {
                                            $data_tanggal = '<p>'.date('d', strtotime($item->tanggal_awal)).'</p><small>'.date('M', strtotime($item->tanggal_awal)).'</small>';
                                        } else {
                                            $data_tanggal = '<p>'.date('d', strtotime($item->tanggal_awal)).'</p><small>'.date('M', strtotime($item->tanggal_awal)).'</small><br><small>-</small><p>'.date('d', strtotime($item->tanggal_akhir)).'</p><small>'.date('M', strtotime($item->tanggal_akhir)).'</small>';
                                        }
                                    @endphp
                                    <div class="col-sm-6 news-item {{ $loop->even ? 'right' : '' }}">
                                        <div class="news-content">
                                            <div class="date">
                                                {!! $data_tanggal !!}
                                            </div>
                                            <h2 class="news-title">{{ $item->judul }}</h2>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                @include('front.portal.sidebar')

            </div>
        </div>
    </section>

    <div class="height1 gradient-to-right"></div>
@endsection

@section('sidebar')
    <div id="de-extra-wrap" class="de_light">
        <span id="b-menu-close">
            <span></span>
            <span></span>
        </span>
        <div class="de-extra-content">
            <h3>{{ __('messages.information') }}</h3>
            <img src="{{ asset('images/conference/pamflet/'.$data->conference_pamflet) }}" alt="{{ $data->conference_nama }}" class="img-fluid">

            <div class="spacer-single"></div>

            <h3>{{ __('messages.where when') }}</h3>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-calendar-check-o id-color"></i>{{ $data->tanggal_awal == $data->tanggal_akhir ? Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y') : Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y').' - '.Carbon\Carbon::parse($data->tanggal_akhir)->formatLocalized('%d %B %Y') }}</div>
            <div class="h6 padding10 pt0 pb0">
                @if ($data->conference_google_maps != NULL)
                    <a href="{{ $data->conference_google_maps }}" target="_blank">
                        <i class="i_h3 fa fa-map-marker id-color"></i>{{ $data->conference_lokasi ?? '-' }}
                    </a>
                @else
                    <i class="i_h3 fa fa-map-marker id-color"></i>{{ $data->conference_lokasi ?? '-' }}
                @endif
            </div>

            <div class="spacer-single"></div>

            <h3>{{ __('messages.contact person') }}</h3>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-user id-color"></i>{{ $data->conference_kontak_nama1 }}</div>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-phone id-color"></i>{{ $data->conference_kontak_hp1 }}</div>
        </div>
    </div>
    <div id="de-overlay"></div>
@endsection
