@extends('front.layouts.app')

@section('title', 'Gallery')

@section('content')
    <!-- parallax section -->
    <section id="section-hero" class="text-light" data-bgimage="url({{ asset('front-assets/images-event/bg/10.jpg') }}) fixed top center"  data-stellar-background-ratio=".2">
        <div class="wm wm-border dark wow fadeInDown">photos</div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-2 mt40 mb40">
                        Gallery
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->

    <section id="section-gallery" aria-label="section" data-bgimage="url({{ asset('front-assets/images-event/bg/3.png') }}) fixed center no-repeat" data-stellar-background-ratio=".2">
        <div id="gallery" class="gallery zoom-gallery full-gallery de-gallery wow fadeInUp" data-wow-delay=".3s">
            <div class="container">
                <div class="row">

                    <!-- gallery item -->
                    <div class="col-md-3 item residential">
                        <div class="picframe">
                            <a href="{{ asset('front-assets/images-event/portfolio/1.jpg') }}" title="View Details">
                                <span class="overlay">
                                    <span class="pf_text">
                                        <span class="project-name">View Image</span>
                                    </span>
                                </span>
                                <img src="{{ asset('front-assets/images-event/portfolio/1.jpg') }}" alt="" />
                            </a>
                        </div>
                    </div>
                    <!-- close gallery item -->

                    <!-- gallery item -->
                    <div class="col-md-3 item hospitaly">
                        <div class="picframe">
                            <a href="{{ asset('front-assets/images-event/portfolio/2.jpg') }}" title="View Details">
                                <span class="overlay">
                                    <span class="pf_text">
                                        <span class="project-name">View Image</span>
                                    </span>
                                </span>
                                <img src="{{ asset('front-assets/images-event/portfolio/2.jpg') }}" alt="" />
                            </a>
                        </div>
                    </div>
                    <!-- close gallery item -->

                    <!-- gallery item -->
                    <div class="col-md-3 item hospitaly">
                        <div class="picframe">
                            <a href="{{ asset('front-assets/images-event/portfolio/3.jpg') }}" title="View Details">
                                <span class="overlay">
                                    <span class="pf_text">
                                        <span class="project-name">View Image</span>
                                    </span>
                                </span>
                                <img src="{{ asset('front-assets/images-event/portfolio/3.jpg') }}" alt="" />
                            </a>
                        </div>
                    </div>
                    <!-- close gallery item -->

                    <!-- gallery item -->
                    <div class="col-md-3 item residential">
                        <div class="picframe">
                            <a href="{{ asset('front-assets/images-event/portfolio/4.jpg') }}" title="View Details">
                                <span class="overlay">
                                    <span class="pf_text">
                                        <span class="project-name">View Image</span>
                                    </span>
                                </span>
                                <img src="{{ asset('front-assets/images-event/portfolio/4.jpg') }}" alt="" />
                            </a>
                        </div>
                    </div>
                    <!-- close gallery item -->

                </div>
            </div>
        </div>
    </section>

    <div class="height1 gradient-to-right"></div>
@endsection

@section('sidebar')
    <div id="de-extra-wrap" class="de_light">
        <span id="b-menu-close">
            <span></span>
            <span></span>
        </span>
        <div class="de-extra-content">
            <h3>Information</h3>
            <img src="{{ asset('images/conference/pamflet/'.$data->conference_pamflet) }}" alt="{{ $data->conference_nama }}" class="img-fluid">

            <div class="spacer-single"></div>

            <h3>Where &amp; When?</h3>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-calendar-check-o id-color"></i>{{ $data->tanggal_awal == $data->tanggal_akhir ? Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y') : Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y').' - '.Carbon\Carbon::parse($data->tanggal_akhir)->formatLocalized('%d %B %Y') }}</div>
            <div class="h6 padding10 pt0 pb0">
                @if ($data->conference_google_maps != NULL)
                    <a href="{{ $data->conference_google_maps }}" target="_blank">
                        <i class="i_h3 fa fa-map-marker id-color"></i>{{ $data->conference_lokasi ?? '-' }}
                    </a>
                @else
                    <i class="i_h3 fa fa-map-marker id-color"></i>{{ $data->conference_lokasi ?? '-' }}
                @endif
            </div>

            <div class="spacer-single"></div>

            <h3>Contact Person</h3>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-user id-color"></i>{{ $data->conference_kontak_nama1 }}</div>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-phone id-color"></i>{{ $data->conference_kontak_hp1 }}</div>
        </div>
    </div>
    <div id="de-overlay"></div>
@endsection
