@extends('front.layouts.app')

@section('title')
    Download
@endsection

@section('content')
    <!-- parallax section -->
    <section id="section-hero" class="text-light" data-bgimage="url({{ asset('front-assets/images-event/bg/10.jpg') }}) fixed top center"  data-stellar-background-ratio=".2">
        <div class="wm wm-border dark wow fadeInDown">{{ strtolower(__('messages.download')) }}</div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-2 mt40 mb40">
                        {{ __('messages.files') }}
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->

    <!-- section begin -->
    <section data-bgimage="url({{ asset('front-assets/images-event/bg/3.png') }}) fixed top center">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="wow fadeIn" data-wow-delay="0s">
                        <a href="{{ asset('files/template/' . $data->conference_template) }}" download>
                            <div class="box-number square">
                                <i class="bg-color hover-color-2 fa fa-file-o text-light"></i>
                                <div class="text">
                                    <h3><span>{{ __('messages.article template') }}</span></h3>
                                    <p>{{ $data->conference_template }}</p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="spacer-single"></div>

                    <div class="embed-responsive embed-responsive-1by1">
                        <iframe class="embed-responsive-item" src="https://view.officeapps.live.com/op/embed.aspx?src={{ asset('files/template/' . $data->conference_template) }}"></iframe>
                    </div>
                </div>

                @include('front.portal.sidebar')

            </div>
        </div>
    </section>

    <div class="height1 gradient-to-right"></div>
@endsection

@section('sidebar')
    <div id="de-extra-wrap" class="de_light">
        <span id="b-menu-close">
            <span></span>
            <span></span>
        </span>
        <div class="de-extra-content">
            <h3>{{ __('messages.information') }}</h3>
            <img src="{{ asset('images/conference/pamflet/'.$data->conference_pamflet) }}" alt="{{ $data->conference_nama }}" class="img-fluid">

            <div class="spacer-single"></div>

            <h3>{{ __('messages.where when') }}</h3>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-calendar-check-o id-color"></i>{{ $data->tanggal_awal == $data->tanggal_akhir ? Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y') : Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y').' - '.Carbon\Carbon::parse($data->tanggal_akhir)->formatLocalized('%d %B %Y') }}</div>
            <div class="h6 padding10 pt0 pb0">
                @if ($data->conference_google_maps != NULL)
                    <a href="{{ $data->conference_google_maps }}" target="_blank">
                        <i class="i_h3 fa fa-map-marker id-color"></i>{{ $data->conference_lokasi ?? '-' }}
                    </a>
                @else
                    <i class="i_h3 fa fa-map-marker id-color"></i>{{ $data->conference_lokasi ?? '-' }}
                @endif
            </div>

            <div class="spacer-single"></div>

            <h3>{{ __('messages.contact person') }}</h3>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-user id-color"></i>{{ $data->conference_kontak_nama1 }}</div>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-phone id-color"></i>{{ $data->conference_kontak_hp1 }}</div>
        </div>
    </div>
    <div id="de-overlay"></div>
@endsection
