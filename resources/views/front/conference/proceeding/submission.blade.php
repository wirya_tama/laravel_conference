@extends('front.conference.proceeding.layout')

@section('content-proceeding')
<h1 class="text-left">{{ $data->title ?? "-" }}</h1>
<hr class="my-3">
    @php
    $author = "";
    $first = true;
    foreach ($proceeding->submission->author_submission as $author_submission) {
        if ($author_submission->user != null) {
            if ($first) {
                $author .= $author_submission->user->name;
                $first = false;
            } else {
                $author .= ", ". $author_submission->user->name;
            }
        }
    }

    $kata_kunci = "";
    $kata_kunci_first = true;
    $keyword = "";
    $keyword_first = true;
    foreach ($proceeding->submission->kata_kunci_abstrak as $value) {
        if ($value->kka_level == 1) {
            if ($kata_kunci_first == true) {
                $kata_kunci .= $value->kka_kata_kunci;
                $kata_kunci_first = false;
            } else {
                $kata_kunci .= ",". $value->kka_kata_kunci;
            }
        } else {
            if ($keyword_first == true) {
                $keyword .= $value->kka_kata_kunci;
                $keyword_first = false;
            } else {
                $keyword .= ",". $value->kka_kata_kunci;
            }
        }
    }
    @endphp

<div style="font-size: 1.3rem" class="text-dark">{{ $proceeding->submission->submission_judul ?? "-" }}</div>
<div class="my-2">
    <b style="font-size: 15px" class="text-dark">{{ __('messages.author') }}</b><br>
    <b style="font-size: 16px" class="text-dark">{{ $author ?? "-" }}</b>
</div>

<div class="my-2">
    <b style="font-size: 15px" class="text-dark">Kata Kunci</b><br>
    <p style="font-size: 19px" class="text-dark">{{ $kata_kunci }}</p>
    <b style="font-size: 15px" class="text-dark">Abstrak</b><br>
    <p style="font-size: 19px" class="text-dark">{{ $proceeding->submission->submission_abstrakindo ?? "-" }}</p>
</div>

@if ($conference->conference_level == 2)
<div class="my-2">
    <b style="font-size: 15px" class="text-dark">Keyword</b><br>
    <p style="font-size: 19px" class="text-dark">{{ $keyword }}</p>
    <b style="font-size: 15px" class="text-dark">Abtract</b><br>
    <p style="font-size: 19px" class="text-dark">{{ $proceeding->submission->submission_abstrak ?? "-" }}</p>
</div>
@endif

<ul>
    <li style="font-size: 19px"><a href="{{ asset('files/submission/full-paper/' . $proceeding->submission->submission_full_paper) }}" class="text-dark">{{ __('messages.download article') }} (PDF)</a></li>
</ul>
@endsection