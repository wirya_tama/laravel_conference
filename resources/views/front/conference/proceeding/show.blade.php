@extends('front.conference.proceeding.layout')

@section('content-proceeding')
<form action="" method="get">
    <input type="text" class="form-control" placeholder="Pencarian" name="q" value="{{ $request->q }}">
</form>
<hr class="my-3">

    @foreach ($proceeding as $item)
        @php
            $author = "";
            $first = true;
            foreach ($item->submission->author_submission as $author_submission) {
                if ($author_submission->user != null) {
                    if ($first) {
                        $author .= $author_submission->user->name;
                        $first = false;
                    } else {
                        $author .= ", ". $author_submission->user->name;
                    }
                }
            }
        @endphp

    <p class="text-dark">{{ __('messages.proceedings article') }}</p>
    <div style="font-size: 1.3rem" class="text-dark">{{ $item->submission->submission_judul }}</div>
    <div class="mb-3">
        <b style="font-size: 16px" class="text-dark">{{ $author }}</b>
    </div>
    <p style="font-size: 19px" class="text-dark">{{ $conference->conference_level == 1 ? Str::words($item->submission->submission_abstrakindo, 60) : Str::words($item->submission->submission_abstrak, 60) }}</p>

    <ul>
        <li style="font-size: 19px"><a href="{{ route('conference.proceeding.submission', ['id' => $id, 'slug' => $slug, 'code' => $item->kode]) }}" class="text-dark">{{ __('messages.article details') }}</a></li>
        <li style="font-size: 19px"><a href="{{ asset('files/submission/full-paper/' . $item->submission->submission_full_paper) }}" class="text-dark">{{ __('messages.download article') }} (PDF)</a></li>
    </ul>

    <hr class="my-3">
    @endforeach
@endsection
