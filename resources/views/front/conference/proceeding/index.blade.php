@extends('front.layouts.app')

@section('title')
{{ __('messages.proceedings') }}
@endsection

@section('content')
<!-- parallax section -->
<section id="section-hero" class="bg-dark text-light">
    <div class="wm wm-border dark wow fadeInDown"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="title-2 mt40 mb40">
                    {{ __('messages.proceedings') }}
                </h1>
            </div>
        </div>
    </div>
</section>
<!-- section close -->

<section>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <ul>
                @foreach ($conference->proceeding as $item)
                <li style="font-size: 19px"><a href="{{ route('conference.proceeding.show', ['id' => $id, 'slug' => $item->slug]) }}">{{ $item->title }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
</section>
@endsection
