@extends('front.layouts.app')

@section('title')
{{ __('messages.proceeding') }}
@endsection

@section('content')
<!-- parallax section -->
<section id="section-hero" class="bg-dark text-light">
    <div class="wm wm-border dark wow fadeInDown"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="title-2 mt40 mb40">
                    {{ __('messages.proceeding') }}
                </h1>
            </div>
        </div>
    </div>
</section>
<!-- section close -->

<section>
    <div class="row justify-content-center">
        <div class="col-md-11">
            <div class="row">
                <div class="col-md-3">
                    <ul class="list-group list-group-flush text-dark" style="font-size: 19px">
                        <li class="list-group-item"><a href="{{ route('conference.proceeding.show', ['id' => $id, 'slug' => $slug]) }}">{{ __('messages.publishing info') }}</a></li>
                        <li class="list-group-item"><a href="{{ route('conference.proceeding.search', ['id' => $id, 'slug' => $slug]) }}">{{ __('messages.article') }}</a></li>
                    </ul>
                </div>

                <div class="col-md-9">
                    @yield('content-proceeding')
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
