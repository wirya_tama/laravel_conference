@extends('front.conference.proceeding.layout')

@php
    $info = [
        [
            'label' => trans('messages.title'),
            'value' => $data->title
        ],
        [
            'label' => trans('messages.editor'),
            'value' => $data->editor
        ],
        [
            'label' => trans('messages.volume'),
            'value' => $data->volume
        ],
        [
            'label' => "ISSN",
            'value' => $data->issn
        ],
        [
            'label' => "ISBN",
            'value' => $data->isbn
        ],
        [
            'label' => trans('messages.published date'),
            'value' => date('d/m/Y', strtotime($data->tanggal))
        ],
    ]
@endphp

@section('content-proceeding')
<div class="bg-light text-dark px-5 py-3">
    <table class="table table-borderless m-0">
        <tbody>
            @foreach ($info as $item)
                @if ($item['value'] !== NULL)
                <tr>
                    <td class="text-dark align-top text-nowrap" style="font-size: 19px; width: 6px">{{ $item['label'] }}</td>
                    <td class="text-dark align-top" style="font-size: 19px">{{ $item['value'] }}</td>
                </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>
@endsection
