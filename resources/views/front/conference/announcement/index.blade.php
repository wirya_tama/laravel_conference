@extends('front.layouts.app')

@section('title', 'Announcements')

@section('content')
    <!-- parallax section -->
    <section id="section-hero" class="text-light" data-bgimage="url({{ asset('front-assets/images-event/bg/10.jpg') }}) fixed top center"  data-stellar-background-ratio=".2">
        <div class="wm wm-border dark wow fadeInDown">{{ strtolower(__('messages.latest')) }}</div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="title-2 mt40 mb40">
                        {{ _('messages.announcements') }}
                    </h1>
                </div>
            </div>
        </div>
    </section>
    <!-- section close -->

    <section id="section-news" data-bgimage="url({{ asset('front-assets/images-event/bg/3.png') }}) fixed center no-repeat" data-stellar-background-ratio=".2">
        <div class="container">
            <div class="row justify-content-center">
                @if (count($pengumuman) > 0)
                    @foreach ($pengumuman as $item)
                        <div class="col-lg-4 col-md-6 mb30">
                            <div class="card-dark">
                                <div class="card-body">
                                    <div class="picframe rounded mb30">
                                        @if ($item->gambar != 'blank.png')
                                            <a class="pf-click" href="{{ route('conference.announcements.show', [$data->conference_slug, $item->id] ) }}">
                                                <span class="overlay-v">
                                                </span>
                                                <img src="{{ asset('images/pengumuman/'. $item->gambar) }}" alt="{{ $item->judul }}">
                                            </a>
                                        @endif
                                    </div>
                                    <div class="post-text">
                                        <div class="post-info">
                                            <span class="post-date text-dark">{{ date('d/m/Y', strtotime($item->tanggal)) }}</span>
                                        </div>
                                        <a href="{{ route('conference.announcements.show', [$data->conference_slug, $item->id] ) }}"><h3 class="text-dark">{{ $item->judul }}</h3></a>
                                        @php
                                            $deskripsi = strip_tags($item->deskripsi);
                                        @endphp
                                        <p class="text-black-50">{{ (Str::words($deskripsi, 20)) }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <h6 class="text-center my-5 text-uppercase">{{ __('messages.no announcement available') }}</h6>
                @endif
            </div>
        </div>
    </section>

    <div class="height1 gradient-to-right"></div>
@endsection

@section('sidebar')
    <div id="de-extra-wrap" class="de_light">
        <span id="b-menu-close">
            <span></span>
            <span></span>
        </span>
        <div class="de-extra-content">
            <h3>{{ __('messages.information') }}</h3>
            <img src="{{ asset('images/conference/pamflet/'.$data->conference_pamflet) }}" alt="{{ $data->conference_nama }}" class="img-fluid">

            <div class="spacer-single"></div>

            <h3>{{ __('messages.where when') }}</h3>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-calendar-check-o id-color"></i>{{ $data->tanggal_awal == $data->tanggal_akhir ? Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y') : Carbon\Carbon::parse($data->tanggal_awal)->formatLocalized('%d %B %Y').' - '.Carbon\Carbon::parse($data->tanggal_akhir)->formatLocalized('%d %B %Y') }}</div>
            <div class="h6 padding10 pt0 pb0">
                @if ($data->conference_google_maps != NULL)
                    <a href="{{ $data->conference_google_maps }}" target="_blank">
                        <i class="i_h3 fa fa-map-marker id-color"></i>{{ $data->conference_lokasi ?? '-' }}
                    </a>
                @else
                    <i class="i_h3 fa fa-map-marker id-color"></i>{{ $data->conference_lokasi ?? '-' }}
                @endif
            </div>

            <div class="spacer-single"></div>

            <h3>{{ __('messages.contact person') }}</h3>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-user id-color"></i>{{ $data->conference_kontak_nama1 }}</div>
            <div class="h6 padding10 pt0 pb0"><i class="i_h3 fa fa-phone id-color"></i>{{ $data->conference_kontak_hp1 }}</div>
        </div>
    </div>
    <div id="de-overlay"></div>
@endsection
