@extends('layout.layout')

@section('title', 'Data Pembayaran Submission')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button title="Data Peserta Conference" id='modal' buttonTitle=''>
			<div class="row mx-1 mt-1">
				<select id="conference" class="form-control" onchange="tombol()">
					<option value="" hidden>-- Pilih Conference --</option>
					@foreach ($data as $item)
						<option value="{{ $item->id }}">{{ $item->nama }}</option>
					@endforeach
				</select>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th data-priority="1">Submission</th>
							<th>Author</th>
							<th>Afiliasi</th>
							<th width="15%">Abstrak</th>
							<th>Voucher</th>
							<th>Tanggal</th>
							<th>Nominal</th>
							<th>Bank</th>
							<th>Rekening</th>
							<th>No Invoice</th>
							<th>Pesan</th>
							<th width="15%">Publikasi</th>
							<th>Voucher</th>
							<th>Tanggal</th>
							<th>Nominal</th>
							<th>Bank</th>
							<th>Rekening</th>
							<th>No Invoice</th>
							<th>Pesan</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</x-datatable-button>
		{{-- END: Table --}}
	</section>
	{{-- END: Datatable Button --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="Detail Pembayaran" type="normal" class="modal-lg" id="modal-detail">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<h3>Pembayaran Abstrak</h3>

						<div id="abstrak"></div>
					</div>
					
					<div class="col-md-6">
						<h3>Pembayaran Publikasi</h3>

						<div id="publikasi"></div>
					</div>
				</div>
			</div>
		</x-modal>
		{{-- END: Modal --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>

    {{-- Datatable --}}
	<script>
		var link = "{{ route('admin-conference.laporan.peserta.show', 0) }}";
		var column = [
			{data: 'submission_judul', name: 'submission_judul'},
			{data: 'name', name: 'users.name'},
			{data: 'du_afiliasi', name: 'data_user.du_afiliasi'},
			{data: 'status_abstrak', name: 'status_abstrak', orderable: false, searchable: false},
			{data: 'submission_pembayaran_abstrak_voucher', name: 'submission_pembayaran_abstrak_voucher', orderable: false, searchable: false},
			{data: 'submission_pembayaran_abstrak_tanggal', name: 'submission_pembayaran_abstrak_tanggal', orderable: false, searchable: false},
			{data: 'submission_pembayaran_abstrak_nominal', name: 'submission_pembayaran_abstrak_nominal', orderable: false, searchable: false},
			{data: 'submission_pembayaran_abstrak_bank', name: 'submission_pembayaran_abstrak_bank', orderable: false, searchable: false},
			{data: 'submission_pembayaran_abstrak_rekening', name: 'submission_pembayaran_abstrak_rekening', orderable: false, searchable: false},
			{data: 'submission_pembayaran_abstrak_invoice', name: 'submission_pembayaran_abstrak_invoice', orderable: false, searchable: false},
			{data: 'submission_pembayaran_abstrak_pesan', name: 'submission_pembayaran_abstrak_pesan', orderable: false, searchable: false},
			{data: 'status_publikasi', name: 'status_publikasi', orderable: false, searchable: false},
			{data: 'submission_pembayaran_abstrak_voucher', name: 'submission_pembayaran_publikasi_voucher', orderable: false, searchable: false},
			{data: 'submission_pembayaran_abstrak_tanggal', name: 'submission_pembayaran_publikasi_tanggal', orderable: false, searchable: false},
			{data: 'submission_pembayaran_abstrak_nominal', name: 'submission_pembayaran_publikasi_nominal', orderable: false, searchable: false},
			{data: 'submission_pembayaran_abstrak_bank', name: 'submission_pembayaran_publikasi_bank', orderable: false, searchable: false},
			{data: 'submission_pembayaran_abstrak_rekening', name: 'submission_pembayaran_publikasi_rekening', orderable: false, searchable: false},
			{data: 'submission_pembayaran_abstrak_invoice', name: 'submission_pembayaran_publikasi_invoice', orderable: false, searchable: false},
			{data: 'submission_pembayaran_abstrak_pesan', name: 'submission_pembayaran_publikasi_pesan', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		$('#modal').hide();
		$('.dt-button').removeClass('mr-2');

		// Update Datatable with new link
		function tombol() {
			const id = $('#conference').children('option:selected').val();
			table.ajax.url("{{ url('admin-conference/laporan/pembayaran/data') }}/" + id).load();
		}
	</script>
@endsection