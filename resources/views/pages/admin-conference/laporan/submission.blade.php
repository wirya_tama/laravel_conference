@extends('layout.layout')

@section('title', 'Data Laporan Submission')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button title="Data Submission" id='modal' buttonTitle=''>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th data-priority="1">Submission</th>
						<th>Conference</th>
						<th hidden></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Contoh Submission</td>
						<td>Contoh Conference</td>
						<td hidden></td>
					</tr>
				</tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>

    {{-- Datatable --}}
	<script>
		var link = null;
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		$('#modal').hide();
		$('.dt-button').removeClass('mr-2');
	</script>
@endsection