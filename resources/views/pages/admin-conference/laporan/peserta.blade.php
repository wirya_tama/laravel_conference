@extends('layout.layout')

@section('title', 'Data Laporan Peserta Conference')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button title="Data Peserta Conference" id='modal' buttonTitle=''>
			<div class="row mx-1 mt-1">
				<select id="conference" class="form-control" onchange="tombol()">
					<option value="" hidden>-- Pilih Conference --</option>
					@foreach ($data as $item)
						<option value="{{ $item->id }}">{{ $item->nama }}</option>
					@endforeach
				</select>
			</div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th data-priority="1">Submission</th>
						<th>Author</th>
						<th>Afiliasi</th>
						<th width="15%">Abstrak</th>
						<th width="15%">Publikasi</th>
						<th width="5%">Aksi</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
	</section>
	{{-- END: Datatable Button --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="Detail Pembayaran" type="normal" class="modal-lg" id="modal-detail">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">
						<h3>Pembayaran Abstrak</h3>

						<div id="abstrak"></div>
					</div>
					
					<div class="col-md-6">
						<h3>Pembayaran Publikasi</h3>

						<div id="publikasi"></div>
					</div>
				</div>
			</div>
		</x-modal>
		{{-- END: Modal --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>

    {{-- Datatable --}}
	<script>
		var link = "{{ route('admin-conference.laporan.peserta.show', 0) }}";
		var column = [
			{data: 'submission_judul', name: 'submission_judul'},
			{data: 'name', name: 'users.name'},
			{data: 'du_afiliasi', name: 'data_user.du_afiliasi'},
			{data: 'status_abstrak', name: 'status_abstrak', orderable: false, searchable: false},
			{data: 'status_publikasi', name: 'status_publikasi', orderable: false, searchable: false},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		$('#modal').hide();
		$('.dt-button').removeClass('mr-2');

		// Update Datatable with new link
		function tombol() {
			const id = $('#conference').children('option:selected').val();
			table.ajax.url("{{ url('admin-conference/laporan/peserta-conference') }}/" + id).load();
		}
	</script>

	{{-- Modal --}}
	<script>
		function detail(id) {
			$.get(`{{ url('admin-conference/laporan/peserta-conference/data/${id}') }}`, function(data) {
				$('#abstrak').children().remove();
				$('#publikasi').children().remove();

				if (data.submission_pembayaran_abstrak_status == 1) {
					if (data.submission_pembayaran_abstrak_voucher != null) {
						$('#abstrak').html(`
							<x-form-group title="Metode">
								<br><p>Voucher</p>
							</x-form-group>
			
							<x-form-group title="Kode Voucher">
								<br><p>${data.submission_pembayaran_abstrak_voucher}</p>
							</x-form-group>
						`);
					} else {
						$('#abstrak').html(`
							<x-form-group title="Metode">
								<br><p>Transfer Bank</p>
							</x-form-group>
			
							<x-form-group title="Tanggal Transfer">
								<br><p>${data.submission_pembayaran_abstrak_tanggal ?? "-"}</p>
							</x-form-group>
							
							<x-form-group title="Nominal Transfer">
								<br><p>${data.submission_pembayaran_abstrak_nominal ?? "-"}</p>
							</x-form-group>
							
							<x-form-group title="Nama Rekening Pengirim">
								<br><p>${data.submission_pembayaran_abstrak_rekening ?? "-"}</p>
							</x-form-group>
							
							<x-form-group title="Bank Tujuan">
								<br><p>${data.submission_pembayaran_abstrak_bank ?? "-"}</p>
							</x-form-group>
							
							<x-form-group title="No. Tagihan/Invoice">
								<br><p>${data.submission_pembayaran_abstrak_invoice ?? "-"}</p>
							</x-form-group>
							
							<x-form-group title="Pesan Tambahan">
								<br><p>${data.submission_pembayaran_abstrak_pesan ?? "-"}</p>
							</x-form-group>
							
							<x-form-group title="Bukti Pembayaran">
								<br><img src="{{ url('/images/submission/abstrak/${data.submission_pembayaran_abstrak_file}') }}" class="img-fluid" />
							</x-form-group>
						`);
					}
				} else {
					$('#abstrak').html(`
						<h2 class="text-center mt-3">Belum Bayar</h2>
					`);
				}

				if (data.submission_pembayaran_publikasi_status == 1) {
					if (data.submission_pembayaran_publikasi_voucher != null) {
						$('#publikasi').html(`
							<x-form-group title="Metode">
								<br><p>Voucher</p>
							</x-form-group>
			
							<x-form-group title="Kode Voucher">
								<br><p>${data.submission_pembayaran_publikasi_voucher}</p>
							</x-form-group>
						`);
					} else {
						$('#publikasi').html(`
							<x-form-group title="Metode">
								<br><p>Transfer Bank</p>
							</x-form-group>
			
							<x-form-group title="Tanggal Transfer">
								<br><p>${data.submission_pembayaran_publikasi_tanggal ?? "-"}</p>
							</x-form-group>
							
							<x-form-group title="Nominal Transfer">
								<br><p>${data.submission_pembayaran_publikasi_nominal ?? "-"}</p>
							</x-form-group>
							
							<x-form-group title="Nama Rekening Pengirim">
								<br><p>${data.submission_pembayaran_publikasi_rekening ?? "-"}</p>
							</x-form-group>
							
							<x-form-group title="Bank Tujuan">
								<br><p>${data.submission_pembayaran_publikasi_bank ?? "-"}</p>
							</x-form-group>
							
							<x-form-group title="No. Tagihan/Invoice">
								<br><p>${data.submission_pembayaran_publikasi_invoice ?? "-"}</p>
							</x-form-group>
							
							<x-form-group title="Pesan Tambahan">
								<br><p>${data.submission_pembayaran_publikasi_pesan ?? "-"}</p>
							</x-form-group>
							
							<x-form-group title="Bukti Pembayaran">
								<br><img src="{{ url('/images/submission/publikasi/`+ data.submission_pembayaran_publikasi_file +`') }}" class="img-fluid" />
							</x-form-group>
						`);
					}
				} else {
					$('#publikasi').html(`
						<h2 class="text-center mt-3">Belum Bayar</h2>
					`);
				}

				$('#modal-detail').modal('show');
			});
		}
	</script>
@endsection