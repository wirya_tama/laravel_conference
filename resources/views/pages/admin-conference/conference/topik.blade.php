@extends('layout.layout')

@section('title', 'Data Topik')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		<div class="row justify-content-center">
			<div class="col-md-8">
				{{-- BEGIN: Table --}}
				<x-datatable-button title="Data Topik" id="tambah" buttonTitle='<i data-feather="plus"></i> Tambah'>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th data-priority="1">Nama</th>
								<th>Conference</th>
								<th data-priority="2" width="15%"></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</x-datatable-button>
				{{-- END: Table --}}
			</div>
		</div>
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
			@if (count($conference) > 0)
				<form id="form">
					<div class="modal-body">
						<x-form-group title="Nama">
							<input type="text" class="form-control" id="nama" placeholder="Nama Topik" required/>
						</x-form-group>
						
						<x-form-group title="Conference">
							<select class="form-control" id="conference" required>
								<option value="" hidden>-- Pilih Conference --</option>
								@foreach ($conference as $item)
									<option value="{{ $item->id }}">{{ $item->nama }}</option>
								@endforeach
							</select>
						</x-form-group>
					</div>
					
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary data-submit mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
						<button type="submit" class="btn btn-primary data-submit mr-1" id="button-ubah"><i data-feather="edit-2"></i> Ubah</button>
						<button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
						<button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> Hapus</button>
					</div>
				</form>
			@else
				<x-kosong>Tidak Ada Conference</x-kosong>
			@endif
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin-conference.conference.topik.data') }}";
		var column = [
			{data: 'topik_nama', name: 'topik_nama'},
			{data: 'conference_nama', name: 'conference.conference_nama'},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			// Modal
			$('#tambah').on('click', function() {
				$('.modal-title').text('Tambah Topik');
				$('#form').attr("action", "{{ url('admin-conference/conference/topik') }}");
				$('#form').attr('data-value', 1);

				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				
				$('#modal').modal('show');
			});

			$(document).on('click', '.ubah', function() {
				const id = $(this).attr('data-value');
				$('.modal-title').text('Ubah Topik');
				$('#form').attr('data-value', 2);
				$('#button-hapus').attr('onClick', "hapus("+ $(this).attr('data-value') +")");

				$.get( "{{ url('admin-conference/conference/topik') }}/"+ id, function( data ) {
					var d = JSON.parse(data);
					$('#form').attr("action", "{{ url('admin-conference/conference/topik') }}/" + id);
					$('#nama').val(d.topik_nama);
					$('#conference').val(d.conference_id);
				});

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);


				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('#nama').val('');
				$('#conference').val('');
			});

			$('#form').on('submit', function(e) {
				e.preventDefault();

				var request = {};
				if ($('#form').attr('data-value') == 1) {
					request = {
						nama: $('#nama').val(),
						conference: $('#conference').val()
					};	
				} else {
					request = {
						_method: 'PUT',
						nama: $('#nama').val(),
						conference: $('#conference').val()
					};	
				}

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: request
				}).done(function(response){
					// console.log(response);
					formExecuted(response);
				});
			});

			$('form').submit(function() {
				$(this).find("button[type='submit']").prop('disabled', true);
			});
		});

		function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
				$('#modal').modal('hide');
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("admin-conference/conference/topik")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							table.ajax.reload();
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
	</script>
@endsection