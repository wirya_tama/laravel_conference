@extends('layout.layout')

@section('title', 'Data Voucher')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/forms/spinner/jquery.bootstrap-touchspin.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button title="Data Voucher" id="tambah" buttonTitle='<i data-feather="plus"></i> Tambah'>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th data-priority="1" width="18%">Kode</th>
						<th>Conference</th>
						<th width="10%">Status</th>
						<th data-priority="2" width="15%"></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="Tambah Voucher" type="normal" class="" id="modal">
			@if (count($conference) > 0)
				<form id="form" action="{{ route('admin-conference.conference.voucher.store') }}" method="POST">
					@csrf
					<div class="modal-body">
						<x-form-group title="Conference">
							<select class="form-control" id="conference" name="conference" required>
								<option value="" hidden>-- Pilih Conference --</option>
								@foreach ($conference as $item)
									<option value="{{ $item->id }}">{{ $item->nama }}</option>
								@endforeach
							</select>
						</x-form-group>

						<x-form-group title="Jumlah Voucher">
							<div class="input-group input-group-lg">
								<input type="number" class="touchspin" id="jumlah" name="jumlah" value="1" readonly/>
							</div>
						</x-form-group>
					</div>
					
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
						<button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
					</div>
				</form>
			@else
				<x-kosong>Tidak ada Conference</x-kosong>
			@endif
		</x-modal>
		{{-- END: Modal --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="Ubah Voucher" type="normal" class="" id="voucherModal">
			@if (count($conference) > 0)
				<form id="voucherForm" method="POST">
					@csrf
					@method('PUT')
					<div class="modal-body">
						<x-form-group title="Kode">
							<input type="text" class="form-control" id="kode" disabled/>
						</x-form-group>

						<x-form-group title="Conference">
							<select class="form-control" id="conference-ubah" name="conference" required>
								<option value="" hidden>-- Pilih Conference --</option>
								@foreach ($conference as $item)
									<option value="{{ $item->id }}">{{ $item->nama }}</option>
								@endforeach
							</select>
						</x-form-group>

						<x-form-group title="Status">
							<select class="form-control" id="status" name="status" required>
								<option value="1">Aktif</option>
								<option value="0">Tidak Aktif</option>
							</select>
						</x-form-group>
					</div>
					
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary mr-1" id="button-ubah"><i data-feather="check"></i> Ubah</button>
						<button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> Hapus</button>
					</div>
				</form>
			@else
				<x-kosong>Tidak ada Conference</x-kosong>
			@endif
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/forms/spinner/jquery.bootstrap-touchspin.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin-conference.conference.voucher.data') }}";
		var column = [
			{data: 'voucher_kode', name: 'voucher_kode'},
			{data: 'conference_nama', name: 'conference.conference_nama'},
			{data: 'status', name: 'status', orderable: false, searchable: false},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		// Touchspin
		$('.touchspin').TouchSpin({
			min: 1,
			buttondown_class: 'btn btn-primary',
			buttonup_class: 'btn btn-primary',
			buttondown_txt: feather.icons['minus'].toSvg(),
			buttonup_txt: feather.icons['plus'].toSvg()
		});

		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			$('#tambah').on('click', function() {
				$('#modal').modal('show');
			});

			// Tambah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					if (response.id == 1) {
						$('#modal').modal('hide');
					}
					formExecuted(response);
				});
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('#conference').val('');
				$('#jumlah').val(1);
			});

			$(document).on('click', '.ubah', function() {
				$('.modal-title').text('Ubah Voucher');
				$('#voucherForm').attr('action', "{{ url('admin-conference/conference/voucher') }}/" + $(this).attr('data-value'));
				$('#button-hapus').attr('onClick', "hapus("+ $(this).attr('data-value') +")");

				$('#kode').val($(this).attr('data-kode'));
				$('#conference-ubah').val($(this).attr('data-conference'));
				$('#status').val($(this).attr('data-status'));

				$('#voucherModal').modal('show');
			});

			// Ubah Data
			$('#voucherForm').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					if (response.id == 1) {
						$('#voucherModal').modal('hide');
					}
					formExecuted(response);
				});
			});

			$('form').submit(function() {
				$(this).find("button[type='submit']").prop('disabled', true);
			});
		});

		function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("admin-conference/conference/voucher")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							table.ajax.reload();
							$('#voucherModal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
	</script>
@endsection