@extends('layout.layout')

@section('title', 'Data Keynote Speaker')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">

    {{-- Croppie --}}
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.min.css">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
        {{-- BEGIN: Table --}}
        <x-datatable-button title="Data Keynote Speaker" id="tambah" buttonTitle='<i data-feather="plus"></i> Tambah'>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th data-priority="1">Nama</th>
                        <th>Conference</th>
                        <th data-priority="2" width="15%"></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </x-datatable-button>
        {{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="Menunggu..." type="normal" class="" id="modal">
			@if (count($conference) > 0)
				<form id="form">
					<div class="modal-body">
						<x-form-group title="Nama">
							<input type="text" id="nama" class="form-control" placeholder="Nama Keynote Speaker" required/>
						</x-form-group>
						
						<x-form-group title="Conference">
							<select class="form-control" id="conference" required>
								@foreach ($conference as $item)
									<option value="{{ $item->id }}">{{ $item->nama }}</option>
								@endforeach
							</select>
						</x-form-group>
						
						<x-form-group title="Afiliasi">
							<input type="text" class="form-control" id="afiliasi" placeholder="Afiliasi Keynote Speaker" required/>
						</x-form-group>
						
						<x-form-group title="Deskripsi">
							<textarea class="form-control" id="deskripsi" rows="5" placeholder="Deskripsi Keynote Speaker"></textarea>
						</x-form-group>
					</div>

					<div class="modal-footer">
						<button type="submit" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
						<button type="submit" class="btn btn-primary mr-1" id="button-ubah"><i data-feather="edit-2"></i> Ubah</button>
						<button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
						<button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> Hapus</button>
					</div>
				</form>
			@else
				<x-kosong>Tidak ada Conference</x-kosong>
			@endif
		</x-modal>
		{{-- END: Modal --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="Menunggu..." type="normal" class="modal-lg" id="fileModal">
			<form id="fileForm" method="post" enctype="multipart/form-data">
				@csrf
				@method('PUT')
				<div class="modal-body">
					<div class="row">
						<div class="col-lg-6">
							<x-form-group title="Foto Keynote Speaker">
								<div id="fileFoto" class="text-center"></div>
							</x-form-group>
						</div>

						<div class="col-lg-6">
							<label>Ubah Foto</label><br>

							<input type="hidden" name="profile_img" id="foto-anggota"/>
							<input type="file" class="form-control-file mb-2" accept="image/jpeg,jpg,png" id="upload"/>

							<div id="upload-input"></div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary data-submit mr-1"><i data-feather="edit-2"></i> Ubah</button>
					<button type="reset" class="btn btn-outline-danger" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
				</div>
			</form>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin-conference.conference.keynote.data') }}";
		var column = [
			{data: 'ks_nama', name: 'ks_nama'},
			{data: 'conference_nama', name: 'conference.conference_nama'},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		// Modal
		$(document).ready(function() {
			$('#tambah').on('click', function() {
				$('.modal-title').text('Tambah Keynote Speaker');
				$('#form').attr('action', "{{ url('admin-conference/conference/keynote') }}");
				$('#form').attr('data-value', '1');

				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				
				$('#modal').modal('show');
			});

			$(document).on('click', '.ubah', function() {
				const id = $(this).attr('data-value');
				$('#form').attr('data-value', '2');
				$('#button-hapus').attr('onClick', "hapus("+ $(this).attr('data-value') +")");

				$.get( "{{ url('admin-conference/conference/keynote') }}/"+ id, function( data ) {
					var d = JSON.parse(data);
					$('.modal-title').text('Ubah Keynote Speaker');
					$('#form').attr('action', "{{ url('admin-conference/conference/keynote') }}/" + id);

					$('#nama').val(d.ks_nama);
					$('#conference').val(d.conference_id);
					$('#afiliasi').val(d.ks_afiliasi);
					$('#deskripsi').val(d.ks_deskripsi);
				});

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);


				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('.modal-title').text('Menunggu...');

				$('#nama').val('');
				$('#conference').val('');
				$('#afiliasi').val('');
				$('#deskripsi').val('');
			});

			$('#form').on('submit', function(e) {
				e.preventDefault();

				var request = {};
				if ($('#form').attr('data-value') == 1) {
					request = {
						nama: $('#nama').val(),
						conference: $('#conference').val(),
						afiliasi: $('#afiliasi').val(),
						deskripsi: $('#deskripsi').val()
					};	
				} else {
					request = {
						_method: 'PUT',
						nama: $('#nama').val(),
						conference: $('#conference').val(),
						afiliasi: $('#afiliasi').val(),
						deskripsi: $('#deskripsi').val()
					};	
				}

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: request
				}).done(function(response){
					// console.log(response);
					formExecuted(response);
				});
			});

			$(document).on('click', '.file', function() {
				const id = $(this).attr('data-value');
				$('#fileForm').attr("action", "{{ url('admin-conference/conference/keynote') }}/" + id);

				$.get( "{{ url('admin-conference/conference/keynote') }}/"+ id, function( data ) {
					$('.modal-title').text('File Keynote Speaker');
					var d = JSON.parse(data);

					$('#fileFoto').append('<img src="/images/keynote_speaker/'+ d.ks_foto +'" class="img-fluid">');
				});

				$('#fileModal').modal('show');
			});

			$('#fileModal').on('hidden.bs.modal', function (e) {
				$('.modal-title').text('Menunggu...');

				$('#fileFoto').children().remove();
			});

			$('form').submit(function() {
				$(this).find("button[type='submit']").prop('disabled', true);
			});
		});

		function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
				$('#modal').modal('hide');
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("admin-conference/conference/keynote")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							table.ajax.reload();
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
	</script>

	{{-- Croppie --}}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.min.js"></script>
	<script type="text/javascript">
		// Anggota
		$uploadCrop = $('#upload-input').croppie({
			enableExif: true,
			viewport: {
				width: 300,
				height: 400,
				type: 'square'
			},
			boundary: {
				width: 320,
				height: 420
			}
		});

		$uploadCrop.croppie('bind', {
			url: "{{ asset('/images/keynote_speaker/blank.png') }}"
		});

		$('#upload').on('change', function () {
			var reader = new FileReader();
			reader.onload = function (e) {
				$uploadCrop.croppie('bind', {
					url: e.target.result
				});
			}
			reader.readAsDataURL(this.files[0]);
		});

		$('#fileForm').submit(function() {
			$uploadCrop.croppie('result', {
				type: 'canvas',
				size: 'viewport'
			}).then(function (response) {
				toastr['success']("Foto Keynote Speaker berhasil diubah", "Berhasil", {
					closeButton: true,
					tapToDismiss: true
				});
				
				$('#foto-anggota').val(response);
			});
		});
	</script>
@endsection