@extends('layout.layout')

@section('title')
	{{ __('messages.letter of acceptance data') }}
@endsection

@section('css')
	{{-- Quill Editor --}}
	<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
	
    {{-- Toastr --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">

    {{-- SweetAlert --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Data LOA --}}
	<section>
		<div class="card">
			<div class="card-header">
				<div class="card-title">{{ __('messages.letter of acceptance data') }}</div>
				<div class="float-right">
					<button type="button" onclick="window.location.replace('/admin-conference/conference/loa')" class="btn btn-outline-dark"><i data-feather="chevron-left"></i> Kembali</button>
				</div>
			</div>
		</div>

		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-md-2">
						<small>Logo</small><br>
						@if ($data->gambar1 == null)
							<strong>Tidak ada Gambar</strong><br>
						@else
							<img src="{{ asset('images/loa/'. $data->gambar1) }}" class="img-thumbnail" id="gambar1">
						@endif
						
						<small>Ubah Logo</small>
						<form action="{{ route('admin-conference.conference.loa.gambar', $data->id) }}" method="post" id="form_gambar1" enctype="multipart/form-data">
							@csrf
							@method('PUT')
							<input type="file" class="form-control-file" name="gambar1" onchange="submit_form('form_gambar1')">
						</form>
					</div>

					<div class="col-md-8">
						<div id="kop">
							<div class="editor">
								{!! $data->kop !!}
							</div>
						</div>
					</div>

					<div class="col-md-2">
						<small>Logo</small><br>
						@if ($data->gambar2 == null)
							<strong>Tidak ada Gambar</strong><br>
						@else
							<img src="{{ asset('images/loa/'. $data->gambar2) }}" class="img-thumbnail" id="gambar2">
						@endif

						<small>Ubah Logo</small>
						<form action="{{ route('admin-conference.conference.loa.gambar', $data->id) }}" method="post" id="form_gambar2" enctype="multipart/form-data">
							@csrf
							@method('PUT')
							<input type="file" class="form-control-file" name="gambar2" onchange="submit_form('form_gambar2')">
						</form>
					</div>
				</div>
			</div>
		</div>

		<div class="card">
			<div class="card-body">
				<p>Sisipkan kata-kata berikut untuk menampilkan data : </p>
				<ul>
					<li>#waktu : Waktu & Hari ini</li>
					<li>#judul : Judul Submission Author</li>
					<li>#author : Penulis-penulis pada setiap Submission</li>
				</ul>

				<small>Isi LOA</small>
				<div id="isi">
					<div class="editor">
						{!! $data->isi !!}
					</div>
				</div>

				<div class="text-right mt-2">
					<button type="button" class="btn btn-primary" onclick="submit_form_text()">Update LOA</button>
				</div>
			</div>
		</div>

		@if (count($data->ttd) > 0)
			<div class="row">
				@foreach ($data->ttd as $item)
					<div class="col-md-6">
						<div class="card">
							<div class="card-body p-3">
								{!! $item->title !!}
								<img src="{{ asset('images/loa/'. $item->gambar) }}" alt="" width="200px" style="margin-bottom: -2rem">
								{!! $item->keterangan !!}

								<div class="text-center">
									<button type="button" onclick="hapus({{ $item->id }})" data-toggle="tooltip" data-placement="top" title="Hapus Tanda Tangan" class="btn btn-danger p-1">
										<i data-feather="x"></i>
									</button>
								</div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		@endif

		<button type="button" class="btn btn-primary mb-2" onclick="tambah()">Tambah TTD</button>
	</section>
	{{-- END: Data LOA --}}

	{{-- BEGIN: Modal --}}
		<x-modal id="modal" class="modal-dialog-centered" title="Tambah Tanda Tangan" type="">
			<form action="{{ route('admin-conference.conference.loa.store') }}" method="post" id="form_ttd" onsubmit="submit_ttd()">
				<div class="modal-body">
					@csrf
					<input type="hidden" name="loa_id" value="{{ $data->id }}">
					<input type="hidden" name="title">
					<input type="hidden" name="keterangan">
					
					<label>Tempat & Tanggal Surat</label>
					<div id="title">
						<div class="editor"></div>
					</div>
					<hr>
					
					<label>Gambar Tanda Tangan</label>
					<input type="file" class="form-control-file" name="gambar" required>
					<hr>

					<label>Identitas</label>
					<div id="keterangan">
						<div class="editor"></div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</x-modal>
	{{-- END: Modal --}}

	{{-- BEGIN: Delete Tanda tangan --}}
		<form id="delete-form" method="POST">
			@csrf
			@method('DELETE')
		</form>
	{{-- END: Delete Tanda tangan --}}
@endsection

@section('js')
	{{-- Quill Editor --}}
	<script src="https://cdn.quilljs.com/1.3.6/quill.min.js" type="text/javascript"></script>
	<script>
		var modul = {
			toolbar: [
				[
					{
						header: '1'
					},
					{
						header: '2'
					},
					'blockquote',
					'code-block'
				],
				[
					{
						font: []
					},
					{
						size: []
					}
				],
				['bold', 'italic', 'underline', 'strike'],
				[
					{
						color: []
					},
					{
						background: []
					}
				],
				[
					{
						script: 'super'
					},
					{
						script: 'sub'
					}
				],
				[
					{
						list: 'ordered'
					},
					{
						list: 'bullet'
					},
					{
						indent: '-1'
					},
					{
						indent: '+1'
					}
				],
				[
					'direction',
					{
						align: []
					}
				],
				['link'],
				['clean']
			]
		}

		var kop = new Quill('#kop .editor', {
			bounds: '#kop .editor',
			modules: modul,
			theme: 'snow'
		});

		var isi = new Quill('#isi .editor', {
			bounds: '#isi .editor',
			modules: modul,
			theme: 'snow'
		});

		var title = new Quill('#title .editor', {
			bounds: '#title .editor',
			modules: modul,
			theme: 'snow'
		});

		var keterangan = new Quill('#keterangan .editor', {
			bounds: '#keterangan .editor',
			modules: modul,
			theme: 'snow'
		});
	</script>

	{{-- Form --}}
	<script>
		function submit_form(value) {
			$(`#${value}`).submit()

			setTimeout(() => {
				$('input[type="file"]').val('')
			}, 2000);
		}
	</script>

	{{-- Submit Form --}}
	<script>
		$('form').on('submit', function(event) {
			$('form').find("button[type='submit']").prop('disabled', true)

			$('input[name="title"]').val($('#title .ql-editor').html())
			$('input[name="keterangan"]').val($('#keterangan .ql-editor').html())

			event.preventDefault()

			var formData = new FormData(this)

			$.ajax({
                url: $(this).attr('action'),
                type: "POST",
                data: formData,
                contentType: false,
                processData: false
            }).done(function(response){
				formExecuted(response);

                if (response.id == 1) {
                    window.location.reload()
                }
            }).fail(function(response) {
                serverError(response.statusText, response.responseJSON.message);
            }).always(function() {
				$('form').find("button[type='submit']").prop('disabled', false)
			})
		})

		function submit_form_text() {
			$.ajax({
                url: `{{ route('admin-conference.conference.loa.update', $data->id) }}`,
                type: "POST",
                data: {
					'_token': $('input[name="_token"]').val(),
					'_method': "PUT",
					'kop': $('#kop .ql-editor').html(),
					'isi': $('#isi .ql-editor').html(),
				},
            }).done(function(response){
				formExecuted(response);

                if (response.id == 1) {
                    window.location.reload()
                }
            }).fail(function(response) {
                serverError(response.statusText, response.responseJSON.message);
            })
		}
	</script>

	{{-- Toastr --}}
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script>
		function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
			} else {
				status = 'error';
			}

			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			if (response.id == 1) {
				location.reload();
			}

			$('form').find("button[type='submit']").prop('disabled', false);
		}

		function serverError(status, message) {
			toastr['error'](message, status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}
	</script>

	{{-- Modal --}}
	<script>
		function tambah() {
			$('#modal').modal('show')
		}
	</script>

	{{-- SweetAlert --}}
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
	<script>
		// Hapus Data
		function hapus(id) {
			$('#delete-form').attr('action', `{{ url('admin-conference/conference/loa/${id}') }}`);

			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					$('#delete-form').submit();
				}
			});
		}
	</script>
@endsection