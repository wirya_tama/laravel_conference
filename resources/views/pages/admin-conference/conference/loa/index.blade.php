@extends('layout.layout')

@section('title', 'Data Conference')

@section('css')
	{{-- Datatable --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button id="tambah" title="{{ __('messages.letter of acceptance data') }}" buttonTitle="">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th data-priority="1">{{ __('messages.name') }}</th>
						<th>{{ __('messages.portal') }}</th>
						<th data-priority="2" width="10%"></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
	{{-- Datatable --}}
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin-conference.conference.loa.index') }}";
		var column = [
			{data: 'conference_nama', name: 'conference_nama'},
			{data: 'portal_nama', name: 'portal.portal_nama'},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
		</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		$('#tambah').parent().parent().hide()
	</script>

	<script>
		function ubah(id) {
			window.location.replace(`{{ url('admin-conference/conference/loa/${id}') }}`)
		}
	</script>
@endsection