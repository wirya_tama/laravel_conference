@extends('layout.layout')

@section('title', 'Tanggal Penting')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/calendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-calendar.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
@endsection

@section('content')
    <section>
        <div class="app-calendar overflow-hidden border">
            <div class="card shadow-none mb-0 rounded-0">
                <div class="card-header border-bottom py-1">
                    <p class="card-title">Data Tanggal Penting</p>
                    <button class="btn btn-primary btn-toggle-sidebar float-right" data-toggle="modal" data-target="#add-new-sidebar">
                        <span class="align-middle"><i data-feather="plus"></i> Tambah</span>
                    </button>
                    <input type="hidden" id="date" value="{{ date('Y-m-d') }}">
                </div>
                <div class="card-body mt-1 pb-0">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
        <!-- Calendar Add/Update/Delete event modal-->
        <div class="modal event-sidebar fade" id="add-new-sidebar">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content p-0">
                    <div class="modal-header mb-1">
                        <h5 class="modal-title">Menunggu...</h5>
                        <button type="button" class="close float-right" data-dismiss="modal" aria-label="Close">×</button>
                    </div>
                    <form class="event-form needs-validation" data-ajax="false">
                        @csrf
                        @if (count($conference) > 0)
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="title" class="form-label">Judul</label>
                                    <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Kegiatan" required />
                                </div>

                                <div class="form-group">
                                    <label for="select-label" class="form-label">Conference</label>
                                    <select class="select2 select-label form-control w-100" id="conference" name="conference" required>
                                        @foreach ($conference as $item)
                                            <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group position-relative">
                                    <label for="start-date" class="form-label">Tanggal Awal</label>
                                    <input type="text" class="form-control" id="tanggal-awal" name="tanggal_awal" placeholder="Tanggal Awal" required/>
                                </div>

                                <div class="form-group position-relative">
                                    <label for="end-date" class="form-label">Tanggal Akhir</label>
                                    <input type="text" class="form-control" id="tanggal-akhir" name="tanggal_akhir" placeholder="Tanggal Akhir" required/>
                                </div>
                            </div>
                            
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary add-event-btn mr-1"><i data-feather="plus"></i> Tambah</button>
                                <button type="button" class="btn btn-outline-danger btn-cancel" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
                                <button type="submit" class="btn btn-primary update-event-btn d-none mr-1"><i data-feather="edit-2"></i> Ubah</button>
                                <button type="button" class="btn btn-outline-danger btn-delete-event d-none"><i data-feather="trash"></i> Hapus</button>
                            </div>
                        @else
                            <x-kosong>Tidak ada Conference</x-kosong>
                        @endif
                    </form>
                </div>
            </div>
        </div>
        <!--/ Calendar Add/Update/Delete event modal-->
    </section>
    <!-- Full calendar end -->
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/calendar/fullcalendar.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/extensions/moment.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

    <script>
        var data = "{{ route('admin-conference.conference.tanggal-penting.data') }}";

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).on('click', '.fc-sidebarToggle-button', function (e) {
            $('.app-calendar-sidebar, .body-content-overlay').addClass('show');
        });

        $(document).on('click', '.body-content-overlay', function (e) {
            $('.app-calendar-sidebar, .body-content-overlay').removeClass('show');
        });

        document.addEventListener('DOMContentLoaded', function () {
            var calendarEl = document.getElementById('calendar'),
                eventToUpdate,
                sidebar = $('.event-sidebar'),
                addEventBtn = $('.add-event-btn'),
                cancelBtn = $('.btn-cancel'),
                updateEventBtn = $('.update-event-btn'),
                toggleSidebarBtn = $('.btn-toggle-sidebar'),
                eventTitle = $('#judul'),
                eventLabel = $('#conference'),
                startDate = $('#tanggal-awal'),
                endDate = $('#tanggal-akhir'),
                btnDeleteEvent = $('.btn-delete-event');

            // --------------------------------------------
            // On add new item, clear sidebar-right field fields
            // --------------------------------------------
            $('.add-event').on('click', function (e) {
                $('.event-sidebar').addClass('show');
                $('.sidebar-left').removeClass('show');
                $('.app-calendar .body-content-overlay').addClass('show');
            });

            // Start date picker
            if (startDate.length) {
                var start = startDate.flatpickr({
                    altFormat: 'Y-m-d',
                    onReady: function (selectedDates, dateStr, instance) {
                        if (instance.isMobile) {
                            $(instance.mobileInput).attr('step', null);
                        }
                    }
                });
            }

            // End date picker
            if (endDate.length) {
                var end = endDate.flatpickr({
                    altFormat: 'Y-m-d',
                    onReady: function (selectedDates, dateStr, instance) {
                        if (instance.isMobile) {
                            $(instance.mobileInput).attr('step', null);
                        }
                    }
                });
            }

            // Event click function
            function eventClick(info) {
                $('.modal-title').text('Ubah Tanggal Penting');
                eventToUpdate = info.event;

                sidebar.modal('show');
                addEventBtn.addClass('d-none');
                cancelBtn.addClass('d-none');
                updateEventBtn.removeClass('d-none');
                btnDeleteEvent.removeClass('d-none');

                eventTitle.val(eventToUpdate.extendedProps.judul);
                start.setDate(eventToUpdate.start, true, 'Y-m-d');
                eventToUpdate.end !== null
                ? end.setDate(eventToUpdate.end, true, 'Y-m-d')
                : end.setDate(eventToUpdate.start, true, 'Y-m-d');
                eventLabel.val(eventToUpdate.extendedProps.conference);

                $('.btn-delete-event').attr('data-value', eventToUpdate.id);
                $('.event-form').append('<input type="hidden" id="method" name="_method" value="PUT"/>');
                $('.event-form').attr('action', "/admin-conference/conference/tanggal-penting/" + eventToUpdate.id);
            }

            // Modify sidebar toggler
            function modifyToggler() {
                $('.fc-sidebarToggle-button')
                .empty()
                .append(feather.icons['menu'].toSvg({ class: 'ficon' }));
            }

            // Calendar plugins
            var calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'dayGridMonth',
                events: data,
                defaultAllDay: true,
                displayEventTime: false,
                dragScroll: true,
                dayMaxEvents: 2,
                eventResizableFromStart: true,
                customButtons: {
                    sidebarToggle: {
                        text: 'Sidebar'
                    }
                },
                headerToolbar: {
                    start: 'sidebarToggle, prev,next, title',
                    end: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
                },
                initialDate: new Date(),
                navLinks: true, // can click day/week names to navigate views
                eventClassNames: 'bg-light-primary',
                dateClick: function (info) {
                    var date = moment(info.date).format('YYYY-MM-DD');
                    resetValues();
                    sidebar.modal('show');
                    $('.modal-title').text('Tambah Tanggal Penting');
                    $('.event-form').attr('action', "/admin-conference/conference/tanggal-penting");
                    addEventBtn.removeClass('d-none');
                    cancelBtn.removeClass('d-none');
                    updateEventBtn.addClass('d-none');
                    btnDeleteEvent.addClass('d-none');
                    startDate.val(date);
                    endDate.val(date);
                },
                eventClick: function (info) {
                    eventClick(info);
                },
                datesSet: function () {
                    modifyToggler();
                },
                viewDidMount: function () {
                    modifyToggler();
                }
            });

            // Render calendar
            calendar.render();
            // Modify sidebar toggler
            modifyToggler();
            // updateEventClass();

            // Sidebar Toggle Btn
            if (toggleSidebarBtn.length) {
                toggleSidebarBtn.on('click', function () {
                    cancelBtn.removeClass('d-none');
                });
            }

            // Reset sidebar input values
            function resetValues() {
                endDate.val('');
                startDate.val('');
                eventTitle.val('');
                $('#method').remove();
            }

            // When modal hides reset input values
            sidebar.on('hidden.bs.modal', function () {
                resetValues();
            });

            // Hide left sidebar if the right sidebar is open
            $('.btn-toggle-sidebar').on('click', function () {
                $('.modal-title').text('Tambah Tanggal Penting');
                endDate.val($('#date').val());
                startDate.val($('#date').val());
                btnDeleteEvent.addClass('d-none');
                updateEventBtn.addClass('d-none');
                addEventBtn.removeClass('d-none');
                $('.app-calendar-sidebar, .body-content-overlay').removeClass('show');
            });
            
            // Tambah/Ubah Data
            $('.event-form').on('submit', function(e) {
                e.preventDefault();

                var formData = new FormData(this);

                $.ajax({
                    url: $(this).attr('action'),
                    type: "POST",
                    data: formData,
                    contentType: false,
                    processData: false
                }).done(function(response){
                    if (response.id == 1) {
                        $('.event-sidebar').modal('hide');
                    }
                    formExecuted(response);
                });
            });
            
            function formExecuted(response) {
                var status = '';
                if (response.id == 1) {
                    status = 'success';
                    calendar.refetchEvents();
                } else {
                    status = 'error';
                }
                toastr[status](response.keterangan, response.status, {
                    closeButton: true,
                    tapToDismiss: true
                });

                $('form').find("button[type='submit']").prop('disabled', false);
            }

            // Hapus Data
            $('.btn-delete-event').on('click', function(e) {
                const id = $(this).attr('data-value');
                Swal.fire({
                    title: 'Yakin ingin hapus?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                    customClass: {
                        confirmButton: 'btn btn-primary',
                        cancelButton: 'btn btn-outline-danger ml-1'
                    },
                    buttonsStyling: false
                }).then(function (result) {
                    if (result.value) {
                        var status = '';
                        $.ajax({
                            url: '{{ url("admin-conference/conference/tanggal-penting")}}/'+ id,
                            type: "POST",
                            data: {
                                _method: 'DELETE',
                            },
                        }).done(function(response){
                            if (response.id == 1) {
                                $('.event-sidebar').modal('hide');
                            }
                            formExecuted(response);
                        });
                    }
                });
            });

            $('form').submit(function() {
                $(this).find("button[type='submit']").prop('disabled', true);
            });
        });
    </script>
@endsection