@extends('layout.layout')

@section('title', 'Data Sponsor')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
        <div class="row justify-content-center">
            <div class="col-md-8">
                {{-- BEGIN: Table --}}
                <x-datatable-button title="Data Sponsor" id="tambah" buttonTitle='<i data-feather="plus"></i> Tambah'>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th data-priority="1">Nama</th>
                                <th>Conference</th>
                                <th data-priority="2" width="20%"></th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </x-datatable-button>
                {{-- END: Table --}}
            </div>
        </div>
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
			@if (count($conference) > 0)
				<form id="form" method="POST">
					@csrf
					<div class="modal-body">
						<x-form-group title="Nama">
							<input type="text" class="form-control" id="nama" name="nama" placeholder="Judul Sponsor" required/>
						</x-form-group>
						
						<x-form-group title="Conference">
							<select class="form-control" id="conference" name="conference" required>
								<option value="" hidden>-- Pilih Conference --</option>
								@foreach ($conference as $item)
									<option value="{{ $item->id }}">{{ $item->nama }}</option>
								@endforeach
							</select>
						</x-form-group>
					</div>
					
					<div class="modal-footer">
						<button type="submit" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
						<button type="submit" class="btn btn-primary mr-1" id="button-ubah"><i data-feather="edit-2"></i> Ubah</button>
						<button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
						<button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> Hapus</button>
					</div>
				</form>
			@else
				<x-kosong>Tidak ada Conference</x-kosong>
			@endif
		</x-modal>
		{{-- END: Modal --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="Menunggu..." type="normal" class="" id="fileModal">
			<form id="fileForm" method="post" enctype="multipart/form-data">
				@csrf
				@method('PUT')
				<div class="modal-body">
					<div id="fileLogo"></div>
					<x-form-group title="Ubah Logo">
						<input type="file" class="form-control-file" accept="image/*" name="logo" id="logo" required/>
					</x-form-group>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary data-submit mr-1"><i data-feather="edit-2"></i> Ubah</button>
					<button type="reset" class="btn btn-outline-danger" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
				</div>
			</form>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin-conference.conference.sponsor.data') }}";
		var column = [
			{data: 'sponsor_nama', name: 'sponsor_nama'},
			{data: 'conference_nama', name: 'conference.conference_nama'},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			$('#tambah').on('click', function() {
				$('.modal-title').text('Tambah Sponsor');
				$('#form').attr('action', "{{ url('admin-conference/conference/sponsor') }}");

				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				
				$('#modal').modal('show');
			});

			$(document).on('click', '.ubah', function() {
				$('.modal-title').text('Ubah Pengumuman');
				$('#form').attr('action', "{{ url('admin-conference/conference/sponsor') }}/" + $(this).attr('data-value'));
				$('#form').append('<input type="hidden" id="method" name="_method" value="PUT"/>');
				$('#button-hapus').attr('onClick', "hapus("+ $(this).attr('data-value') +")");

				$('#nama').val($(this).attr('data-nama'));
				$('#conference').val($(this).attr('data-conference'));

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);


				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('#method').remove();
				$('#nama').val('');
				$('#conference').val('');
			});

			$(document).on('click', '.file', function() {
				$('.modal-title').text('File Sponsor');
				$('#fileForm').attr('action', "{{ url('admin-conference/conference/sponsor') }}/" + $(this).attr('data-value'));

				$('#fileLogo').append('<img src="/images/sponsor/'+ $(this).attr('data-logo') +'" class="img-fluid">');

				$('#fileModal').modal('show');
			});

			$('#fileModal').on('hidden.bs.modal', function (e) {
				$('.modal-title').text('Menunggu...');
				
				$('#fileLogo').children().remove();
				$('#logo').val('');
			});

			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					if (response.id == 1) {
						$('#modal').modal('hide');
					}
					formExecuted(response);
				});
			});

			// Ubah File
			$('#fileForm').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					if (response.id == 1) {
						$('#fileModal').modal('hide');
					}
					formExecuted(response);
				});
			});

			$('form').submit(function() {
				$(this).find("button[type='submit']").prop('disabled', true);
			});
		});

		function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("admin-conference/conference/sponsor")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							table.ajax.reload();
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
	</script>
@endsection