@extends('layout.layout')

@section('title', 'Data Organizer')

@section('css')
    {{-- Toastr --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">

    {{-- SweetAlert --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">

    {{-- Croppie --}}
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.min.css">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: List Organizer --}}
        <button type="button" class="btn btn-success mb-2" onclick="tambah()">
            <i data-feather="plus"></i> Tambah
        </button>

        @if (count($data) > 0)
            <div class="row match-height justify-content-center">
                @foreach ($data as $item)
                    <div class="col-md-6 col-lg-4">
                        <div class="card">
                            <img class="card-img-top" src="{{ asset('images/organizer/'. $item->gambar) }}" alt="{{ $item->gambar }}"/>

                            <div class="card-body">
                                <div class="text-center">
                                    <h4 class="card-title">{{ $item->nama }}</h4>
                                    <h6 class="card-subtitle mb-2">{{ $item->conference }}</h6>

                                    <div class="btn-group">
                                        <button type="button" class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="Ubah Organizer" onclick="ubah({{ $item->id }})">
                                            <i data-feather="edit-2"></i>
                                        </button>

                                        <button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Hapus Organizer" onclick="hapus({{ $item->id }})">
                                            <i data-feather="trash"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @else
            <x-kosong>Tidak ada Organizer</x-kosong>
        @endif

		{{-- END: List Organizer --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
            @if (count($conference) > 0)
                <form id="form" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="modal-body">
                        <x-form-group title="Nama">
                            <input type="text" name="nama" placeholder="Nama Organizer" class="form-control" required>
                        </x-form-group>
                        
                        <x-form-group title="Conference">
                            <select class="form-control" id="conference" name="conference_id" required>
                                <option value="" hidden>-- Pilih Conference --</option>
                                @foreach ($conference as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </x-form-group>
                        
                        <x-form-group title="Link">
                            <input type="text" name="link" placeholder="Link Organizer" class="form-control">
                        </x-form-group>
                        
                        <div id="gambar-old" class="mb-1"></div>

                        <x-form-group title="Gambar Baru">
                            <input type="file" name="gambar" accept="image/*" class="form-control-file">
                        </x-form-group>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
                        <button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
                    </div>
                </form>
            @else
                <x-kosong>Tidak ada Conference</x-kosong>
            @endif
		</x-modal>
		{{-- END: Modal --}}
		
		{{-- BEGIN: Delete --}}
        <form id="delete-form" method="POST">
            @csrf
            @method('DELETE')
        </form>
		{{-- END: Delete --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    {{-- Modal --}}
	<script>
        function tambah() {
            $('.modal-title').text('Tambah Organizer');
            $('#form').attr('action', '{{ url("admin-conference/conference/organizer") }}');
            
            $('#gambar-old').hide();

            $('#modal').modal('show');
        }

        function ubah(id) {
            $('.modal-title').text('Ubah Organizer');
            $('#form').attr('action', `{{ url('admin-conference/conference/organizer/${id}') }}`);

            $('#form').append('<input type="hidden" name="_method" value="PUT">');
            $('#gambar-old').show();

            $.get(`{{ url('admin-conference/conference/organizer/${id}') }}`, function( data ) {
                $('input[name="nama"]').val(data.nama);
                $('select[name="conference_id"]').val(data.conference_id);
                $('input[name="link"]').val(data.link);
                $('#gambar-old').html(`
                    <label>Gambar</label>
                    <div class="text-center">
                        <img src="/images/organizer/`+ data.gambar +`" class="img-fluid" />
                    </div>
                `);

                $('#modal').modal('show');
            });
        }

        $('#modal').on('hidden.bs.modal', function () {
            $('.modal-title').text('');
            $('#form').attr('action', '');

            $('#form input[name="_method"]').remove();
            $('#form input[name!="_token"]').val('');
            $('#form select').val('');
        });
	</script>

    {{-- Form --}}
	<script type="text/javascript">
		$('form').submit(function(e) {
            e.preventDefault();

            var formData = new FormData(this);

            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                data: formData,
                contentType: false,
                processData: false
            }).done(function(response){
                if (response.id == 1) {
                    $('#modal').modal('hide');
                }
                formExecuted(response);
            }).fail(function(response) {
                serverError(response.statusText, response.responseJSON.message);
            });
		});
	</script>

    {{-- Toastr --}}
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script>
        function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
			} else {
				status = 'error';
			}

			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

            if (response.id == 1) {
				location.reload();
			}

			$('form').find("button[type='submit']").prop('disabled', false);
		}

        function serverError(status, message) {
            toastr['error'](message, status, {
                closeButton: true,
                tapToDismiss: true
            });

            $('form').find("button[type='submit']").prop('disabled', false);
        }
    </script>

    {{-- SweetAlert --}}
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
	<script>
        // Hapus Data
		function hapus(id) {
            $('#delete-form').attr('action', `{{ url('admin-conference/conference/organizer/${id}') }}`);

			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
                    $('#delete-form').submit();
				}
			});
		}
    </script>
@endsection