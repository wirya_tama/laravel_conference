@extends('layout.layout')

@section('title', 'Data Halaman Statis')

@section('css')
	{{-- Datatables --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">

	{{-- Toastr --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">

	{{-- SweetAlert --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">

	{{-- Summernote --}}
	<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
        {{-- BEGIN: Table --}}
        <x-datatable-button title="Data Halaman Statis" id="tambah" buttonTitle='<i data-feather="plus"></i> Tambah'>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th data-priority="1">Judul</th>
                        <th>Conference</th>
                        <th data-priority="2" width="15%"></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </x-datatable-button>
        {{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="Menunggu..." type="normal" class="modal-lg" id="modal">
			@if (count($conference) > 0)
				<form id="form" method="POST" enctype="multipart/form-data">
					@csrf

					<div class="modal-body">
						<x-form-group title="Judul (Maksimal 20 Karakter)">
							<input type="text" name="judul" class="form-control" placeholder="Judul Halaman Statis" required/>
						</x-form-group>

						<x-form-group title="Slug">
							<input type="text" name="slug" class="form-control" placeholder="Slug Halaman Statis" required/>
						</x-form-group>
						
						<x-form-group title="Conference">
							<select name="conference_id" class="form-control" required>
								<option value="" hidden>-- Pilih Conference --</option>

								@foreach ($conference as $item)
									<option value="{{ $item->id }}">{{ $item->nama }}</option>
								@endforeach
							</select>
						</x-form-group>
                        
                        <div id="gambar-old" class="mb-1"></div>

                        <x-form-group title="Gambar Baru">
                            <input type="file" name="gambar" accept="image/*" class="form-control-file">
                        </x-form-group>
						
						<x-form-group title="Isi">
							<textarea class="form-control" name="isi" rows="5" placeholder="Isi Halaman Statis"></textarea>
						</x-form-group>
					</div>

					<div class="modal-footer">
						<button type="submit" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
						<button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
					</div>
				</form>
			@else
				<x-kosong>Tidak ada Conference</x-kosong>
			@endif
		</x-modal>
		{{-- END: Modal --}}
		
		{{-- BEGIN: Delete --}}
        <form id="delete-form" method="POST">
            @csrf
            @method('DELETE')
        </form>
		{{-- END: Delete --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
	{{-- Datatable --}}
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
	<script>
		var link = "{{ route('admin-conference.conference.page.data') }}";
		var column = [
			{data: 'judul', name: 'judul'},
			{data: 'conference_nama', name: 'conference.conference_nama'},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>

	{{-- Summernote --}}
	<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
	<script>
		$('textarea[name="isi"]').summernote({
			height: 200,
			toolbar: [
				['style', ['style', 'bold', 'italic', 'underline']],
				['font', ['strikethrough', 'superscript', 'subscript']],
				['para', ['ul', 'ol', 'paragraph']],
				['fontsize', ['fontsize']],
			],
			styleTags: ['p','pre', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
		});
	</script>

	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

    {{-- Modal --}}
	<script>
		$('#tambah').on('click', function() {
            $('.modal-title').text('Tambah Halaman Statis');
            $('#form').attr('action', "{{ route('admin-conference.conference.page.store') }}");

            $('#gambar-old').hide();

            $('#modal').modal('show');
        })

        function ubah(id) {
            $('.modal-title').text('Ubah Halaman Statis');
            $('#form').attr('action', `{{ url('admin-conference/conference/page/${id}') }}`);

            $('#form').append('<input type="hidden" name="_method" value="PUT">');
			
            $.get(`{{ url('admin-conference/conference/page/${id}') }}`, function( data ) {
				$('input[name="judul"]').val(data.judul);
                $('select[name="conference_id"]').val(data.conference_id);
                $('input[name="slug"]').val(data.slug);
				$('textarea[name="isi"]').summernote('pasteHTML', data.isi);
				
				if (data.gambar == null) {
					$('#gambar-old').hide();
				} else {
					$('#gambar-old').html(`
						<label>Gambar</label>
						<div class="text-center">
							<img src="/images/page/`+ data.gambar +`" class="img-fluid" />
						</div>
					`);
					$('#gambar-old').show();
				}

                $('#modal').modal('show');
            });
        }

        $('#modal').on('hidden.bs.modal', function () {
            $('.modal-title').text('');
            $('#form').attr('action', '');

            $('#form input[name="_method"]').remove();
            $('#form input[name!="_token"]').val('');
            $('#form select').val('');
			$('textarea[name="isi"]').summernote('reset');
        });
	</script>

    {{-- Form --}}
	<script type="text/javascript">
		$('form').submit(function(e) {
            e.preventDefault();

			$('form').find("button[type='submit']").prop('disabled', true);

            var formData = new FormData(this);

            $.ajax({
                url: $(this).attr('action'),
                type: "POST",
                data: formData,
                contentType: false,
                processData: false
            }).done(function(response){
                if (response.id == 1) {
                    $('#modal').modal('hide');
                }
                formExecuted(response);
            }).fail(function(response) {
                serverError(response.statusText, response.responseJSON.message, response.responseJSON.line);
            });
		});
	</script>

    {{-- Toastr --}}
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script>
        function formExecuted(response) {
			var status = '';

			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
				$('.modal').modal('hide');
			} else {
				status = 'error';
			}

			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}

        function serverError(status, message, line) {
            toastr['error'](message, status +" on line "+ line, {
                closeButton: true,
                tapToDismiss: true
            });

            $('form').find("button[type='submit']").prop('disabled', false);
        }
    </script>

    {{-- SweetAlert --}}
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
	<script>
        // Hapus Data
		function hapus(id) {
            $('#delete-form').attr('action', `{{ url('admin-conference/conference/page/${id}') }}`);

			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
                    $('#delete-form').submit();
				}
			});
		}
    </script>
@endsection