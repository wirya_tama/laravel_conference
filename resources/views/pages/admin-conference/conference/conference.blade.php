@extends('layout.layout')

@section('title', 'Data Conference')

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

    {{-- Croppie --}}
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.min.css">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button id="tambah" title="Data Conference" buttonTitle=''>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th data-priority="1">Nama</th>
						<th>Portal</th>
						<th width="10%">Status</th>
						<th data-priority="2" width="15%"></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
			<form id="form">
				<div class="modal-body">
					<x-form-group title="Nama">
						<input type="text" class="form-control" id="nama" placeholder="Nama Conference" required/>
					</x-form-group>

					<x-form-group title="Tampilkan nama pada halaman depan">
						<div class="custom-control custom-radio">
							<input type="radio" id="customRadio1" name="nama_status" class="custom-control-input" value="1" required />
							<label class="custom-control-label" for="customRadio1">Ya</label>
						</div>

						<div class="custom-control custom-radio">
							<input type="radio" id="customRadio2" name="nama_status" class="custom-control-input" value="0" required />
							<label class="custom-control-label" for="customRadio2">Tidak</label>
						</div>
					</x-form-group>

					<x-form-group title="Afiliasi">
						<input type="text" class="form-control" id="afiliasi" placeholder="Afiliasi Conference"/>
					</x-form-group>

					<x-form-group title="Link">
						<input type="text" class="form-control" id="slug" title="Hanya boleh diisi dengan huruf(A-Z & a-z), angka(0-9), dan kurang(-)" pattern="[A-Za-z0-9-]*" placeholder="Link Conference" required/>
					</x-form-group>

					<x-form-group title="Tema">
						<textarea class="form-control" id="tema" placeholder="Tema Conference" rows="5"></textarea>
					</x-form-group>

					<x-form-group title="Tentang">
						<textarea class="form-control" id="tentang" placeholder="Tentang Conference" rows="5"></textarea>
					</x-form-group>

					<x-form-group title="Lokasi">
						<textarea class="form-control" id="lokasi" placeholder="Lokasi Conference" rows="5"></textarea>
					</x-form-group>

					<x-form-group title="Latitude">
						<input type="text" pattern="[0-9\.\-]*" id="latitude" title="Hanya boleh diisi dengan angka(0-9), titik(.), dan kurang(-)" class="form-control" placeholder="Latitude Conference"/>
					</x-form-group>

					<x-form-group title="Longitude">
						<input type="text" pattern="[0-9\.\-]*" id="longitude" title="Hanya boleh diisi dengan angka(0-9), titik(.), dan kurang(-)" class="form-control" placeholder="Longitude Conference"/>
					</x-form-group>

					<x-form-group title="Link Google Maps">
						<input type="text" id="google_maps" class="form-control" placeholder="Link Google Maps"/>
					</x-form-group>

					<x-form-group title="Pembayaran Abstrak">
						<div class="input-group input-group-merge mb-2">
							<div class="input-group-prepend">
								<span class="input-group-text">Rp. </span>
							</div>
							<input type="text" id="bayar_abstrak" maxlength="12" class="form-control bayar_abstrak" placeholder="Nominal Pembayaran Abstrak"/>
						</div>
					</x-form-group>

					<x-form-group title="Pembayaran Publikasi">
						<div class="input-group input-group-merge mb-2">
							<div class="input-group-prepend">
								<span class="input-group-text">Rp. </span>
							</div>
							<input type="text" id="bayar_publikasi" maxlength="12" class="form-control bayar_publikasi" placeholder="Nominal Pembayaran Publikasi"/>
						</div>
					</x-form-group>

					<x-form-group title="Contact Person 1">
						<input type="text" class="form-control" id="kontak_nama1" placeholder="Nama Contact Person"/>
						<input type="number" class="form-control" id="kontak_hp1" placeholder="No. HP Contact Person"/>
					</x-form-group>

					<x-form-group title="Contact Person 2 (Tidak Wajib Diisi)">
						<input type="text" class="form-control" id="kontak_nama2" placeholder="Nama Contact Person"/>
						<input type="number" class="form-control" id="kontak_hp2" placeholder="No. HP Contact Person"/>
					</x-form-group>

					<x-form-group title="Level">
						<select class="form-control" id="level" required>
							<option value="" hidden>-- Pilih Level Conference --</option>
							<option value="1">Nasional</option>
							<option value="2">Internasional</option>
						</select>
					</x-form-group>

					<x-form-group title="Email">
						<input type="email" class="form-control" id="email" placeholder="Email Conference"/>
					</x-form-group>

					<x-form-group title="Instagram">
						<input type="text" class="form-control" id="instagram" placeholder="Instagram Conference"/>
					</x-form-group>

					<x-form-group title="Facebook">
						<input type="text" class="form-control" id="facebook" placeholder="Facebook Conference"/>
					</x-form-group>

					<x-form-group title="Twitter">
						<input type="text" class="form-control"  id="twitter" placeholder="Twitter Conference"/>
					</x-form-group>

					<x-form-group title="Youtube">
						<input type="text" class="form-control" id="youtube" placeholder="Youtube Conference"/>
					</x-form-group>
					
					<x-form-group title="Status">
						<select class="form-control" id="status" required>
							<option value="" hidden>-- Pilih Status Conference --</option>
							<option value="2">Draft</option>
							<option value="1">Aktif</option>
							<option value="0">Tidak Aktif</option>
						</select>
					</x-form-group>

					<x-form-group title="Panduan Pembayaran">
						<div id="pembayaran"></div>
					</x-form-group>
				</div>
				
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" id="button-ubah"><i data-feather="edit-2"></i> Ubah</button>
				</div>
			</form>
		</x-modal>
		{{-- END: Modal --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="Menunggu..." type="normal" class="modal-lg" id="fileModal">
			<div class="modal-body">
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="logo-pamflet-tab" data-toggle="tab" href="#logo-pamflet" aria-controls="logo-pamflet" role="tab" aria-selected="true">Logo & Pamflet</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" id="banner-tab" data-toggle="tab" href="#banner" aria-controls="banner" role="tab" aria-selected="false">Banner</a>
					</li>

					<li class="nav-item">
						<a class="nav-link" id="template-tab" data-toggle="tab" href="#template" aria-controls="template" role="tab" aria-selected="false">Template</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="logo-pamflet" aria-labelledby="logo-pamflet-tab" role="tabpanel">
						<form id="fileForm" method="post" enctype="multipart/form-data">
							@csrf
							@method('PUT')
							<input type="hidden" name="file" value="1">
							
							<div class="row">
								<div class="col-md-6">
									<div id="fileLogo"></div>
									<x-form-group title="Ubah Logo">
										<input type="file" class="form-control-file" accept="image/*" name="logo" id="logo"/>
									</x-form-group>
								</div>
								<div class="col-md-6">
									<div id="filePamflet"></div>
									<x-form-group title="Ubah Pamflet">
										<input type="file" class="form-control-file" accept="image/*" name="pamflet" id="pamflet"/>
									</x-form-group>
								</div>
							</div>

							<div class="text-right my-1">
								<button type="submit" class="btn btn-primary data-submit mr-1"><i data-feather="edit-2"></i> Ubah</button>
								<button type="reset" class="btn btn-outline-danger" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
							</div>
						</form>
					</div>

					<div class="tab-pane" id="banner" aria-labelledby="banner-tab" role="tabpanel">
						<form id="banner-form" method="post">
							@csrf
							@method('PUT')
							<input type="hidden" name="file" value="1">

							<div class="row">
								<div class="col-lg-6">
									<label>Gambar Banner</label><br>

									<div class="text-center">
										<img id="banner-image" class="img-fluid" />
									</div>
								</div>
								<div class="col-lg-6">
									<label>Ubah Banner</label><br>
									<div class="row justify-content-center mb-2">
										<div class="col-md-8">
											<input type="hidden" name="profile_img" id="foto-anggota"/>
											<input type="file" class="form-control-file" accept="image/jpeg,jpg,png" id="upload"/>
										</div>
									</div>
									<div id="upload-input"></div>
								</div>
							</div>

							<div class="text-right my-1">
								<button type="submit" class="btn btn-primary data-submit mr-1"><i data-feather="edit-2"></i> Ubah</button>
								<button type="reset" class="btn btn-outline-danger" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
							</div>
						</form>
					</div>

					<div class="tab-pane" id="template" aria-labelledby="template-tab" role="tabpanel">
						<form id="template-form" method="post" enctype="multipart/form-data">
							@csrf
							@method('PUT')
							<div class="form-group" id="template-data"></div>

							<x-form-group title="Ubah Template (format .doc, .docx, .pdf)">
								<input type="file" name="template" class="form-control-file" accept=".doc,.docx,.pdf" required>
							</x-form-group>

							<div class="text-right my-1">
								<button type="submit" class="btn btn-primary data-submit mr-1"><i data-feather="edit-2"></i> Ubah</button>
								<button type="reset" class="btn btn-outline-danger" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</x-modal>
		{{-- END: Modal --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="Menunggu..." type="normal" class="" id="tanggalModal">
			<form id="tanggalForm" method="post">
				@csrf
				@method('PUT')
				<div class="modal-body">
					<div id="tanggalContent"></div>
					<button type="button" class="btn btn-success" id="tambah-tanggal"><i data-feather="plus"></i> Tambah</button>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary data-submit mr-1"><i data-feather="edit-2"></i> Ubah</button>
					<button type="reset" class="btn btn-outline-danger" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
				</div>
			</form>
		</x-modal>
		{{-- END: Modal --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="Menunggu..." type="normal" class="" id="publisherModal">
			<form id="publisherForm" method="post">
				@csrf
				@method('PUT')
				<div class="modal-body">
					<div id="publisherContent"></div>
					<button type="button" class="btn btn-success" id="tambah-publisher"><i data-feather="plus"></i> Tambah</button>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary data-submit mr-1"><i data-feather="edit-2"></i> Ubah</button>
					<button type="reset" class="btn btn-outline-danger" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
				</div>
			</form>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/forms/cleave/cleave.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin-conference.conference.conference.data') }}";
		var column = [
			{data: 'conference_nama', name: 'conference_nama'},
			{data: 'portal_nama', name: 'portal.portal_nama'},
			{data: 'status', name: 'status', orderable: false, searchable: false},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>

	{{-- Quill Editor --}}
	<script src="https://cdn.quilljs.com/1.3.6/quill.min.js" type="text/javascript"></script>
	<script>
		var pembayaran = new Quill('#pembayaran', {
			theme: 'snow'
		});
	</script>

	<script>
		// Date Picker
		$('.flatpickr-basic').flatpickr();

		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			$(document).on('click', '.ubah', function() {
				const id = $(this).attr('data-value');
				$('.modal-title').text('Ubah Conference');

				$.get( "{{ url('admin-conference/conference/conference') }}/"+ id, function( data ) {
					var d = JSON.parse(data);
					$('#form').attr('action', "{{ url('admin-conference/conference/conference') }}/" + id)
					$('#nama').val(d.conference_nama);
					$('input[name="nama_status"][value='+ d.conference_nama_status +']').attr('checked', true);
					$('#afiliasi').val(d.conference_afiliasi);
					$('#slug').val(d.conference_slug);
					$('#tema').val(d.conference_tema);
					$('#tentang').val(d.conference_tentang);
					$('#lokasi').val(d.conference_lokasi);
					$('#latitude').val(d.conference_latitude);
					$('#longitude').val(d.conference_longitude);
					$('#google_maps').val(d.conference_google_maps);
					$('#kontak_nama1').val(d.conference_kontak_nama1);
					$('#kontak_hp1').val(d.conference_kontak_hp1);
					$('#kontak_nama2').val(d.conference_kontak_nama2);
					$('#kontak_hp2').val(d.conference_kontak_hp2);
					$('#level').val(d.conference_level);
					$('#email').val(d.conference_email);
					$('#instagram').val(d.conference_instagram);
					$('#facebook').val(d.conference_facebook);
					$('#twitter').val(d.conference_twitter);
					$('#youtube').val(d.conference_youtube);
					$('#status').val(d.conference_status);
					$('#bayar_abstrak').val(d.conference_bayar_abstrak);
					$('#bayar_publikasi').val(d.conference_bayar_publikasi);
					pembayaran.clipboard.dangerouslyPasteHTML(d.conference_pembayaran);
				});

				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('#nama').val('');
				$('input[name="nama_status"][value=1]').attr('checked', true);
				$('#afiliasi').val('');
				$('#slug').val('');
				$('#tema').val('');
				$('#tentang').val('');
				$('#lokasi').val('');
				$('#latitude').val('');
				$('#longitude').val('');
				$('#google_maps').val('');
				$('#kontak_nama1').val('');
				$('#kontak_hp1').val('');
				$('#kontak_nama2').val('');
				$('#kontak_hp2').val('');
				$('#level').val('');
				$('#email').val('');
				$('#instagram').val('');
				$('#facebook').val('');
				$('#twitter').val('');
				$('#youtube').val('');
				$('#status').val('');
				$('#bayar_abstrak').val('');
				$('#bayar_publikasi').val('');
				pembayaran.setText('');
			});

			$(document).on('click', '.file', function() {
				const id = $(this).attr('data-value');
				$('#fileForm').attr("action", "{{ url('admin-conference/conference/conference') }}/" + id);
				$('#banner-form').attr("action", "{{ url('admin-conference/conference/conference') }}/" + id);
				$('#template-form').attr("action", "{{ url('admin-conference/conference/conference') }}/" + id);

				$.get( "{{ url('admin-conference/conference/conference') }}/"+ id, function( data ) {
					$('.modal-title').text('File Conference');
					var d = JSON.parse(data);
					$('#fileLogo').append('<img src="/images/conference/logo/'+ d.conference_logo +'" class="img-fluid">');
					$('#filePamflet').append('<img src="/images/conference/pamflet/'+ d.conference_pamflet +'" class="img-fluid">');
					$('#banner-image').attr('src', '/images/conference/banner/'+ d.conference_banner);

					if (d.conference_template == null) {
						$('#template-data').html(`<label class="form-label">File Template</label>
													<div class="text-center">														
														<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-slash mb-50" style="width:100px; height:100px; color: #EA5455;"><circle cx="12" cy="12" r="10"></circle><line x1="4.93" y1="4.93" x2="19.07" y2="19.07"></line></svg>
														<br>
														<button type="button" class="btn btn-danger">
															<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-slash"><circle cx="12" cy="12" r="10"></circle><line x1="4.93" y1="4.93" x2="19.07" y2="19.07"></line></svg>
															Tidak ada Template
														</button>
													</div>`);
					} else {
						$('#template-data').html(`<label class="form-label">File Template</label>
													<div class="text-center">
														<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text mb-50" style="width:100px; height:100px"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
														<br>
														<a class="btn btn-primary" href="/files/template/`+ d.conference_template +`" role="button">
															<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-download"><path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4"></path><polyline points="7 10 12 15 17 10"></polyline><line x1="12" y1="15" x2="12" y2="3"></line></svg>
															Unduh Template
														</a>
													</div>`);
					}
				});

				$('#fileModal').modal('show');
			});
			
			$('#fileModal').on('hidden.bs.modal', function() {
				$('.modal-title').text('Menunggu...');

				$('#logo').val('');
				$('#fileLogo').children().remove();
				$('#pamflet').val('');
				$('#filePamflet').children().remove();
				$('input[name="template"]').val('');
			});

			$(document).on('click', '.publisher', function() {
				const id = $(this).attr('data-value');
				const level = $(this).attr('data-publisher');
				$('#publisherForm').attr("action", "{{ url('admin-conference/conference/conference') }}/" + id);
				
				$.get("{{ url('admin-conference/conference/publishers/conference') }}/"+ id, function( data ) {
					$('.modal-title').text('Publisher');
					var d = JSON.parse(data);

					for (let i = 0; i < d.length; i++) {
						$('#publisherContent').append(`<div class="row mb-50">
															<div class="col-sm-10">
																<select type="text" name="publisher[`+ i +`]"  class="form-control" readonly>
																	<option value="`+ d[i].id +`">`+ d[i].publisher +`</option>
																</select>
															</div>
															<div class="col-sm-2">
																<button type="button" class="btn btn-danger hapus-publisher"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button>
															</div>
														</div>`);
						
					}

					var no = d.length + 1;
					$('#tambah-publisher').attr('onClick', 'tambahPublisher('+ no +', '+ level +')');
				});

				$('#publisherModal').modal('show');
			});

			$('#publisherModal').on('hidden.bs.modal', function() {
				$('.modal-title').text('Menunggu...');

				$('#publisherContent').children().remove();
			});

			$(document).on('click', '.hapus-publisher', function() {
				$(this).parent().parent().remove();
			});

			// Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var request = {
					_method: 'PUT',
					nama: $('#nama').val(),
					slug: $('#slug').val(),
					nama_status: $('input[name="nama_status"]:checked').val(),
					afiliasi: $('#afiliasi').val(),
					tema: $('#tema').val(),
					tentang: $('#tentang').val(),
					lokasi: $('#lokasi').val(),
					latitude: $('#latitude').val(),
					longitude: $('#longitude').val(),
					google_maps: $('#google_maps').val(),
					kontak_nama1: $('#kontak_nama1').val(),
					kontak_hp1: $('#kontak_hp1').val(),
					kontak_nama2: $('#kontak_nama2').val(),
					kontak_hp2: $('#kontak_hp2').val(),
					level: $('#level').val(),
					email: $('#email').val(),
					instagram: $('#instagram').val(),
					facebook: $('#facebook').val(),
					twitter: $('#twitter').val(),
					youtube: $('#youtube').val(),
					status: $('#status').val(),
					bayar_abstrak: bayar_abstrak.getRawValue(),
					bayar_publikasi: bayar_publikasi.getRawValue(),
					pembayaran: $('#pembayaran .ql-editor').html()
				};

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: request
				}).done(function(response){
					if (response.id == 1) {
						$('#modal').modal('hide');
					}
					formExecuted(response);
				});
			});

			$('#fileForm').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					if (response.id == 1) {
						$('#fileModal').modal('hide');
					}
					formExecuted(response);
				});
			});

			$('#publisherForm').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					if (response.id == 1) {
						$('#publisherModal').modal('hide');
					}
					formExecuted(response);
				});
			});
			
			$('#template-form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					if (response.id == 1) {
						$('#fileModal').modal('hide');
					}
					formExecuted(response);
				});
			});

			$(document).on('click', '.tanggal', function() {
				const id = $(this).attr('data-value');
				$('#tanggalForm').attr("action", "{{ url('admin-conference/conference/conference') }}/" + id);

				$.get( "{{ url('admin-conference/conference/tanggal/conference') }}/" + id, function( data ) {
					$('.modal-title').text('Tanggal Conference');
					const d = JSON.parse(data);
					for (let i = 0; i < d.length; i++) {
						if (i == 0) {
							$('#tanggalContent').append(`<div id="data-`+ i +`">
															<input type="hidden" name="tp[`+ i +`][conference]" value="`+ d[i].conference +`" required>
															<div class="form-group">
																<label for="">Judul</label>
																<input type="text" name="tp[`+ i +`][judul]" class="form-control d-inline" value="`+ d[i].judul +`" placeholder="Judul Kegiatan" required>
															</div>
															<div class="row">
																<div class="col-md-6 col-12">
																	<div class="form-group">
																		<label>Tanggal Awal</label>
																		<input type="text" class="form-control flatpickr-basic-`+ i +`" name="tp[`+ i +`][tanggal_awal]" value="`+ d[i].tanggal_awal +`" placeholder="Tanggal Awal Kegiatan" required/>
																	</div>
																</div>
																<div class="col-md-6 col-12">
																	<div class="form-group">
																		<label>Tanggal Akhir</label>
																		<input type="text" class="form-control flatpickr-basic-`+ i +`" name="tp[`+ i +`][tanggal_akhir]" value="`+ d[i].tanggal_akhir +`" placeholder="Tanggal Akhir Kegiatan" required/>
																	</div>
																</div>
															</div>
															<hr>
														</div>`);
						} else {
							$('#tanggalContent').append(`<div id="data-`+ i +`">
															<input type="hidden" name="tp[`+ i +`][conference]" value="`+ d[i].conference +`" required>
															<div class="form-group">
																<label for="">Judul</label>
																<div class="row">
																	<div class="col-sm-10 mb-50">
																		<input type="text" name="tp[`+ i +`][judul]" class="form-control d-inline" value="`+ d[i].judul +`" placeholder="Judul Kegiatan" required>
																	</div>
																	<div class="col-sm-2">
																		<button type="button" class="btn btn-danger px-1 d-inline" onclick="hapustanggal(`+ i +`)"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button>
																	</div>
																</div>
															</div>
															<div class="row">
																<div class="col-md-6 col-12">
																	<div class="form-group">
																		<label>Tanggal Awal</label>
																		<input type="text" class="form-control flatpickr-basic-`+ i +`" name="tp[`+ i +`][tanggal_awal]" value="`+ d[i].tanggal_awal +`" placeholder="Tanggal Awal Kegiatan" required/>
																	</div>
																</div>
																<div class="col-md-6 col-12">
																	<div class="form-group">
																		<label>Tanggal Akhir</label>
																		<input type="text" class="form-control flatpickr-basic-`+ i +`" name="tp[`+ i +`][tanggal_akhir]" value="`+ d[i].tanggal_akhir +`" placeholder="Tanggal Akhir Kegiatan" required/>
																	</div>
																</div>
															</div>
															<hr>
														</div>`);
						}

						$('.flatpickr-basic-' + i).flatpickr();
					}

					$('#tambah-tanggal').attr('onClick', 'tambah_tanggal('+ d.length +')');
				});

				$('#tanggalModal').modal('show');
			});

			$('#tanggalForm').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					if (response.id == 1) {
						$('#tanggalModal').modal('hide');
					}
					formExecuted(response);
				});
			});

			$('#tanggalModal').on('hidden.bs.modal', function() {
				$('.modal-title').text('Menunggu...');

				$('#tanggalContent').children().remove();
			});

			$('form').submit(function() {
				$(this).find("button[type='submit']").prop('disabled', true);
			});
		});

		function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}

		function hapustanggal(i) {
			$('#data-' + i).remove();
		}

		function tambahPublisher(i, level) {
			$('#publisherContent').append(`<div class="row mb-50">
												<div class="col-sm-10">
													<select type="text" name="publisher[`+ i +`]" id="publisher-`+ i +`"  class="form-control"></select>
												</div>
												<div class="col-sm-2">
													<button type="button" class="btn btn-danger hapus-publisher"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button>
												</div>
											</div>`);
			$.get("{{ url('admin-conference/conference/publisher/conference') }}/"+ level, function( data ) {
				var d = JSON.parse(data);
				for (let a = 0; a < d.length; a++) {
					$('#publisher-'+ i +'').append('<option value="'+ d[a].id +'">'+ d[a].pu_nama +'</option>');
				}
			});

			var no = i + 1;
			$('#tambah-publisher').attr('onClick', 'tambahPublisher('+ no +', '+ level +')');
		}

		function tambah_tanggal(i) {
			$('#tanggalContent').append(`<div id="data-`+ i +`">
											<input type="hidden" name="tp[`+ i +`][conference]" value="0" required>
											<div class="form-group">
												<label for="">Judul</label>
												<div class="row">
													<div class="col-sm-10 mb-50">
														<input type="text" name="tp[`+ i +`][judul]" class="form-control d-inline" placeholder="Judul Kegiatan" required>
													</div>
													<div class="col-sm-2">
														<button type="button" class="btn btn-danger px-1 d-inline" onclick="hapustanggal(`+ i +`)"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg></button>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6 col-12">
													<div class="form-group">
														<label>Tanggal Awal</label>
														<input type="text" class="form-control flatpickr-basic-`+ i +`" name="tp[`+ i +`][tanggal_awal]" placeholder="Tanggal Awal Kegiatan" required/>
													</div>
												</div>
												<div class="col-md-6 col-12">
													<div class="form-group">
														<label>Tanggal Akhir</label>
														<input type="text" class="form-control flatpickr-basic-`+ i +`" name="tp[`+ i +`][tanggal_akhir]" placeholder="Tanggal Akhir Kegiatan" required/>
													</div>
												</div>
											</div>
											<hr>
										</div>`);

			$('.flatpickr-basic-' + i).flatpickr();

			var a = i + 1;
			$('#tambah-tanggal').attr('onClick', 'tambah_tanggal('+ a +')');
		}

		// Datatable
		$('#tambah').hide();
		$('.dt-button').removeClass('mr-2');

		// Cleave
		var bayar_abstrak = new Cleave('.bayar_abstrak', {
			numeral: true,
      		numeralThousandsGroupStyle: 'thousand'
		});
		var bayar_publikasi = new Cleave('.bayar_publikasi', {
			numeral: true,
      		numeralThousandsGroupStyle: 'thousand'
		});
	</script>

	{{-- Croppie --}}
	<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.5/croppie.min.js"></script>
	<script type="text/javascript">
		// Anggota
		$uploadCrop = $('#upload-input').croppie({
			enableExif: true,
			viewport: {
				width: 300,
				height: 169,
				type: 'square'
			},
			boundary: {
				width: 320,
				height: 180
			}
		});

		$uploadCrop.croppie('bind', {
			url: "{{ asset('/images/conference/banner/blank.png') }}"
		});

		$('#upload').on('change', function () {
			var reader = new FileReader();
			reader.onload = function (e) {
				$uploadCrop.croppie('bind', {
					url: e.target.result
				});
			}
			reader.readAsDataURL(this.files[0]);
		});

		$('#banner-form').submit(function() {
			$uploadCrop.croppie('result', {
				type: 'canvas', 
				size: {
					width: 1360,
					heigth: 720,
					type: 'square'
				}, 
				format: "png", 
				quality: 1
			}).then(function (response) {
				toastr['success']("Banner Conference berhasil diubah", "Berhasil", {
					closeButton: true,
					tapToDismiss: true
				});
				
				$('#foto-anggota').val(response);
			});
		});
	</script>
@endsection