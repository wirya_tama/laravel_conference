@extends('layout.layout')

@section('title', 'Data Review')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable title="Data Review">
			{{-- <div class="card-body"> --}}
				<ul class="nav nav-tabs nav-justified mb-0" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="review-tab" data-toggle="tab" href="#review" aria-controls="review" role="tab" aria-selected="true">Review</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="riwayat-review-tab" data-toggle="tab" href="#riwayat-review" aria-controls="riwayat-review" role="tab" aria-selected="false">Riwayat Review</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="review" aria-labelledby="review-tab" role="tabpanel">
						<table class="table review-table table-bordered">
							<thead>
								<tr>
									<th data-priority="1">Submission</th>
									<th>Author</th>
									<th data-priority="2" width="5%"></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
					<div class="tab-pane" id="riwayat-review" aria-labelledby="riwayat-review-tab" role="tabpanel">
						<table class="table riwayat-table table-bordered">
							<thead>
								<tr>
									<th data-priority="1">Submission</th>
									<th>Author</th>
									<th data-priority="2" width="5%"></th>
								</tr>
							</thead>
							<tbody></tbody>
						</table>
					</div>
				</div>
			{{-- </div> --}}
		</x-datatable>
		{{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="Menunggu..." type="normal" class="modal-lg" id="modal">
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6 mb-1">
						<h4>Riwayat Review</h4>
						<hr>

						<ul class="timeline"></ul>
					</div>
					<div class="col-md-6">
						<form id="formReview" method="post" enctype="multipart/form-data">
							@csrf
							@method('PUT')
							<x-form-group title="Unggah hasil review (format .doc, .docx)">
								<input type="file" name="file" id="file" class="form-control-file" accept=".doc,.docx" required>
							</x-form-group>
	
							<x-form-group title="Pesan">
								<textarea class="form-control" name="pesan" id="pesan" rows="3"></textarea>
							</x-form-group>
	
							<button type="submit" class="btn btn-primary btn-block my-50"><i data-feather="check"></i> Simpan</button>
						</form>
						
						<form id="formValidasi" method="post">
							@csrf
							@method('PUT')
							<button type="submit" class="btn btn-success btn-block my-50"><i data-feather="check"></i> Validasi Review</button>
							
							<hr>
						</form>

						<h4>Author</h4>
						<hr>
						<div id="author"></div>
					</div>
				</div>
            </div>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin-conference.submission.review.data') }}";
		var riwayatlink = "{{ route('admin-conference.submission.review.riwayat-data') }}";
	</script>
	<script src="{{ asset('js/component/datatable-button-review.js') }}"></script>

	<script>
		// Modal
		$(document).ready(function() {
			$(document).on('click', '.ubah', function() {
				const id = $(this).attr('data-value');
				if ($(this).attr('data-form') == 1) {
					$('#formReview').attr('action', "{{ url('admin-conference/submission/review/review' ) }}/"+ id);
					$('#formValidasi').attr('action', "{{ url('admin-conference/submission/review/validasi' ) }}/"+ id);

					$('#formReview').attr('hidden', false);
					$('#formValidasi').attr('hidden', false);
				} else {
					$('#formReview').attr('hidden', true);
					$('#formValidasi').attr('hidden', true);
				}

				$.get( "{{ url('admin-conference/submission/review' ) }}/"+ id, function( data ) {
					const d = JSON.parse(data);
					$('.modal-title').text('Review');

					for (let i = 0; i < d['author'].length; i++) {
						$('#author').append('<input type="text" class="form-control mb-50" value="'+ d['author'][i]['nama'] +'" readonly>');
					}
					for (let review = 0; review < d['review'].length; review++) {
						if (d['review'][review]['status'] == 0) {
							var color = "primary";
							var icon = '<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>';
							var title = "Artikel";
						} else if (d['review'][review]['status'] == 1) {
							var color = "warning";
							var icon = '<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-users"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>';
							var title = "Review";
						} else {
							var color = "primary";
							var icon = '<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>';
							var title = "Revisi";
						}

						var pesan = "";
						if (d['review'][review]['pesan'] == null) {
							pesan = "";
						} else {
							pesan = d['review'][review]['pesan'];
						}

						$('.timeline').append(`<li class="timeline-item">
													<span class="timeline-point timeline-point-`+ color +`">
														`+ icon +`
													</span>
													<div class="timeline-event">
														<div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
															<h6>`+ title +`</h6>
															<span class="timeline-event-time">`+ d['review'][review]['tanggall'] +`</span>
														</div>
														<p class="mb-50">`+ pesan +`</p>
														<a class="btn btn-outline-`+ color +` btn-sm" href="{{ url('files/submission/full-paper') }}/`+ d['review'][review]['file'] +`">
															<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg> File Artikel
														</a>
													</div>
												</li>`);
					}
				});
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('.modal-title').text('Menunggu...');
				$('#author').children().remove();
				$('.timeline').children().remove();
				$('#file').val('');
				$('#pesan').val('');
			});

			// Ubah Data
			$('#formReview').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					if (response.id == 1) {
						$('#modal').modal('hide');
					}
					formExecuted(response);
				});
			});

			// Ubah Data
			$('#formValidasi').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					if (response.id == 1) {
						$('#modal').modal('hide');
					}
					formExecuted(response);
				});
			});
		});

		function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
				riwayattable.ajax.reload();
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}

		// Disable Button Submit
		$('form').submit(function() {
			$(this).find("button[type='submit']").prop('disabled', true);
		});

		// Datatable Button
		$('#tambah').hide();
		$('.dt-button').removeClass('mr-2');
	</script>
@endsection