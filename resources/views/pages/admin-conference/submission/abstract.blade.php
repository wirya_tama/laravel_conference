@extends('layout.layout')

@section('title', 'Data Book of Abstract')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button title="Data Book of Abstract" id="tambah" buttonTitle='<i data-feather="plus"></i> Tambah'>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th data-priority="1">Conference</th>
						<th>Portal</th>
						<th data-priority="2" width="5%"></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin-conference.submission.abstract.data') }}";
		var column = [
			{data: 'conference_nama', name: 'conference_nama'},
			{data: 'portal_nama', name: 'portal.portal_nama'},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		// Modal
		$(document).ready(function() {
			$(document).on('click', '.ubah', function() {
				$.get( "{{ url('admin/master/user') }}/"+ id, function( data ) {
                    var d = JSON.parse(data);
					// $('.modal-title').text('Detail');
	
					$('#submission').val(1);
					$('#abstrak').val('Contoh Abstrak');
	
					$('#modal').modal('show');
				});
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('.modal-title').text('Menunggu...');

				$('input').val('');
			});
		});

		$('#tambah').remove();
		$('.dt-button').removeClass('mr-2');
	</script>
@endsection