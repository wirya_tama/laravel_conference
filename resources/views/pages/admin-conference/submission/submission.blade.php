@extends('layout.layout')

@section('title', 'Data Submission')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button title="Data Submission" buttonTitle='<i data-feather="plus"></i> Tambah' id="tambah">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th data-priority="1">Judul</th>
						<th>Conference</th>
						<th>Status</th>
						<th data-priority="2"></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Contoh Submission</td>
						<td>Contoh Conference</td>
						<td class="text-center"><div disabled class="badge badge-md badge-success">Aktif</div></td>
						<td class="text-center">
							<div class="btn-group">
								<button type="button" class="btn btn-warning ubah"><i data-feather="edit-2"></i></button>
								<button type="button" class="btn btn-danger"><i data-feather="trash"></i></button>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
			<div class="modal-body">
				<x-form-group title="Judul">
					<input type="text" class="form-control" id="judul" placeholder="Judul Submission" required/>
                </x-form-group>
				
				<x-form-group title="Conference">
					<select class="form-control" id="conference" required>
						<option value="" hidden>-- Pilih Conference --</option>
						<option value="1">Contoh Conference</option>
					</select>
				</x-form-group>
                
				<x-form-group title="Abstrak">
					<textarea class="form-control" id="abstrak" placeholder="Abstrak Submission" rows="5" required></textarea>
				</x-form-group>
				
				<x-form-group title="Keyword">
					<input type="text" class="form-control" id="keyword" placeholder="Keyword Submission (Dipisah dengan tanda ;)" required/>
                </x-form-group>
                
                <x-form-group title="Submit">
					<select class="form-control" id="submitter" required>
						<option value="" hidden>-- Pilih Submitter --</option>
						<option value="1">Aktif</option>
						<option value="2">Tidak Aktif</option>
					</select>
					<input type="text" class="form-control flatpickr-basic" id="tanggal-submit" placeholder="Tanggal Submit" required/>
				</x-form-group>
                
                <x-form-group title="Status">
					<select class="form-control" id="status" required>
						<option value="" hidden>-- Pilih Status --</option>
						<option value="2">Submit Abstract</option>
						<option value="1">Payment Abstract</option>
						<option value="1">Upload Full Paper</option>
						<option value="1">Review</option>
						<option value="1">Payment Publish</option>
						<option value="1">Publish</option>
					</select>
				</x-form-group>
            </div>
            
			<div class="modal-footer">
				<button type="button" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Simpan</button>
				<button type="button" class="btn btn-primary mr-1" id="button-ubah"><i data-feather="edit-2"></i> Ubah</button>
				<button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
				<button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> Hapus</button>
			</div>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = null;
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
    <script>
		// Date Picker
		$('.flatpickr-basic').flatpickr();

		// Modal
		$(document).ready(function() {
			$('#tambah').on('click', function() {
				$('.modal-title').text('Tambah Submission');

				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				
				$('#modal').modal('show');
			});

			$('.ubah').on('click', function() {
				$('.modal-title').text('Ubah Submission');

				$('#judul').val('Judul Submission');
				$('#conference').val(1);
				$('#abstrak').val('Contoh Abstrak');
				$('#keyword').val('keyword');
				$('#submitter').val(1);
				$('#tanggal-submit').val('2021-02-09');
				$('#status').val(2);

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);


				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('#judul').val('');
				$('#conference').val('');
				$('#abstrak').val('');
				$('#keyword').val('');
				$('#submitter').val('');
				$('#tanggal-submit').val('');
				$('#status').val('');
			});
		});
	</script>
@endsection