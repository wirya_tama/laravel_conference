@extends('layout.layout')

@section('title', 'Pembayaran Submission')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button title="Data Submission" buttonTitle='' id="tambah">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th data-priority="1">Judul</th>
						<th>Conference</th>
						<th>Author</th>
						<th width="15%">Abstrak</th>
						<th width="15%">Publikasi</th>
						<th data-priority="2" width="5%"></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="Menunggu..." type="normal" class="modal-lg" id="modal">
			<form id="form" method="POST">
				@csrf
				@method('PUT')
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<x-form-group title="Judul">
								<br><p id="judul"></p>
							</x-form-group>
							
							<x-form-group title="Jenis Pembayaran">
								<br><p id="jenis"></p>
							</x-form-group>
							
							<x-form-group title="Biaya">
								<br><p id="biaya"></p>
							</x-form-group>
							
							<x-form-group title="Bukti Pembayaran">
								<br><div id="bukti-bayar"></div>
							</x-form-group>
						</div>
						<div class="col-md-6">
							<x-form-group title="Tanggal Transfer">
								<br><p id="tanggal"></p>
							</x-form-group>
							
							<x-form-group title="Nominal Transfer">
								<br><p id="nominal"></p>
							</x-form-group>
							
							<x-form-group title="Nama Rekening Pengirim">
								<br><p id="nama"></p>
							</x-form-group>
							
							<x-form-group title="Bank Tujuan">
								<br><p id="bank"></p>
							</x-form-group>
							
							<x-form-group title="No. Tagihan/Invoice">
								<br><p id="invoice"></p>
							</x-form-group>
							
							<x-form-group title="Pesan Tambahan">
								<br><p id="pesan"></p>
							</x-form-group>
						</div>
					</div>
				</div>
				
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary mr-1" id="button-tambah"><i data-feather="check"></i> Validasi</button>
					<button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
				</div>
			</form>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin-conference.submission.pembayaran.data') }}";
		var column = [
			{data: 'submission_judul', name: 'submission_judul'},
			{data: 'conference', name: 'conference', orderable: false, searchable: false},
			{data: 'nama', name: 'nama', orderable: false, searchable: false},
			{data: 'status_abstrak', name: 'status_abstrak', orderable: false, searchable: false},
			{data: 'status_publikasi', name: 'status_publikasi', orderable: false, searchable: false},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>

    <script>
		// Modal
		$(document).ready(function() {
			$(document).on('click', '.validasi', function() {
				const id = $(this).attr('data-value');
				const pembayaran = $(this).attr('data-pembayaran')

				$.get( "{{ url('admin-conference/submission/pembayaran') }}/"+ id, function( data ) {
					$('#form').attr('action', "{{ url('admin-conference/submission/pembayaran/') }}/"+ id);

					if (pembayaran == 1) {
						let abstrak_pesan = "";
						if (data.submission_pembayaran_abstrak_pesan == null) {
							abstrak_pesan = "-";
						} else {
							abstrak_pesan = data.submission_pembayaran_abstrak_pesan;
						}

						$('.modal-title').text('Pembayaran Abstrak');
						$('#jenis').text('Abstrak');
						$('#biaya').text("Rp. "+ data.conference_bayar_abstrak);
						$('#bukti-bayar').html(`<img src="{{ url('images/submission/abstrak/`+ data.submission_pembayaran_abstrak_file +`') }}" class="img-thumbnail" width="80%">`);
						$('#tanggal').text(data.submission_pembayaran_abstrak_tanggal);
						$('#nominal').text("Rp. "+ data.submission_pembayaran_abstrak_nominal);
						$('#nama').text(data.submission_pembayaran_abstrak_rekening);
						$('#bank').text(data.submission_pembayaran_abstrak_bank);
						$('#invoice').text(data.submission_pembayaran_abstrak_invoice);
						$('#pesan').text(abstrak_pesan);
					} else {
						let publikasi_pesan = "";
						if (data.submission_pembayaran_publikasi_pesan == null) {
							publikasi_pesan = "-";
						} else {
							publikasi_pesan = data.submission_pembayaran_publikasi_pesan;
						}
						$('.modal-title').text('Pembayaran Publikasi');
						$('#jenis').text('Publikasi');
						$('#biaya').text("Rp. "+ data.conference_bayar_publikasi);
						$('#bukti-bayar').html(`<img src="{{ url('images/submission/publikasi/`+ data.submission_pembayaran_publikasi_file +`') }}" class="img-thumbnail" width="80%">`);
						$('#tanggal').text(data.submission_pembayaran_publikasi_tanggal);
						$('#nominal').text("Rp. "+ data.submission_pembayaran_publikasi_nominal);
						$('#nama').text(data.submission_pembayaran_publikasi_rekening);
						$('#bank').text(data.submission_pembayaran_publikasi_bank);
						$('#invoice').text(data.submission_pembayaran_publikasi_invoice);
						$('#pesan').text(publikasi_pesan);
					}

					$('#judul').text(data.submission_judul);
				});

				$('#modal').modal('show');
			});

			// Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					if (response.id == 1) {
						$('#modal').modal('hide');
					}
					formExecuted(response);
				});
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('.modal-title').text('Menunggu...');
				$('#judul').text('');
				$('#jenis').text('');
				$('#nominal').text('');
				$('#bukti-bayar').children().remove();
			});

			$('form').submit(function() {
				$(this).find("button[type='submit']").prop('disabled', true);
			});

			$('#tambah').hide();
			$('.dt-button').removeClass('mr-2');
		});

		function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}
	</script>
@endsection