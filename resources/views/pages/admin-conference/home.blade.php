@extends('layout.layout')

@section('title', 'Home')

@section('content')
    {{-- BEGIN: Card Statistic --}}
    <section>
        <div class="row">
            <div class="col-lg-3 col-sm-6 col-12">
                <x-card-statistic title="{{ count($data) }}" subtitle="Jumlah Conference" color="primary" icon="cloud"/>
            </div>
        </div>
    </section>
    {{-- END: Card Statistic --}}

    {{-- BEGIN: Card Statistic --}}
    <section>
        @if (count($data) > 0)
            <h4>Kegiatan Terbaru</h4>
            {{-- BEGIN: Card --}}
            <div class="card bg-primary text-white">
                <div class="card-body pb-1">
                    <h4 class="card-title text-white mb-0">
                        <img src="{{ asset('images/logo.jpg') }}" class="img-fluid d-inline" style="height: 45px" alt="Brand logo">
                        {{ $data[0]->conference }}
                    </h4>

                    <hr>

                    <div class="card-body row py-0">
                        <div class="d-inline pr-2 mr-2 mb-1">
                            <p class="font-weight-bold">Tanggal</p>
                            {{ date('d/m/Y', strtotime($data[0]->tanggal)) }}
                        </div>

                        <div class="d-inline pr-2 mr-2 mb-1">
                            <p class="font-weight-bold">Portal</p>
                            {{ $data[0]->portal }}
                        </div>
                    </div>
                </div>
            </div>
            {{-- END: Card --}}

            @if (count($data) > 1)
                <h4>Conference Lainnya</h4>
                {{-- BEGIN: Card --}}
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            {{-- BEGIN: Table --}}
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr class="text-center">
                                        <th>Nama</th>
                                        <th>Portal</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data as $key => $item)
                                        @if ($key > 0 && $key < 6)
                                            <tr>
                                                <td>{{ $item->conference }}</td>
                                                <td>{{ $item->portal }}</td>
                                                <td class="text-center">{{ date('d-m-Y', strtotime($item->tanggal)) }}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                            {{-- END: Table --}}
                        </div>
                    </div>

                    @if (count($data) > 5)
                        <div class="card-footer py-1">
                            <a class="btn btn-primary float-right" href="#">Lihat lainnya</a>
                        </div>
                    @endif
                </div>
                {{-- END: Card --}}
            @endif
        @else
            <x-kosong>Tidak Ada Conference</x-kosong>
        @endif
    </section>
    {{-- END: Card Statistic --}}
@endsection

@section('js')
    <script>
        $('.content-header').hide();
    </script>
@endsection