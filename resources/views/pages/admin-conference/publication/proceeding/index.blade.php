@extends('layout.layout')

@section('title')
{{ __('messages.proceeding') }}
@endsection

@section('vendor-css')
{{-- Datatables --}}
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">

{{-- Toastr --}}
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">

{{-- Select2 --}}
<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/forms/select/select2.min.css') }}">

{{-- SweetAlert --}}
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
@endsection

@section('css')
{{-- Toastr --}}
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
@endsection

@section('content')
{{-- Tabel --}}
<div class="card">
    <div class="card-header border-bottom d-flex justify-content-between">
        <h6 class="card-title text-capitalize">{{ __('messages.data.proceeding') }}</h6>

        <button type="button" class="btn btn-primary" onclick="tambah()"><i data-feather="plus"></i> {{ __('messages.add') }}</button>
    </div>

    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th>No</th>
                    <th>{{ __('messages.title') }}</th>
                    <th>{{ __('messages.conference') }}</th>
                    <th>{{ __('messages.date') }}</th>
                    <th style="width: 12px"></th>
                </tr>
            </thead>
        </table>
    </div>
</div>

{{-- Modal --}}
<section>
    <div class="modal fade" id="modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form id="form" action="{{ route('admin-conference.publication.proceeding.store') }}" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="post">

                    <div class="modal-body">
                        <div class="mb-1">
                            <label>{{ __('messages.title') }}</label>
                            <input type="text" name="title" class="form-control" required>
                        </div>

                        <div class="mb-1">
                            <label>{{ __('messages.slug') }}</label>
                            <input type="text" name="slug" class="form-control" required>
                        </div>

                        <div class="mb-1">
                            <label>{{ __('messages.conference') }}</label>
                            <select name="conference_id" class="form-control select2" required></select>
                        </div>

                        <div class="mb-1">
                            <label>{{ __('messages.editor') }}</label>
                            <textarea name="editor" class="form-control"></textarea>
                        </div>

                        <div class="mb-1">
                            <label>{{ __('messages.volume') }}</label>
                            <input type="text" name="volume" class="form-control">
                        </div>

                        <div class="mb-1">
                            <label>ISSN</label>
                            <input type="text" name="issn" class="form-control">
                        </div>

                        <div class="mb-1">
                            <label>ISBN</label>
                            <input type="text" name="isbn" class="form-control">
                        </div>

                        <div class="mb-1">
                            <label>{{ __('messages.date') }}</label>
                            <input type="date" name="tanggal" class="form-control" required>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">{{ __('messages.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <form method="post" id="form-proceeding">
        @csrf
        @method('DELETE')
    </form>
</section>
{{-- /Modal --}}
@endsection

@section('vendor-js')
{{-- Datatables --}}
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>

{{-- Toastr --}}
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>

{{-- Select2 --}}
<script src="{{ asset('/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>

{{-- SweetAlert --}}
<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
@endsection

@section('js')
{{-- Datatables --}}
<script>
    var table = $('table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin-conference.publication.proceeding.index') }}",
        columns: [
            { data: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'title', name: 'title' },
            { data: 'conference.nama', name: 'conference.nama' },
            { data: 'tanggal', name: 'tanggal' },
            { data: 'action', name: 'action', orderable: false, searchable: false }
        ],
        dom: '<"d-flex justify-content-between align-items-center mx-1 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-1 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    })
</script>

{{-- Toastr --}}
<script>
    function toastrStatus(icon, status, message, data) {
        toastr[icon](`<b>${message}</b><br>${data}`, status, {
            closeButton: true
        });

        $('form').find("button[type='submit']").prop('disabled', false);
    }
</script>

{{-- Select2 --}}
<script>
    $(`.select2`).select2({
        placeholder: "-- {{ __('messages.choose.conference') }}",
        width: '100%',
        ajax: {
            url: "{{ route('admin-conference.publication.proceeding.dropdown') }}",
            data: function (params) {
                var query = {
                    search: params.term,
                    page: params.page || 1
                }

                return query;
            }
        }
    })
</script>

{{-- SweetAlert --}}
<script>
    // Hapus Data
    function hapus(id) {
        $('#form-proceeding').attr("action", `{{ url('admin-conference/publication/proceeding/${id}') }}`)

        Swal.fire({
            title: 'Yakin ingin hapus?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya',
            cancelButtonText: 'Tidak',
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-outline-danger ml-1'
            },
            buttonsStyling: false
        }).then(function (result) {
            if (result.value) {
                $('#form-proceeding').submit()
            }
        });
    }
</script>

{{-- Script --}}
<script>
    // Modal
    function tambah() {
        $('.modal input').not('.modal input[name="_token"]').val('')
        $('.modal select').children().remove()
        $('.modal textarea').val('')

        $('#form').attr('action', "{{ route('admin-conference.publication.proceeding.store') }}")
        $('.modal input[name="_method"]').val('POST')
        $('.modal-title').text("{{ __('messages.add') }} {{ __('messages.proceeding') }}")

        $('.modal').modal('show')
    }
    
    function ubah(id) {
        $('.modal select').children().remove()

        $('#form').attr('action', `{{ url('admin-conference/publication/proceeding/${id}') }}`)
        $('.modal-title').text("{{ __('messages.edit') }} {{ __('messages.proceeding') }}")
        $('.modal input[name="_method"]').val('PUT')

        $.get(`{{ url('admin-conference/publication/proceeding/${id}/edit') }}`, function(data) {
            $('.modal input[name="title"]').val(data?.data?.title)
            $('.modal input[name="slug"]').val(data?.data?.slug)
            $('.modal select[name="conference_id"]').html(`<option value="${data?.data?.conference.id}">${data?.data?.conference.nama}</option>`)
            $('.modal textarea[name="editor"]').val(data?.data?.editor)
            $('.modal input[name="volume"]').val(data?.data?.volume)
            $('.modal input[name="issn"]').val(data?.data?.issn)
            $('.modal input[name="isbn"]').val(data?.data?.isbn)            
            $('.modal input[name="tanggal"]').val(data?.data?.tanggal)
        })

        $('.modal').modal('show')
    }

    $('.modal').on('shown.bs.modal', function () {
        $('input[name="title"]').focus()
    })
    // /Modal

    // Form
    $('form').on('submit', function (e) {
        e.preventDefault()
        $('form').find("button[type='submit']").prop('disabled', true)
        var formData = new FormData(this)

        $.ajax({
            url: $(this).attr('action'),
            type: "POST",
            data: formData,
            contentType: false,
            processData: false
        }).done(function(response) {
            toastrStatus('success', `{{ __('messages.success') }}`, response.message, "")
            table.ajax.reload()
            $('#modal').modal('hide')
            response.data.id && detail(response.data.id)
        }).fail(function(response) {
            toastrStatus('error', `{{ __('messages.failed') }}`, response.responseJSON.message, response.responseJSON.data)
        })
    })

    // Redirect
    function detail(id) {
        window.location.href = `{{ url('admin-conference/publication/proceeding/${id}') }}`
    }
</script>
@endsection

