@extends('layout.layout')

@section('title')
{{ __('messages.proceeding detail') }}
@endsection

@section('vendor-css')
{{-- Datatables --}}
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">

{{-- Toastr --}}
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
@endsection

@section('css')
{{-- Toastr --}}
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md">
        <div class="card">
            <div class="card-header border-bottom">
                <h6 class="card-title">Publikasi</h6>
            </div>
        
            <div class="table-responsive">
                <table class="table" id="submission">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

        <form action="{{ route('admin-conference.publication.proceeding-detail.update', $proceeding->id)}}" method="post" id="form-submission">
            @csrf
            @method('PUT')
            <input type="hidden" name="submission_id">
        </form>
    </div>

    <div class="col-md">
        <div class="card">
            <div class="card-header border-bottom">
                <h6 class="card-title">Terdaftar Publikasi</h6>
            </div>

            <div class="table-responsive">
                <table class="table" id="proceeding">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>

        <form method="post" id="form-proceeding">
            @csrf
            @method('DELETE')
        </form>
    </div>
</div>

@endsection

@section('vendor-js')
{{-- Datatables --}}
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>

{{-- Toastr --}}
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
@endsection

@section('js')
{{-- Datatables --}}
<script>
    // Proceeding
    var table_proceeding = $('#proceeding').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin-conference.publication.proceeding-detail.show', $proceeding->id) }}",
        columns: [
            { data: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'submission.submission_judul', name: 'submission.submission_judul' },
            { data: 'action', name: 'action', orderable: false, searchable: false }
        ],
        dom: '<"d-flex justify-content-between align-items-center mx-1 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-1 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        pagingType: "simple",
    })

    // Submission
    var table_submission = $('#submission').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin-conference.publication.proceeding-detail.submission', $proceeding->id) }}",
        columns: [
            { data: 'DT_RowIndex', orderable: false, searchable: false },
            { data: 'submission_judul', name: 'submission_judul' },
            { data: 'action', name: 'action', orderable: false, searchable: false }
        ],
        dom: '<"d-flex justify-content-between align-items-center mx-1 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-1 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        pagingType: "simple",
    })
</script>

{{-- Toastr --}}
<script>
    function toastrStatus(icon, status, message, data) {
        toastr[icon](`<b>${message}</b><br>${data}`, status, {
            closeButton: true
        });

        $('form').find("button[type='submit']").prop('disabled', false);
    }
</script>

{{-- Script --}}
<script>
    // Submit
    function tambah(id) {
        $('#form-submission input[name="submission_id"]').val(id)
        $('#form-submission').submit()
    }
    function hapus(id) {
        $('#form-proceeding').attr("action", `{{ url('admin-conference/publication/proceeding-detail/${id}') }}`)
        $('#form-proceeding').submit()
    }

    // Form
    $('form').on('submit', function (e) {
        e.preventDefault()
        $('button').attr('disabled', true)
        var formData = new FormData(this)

        $.ajax({
            url: $(this).attr('action'),
            type: "POST",
            data: formData,
            contentType: false,
            processData: false
        }).done(function(response) {
            toastrStatus('success', `{{ __('messages.success') }}`, response.message, "")
        }).fail(function(response) {
            toastrStatus('error', `{{ __('messages.failed') }}`, response.responseJSON.message, response.responseJSON.data)
        }).always(function() {
            $('button').attr('disabled', false)
            table_submission.ajax.reload()
            table_proceeding.ajax.reload()
        })
    })

    // Redirect
    function detail(id) {
        window.location.href = `{{ url('admin-conference/publication/submission/${id}') }}`
    }
</script>
@endsection

