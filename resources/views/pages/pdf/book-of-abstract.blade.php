<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<!-- BEGIN: Head-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="author" content="PT. ARCOM Bali">
    <title>{{ $conference->nama }} | {{ config('app.name') }}</title>
    <link rel="apple-touch-icon" href="{{ asset('images/logo.jpg') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('images/logo.jpg') }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab:wght@300;400;700&display=swap" rel="stylesheet">

    <!-- BEGIN: Vendor CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/vendors.min.css') }}">
    <!-- END: Vendor CSS-->

    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/bootstrap-extended.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/colors.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/components.min.css') }}">

    <!-- BEGIN: Page CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/pages/app-invoice-print.min.css') }}">
    <!-- END: Page CSS-->

    <!-- BEGIN: Custom CSS-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/pdf/style.css') }}">
    <!-- END: Custom CSS-->
</head>
<!-- END: Head-->

<!-- BEGIN: Body-->
<body class="vertical-layout vertical-menu-modern blank-page navbar-floating footer-static   menu-collapsed" data-open="click" data-menu="vertical-menu-modern" data-col="blank-page">
    <!-- BEGIN: Content-->
    <div class="app-content content ">
        <div class="content-overlay"></div>
        <div class="header-navbar-shadow"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                @foreach ($data['data'] as $item)
                    <div class="invoice-print p-3">
                        <h2 class="text-center">{{ $item['judul'] }}</h2>
                        <p class="text-center mb-0"> @foreach ($item['author'] as $author){{ $loop->first ? $author['nama'] : ', '.$author['nama']}}@endforeach</p>
                        <h5 class="judul-abstrak my-2">Abstract</h5>
                        <p class="abstrak">{{ $item['abstrak_inggris'] }}</p>
                        <p><strong>Keywords : </strong>@foreach ($item['kata_kunci_inggris'] as $kata_kunci_inggris){{ $loop->first ? $kata_kunci_inggris['kata_kunci'] : ','.$kata_kunci_inggris['kata_kunci'] }}@endforeach
                        </p>
                        @if ($data['level'] == 1 && $item['abstrak_indonesia'] != NULL)
                            <h5 class="judul-abstrak my-2">Abstrak</h5>
                            <p class="abstrak">{{ $item['abstrak_indonesia'] }}</p>
                            <p><strong>Kata Kunci : </strong>@foreach ($item['kata_kunci_indonesia'] as $kata_kunci_indonesia){{ $loop->first ? $kata_kunci_indonesia['kata_kunci'] : ', '.$kata_kunci_indonesia['kata_kunci'] }}@endforeach</p>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- END: Content-->

    <!-- BEGIN: Vendor JS-->
    <script src="{{ asset('app-assets/vendors/js/vendors.min.js') }}"></script>
    <!-- BEGIN Vendor JS-->
    
    <!-- BEGIN: Theme JS-->
    <script src="{{ asset('app-assets/js/core/app-menu.min.js') }}"></script>
    <script src="{{ asset('app-assets/js/core/app.min.js') }}"></script>
    <!-- END: Theme JS-->

    <script>
        $(window).on('load', function() {
            if (feather) {
                feather.replace({
                    width: 14,
                    height: 14
                });
            }
        });

        window.print();
    </script>
</body>
<!-- END: Body-->

</html>