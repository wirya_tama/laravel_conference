@extends('layout.pengaturan.layout')

@section('title', 'Pengaturan')

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
@endsection

@section('content')
	<section id="pengaturan">
        <div class="row">
            <div class="col-md-3 col-sm-12">
                <ul class="nav nav-pills flex-column">
                    <li class="nav-item">
                        <a class="nav-link active" id="stacked-pill-1" data-toggle="pill" href="#vertical-pill-1" aria-expanded="true">
                            <i data-feather="user"></i> Biodata
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="stacked-pill-2" data-toggle="pill" href="#vertical-pill-2" aria-expanded="false">
                            <i data-feather="lock"></i> Ubah Password
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('panel') }}">
                            <i data-feather="chevron-left"></i> Kembali ke Panel
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-9 col-sm-12">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="vertical-pill-1" aria-labelledby="stacked-pill-1" aria-expanded="true">
                        <x-card-body>
                            <h3 class="font-weight-bold">Biodata</h3>
                            <hr>
                            
                            <!-- users edit media object start -->
                            <div class="media mb-2">
                                <img src="{{ asset('images/profil/'. $data->gambar) }}" id="gambar" alt="{{ $data->name }}" class="user-avatar users-avatar-shadow rounded mr-2 my-25 cursor-pointer" height="90" width="90" />
                                <div class="media-body mt-50">
                                    <h4 id="nama-user">{{ $data->name }}</h4>
                                    <div class="col-12 d-flex mt-1 px-0">
                                        <label class="btn btn-primary mr-75 mb-0" for="change-picture">
                                            <span class="d-none d-sm-block">Change</span>
                                            <form id="form-biodata-gambar" action="{{ route('pengaturan.biodata', $data->id) }}" method="post" enctype="multipart/form-data">
                                                @csrf
                                                @method('PUT')
                                                <input class="form-control" type="file" id="change-picture" name="gambar" hidden accept="image/*" onchange="uploadGambar()"/>
                                            </form>
                                            <span class="d-block d-sm-none">
                                                <i class="mr-0" data-feather="edit"></i>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <!-- users edit media object ends -->
                            <form id="form-biodata" action="{{ route('pengaturan.biodata', $data->id) }}" method="POST">
                                @csrf
                                @method('PUT')
                                
                                <div class="row">
                                    <div class="col-md-6">
                                        <x-form-group title="Nama">
                                            <input type="text" class="form-control" name="name" placeholder="Nama User" value="{{ $data->name }}" required>
                                        </x-form-group>
                                    </div>

                                    <div class="col-md-6">
                                        <x-form-group title="No Induk">
                                            <input type="text" class="form-control" name="no_induk" pattern="[0-9]+" placeholder="No Induk User" value="{{ $data->no_induk }}" maxlength="15">
                                        </x-form-group>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <x-form-group title="Email">
                                            <input type="email" class="form-control" name="email" placeholder="Email User" value="{{ $data->email }}" required>
                                        </x-form-group>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <x-form-group title="Kewarganegaraan">
                                            <select class="form-control" name="kewarganegaraan" required>
                                                @foreach ($kewarganegaraan as $item)
                                                    <option value="{{ $item->id }}" {{ $data['data_user']->ke_id == $item->id ? 'selected' : '' }}>{{ $item->ke_nama }}</option>
                                                @endforeach
                                            </select>
                                        </x-form-group>
                                    </div>

                                    <div class="col-md-6">
                                        <x-form-group title="Afiliasi">
                                            <input type="text" class="form-control" name="afiliasi" placeholder="Afiliasi User" value="{{ $data['data_user']->du_afiliasi }}">
                                        </x-form-group>
                                    </div>

                                    <div class="col-md-6">
                                        <x-form-group title="No Telepon">
                                            <input type="text" class="form-control" pattern="[0-9]+" name="telepon" placeholder="No HP User (Maksimal diisi 13 digit | Diisi dengan angka)" value="{{ $data['data_user']->du_telepon }}" maxlength="13">
                                        </x-form-group>
                                    </div>
                                    
                                    <div class="col-md-6">
                                        <x-form-group title="Jenis Kelamin">
                                            <select class="form-control" name="jenis_kelamin" required>
                                                <option value="1" {{ $data['data_user']->du_jeniskelamin == 1 ? 'selected' : '' }}>Laki-laki</option>
                                                <option value="0" {{ $data['data_user']->du_jeniskelamin == 0 ? 'selected' : '' }}>Perempuan</option>
                                            </select>
                                        </x-form-group>
                                    </div>

                                    <div class="col-md-6">
                                        <x-form-group title="Alamat">
                                            <textarea class="form-control" name="alamat" cols="30" rows="3" placeholder="Alamat User">{{ $data['data_user']->du_alamat }}</textarea>
                                        </x-form-group>
                                    </div>
                                </div>
    
                                <div class="row">
                                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                        <button type="submit" class="btn btn-primary">Ubah Biodata</button>
                                    </div>
                                </div>
                            </form>
                        </x-card-body>
                    </div>
                    <div class="tab-pane" id="vertical-pill-2" role="tabpanel" aria-labelledby="stacked-pill-2" aria-expanded="false">
                        <x-card-body>
                            <h3 class="font-weight-bold">Ubah Password</h3>
                            <hr>
    
                            <form id="form-password" action="{{ route('pengaturan.password', $data->id) }}" method="POST">
                                @csrf
                                @method('PUT')

                                <div class="form-group">
                                    <div class="controls">
                                        <label for="password-baru">Password Baru</label>
                                        <input type="password" class="form-control" name="password" placeholder="********" minlength="8" required>
                                        <small id="helpId" class="text-muted">Password minimal 8 karakter</small>
                                    </div>
                                </div>
    
                                <div class="form-group">
                                    <div class="controls">
                                        <label for="konfirmasi-password-baru">Konfirmasi Password Baru</label>
                                        <input type="password" class="form-control" name="konfirmasi_password" placeholder="********" minlength="8" required>
                                    </div>
                                </div>
    
                                <div class="row">
                                    <div class="col-12 d-flex flex-sm-row flex-column justify-content-end">
                                        <button type="submit" class="btn btn-primary">Ubah Password</button>
                                    </div>
                                </div>
                            </form>
                        </x-card-body>
                    </div>
                </div>
            </div>
        </div>
	</section>
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    
    <script>
        $(document).ready( function() {
            $('#form-biodata-gambar').on('submit', function(e) {
                e.preventDefault();

                var formData = new FormData(this);
				
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
                    if (response.id == 1) {
                        $('#gambar').attr('src', "{{ url('images/profil') }}/"+ response.gambar);
                    }
					formExecuted(response)
				});
            });

            $('#form-biodata').on('submit', function(e) {
                e.preventDefault();

                var formData = new FormData(this);
				
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
                    if (response.id == 1) {
                        $('#nama-user').text($("input[name='name']").val());
                    }
					formExecuted(response)
				});
            });

            $('#form-password').on('submit', function(e) {
                e.preventDefault();

                var formData = new FormData(this);
				
				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
                    if (response.id == 1) {
                        $('#form-password input').val('');
                    }
					formExecuted(response)
				});
            });
        });

        function formExecuted(response) {
			if (response.id == 1) {
				status = 'success';
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}

        function uploadGambar() {
            if ($("input[name='gambar']").get(0).files.length === 1) {                
                $('#form-biodata-gambar').submit();
            }
        }
    </script>
@endsection