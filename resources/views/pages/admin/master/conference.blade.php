@extends('layout.layout')

@section('title', __('messages.conference master data'))

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button title="{{ __('messages.conference data') }}" id="tambah" buttonTitle='<i data-feather="plus"></i> {{ __("messages.create") }}'>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th data-priority="1">{{ __('messages.name') }}</th>
						<th>{{ __('messages.admin') }}</th>
						<th>{{ __('messages.portal') }}</th>
						<th width="10%">{{ __('messages.status') }}</th>
						<th data-priority="2" width="15%"></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="{{ __('messages.loading') }}..." type="normal" class="" id="modal">
			<form id="form" method="POST">
				@if (count($admin) == 0)
					<div class="my-1">
						<x-kosong>{{ __('messages.no conference administrator available') }}</x-kosong>
					</div>
				@elseif (count($portal) == 0)
					<div class="my-1">
						<x-kosong>{{ __('messages.no portal available') }}</x-kosong>
					</div>
				@else
					<div class="modal-body">
						<x-form-group title="{{ __('messages.name') }}">
							<input type="text" class="form-control" id="nama" placeholder="{{ __('messages.conference title') }}" required/>
						</x-form-group>

						<x-form-group title="{{ __('messages.portal') }}">
							<select class="form-control" id="portal" required>
								<option value="" hidden>-- {{ __('messages.select conference portal') }} --</option>
								@foreach ($portal as $item)
									<option value="{{ $item->id }}">{{ $item->nama }}</option>
								@endforeach
							</select>
						</x-form-group>

						<x-form-group title="{{ __('messages.admin') }}">
							<select class="form-control" id="admin" required>
								<option value="" hidden>-- {{ __('messages.select conference administrator') }} --</option>
								@foreach ($admin as $item)
									<option value="{{ $item->id }}">{{ $item->name }}</option>
								@endforeach
							</select>
						</x-form-group>

						<x-form-group title="{{ __('messages.date') }}">
							<input type="text" class="form-control flatpickr-basic" id="tanggal" placeholder="{{ __('messages.conference date') }}"/>
						</x-form-group>
					</div>

					<div class="modal-footer">
						<button type="submit" class="btn btn-primary data-submit mr-1" id="button-tambah"><i data-feather="check"></i> {{ __('messages.save') }}</button>
						<button type="submit" class="btn btn-primary data-submit mr-1" id="button-ubah"><i data-feather="edit-2"></i> {{ __('messages.update') }}</button>
						<button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> {{ __('messages.cancel') }}</button>
						<button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> {{ __('messages.delete') }}</button>
					</div>
				@endif
			</form>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin.master.conference.data') }}";
		var column = [
			{data: 'conference_nama', name: 'conference_nama'},
			{data: 'name', name: 'users.name'},
			{data: 'portal_nama', name: 'portal.portal_nama'},
			{data: 'status', name: 'status', orderable: false, searchable: false},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
    <script>
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			// Date Picker
			$('.flatpickr-basic').flatpickr();

			// Modal
			$('#tambah').on('click', function() {
				$('.modal-title').text("{{ __('messages.create conference') }}");
				$('#form').attr("action", "{{ url('admin/master/conference') }}");
				$('#form').attr('data-value', 1);

				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				
				$('#modal').modal('show');
			});

			$(document).on('click', '.ubah', function() {
				const id = $(this).attr('data-value');

				$('#form').attr("action", "{{ url('admin/master/conference') }}/" + id);
				$('#form').attr('data-value', 2);

				$.get( "{{ url('admin/master/conference') }}/"+ id, function( data ) {
					var d = JSON.parse(data);
					
					$('.modal-title').text("{{ __('messages.update conference') }}");
					
					$('#nama').val(d.nama);
					$('#portal').val(d.portal);
					$('#admin').val(d.admin);
					$('#tanggal').val(d.tanggal);
				});

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);


				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('.modal-title').text("{{ __('messages.update conference') }}");

				$('#nama').val('');
				$('#portal').val('');
				$('#admin').val('');
				$('#tanggal').val('');
			});

			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var request = {};
				if ($('#form').attr('data-value') == 1) {
					request = {
						nama: $('#nama').val(),
						portal: $('#portal').val(),
						admin: $('#admin').val(),
						tanggal: $('#tanggal').val()
					};	
				} else {
					request = {
						_method: 'PUT',
						nama: $('#nama').val(),
						portal: $('#portal').val(),
						admin: $('#admin').val(),
						tanggal: $('#tanggal').val()
					};	
				}

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: request
				}).done(function(response){
					formExecuted(response);
				});
			});

			$('form').submit(function() {
				$(this).find("button[type='submit']").prop('disabled', true);
			});
		});

		function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
				$('#modal').modal('hide');
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: "{{ __('messages.are you sure') }}?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: "{{ __('messages.yes') }}",
				cancelButtonText: "{{ __('messages.cancel') }}",
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("/admin/master/conference")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							table.ajax.reload();
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
	</script>
@endsection