@extends('layout.layout')

@section('title', __('messages.portal master data'))

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button title="{{ __('messages.portal data') }}" id="tambah" buttonTitle='<i data-feather="plus"></i> {{ __("messages.create") }}'>
			<table class="table table-bordered" id="table">
				<thead>
					<tr>
						<th data-priority="1">{{ __('messages.title') }}</th>
						<th>{{ __('messages.description') }}</th>
						<th data-priority="2" width="20%"></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
			<form id="form" method="post">
				<div class="modal-body">
					<x-form-group title="{{ __('messages.title') }}">
						<input type="text" class="form-control" id="nama" placeholder="{{ __('messages.portal title') }}" required/>
					</x-form-group>

					<x-form-group title="{{ __('messages.description') }}">
						<textarea class="form-control" id="deskripsi" placeholder="{{ __('messages.portal description') }}" rows="5" required></textarea>
					</x-form-group>

					<x-form-group title="{{ __('messages.about') }}">
						<textarea class="form-control" id="tentang" placeholder="{{ __('messages.portal about') }}" rows="5" required></textarea>
					</x-form-group>

					<x-form-group title="{{ __('messages.latitude') }}">
						<input type="text" pattern="[0-9\.\-]*" id="latitude" title="{{ __('messages.fill this field with numbers, dot, or minus') }}" class="form-control" placeholder="{{ __('messages.portal latitude') }}" required/>
					</x-form-group>

					<x-form-group title="{{ __('messages.longitude') }}">
						<input type="text" pattern="[0-9\.\-]*" id="longitude" title="{{ __('messages.fill this field with numbers, dot, or minus') }}" class="form-control" placeholder="{{ __('messages.portal longitude') }}" required/>
					</x-form-group>

					<x-form-group title="{{ __('messages.contact person') }} 1">
						<input type="text" class="form-control" id="kontak_nama1" placeholder="{{ __('messages.contact person name') }}" required/>
						<input type="number" class="form-control" id="kontak_hp1" placeholder="{{ __('messages.contact person phone') }}" required/>
					</x-form-group>

					<x-form-group title="{{ __('messages.contact person') }} 2 ({{ __('messages.not required') }})">
						<input type="text" class="form-control" id="kontak_nama2" placeholder="{{ __('messages.contact person name') }}"/>
						<input type="number" class="form-control" id="kontak_hp2" placeholder="{{ __('messages.contact person phone') }}"/>
					</x-form-group>

					<x-form-group title="{{ __('messages.email') }}">
						<input type="email" class="form-control" id="email" placeholder="{{ __('messages.portal email') }}" required/>
					</x-form-group>

					<x-form-group title="Instagram">
						<input type="text" class="form-control" id="instagram" placeholder="{{ __('messages.portal instagram') }}"/>
					</x-form-group>

					<x-form-group title="Facebook">
						<input type="text" class="form-control" id="facebook" placeholder="{{ __('messages.portal facebook') }}"/>
					</x-form-group>

					<x-form-group title="Twitter">
						<input type="text" class="form-control" id="twitter" placeholder="{{ __('messages.portal twitter') }}"/>
					</x-form-group>

					<x-form-group title="YouTube">
						<input type="text" class="form-control" id="youtube" placeholder="{{ __('messages.portal youtube') }}"/>
					</x-form-group>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" id="button-tambah"><i data-feather="check"></i> {{ __('messages.save') }}</button>
					<button type="submit" class="btn btn-primary" id="button-ubah"><i data-feather="edit-2"></i> {{ __('messages.update') }}</button>
					<button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> {{ __('messages.cancel') }}</button>
					<button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> {{ __('messages.delete') }}</button>
				</div>
			</form>
		</x-modal>
		{{-- END: Modal --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="{{ __('messages.loading') }}..." type="normal" class="" id="fileModal">
			<form id="fileForm" method="post" enctype="multipart/form-data">
				@csrf
				@method('PUT')
				<div class="modal-body">
					<div id="fileLogo"></div>
					<x-form-group title="{{ __('messages.change logo') }}">
						<input type="file" class="form-control-file" accept="image/*" name="logo" id="logo" required/>
					</x-form-group>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary data-submit mr-1"><i data-feather="edit-2"></i> {{ __('messages.update') }}</button>
					<button type="reset" class="btn btn-outline-danger" data-dismiss="modal"><i data-feather="x"></i> {{ __('messages.cancel') }}</button>
				</div>
			</form>
		</x-modal>
		{{-- END: Modal --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="{{ __('messages.deleted portal') }}" type="normal" class="" id="trashModal">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>{{ __('messages.portal title') }}</th>
						<th></th>
					</tr>
				</thead>
				
				<tbody id="trashData"></tbody>
			</table>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin.master.portal.data') }}";
		var column = [
			{data: 'portal_nama', name: 'portal_nama'},
			{data: 'portal_deskripsi', name: 'portal_deskripsi'},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button-portal.js') }}"></script>
	<script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			// $('#tambah').after('<button type="button" data-toggle="modal" data-target="#trashModal" id="trash" onmouseover="trash_data(0)" class="btn btn-danger ml-2"><i data-feather="trash"></i> {{ __("messages.deleted portal") }}</button>');

			$('#tambah').on('click', function() {
				$('.modal-title').text("{{ __('messages.create portal') }}");
				$('#form').attr("action", "{{ url('admin/master/portal') }}");
				$('#form').attr('data-value', 1);

				$('#ubah-logo').attr('hidden', true);

				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				
				$('#modal').modal('show');
			});

			$(document).on('click', '.ubah', function() {
				const id = $(this).attr('data-value');
				$('.modal-title').text("{{ __('messages.update portal') }}");
				$('#form').attr("action", "{{ url('admin/master/portal') }}/" + id);
				$('#form').attr('data-value', 2);

				$.get( "{{ url('admin/master/portal') }}/"+ id, function( data ) {
					var d = JSON.parse(data);
					$('#nama').val(d.portal_nama);
					$('#deskripsi').val(d.portal_deskripsi);
					$('#tentang').val(d.portal_tentang);
					$('#latitude').val(d.portal_latitude);
					$('#longitude').val(d.portal_longitude);
					$('#kontak_nama1').val(d.portal_kontak_nama1);
					$('#kontak_hp1').val(d.portal_kontak_hp1);
					$('#kontak_nama2').val(d.portal_kontak_nama2);
					$('#kontak_hp2').val(d.portal_kontak_hp2);
					$('#email').val(d.portal_email);
					$('#instagram').val(d.portal_instagram);
					$('#facebook').val(d.portal_facebook);
					$('#twitter').val(d.portal_twitter);
					$('#youtube').val(d.portal_youtube);
				});

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);


				$('#modal').modal('show');
			});

			$(document).on('click', '.file', function() {
				const id = $(this).attr('data-value');
				$('#fileForm').attr("action", "{{ url('admin/master/portal') }}/" + id);

				$.get( "{{ url('admin/master/portal') }}/"+ id, function( data ) {
					$('.modal-title').text('File Portal');
					var d = JSON.parse(data);
					$('#fileLogo').append('<img src="/images/portal/'+ d.portal_logo +'" class="img-fluid">');
				});

				$('#fileModal').modal('show');
			});

			$('#fileModal').on('hidden.bs.modal', function() {
				$('.modal-title').text("{{ __('messages.loading') }}...");

				$('#logo').val('');
				$('#fileLogo').children().remove();
			});

			$('#modal').on('hidden.bs.modal', function() {
				$('.modal-title').text("{{ __('messages.loading') }}...");

				$('#nama').val('');
				$('#ubah-logo input').val('');
				$('#tambah-logo input').val('');
				$('#deskripsi').val('');
				$('#tentang').val('');
				$('#latitude').val('');
				$('#longitude').val('');
				$('#kontak_nama1').val('');
				$('#kontak_hp1').val('');
				$('#kontak_nama2').val('');
				$('#kontak_hp2').val('');
				$('#email').val('');
				$('#instagram').val('');
				$('#facebook').val('');
				$('#twitter').val('');
				$('#youtube').val('');
			});

			$('#form').on('submit', function(e) {
				e.preventDefault();

				if ($('#form').attr('data-value') == 1) {
					request = {
						nama: $('#nama').val(),
						deskripsi: $('#deskripsi').val(),
						tentang: $('#tentang').val(),
						latitude: $('#latitude').val(),
						longitude: $('#longitude').val(),
						kontak_nama1: $('#kontak_nama1').val(),
						kontak_hp1: $('#kontak_hp1').val(),
						kontak_nama2: $('#kontak_nama2').val(),
						kontak_hp2: $('#kontak_hp2').val(),
						email: $('#email').val(),
						instagram: $('#instagram').val(),
						facebook: $('#facebook').val(),
						twitter: $('#twitter').val(),
						youtube: $('#youtube').val()
					};	
				} else {
					request = {
						_method: 'PUT',
						nama: $('#nama').val(),
						deskripsi: $('#deskripsi').val(),
						tentang: $('#tentang').val(),
						latitude: $('#latitude').val(),
						longitude: $('#longitude').val(),
						kontak_nama1: $('#kontak_nama1').val(),
						kontak_hp1: $('#kontak_hp1').val(),
						kontak_nama2: $('#kontak_nama2').val(),
						kontak_hp2: $('#kontak_hp2').val(),
						email: $('#email').val(),
						instagram: $('#instagram').val(),
						facebook: $('#facebook').val(),
						twitter: $('#twitter').val(),
						youtube: $('#youtube').val()
					};	
				}

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: request
				}).done(function(response){
					// console.log(response);
					formExecuted(response);
				});
			});

			$('#fileForm').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					// console.log(response);
					formExecuted(response);
				});
			});

			$('form').submit(function() {
				$(this).find("button[type='submit']").prop('disabled', true);
			});
		});

		function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
				$('#modal').modal('hide');
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: "{{ __('messages.are you sure') }}?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: "{{ __('messages.yes') }}",
				cancelButtonText: "{{ __('messages.cancel') }}",
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("/admin/master/portal")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							table.ajax.reload();
							$('#modal').modal('hide');
							$('#trash').attr('onmouseover', 'trash_data(0)');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}

		// // Restore Data
		// function restorePortal(id) {
		// 	var status = '';
		// 	$.ajax({
		// 		url: '{{ url("/admin/master/portal")}}',
		// 		type: "POST",
		// 		data: {
		// 			restore: id
		// 		},
		// 	}).done(function(response){
		// 		if (response.id == 1) {
		// 			status = 'success';
		// 			table.ajax.reload();
		// 			$('#trashModal').modal('hide');
		// 			$('#trash').attr('onmouseover', 'trash_data(0)');
		// 		} else {
		// 			status = 'error';
		// 		}
		// 		toastr[status](response.keterangan, response.status, {
		// 			closeButton: true,
		// 			tapToDismiss: true
		// 		});
		// 	});
		// }

		// // Restore Data
		// function trash_data(id) {
		// 	if (id == 0) {
		// 		$('#trashData').children().remove();

		// 		var count = id + 1;
		// 		$.get( "{{ url('admin/master/data/portal/trashed') }}", function( data ) {
		// 			var d = JSON.parse(data);
		// 			for (let i = 0; i < d.length; i++) {
		// 				$('#trashData').append(`<tr>
		// 											<td>`+ d[i].nama +`</td>
		// 											<td>
		// 												<button class="btn btn-success" onclick="restorePortal(`+ d[i].id +`)"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg></button>
		// 											</td>
		// 										</tr>`);
		// 			}
		// 		});
				
		// 		$('#trash').attr('onmouseover', 'trash_data('+ count +')');
		// 	}
		// }
	</script>
@endsection