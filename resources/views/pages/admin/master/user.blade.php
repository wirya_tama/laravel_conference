@extends('layout.layout')

@section('title', __('messages.user master data'))

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button title="{{ __('messages.user data') }}" id="tambah" buttonTitle='<i data-feather="plus"></i> {{ __("messages.create") }}'>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>{{ __('messages.registration number') }}</th>
						<th data-priority="1">{{ __('messages.name') }}</th>
						<th>{{ __('messages.role') }}</th>
						<th data-priority="2" width="15%"></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
			<form id="form" data-value="">
				<div class="modal-body">
					<x-form-group title="{{ __('messages.registration number') }}">
						<input type="number" class="form-control" id="no_induk" placeholder="No Induk User"/>
					</x-form-group>
				
					<x-form-group title="{{ __('messages.name') }}">
						<input type="text" class="form-control" id="nama" placeholder="Nama User" required/>
					</x-form-group>
				
					<x-form-group title="{{ __('messages.role') }} ({{ __('messages.must be at least 1') }})">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" value="1" name="role[]" id="check_1" />
							<label class="custom-control-label" for="check_1">{{ __('messages.admin') }}</label>
						</div>
				
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" value="2" name="role[]" id="check_2" />
							<label class="custom-control-label" for="check_2">{{ __('messages.conference admin') }}</label>
						</div>
				
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" value="3" name="role[]" id="check_3" />
							<label class="custom-control-label" for="check_3">{{ __('messages.author') }}</label>
						</div>
				
						{{-- <div class="custom-control custom-checkbox">
							<input type="checkbox" class="custom-control-input" value="4" name="role[]" id="check_4" />
							<label class="custom-control-label" for="check_4">Reviewer</label>
						</div> --}}
					</x-form-group>
				
					<x-form-group title="{{ __('messages.nation') }}">
						<select class="form-control" id="kewarganegaraan" required>
							<option value="" hidden>-- {{ __('messages.select nation') }} --</option>
							<option value="1">Indonesia</option>
						</select>
					</x-form-group>
				
					<x-form-group title="{{ __('messages.gender') }}">
						<div class="custom-control custom-radio">
							<input type="radio" id="radio_1" name="jenis_kelamin" class="custom-control-input" value="1" />
							<label class="custom-control-label" for="radio_1">{{ __('messages.male') }}</label>
						</div>
				
						<div class="custom-control custom-radio">
							<input type="radio" id="radio_0" name="jenis_kelamin" class="custom-control-input" value="0" />
							<label class="custom-control-label" for="radio_0">{{ __('messages.female') }}</label>
						</div>
					</x-form-group>
				
					<x-form-group title="{{ __('messages.email') }}">
						<input type="email" class="form-control" id="email" placeholder="{{ __('messages.user email') }}" required>
					</x-form-group>
				</div>
			</form>

			<div class="modal-footer">
				<button type="submit" class="btn btn-primary" id="button-tambah" form="form"><i data-feather="check"></i> {{ __('messages.save') }}</button>
				<form id="verifiedForm" method="post">
					@csrf
					<button type="submit" class="btn btn-outline-success" id="button-verified"><i data-feather="check"></i> {{ __('messages.verify email') }}</button>
				</form>
				<button type="submit" class="btn btn-primary" id="button-ubah" form="form"><i data-feather="edit-2"></i> {{ __('messages.update') }}</button>
				<button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> {{ __('messages.cancel') }}</button>
				<button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> {{ __('messages.delete') }}</button>
			</div>
		</x-modal>
		{{-- END: Modal --}}
		
		{{-- BEGIN: Form Action --}}
		<form id="form-action" method="post">
			@csrf
		</form>
		{{-- END: Form Action --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin.master.user.data') }}";
		var column = [
			{data: 'no_induk', name: 'no_induk'},
			{data: 'name', name: 'name'},
			{data: 'role', name: 'role', orderable: false, searchable: false},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>

	{{-- SweetAlert --}}
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
	<script>
		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: "{{ __('messages.are you sure') }}?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: "{{ __('messages.yes') }}",
				cancelButtonText: "{{ __('messages.cancel') }}",
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("/admin/master/user")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						formExecuted(response);
					});
				}
			});
		}

		// Warning Aksi
		function aksi(id) {
			$('#form-action').attr('action', `{{ url('admin/master/data/user/reset_password/${id}') }}`);

			Swal.fire({
				title: "{{ __('messages.are you sure') }}?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: "{{ __('messages.yes') }}",
				cancelButtonText: "{{ __('messages.cancel') }}",
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					$('#form-action').submit();
				}
			});
		}
	</script>

	{{-- Toastr --}}
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script>
		function formDataExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
			} else {
				status = 'error';
			}

			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});
			
			if (response.id == 1) {
				table.ajax.reload();
				$('#modal').modal('hide');
			}

			$('form').find("button[type='submit']").prop('disabled', false);
		}

		function serverError(status, message) {
			toastr['error'](message, status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}
	</script>

	{{-- Form --}}
	<script type="text/javascript">
		$('form').submit(function() {
			$(this).find("button[type='submit']").prop('disabled', true);
		});

		// Form Action
		$('form').not('#form').submit(function(e) {
			e.preventDefault();

			var formData = new FormData(this);

			$.ajax({
				url: $(this).attr('action'),
				type: "POST",
				data: formData,
				contentType: false,
				processData: false
			}).done(function(response){
				formDataExecuted(response);
			}).fail(function(response) {
				serverError(response.statusText, response.responseJSON.message);
			});
		});
	</script>

    {{-- Toastr --}}
    <script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
    <script>
        function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
			} else {
				status = 'error';
			}

			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

            if (response.id == 1) {
				location.reload();
			}

			$('form').find("button[type='submit']").prop('disabled', false);
		}

        function serverError(status, message) {
            toastr['error'](message, status, {
                closeButton: true,
                tapToDismiss: true
            });

            $('form').find("button[type='submit']").prop('disabled', false);
        }
    </script>

    <script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			$('#tambah').on('click', function() {
				$('.modal-title').text("{{ __('messages.create user') }}");

				$('#form').attr("action", "{{ url('/admin/master/user') }}");
				$('#form').attr("data-value", "1");

				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);

				$('#radio_1').prop('checked', true);
				
				$('#modal').modal('show');
			});

			$(document).on('click', '.ubah', function() {
				var id = $(this).attr('data-value');
				$('.modal-title').text("{{ __('messages.update user') }}");

				$('#form').attr("data-value", "2");

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);
				$('#button-verified').attr('hidden', true);

				$.get( "{{ url('admin/master/user') }}/"+ id, function( data ) {
                    var d = JSON.parse(data);
					$('#form').attr("action", "{{ url('/admin/master/user')}}/"+ id);
					$('#no_induk').val(d.no_induk);
					$('#nama').val(d.name);
					$('#kewarganegaraan').val(d.data_user['ke_id']);
					$('#radio_' + d.data_user['du_jenis_kelamin']).prop('checked', true);
					$('#email').val(d.email);
					for (let i = 0; i < d.roles.length; i++) {
						$('#check_' + d.roles[i]['role_id']).prop('checked', true);
					}
					$('#button-hapus').attr('onClick', 'hapus('+ id +')');
					if (d.email_verified_at == null) {
						$('#verifiedForm').attr('action', "{{ url('admin/master/verified/user') }}/"+ id);
						$('#button-verified').attr('hidden', false);
					}
                });

				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				$('.modal-title').text("{{ __('messages.loading') }}...");
				$('#no_induk').val('');
				$('#nama').val('');
				$('#check_1').prop('checked', false);
				$('#check_2').prop('checked', false);
				$('#check_3').prop('checked', false);
				$('#check_4').prop('checked', false);
				$('#kewarganegaraan').val('');
				$('#radio_1').prop('checked', true);
				$('#radio_0').prop('checked', false);
				$('#email').val('');
			});

			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();
				
				var roles = [];
				$("input[name='role[]']:checked").each(function () {
					roles.push($(this).val());
				});

				if ($("input[name='role[]']:checked").length > 0) {
					var status = '';
					if ($('#form').attr('data-value') == 1) {
						$.ajax({
							url: $(this).attr('action'),
							type: "POST",
							data: {
								name: $('#nama').val(),
								role: roles,
								no_induk: $('#no_induk').val(),
								kewarganegaraan: $('#kewarganegaraan').val(),
								jenis_kelamin: $("input[name='jenis_kelamin']:checked").val(),
								email: $('#email').val(),
						},
						}).done(function(response){
							formExecuted(response)
						});
					} else {
						$.ajax({
							url: $(this).attr('action'),
							type: "POST",
							data: {
								_method: 'PUT',
								name: $('#nama').val(),
								role: roles,
								no_induk: $('#no_induk').val(),
								kewarganegaraan: $('#kewarganegaraan').val(),
								jenis_kelamin: $("input[name='jenis_kelamin']:checked").val(),
								email: $('#email').val(),
						},
						}).done(function(response){
							formExecuted(response)
						});
					}
				} else {
					roleKosong();
				}
			});
		});

		function roleKosong() {
			toastr['error']('Role wajib diisi minimal 1', 'Gagal', {
				closeButton: true,
				tapToDismiss: true
			});
		}

		function formExecuted(response) {
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
				$('#modal').modal('hide');
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}
	</script>
@endsection