@extends('layout.layout')

@section('title', __('messages.publisher master data'))

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button title="{{ __('messages.publisher data') }}" id="tambah" buttonTitle='<i data-feather="plus"></i> {{ __("messages.create") }}'>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th data-priority="1">{{ __('messages.name') }}</th>
						<th>{{ __('messages.level') }}</th>
						<th data-priority="2" width="15%"></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
			<form id="form" method="post">
				@csrf
				<div class="modal-body">
					<x-form-group title="{{ __('messages.name') }}">
						<input type="text" class="form-control" name="nama" placeholder="{{ __('messages.publisher name') }}" required/>
					</x-form-group>
					
					<x-form-group title="{{ __('messages.level') }}">
						<select class="form-control" name="level" required>
							<option value="" hidden>-- {{ __('messages.select publisher level') }} --</option>
							<option value="1">{{ __('messages.national') }}</option>
							<option value="2">{{ __('messages.international') }}</option>
						</select>
					</x-form-group>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" id="button-tambah"><i data-feather="check"></i> {{ __('messages.save') }}</button>
					<button type="submit" class="btn btn-primary" id="button-ubah"><i data-feather="edit-2"></i> {{ __('messages.update') }}</button>
					<button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> {{ __('messages.cancel') }}</button>
					<button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> {{ __('messages.delete') }}</button>
				</div>
			</form>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin.master.publisher.data') }}";
		var column = [
			{data: 'pu_nama', name: 'pu_nama'},
			{data: 'level', name: 'level', searchable: false},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			$('#tambah').on('click', function() {
				$('.modal-title').text("{{ __('messages.create publisher') }}");
				$('#form').attr("action", "{{ url('admin/master/publisher') }}");

				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				
				$('#modal').modal('show');
			});

			$(document).on('click', '.ubah', function() {
				const id = $(this).attr('data-value');
				$('.modal-title').text("{{ __('messages.update publisher') }}");
				$('#form').attr("action", "{{ url('admin/master/publisher') }}/" + id);
				$('#form').append('<input type="hidden" id="method" name="_method" value="PUT"/>');

				$.get( "{{ url('admin/master/publisher') }}/"+ id, function( data ) {
					var d = JSON.parse(data);
					$('input[name=nama]').val(d.pu_nama);
					$('select[name=level]').val(d.pu_level);
				});

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);

				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function() {
				$('.modal-title').text("{{ __('messages.loading') }}...");

				$('#method').remove();
				$('input').val('');
				$('select').val('');
			});

			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					if (response.id == 1) {
						$('#modal').modal('hide');
					}
					formExecuted(response);
				});
			});

			$('form').submit(function() {
				$(this).find("button[type='submit']").prop('disabled', true);
			});
		});

		function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: "{{ __('messages.are you sure') }}?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: "{{ __('messages.yes') }}",
				cancelButtonText: "{{ __('messages.cancel') }}",
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					var status = '';
					$.ajax({
						url: '{{ url("/admin/master/publisher")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						if (response.id == 1) {
							status = 'success';
							table.ajax.reload();
							$('#modal').modal('hide');
						} else {
							status = 'error';
						}
						toastr[status](response.keterangan, response.status, {
							closeButton: true,
							tapToDismiss: true
						});
					});
				}
			});
		}
	</script>
@endsection