@extends('layout.layout')

@section('title', __('messages.faq master data'))

@section('css')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
	<link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
	<style>
		tbody p {
			margin-bottom: 0%;
		}
	</style>
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button title="{{ __('messages.faq data') }}" id="tambah" buttonTitle='<i data-feather="plus"></i> {{ __("messages.create") }}'>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th data-priority="1">{{ __('messages.question') }}</th>
						<th>{{ __('messages.answer') }}</th>
						<th data-priority="2" width="15%"></th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
		
		{{-- BEGIN: Modal --}}
		<x-modal title="" type="normal" class="" id="modal">
			<form id="form" method="post">
				<div class="modal-body">
					<x-form-group title="{{ __('messages.question') }}">
						<div id="pertanyaan"></div>
					</x-form-group>

					<x-form-group title="{{ __('messages.answer') }}">
						<div id="jawaban"></div>
					</x-form-group>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary data-submit mr-1" id="button-tambah"><i data-feather="check"></i> {{ __('messages.save') }}</button>
					<button type="submit" class="btn btn-primary data-submit mr-1" id="button-ubah"><i data-feather="edit-2"></i> {{ __('messages.update') }}</button>
					<button type="reset" class="btn btn-outline-danger" id="button-batal" data-dismiss="modal"><i data-feather="x"></i> {{ __('messages.cancel') }}</button>
					<button type="button" class="btn btn-outline-danger" id="button-hapus"><i data-feather="trash"></i> {{ __('messages.delete') }}</button>
				</div>
			</form>
		</x-modal>
		{{-- END: Modal --}}
	</section>
	{{-- END: Datatable Button --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = "{{ route('admin.master.faq.data') }}";
		var column = [
			{data: 'faq_pertanyaan', name: 'faq_pertanyaan'},
			{data: 'faq_jawaban', name: 'faq_jawaban'},
			{data: 'aksi', name: 'aksi', orderable: false, searchable: false}
		];
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>

	{{-- Quill Editor --}}
	<script src="https://cdn.quilljs.com/1.3.6/quill.min.js" type="text/javascript"></script>
	<script>
		var pertanyaan = new Quill('#pertanyaan', {
			theme: 'snow'
		});
		var jawaban = new Quill('#jawaban', {
			theme: 'snow'
		});
	</script>
	
	<script>
		// Modal
		$(document).ready(function() {
			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

			// Modal tambah
			$('#tambah').on('click', function() {
				$('#form').attr('data-value', 1);
				$('#form').attr("action", "{{ url('admin/master/faq') }}");

				$('.modal-title').text("{{ __('messages.create faq') }}");

				$('#button-tambah').attr('hidden', false);
				$('#button-batal').attr('hidden', false);
				$('#button-ubah').attr('hidden', true);
				$('#button-hapus').attr('hidden', true);
				
				$('#modal').modal('show');
			});

			// Modal Ubah
			$(document).on('click', '.ubah', function() {
				$('#form').attr('data-value', 2);
				$('#form').attr("action", "{{ url('admin/master/faq') }}/" + $(this).attr('data-value'));

				$('.modal-title').text("{{ __('messages.update faq') }}");

				pertanyaan.clipboard.dangerouslyPasteHTML($(this).attr('data-pertanyaan'));
				jawaban.clipboard.dangerouslyPasteHTML($(this).attr('data-jawaban'));

				$('#button-tambah').attr('hidden', true);
				$('#button-batal').attr('hidden', true);
				$('#button-ubah').attr('hidden', false);
				$('#button-hapus').attr('hidden', false);

				$('#button-hapus').attr('onClick', 'hapus('+ $(this).attr('data-value') +')');

				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function (e) {
				pertanyaan.setText('');
				jawaban.setText('');
			});

			// Tambah/Ubah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var request = {};
				if ($('#form').attr('data-value') == 1) {
					request = {
						pertanyaan: $('#pertanyaan .ql-editor').html(),
						jawaban: $('#jawaban .ql-editor').html()
					};	
				} else {
					request = {
						_method: 'PUT',
						pertanyaan: $('#pertanyaan .ql-editor').html(),
						jawaban: $('#jawaban .ql-editor').html()
					};	
				}

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: request
				}).done(function(response){
					formExecuted(response);
				});
			});

			$('form').submit(function() {
				$(this).find("button[type='submit']").prop('disabled', true);
			});
		});

		// Hapus Data
		function hapus(id) {
			Swal.fire({
				title: "{{ __('messages.are you sure') }}?",
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: "{{ __('messages.yes') }}",
				cancelButtonText: "{{ __('messages.cancel') }}",
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
					$.ajax({
						url: '{{ url("/admin/master/faq")}}/'+ id,
						type: "POST",
						data: {
							_method: 'DELETE',
						},
					}).done(function(response){
						formExecuted(response);
					});
				}
			});
		}

		function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
				table.ajax.reload();
				$('#modal').modal('hide');
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			$('form').find("button[type='submit']").prop('disabled', false);
		}
	</script>
@endsection