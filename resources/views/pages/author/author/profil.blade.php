@extends('layout.layout')

@section('title', 'Profil')

@section('content')
    {{-- Card --}}
    <section>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 pl-0">
                        <div class="row ml-1">
                            <img src="{{ asset('images/profil/' . $data->gambar) }}" class="img-fluid w-100">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <x-form-group title="Nama">
                            <br><strong>{{ $data->nama ?? "-" }}</strong>
                        </x-form-group>

                        <x-form-group title="Alamat">
                            <br><strong>{{ $data->alamat ?? "-" }}</strong>
                        </x-form-group>
                        
                        <x-form-group title="Afiliasi">
                            <br><strong>{{ $data->afiliasi ?? "-" }}</strong>
                        </x-form-group>
                        
                        <x-form-group title="Kewarganegaraan">
                            <br><strong>{{ $data->kewarganegaraan ?? "-" }}</strong>
                        </x-form-group>
                        
                        <x-form-group title="No. HP">
                            <br><strong>{{ $data->hp ?? "-" }}</strong>
                        </x-form-group>
                        
                        <x-form-group title="Jenis Kelamin">
                            <br><strong>{{ $data->jenis_kelamin ?? "-" }}</strong>
                        </x-form-group>
                        
                        <x-form-group title="Email">
                            <br><strong>{{ $data->email ?? "-" }}</strong>
                        </x-form-group>
                        
                        <a href="{{ route('pengaturan.index') }}" class="btn btn-primary"><i data-feather="edit-2"></i> Ubah Profil</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection