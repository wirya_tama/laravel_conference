@extends('layout.layout')

@section('title')
{{ __('messages.publication') }}
@endsection

@section('vendor-css')
{{-- Toastr --}}
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">

{{-- Datatables --}}
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">

{{-- Select2 --}}
<link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/forms/select/select2.min.css') }}">

{{-- SweetAlert --}}
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
@endsection

@section('css')
{{-- Toastr --}}
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">

{{-- SweetAlert --}}
<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">
@endsection

@section('content')
@if ($message = Session::get('danger'))
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <div class="alert-body">{{ $message }}</div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <div class="alert-body">{{ $message }}</div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    </div>
@endif

{{-- BEGIN: Card --}}
<section>
    <div class="card">
        <div class="card-body">
            <div class="nav-vertical">
                <ul class="nav nav-tabs nav-left flex-column" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="title-tab" data-toggle="tab" aria-controls="title" href="#title" role="tab" aria-selected="true">{{ __('messages.title & abstract') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contributor-tab" data-toggle="tab" aria-controls="contributor" href="#contributor" role="tab" aria-selected="false">{{ __('messages.author') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="metadata-tab" data-toggle="tab" aria-controls="metadata" href="#metadata" role="tab" aria-selected="false">{{ __('messages.metadata') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="galleys-tab" data-toggle="tab" aria-controls="galleys" href="#galleys" role="tab" aria-selected="false">{{ __('messages.galley') }}</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="title" role="tabpanel" aria-labelledby="title-tab">
                        <form action="{{ (request()->routeIs('author.author.publication*')) ? route('author.author.publication.update_abstract', $submission->id) : route('admin-conference.publication.submission.update_abstract', $submission->id) }}" method="POST" id="form-abstract">
                            @csrf
                            @method('PUT')

                            <div class="form-group mb-2">
                                <b>{{ __('messages.title') }}</b>
                                <input type="text" name="submission_judul" class="form-control" value="{{ $submission->submission_judul }}" required>
                            </div>

                            <div class="form-group mb-2">
                                <b>Abstrak</b>
                                <textarea name="submission_abstrakindo" class="form-control" rows="4">{{ $submission->submission_abstrakindo }}</textarea>
                            </div>

                            @if ($conference->conference_level === 2)
                                <div class="form-group mb-2">
                                    <b>Abstract</b>
                                    <textarea name="submission_abstrak" class="form-control" rows="4">{{ $submission->submission_abstrak }}</textarea>
                                </div>
                            @endif

                            <div class="text-right">
                                <button type="submit" class=" btn btn-primary">{{ __('messages.save') }}</button>
                            </div>
                        </form>
                    </div>

                    <div class="tab-pane" id="contributor" role="tabpanel" aria-labelledby="contributor-tab">
                        <div>
                            <div class="border-bottom d-flex align-items-center justify-content-between pb-50">
                                <h4 class="m-0">{{ __('messages.author') }}</h4>

                                <button type="button" class="btn btn-primary mx-50" data-toggle="modal" data-target="#modal-contributor">
                                    <i data-feather="plus"></i> {{ __('messages.add') }}
                                </button>
                            </div>

                            <table class="table table-bordered" id="table-contributor">
                                <thead class="text-center">
                                    <tr>
                                        <th>{{ __('messages.name') }}</th>
                                        <th style="width: 12px"></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                        <div class="modal fade" id="modal-contributor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{ __('messages.add') }} {{ __('messages.author') }}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>

                                    <form action="{{ (request()->routeIs('author.author.publication*')) ? route('author.author.publication.update_author', $submission->id) : route('admin-conference.publication.submission.update_author', $submission->id) }}" method="post" id="form-contributor">
                                        <div class="modal-body">
                                            @csrf
                                            @method('PUT')

                                            <div class="form-group">
                                              <label>{{ __('messages.user') }}</label>
                                              <select name="user_id" class="form-control select-user"></select>
                                            </div>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary">{{ __('messages.save') }}</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <form method="post" id="form-delete-contributor">
                            @csrf
                            @method('DELETE')
                        </form>
                    </div>

                    <div class="tab-pane" id="metadata" role="tabpanel" aria-labelledby="metadata-tab">
                        <form action="{{ (request()->routeIs('author.author.publication*')) ? route('author.author.publication.update_keyword', $submission->id) : route('admin-conference.publication.submission.update_keyword', $submission->id) }}" method="POST" id="form-keyword">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <b>Kata Kunci</b>
                                <label>(Pisah dengan tanda ;)</label>
                                <input type="text" class="form-control" name="kata_kunci_1" value="{{ $kata_kunci_1 }}" required>
                            </div>

                            @if ($conference->conference_level === 2)
                            <div class="form-group">
                                <b>Keywords</b>
                                <label>(Separate with ;)</label>
                                <input type="text" class="form-control" name="kata_kunci_2" value="{{ $kata_kunci_2 }}" required>
                            </div>
                            @endif

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">{{ __('messages.save') }}</button>
                            </div>
                        </form>
                    </div>
                    
                    <div class="tab-pane" id="galleys" role="tabpanel" aria-labelledby="galleys-tab">
                        <h4 class="m-0">{{ __('messages.galley') }}</h4>

                        @if ($submission->submission_full_paper !== null)  
                        <div class="text-center">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text mb-50" style="width:100px; height:100px"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
                            <br>
                            <a class="btn btn-primary" href="{{ asset('files/submission/full-paper/' . $submission->submission_full_paper) }}" role="button"><i data-feather="download"></i> {{ __('messages.download') }} {{ __('messages.file') }}</a>
                        </div>
                        @endif

                        <form action="{{ (request()->routeIs('author.author.publication*')) ? route('author.author.publication.update_galley', $submission->id) : route('admin-conference.publication.submission.update_galley', $submission->id) }}" method="POST" id="form-galley">
                            @csrf
                            @method('PUT')

                            <div class="form-group">
                                <label>{{ __('messages.change') }} {{ __('messages.file') }}</label>
                                <input type="file" required name="submission_full_paper" accept=".pdf" class="form-control-file">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">{{ __('messages.save') }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- END: Card --}}

{{-- BEGIN: Modal --}}
<x-modal title="Bayar Abstrak" type="normal" class="" id="modalAbstrak">
    <form id="form" method="post" enctype="multipart/form-data">
        @csrf
        <div class="modal-body"></div>

        <div class="modal-footer">
            <button type="submit" class="btn btn-primary data-submit mr-1"><i data-feather="check"></i> Simpan</button>
            <button type="reset" class="btn btn-outline-danger" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
        </div>
    </form>
</x-modal>
{{-- END: Modal --}}
@endsection

@section('vendor-js')
{{-- Toastr --}}
<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>

{{-- Datatables --}}
<script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>

{{-- Select2 --}}
<script src="{{ asset('/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>

{{-- SweetAlert --}}
<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
@endsection

@section('js')
{{-- Toastr --}}
<script>
    function toastrStatus(icon, status, message, data) {
        toastr[icon](`<b>${message}</b><br>${data}`, status, {
            closeButton: true
        });

        $('form').find("button[type='submit']").prop('disabled', false);
    }
</script>

{{-- Datatables --}}
<script>
    var table = $('#table-contributor').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ (request()->routeIs('author.author.publication*')) ? route('author.author.publication.show_author', $submission->id) : route('admin-conference.publication.submission.show_author', $submission->id) }}",
        columns: [
            { data: 'name', name: 'users.name' },
            { data: 'aksi', name: 'aksi', className: "text-center", orderable: false, searchable: false }
        ],
        dom: '<"d-flex justify-content-between align-items-center mx-50 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-50 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    })
</script>

{{-- Select2 --}}
<script>
    $(`.select-user`).select2({
        placeholder: "-- {{ __('messages.choose.user') }}",
        width: '100%',
        ajax: {
            url: "{{ (request()->routeIs('author.author.publication*')) ? route('author.author.publication.users') : route('admin-conference.publication.submission.users') }}",
            data: function (params) {
                var query = {
                    search: params.term,
                    page: params.page || 1
                }

                return query;
            }
        }
    })
</script>

{{-- SweetAlert --}}
<script>
    function hapus(id) {
        $('#form-delete-contributor').attr('action', `{{ (request()->routeIs('author.author.publication*')) ? url('author/author/publication/delete-author/${id}') : url('admin-conference/publication/submission/delete-author/${id}') }}`)

        Swal.fire({
            title: "{{ __('messages.are you sure') }}?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: "{{ __('messages.yes') }}",
            cancelButtonText: "{{ __('messages.cancel') }}",
            customClass: {
                confirmButton: 'btn btn-primary',
                cancelButton: 'btn btn-outline-danger ml-1'
            },
            buttonsStyling: false
        }).then(function (result) {
            if (result.value) {
                $('#form-delete-contributor').submit()
            }
        });
    }
</script>

{{-- Script --}}
<script>
    // Form
    $('form').on('submit', function (e) {
        e.preventDefault()
        let id = e.target.id

        $('form').find("button[type='submit']").prop('disabled', true)
        var formData = new FormData(this)

        $.ajax({
            url: $(this).attr('action'),
            type: "POST",
            data: formData,
            contentType: false,
            processData: false
        }).done(function(response) {
            toastrStatus('success', `{{ __('messages.success') }}`, response.message, "")

            if (id === "form-contributor" || id === "form-delete-contributor") {
                table.ajax.reload()

                $('#modal-contributor').modal('hide')
            } else if (id === "form-galley") {
                location.reload()
            }
        }).fail(function(response) {
            toastrStatus('error', "{{ __('messages.failed') }}", response.responseJSON.message, response.responseJSON.data)
        })
    })
</script>
@endsection