@extends('layout.layout')

@section('title', 'Submission')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">

    {{-- SweetAlert --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/sweetalert2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-sweet-alerts.min.css') }}">

    {{-- Select2 --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/forms/select/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/component/select2.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Alert --}}
	@if ($message = Session::get('danger'))
		<div class="alert alert-danger alert-dismissible fade show" role="alert">
			<div class="alert-body">{{ $message }}</div>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
    @endif

	@if ($message = Session::get('success'))
		<div class="alert alert-success alert-dismissible fade show" role="alert">
			<div class="alert-body">{{ $message }}</div>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
	@endif
	{{-- END: Alert --}}

	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable-button title="Data Submission" buttonTitle='' id="table">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th data-priority="1">Judul</th>
						<th>Conference</th>
						<th>Abstrak</th>
						<th>Publikasi</th>
						<th data-priority="3">Status</th>
						<th data-priority="2"></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($data as $item)
						<tr>
							<td>{{ $item->judul }}</td>
							<td>{{ $item->nama }}</td>
							<td class="text-center"><div disabled class="badge badge-md badge-{{ $item->abstrak_status == 1 ? "success" : "danger" }}">{{ $item->abstrak_status == 1 ? "Telah Bayar" : "Belum Bayar" }}</div></td>
							<td class="text-center"><div disabled class="badge badge-md badge-{{ $item->publikasi_status == 1 ? "success" : "danger" }}">{{ $item->publikasi_status == 1 ? "Telah Bayar" : "Belum Bayar" }}</div></td>
							<td class="text-center"><div disabled class="badge badge-md badge-success"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-check"><polyline points="20 6 9 17 4 12"></polyline></svg> {{ $item->status }}</div></td>
							<td class="text-center">
								<div class="btn-group btn-group-sm">
									<a href="{{ url('author/author/submission/'.$item->id) }}" class="btn btn-success px-1"><svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg></a>
									<a class="btn btn-info px-1" href="{{ route('author.author.publication.show', $item->id) }}" role="button" title="Publikasi"><i data-feather="cloud"></i></a>
									@if ($item->submission_status === 6)
									@endif
									@if ($item->submission_status >= 4)
										<a class="btn btn-primary px-1" href="{{ route('author.author.submission.loa', $item->id) }}" target="_blank" role="button" title="Unduh LOA"><i data-feather="download"></i></a>
									@endif
									<button class="btn btn-warning px-1" onclick="ubah({{ $item->id }})"><i data-feather="edit-2"></i></button>
									<button class="btn btn-danger px-1" onclick="hapus({{ $item->id }})"><i data-feather="trash"></i></button>
								</div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</x-datatable-button>
		{{-- END: Table --}}
	</section>
	{{-- END: Datatable Button --}}

	{{-- BEGIN: Delete --}}
	<form id="delete-form" method="POST">
		@csrf
		@method('DELETE')
	</form>
	{{-- END: Delete --}}

	{{-- BEGIN: Modal --}}
	<x-modal title="Ubah Submission" type="normal" class="" id="modal">
		<form method="post" id="form">
			@csrf
			@method('PUT')
			<input type="hidden" class="form-control conference" id="conference-id" name="conference"/>
			<div class="modal-body">
				<x-form-group title="Nama Conference">
					<input type="text" class="form-control conference" id="conference" readonly required/>
				</x-form-group>
				
				<x-form-group title="Judul">
					<input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Artikel" required/>
				</x-form-group>
				
				<div id="abstrak"></div>

				<x-form-group title="Author Lain">
					<div id="author-lain"></div>
					<div class="btn-group">
						<button type="button" class="btn btn-primary" id="tambah-author" onclick="tambahAuthor(0)"><i data-feather="plus"></i> Author</button>
						<button type="button" class="btn btn-success" id="tambah-author-baru" onclick="tambahAuthorBaru(0)"><i data-feather="plus"></i> Author Baru</button>
					</div>
				</x-form-group>
			</div>
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-success" id="button-tambah"><i data-feather="check"></i> Simpan</button>
			</div>
		</form>
	</x-modal>
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.buttons.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jszip.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/pdfmake.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.html5.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/buttons.print.min.js') }}"></script>

	{{-- Datatable --}}
	<script>
		var link = null;
	</script>
	<script src="{{ asset('js/component/datatable-button.js') }}"></script>
	<script>
		// Datatable Button
		$('#table').hide();
		$('.dt-button').removeClass('mr-2');
	</script>

    {{-- SweetAlert --}}
	<script src="{{ asset('app-assets/vendors/js/extensions/sweetalert2.all.min.js') }}"></script>
	<script>
        // Hapus Data
		function hapus(id) {
            $('#delete-form').attr('action', `{{ url('/author/author/submission/${id}') }}`);

			Swal.fire({
				title: 'Yakin ingin hapus?',
				icon: 'warning',
				showCancelButton: true,
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				customClass: {
					confirmButton: 'btn btn-primary',
					cancelButton: 'btn btn-outline-danger ml-1'
				},
				buttonsStyling: false
			}).then(function (result) {
				if (result.value) {
                    $('#delete-form').submit();
				}
			});
		}
    </script>

	{{-- Modal --}}
	<script>
		function ubah(id) {
			$('#author-lain').children().remove()

			$('#form').attr('action', `{{ url('author/author/submission/${id}') }}`)

			$.get(`{{ url('author/author/data/submission/${id}') }}`, function(data) {
				const d = JSON.parse(data)

				$('#conference-id').val(d.data.conference_id)
				$('#conference').val(d.conference.nama)
				$('#judul').val(d.data.submission_judul)

				if (d.conference.level == 1) {
					$('#abstrak').html(`<x-form-group title="Abstrak Bahasa Inggris">
											<textarea class="form-control" name="abstrak_inggris" placeholder="Artikel Abstrak Bahasa Inggris" rows="5" required>${d.data.submission_abstrak}</textarea>
										</x-form-group>

										<x-form-group title="Kata Kunci Bahasa Inggris">
											<input type="text" class="form-control" id="keyword_inggris" name="keyword_inggris" placeholder="Kata Kunci Abstrak (pisahkan dengan tanda titik koma (;) )" value="${d.katakunci[2].join(";")}" required/>
										</x-form-group>

										<x-form-group title="Abstrak Bahasa Indonesia">
											<textarea class="form-control" name="abstrak_indonesia" placeholder="Artikel Abstrak Bahasa Indonesia" rows="5" required>${d.data.submission_abstrakindo}</textarea>
										</x-form-group>

										<x-form-group title="Kata Kunci Bahasa Indonesia">
											<input type="text" class="form-control" id="keyword_indonesia" name="keyword_indonesia" placeholder="Kata Kunci Abstrak (pisahkan dengan tanda titik koma (;) )" value="${d.katakunci[1].join(";")}" required/>
										</x-form-group>`);
				} else {
					$('#abstrak').html(`<x-form-group title="Abstrak">
											<textarea class="form-control" name="abstrak_inggris" placeholder="Artikel Abstrak" rows="5" required>${d.data.submission_abstrak}</textarea>
										</x-form-group>

										<x-form-group title="Kata Kunci Bahasa Inggris">
											<input type="text" class="form-control" id="keyword_inggris" name="keyword_inggris" placeholder="Kata Kunci Abstrak (pisahkan dengan tanda titik koma (;) )" value="${d.katakunci[2].join(";")}" required/>
										</x-form-group>`);
				}
				
				d.author.map((val, index) => {
					$('#author-lain').append(`
						<div class="row mb-1" id="data-author-${index}">
							<div class="col-10">
								<select id="author-${index}" class="form-control">
									<option value="${val.id}">${val.nama}</option>
								</select>
							</div>
							<div class="col-2">
								<button type="button" class="btn btn-danger" onclick="hapusAuthor(${index})">
									<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
								</button>
							</div>
						</div>
					`)

					author(index)

					$('#tambah-author').attr('onclick', `tambahAuthor(${index + 1})`)
					$('#tambah-author-baru').attr('onclick', `tambahAuthorBaru(${index + 1})`)
				})

				$('#modal').modal('show')
			})
		}

		function hapusAuthor(id) {
			$(`#data-author-${id}`).remove()
		}
		
		function tambahAuthor(id) {
			$('#author-lain').append(`<div id="data-author-${id}">
										<div class="row mb-1">
											<div class="col-10">
												<select class="form-control" id="author-`+ id +`" name="author[`+ id +`][author]"></select>
											</div>
											<div class="col-2">
												<button type="button" class="btn btn-danger" onclick="hapusAuthor(${id})">
													<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
												</button>
											</div>
										</div>
									</div>`);
			
			// Select2
			author(id);

			data = id + 1;
			$('#tambah-author').attr('onClick', 'tambahAuthor('+ data +')');
			$('#tambah-author-baru').attr('onClick', 'tambahAuthorBaru('+ data +')');
		};

		function tambahAuthorBaru(id) {
			$('#author-lain').append(`<div id="data-author-${id}">
										<div class="row mb-1">
											<div class="col-10">
												<x-form-group title="Nama">
													<input type="text" class="form-control" placeholder="Nama Author Baru" name="author[`+ id +`][name]" required/>
												</x-form-group>
											</div>
											<div class="col-2">
												<button type="button" class="btn btn-danger mt-2" onclick="hapusAuthor(${id})">
													<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
												</button>
											</div>
											<div class="col-12">
												<x-form-group title="No Induk">
													<p class="text-muted">Password Author Baru disesuaikan dengan No Induk, apabila kosong Password menjadi 123456</p>
													<input type="number" class="form-control" placeholder="No Induk Author Baru" name="author[`+ id +`][no_induk]"/>
												</x-form-group>
											</div>
											<div class="col-12">
												<x-form-group title="Email">
													<input type="email" class="form-control" placeholder="Email Author Baru" name="author[`+ id +`][email]" required/>
												</x-form-group>
											</div>
											<div class="col-12">
												<x-form-group title="Kewarganegaraan">
													<select class="form-control" id="kewarganegaraan-`+ id +`" name="author[`+ id +`][kewarganegaraan]"></select>
												</x-form-group>
											</div>
											<div class="col-12">
												<x-form-group title="Jenis Kelamin">
													<select class="form-control" name="author[`+ id +`][jenis_kelamin]">
														<option value="1" selected>Laki-laki</option>
														<option value="0">Perempuan</option>
													</select>
												</x-form-group>
											</div>
										</div>
									</div>`);

			kewarganegaraan(id)

			data = id + 1;
			$('#tambah-author').attr('onClick', 'tambahAuthor('+ data +')');
			$('#tambah-author-baru').attr('onClick', 'tambahAuthorBaru('+ data +')');
		};
	</script>

	{{-- Select2 --}}
	<script src="{{ asset('/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
	<script>
		function author(id) {
			$(`#author-${id}`).select2({
				placeholder: "-- Pilih Author",
				width: '100%',
				ajax: {
					url: "{{ url('author/author/data/conference') }}",
					data: function (params) {
						var query = {
							search: params.term,
							page: params.page || 1
						}

						return query;
					}
				}
			})
		}

		function kewarganegaraan(id) {
			$(`#kewarganegaraan-${id}`).select2({
				placeholder: "-- Pilih Kewarganegaraan",
				width: '100%',
				ajax: {
					url: "{{ url('author/author/kewarganegaraan/conference') }}",
					data: function (params) {
						var query = {
							search: params.term,
							page: params.page || 1
						}

						return query;
					}
				}
			})
		}
	</script>

	{{-- Form --}}
	<script>
		$('form').submit(function() {
			$(this).find("button[type='submit']").prop('disabled', true);
		});
	</script>
@endsection