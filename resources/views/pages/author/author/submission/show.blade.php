@extends('layout.layout')

@section('title', $data['data']['submission_judul'])

@section('css')
	{{-- Flatpickr --}}
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/pickers/flatpickr/flatpickr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/forms/pickers/form-flat-pickr.min.css') }}">

    {{-- Select2 --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/forms/select/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/component/select2.css') }}">
@endsection

@section('content')
	@if ($message = Session::get('danger'))
		<div class="alert alert-danger alert-dismissible fade show" role="alert">
			<div class="alert-body">{{ $message }}</div>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
    @endif

	@if ($message = Session::get('success'))
		<div class="alert alert-success alert-dismissible fade show" role="alert">
			<div class="alert-body">{{ $message }}</div>
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		</div>
	@endif

	{{-- BEGIN: Card --}}
	<section>
		<input type="hidden" id="status" value="{{ $data['data']['submission_status'] + 1 }}">
		<input type="hidden" id="nextStatus" value="{{ $data['data']['submission_status'] + 2 }}">
		{{-- BEGIN: Table --}}
		<x-card-body>
			<ul class="nav nav-tabs">
				<li class="nav-item">
					<a class="nav-link" id="step1-tab" data-toggle="tab" href="#step1">1. Submit Abstrak</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="step2-tab" data-toggle="tab" href="#step2">2. Pembayaran Abstrak</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="step3-tab" data-toggle="tab" href="#step3">3. Upload Full Paper</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="step4-tab" data-toggle="tab" href="#step4">4. Review</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="step5-tab" data-toggle="tab" href="#step5">5. Pembayaran Publikasi</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="step6-tab" data-toggle="tab" href="#step6">6. Schedule to Publish</a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane" id="step1">
					<x-form-group title="Judul Artikel">
						<input type="text" class="form-control" value="{{ $data['data']['submission_judul'] }}" disabled/>
					</x-form-group>
					
					@if ($data['conference']['level'] == 1)
						<div class="row">
							<div class="col-md-6">
								<x-form-group title="Abstrak Bahasa Inggris">
									<textarea class="form-control" rows="5" disabled>{{ $data['data']['submission_abstrak'] }}</textarea>
								</x-form-group>

								<x-form-group title="Kata Kunci Bahasa Inggris">
									<input type="text" class="form-control mb-50" value="{{ implode(';', $katakunci['2']) }}" disabled/>
								</x-form-group>
							</div>

							<div class="col-md-6">
								<x-form-group title="Abstrak Bahasa Indonesia">
									<textarea class="form-control" rows="5" disabled>{{ $data['data']['submission_abstrakindo'] }}</textarea>
								</x-form-group>

								<x-form-group title="Kata Kunci Bahasa Indonesia">
									<input type="text" class="form-control mb-50" value="{{ isset($katakunci[1]) ? implode(';', $katakunci[1]) : "" }}" disabled/>
								</x-form-group>
							</div>
						</div>
					@else
						<x-form-group title="Abstrak">
							<textarea class="form-control" rows="5" disabled>{{ $data['data']['submission_abstrak'] }}</textarea>
						</x-form-group>

						<x-form-group title="Kata Kunci">
							<input type="text" class="form-control mb-50" value="{{ implode(';', $katakunci['2']) }}" disabled/>
						</x-form-group>
					@endif
	
					<hr>
					<x-form-group title="Author">
						<div id="author">
							<div class="row">
								@foreach ($data['author'] as $author)
									<div class="col-md-6">
										<input type="text" class="form-control mb-50" value="{{ $author['nama'] }}" disabled/>
									</div>
								@endforeach
							</div>
						</div>
					</x-form-group>
				</div>
				<div class="tab-pane" id="step2">
					<div class="row">
						<div class="col-md-4 mb-1">
							<ul class="list-group">
								<li class="list-group-item">
									<h5 class="mb-1 text-primary">Judul Artikel</h5>
									<p class="card-text">
										{{ $data['data']['submission_judul'] }}
									</p>
								</li>
								<li class="list-group-item">
									<h5 class="mb-1 text-primary">Conference</h5>
									<p class="card-text">
										{{ $data['conference']['nama'] }}
									</p>
								</li>
								<li class="list-group-item">
									<h5 class="mb-1 text-primary">Total Pembayaran Abstrak</h5>
									<p class="card-text">
										Rp. {{ number_format($data['conference']['bayar_abstrak']) }}
									</p>
								</li>
							</ul>
						</div>
						<div class="col-md-8">
							@if ($data['data']['submission_pembayaran_abstrak_voucher'] == NULL && $data['data']['submission_pembayaran_abstrak_file'] == NULL)
								@if ($data['conference']['pembayaran'] != "<p><br></p>" && $data['conference']['pembayaran'] != NULL)
									<h4>Panduan Pembayaran</h4>
									<hr>
									{!! $data['conference']['pembayaran'] !!}
									<hr>
								@endif
								<button type="button" class="btn btn-primary btn-block modalBayar" data-toggle="modal" data-target="#modalAbstrak" data-value="1"><i data-feather="dollar-sign"></i> Bayar Abstrak</button>
							@else
								<h5>Metode Pembayaran</h5>
								<p>{{ $data['data']['submission_pembayaran_abstrak_voucher'] != NULL ? 'Dengan Voucher' : 'Tanpa Voucher' }}</p>
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="customCheck1" disabled {{ $data['data']['submission_pembayaran_abstrak_status'] == '1' ? 'checked' : '' }} />
									<label class="custom-control-label" for="customCheck1">{{ $data['data']['submission_pembayaran_abstrak_status'] == '1' ? 'Tervalidasi' : 'Menunggu proses validasi pembayaran oleh Admin' }}</label>
								</div>
								<hr>

								@if ($data['data']['submission_pembayaran_abstrak_voucher'] != NULL)
									<h5>Kode Voucher</h5>
									<p>{{ $data['data']['submission_pembayaran_abstrak_voucher'] }}</p>
								@else
									<h5>Bukti Transfer</h5>
									<img src="{{ asset('images/submission/abstrak/' . $data['data']['submission_pembayaran_abstrak_file']) }}" class="img-thumbnail" width="70%">
								@endif
								<hr>
							@endif
						</div>
					</div>
				</div>
				<div class="tab-pane" id="step3">
					<form action="{{ url('author/author/submission/' . $data['data']['id'] . '/3') }}" method="post" enctype="multipart/form-data">
						@csrf
						@if ($data['data']['submission_status'] <= 2)
							<x-form-group title="Upload Full Paper (format .doc, .docx)">
								<input type="file" name="file" class="form-control-file" accept=".doc,.docx" required>
							</x-form-group>
							<hr>
						@endif

						<x-form-group title="Judul Artikel">
							<input type="text" class="form-control" name="judul" value="{{ $data['data']['submission_judul'] }}" required {{ $data['data']['submission_status'] != 2 ? 'readonly' : ''}}/>
						</x-form-group>
					
						@if ($data['conference']['level'] == 1)
							<div class="row">
								<div class="col-md-6">
									<x-form-group title="Abstrak Bahasa Inggris">
										<textarea class="form-control" rows="5" name="abstrak" required {{ $data['data']['submission_status'] != 2 ? 'readonly' : ''}}>{{ $data['data']['submission_abstrak'] }}</textarea>
									</x-form-group>

									<x-form-group title="Kata Kunci Bahasa Inggris">
										<input type="text" class="form-control mb-50" name="kata_kunci" value="{{ implode(';', $katakunci[2]) }}" required {{ $data['data']['submission_status'] != 2 ? 'readonly' : ''}}/>
									</x-form-group>
								</div>
								
								<div class="col-md-6">
									<x-form-group title="Abstrak Bahasa Indonesia">
										<textarea class="form-control" rows="5" name="abstrakindo" required {{ $data['data']['submission_status'] != 2 ? 'readonly' : ''}}>{{ $data['data']['submission_abstrakindo'] }}</textarea>
									</x-form-group>

									<x-form-group title="Kata Kunci Bahasa Indonesia">
										<input type="text" class="form-control mb-50" name="kata_kunciindo" value="{{ isset($katakunci[1]) ? implode(';', $katakunci[1]) : "" }}" required {{ $data['data']['submission_status'] != 2 ? 'readonly' : ''}}/>
									</x-form-group>
								</div>
							</div>
						@else
							<x-form-group title="Abstrak">
								<textarea class="form-control" rows="5" name="abstrak" required {{ $data['data']['submission_status'] != 2 ? 'readonly' : ''}}>{{ $data['data']['submission_abstrak'] }}</textarea>
							</x-form-group>

							<x-form-group title="Kata Kunci Bahasa Inggris">
								<input type="text" class="form-control mb-50" name="kata_kunci" value="{{ implode(';', $katakunci[2]) }}" required {{ $data['data']['submission_status'] != 2 ? 'readonly' : ''}}/>
							</x-form-group>
						@endif

						<div class="row">
							<div class="col-md-6">
								<x-form-group title="Author">
									@if ($data['data']['submission_status'] > 2)
										@foreach ($data['author'] as $author)
											<input type="text" class="form-control mb-50" value="{{ $author['nama'] }}" disabled/>
										@endforeach
									@else
										<div id="author-lain">
											@php
												$a = 0;
											@endphp
											@foreach ($data['author'] as $author)
												@if ($loop->first)
													<select class="form-control mb-1" name="author[{{ $a }}][author]" readonly>
														<option value="{{ $author['id'] }}">{{ $author['nama'] }}</option>
													</select>
													<hr>
												@else
													<div>
														<div class="row mb-1">
															<div class="col-10">
																<select class="form-control" name="author[{{ $a }}][author]" readonly>
																	<option value="{{ $author['id'] }}">{{ $author['nama'] }}</option>
																</select>
															</div>
															<div class="col-2">
																<button type="button" class="btn btn-danger hapus-author">
																	<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
																</button>
															</div>
														</div>
														<hr>
													</div>
												@endif
												@php
													$a++;
												@endphp
											@endforeach
										</div>
										<div class="btn-group">
											<button type="button" class="btn btn-primary" id="tambah-author" onclick="tambahAuthor({{ count($data['author']) }})"><i data-feather="plus"></i> Author</button>
											<button type="button" class="btn btn-warning" id="tambah-author-baru" onclick="tambahAuthorBaru({{ count($data['author']) }})"><i data-feather="plus"></i> Author Baru</button>
										</div>
									@endif
								</x-form-group>
							</div>
							@if ($data['data']['submission_status'] > 2)
								<div class="col-md-6">
									<x-form-group title="Artikel">
										<div class="text-center">
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text mb-50" style="width:100px; height:100px"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
											<br>
											@if (count($data['review']) > 0)
												<a class="btn btn-primary" href="{{ url('files/submission/full-paper/' . $data['review'][0]['file']) }}" role="button"><i data-feather="download"></i> Unduh Artikel</a>
											@endif
										</div>
									</x-form-group>
								</div>
							@endif
						</div>

						@if ($data['data']['submission_status'] == 2)
							<button type="submit" class="btn btn-success"><i data-feather="check"></i> Simpan</button>
						@endif
					</form>
				</div>
				<div class="tab-pane" id="step4">
					@if ($data['data']['submission_status'] == 3)
					<div class="row">
						<div class="col-md-4">
							<form action="{{ url('author/author/submission/' . $data['data']['id'] . '/4') }}" method="post" enctype="multipart/form-data">
								@csrf
								<x-form-group title="Unggah hasil review (format .doc, .docx)">
									<input type="file" name="file" class="form-control-file" accept=".doc,.docx" required>
								</x-form-group>
		
								<x-form-group title="Pesan">
									<textarea class="form-control" name="pesan" rows="3"></textarea>
								</x-form-group>
		
								<button type="submit" class="btn btn-primary btn-block my-50"><i data-feather="check"></i> Simpan</button>
							</form>
						</div>
						<div class="col-md-8">
					@endif
							<h4>Riwayat Review</h4>
							<ul class="timeline">
								@foreach ($data['review'] as $review)
									<li class="timeline-item">
										<span class="timeline-point timeline-point-{{ $review['status'] == 1 ? 'warning' : 'primary' }}">
											@if ($review['status'] == 1)
												<i data-feather="users"></i>
											@else
												<i data-feather="user"></i>
											@endif
										</span>
										<div class="timeline-event">
											<div class="d-flex justify-content-between flex-sm-row flex-column mb-sm-0 mb-1">
												@if ($review['status'] == 1)
													<h6>Review</h6>
												@elseif ($review['status'] == 2)
													<h6>Revisi</h6>
												@else
													<h6>Artikel</h6>
												@endif

												<span class="timeline-event-time">{{ date('d-m-Y', strtotime($review['tanggal'])) }}</span>
											</div>

											<p class="mb-50">{{ $review['pesan'] }}</p>

											<a class="btn btn-outline-{{ $review['status'] == 1 ? 'warning' : 'primary' }} btn-sm" href="{{ url('files/submission/full-paper/' . $review['file']) }}">
												@if ($review['status'] == 1)
													<i data-feather="file-text"></i> File Review
												@elseif ($review['status'] == 2)
													<i data-feather="file-text"></i> File Revisi
												@else
													<i data-feather="file-text"></i> File Artikel Full Paper
												@endif
											</a>
										</div>
									</li>
								@endforeach
							</ul>
						@if ($data['data']['submission_status'] == 3)
						</div>
					</div>
					@endif
				</div>
				<div class="tab-pane" id="step5">
					<div class="row">
						<div class="col-md-4 mb-1">
							<ul class="list-group">
								<li class="list-group-item">
									<h5 class="mb-1 text-primary">Judul Artikel</h5>
									<p class="card-text">
										{{ $data['data']['submission_judul'] }}
									</p>
								</li>
								<li class="list-group-item">
									<h5 class="mb-1 text-primary">Conference</h5>
									<p class="card-text">
										{{ $data['conference']['nama'] }}
									</p>
								</li>
								<li class="list-group-item">
									<h5 class="mb-1 text-primary">Total Pembayaran Publikasi</h5>
									<p class="card-text">
										Rp. {{ number_format($data['conference']['bayar_publikasi']) }}
									</p>
								</li>
							</ul>
						</div>
						<div class="col-md-8">
							@if ($data['data']['submission_pembayaran_publikasi_voucher'] == NULL && $data['data']['submission_pembayaran_publikasi_file'] == NULL)
								@if ($data['conference']['pembayaran'] != "<p><br></p>" && $data['conference']['pembayaran'] != NULL)
									<h4>Panduan Pembayaran</h4>
									<hr>
									{!! $data['conference']['pembayaran'] !!}
									<hr>
								@endif
								<button type="button" class="btn btn-primary btn-block modalBayar" data-toggle="modal" data-target="#modalAbstrak" data-value="2"><i data-feather="dollar-sign"></i> Bayar Publikasi</button>
							@else
								<h5>Metode Pembayaran</h5>
								<p>{{ $data['data']['submission_pembayaran_publikasi_voucher'] != NULL ? 'Dengan Voucher' : 'Tanpa Voucher' }}</p>
								<div class="custom-control custom-checkbox">
									<input type="checkbox" class="custom-control-input" id="customCheck1" disabled {{ $data['data']['submission_pembayaran_publikasi_status'] == '1' ? 'checked' : '' }} />
									<label class="custom-control-label" for="customCheck1">{{ $data['data']['submission_pembayaran_publikasi_status'] == '1' ? 'Tervalidasi' : 'Menunggu proses validasi pembayaran oleh Admin' }}</label>
								</div>
								<hr>

								@if ($data['data']['submission_pembayaran_publikasi_voucher'] != NULL)
									<h5>Kode Voucher</h5>
									<p>{{ $data['data']['submission_pembayaran_publikasi_voucher'] }}</p>
								@else
									<h5>Bukti Transfer</h5>
									<img src="{{ asset('images/submission/publikasi/' . $data['data']['submission_pembayaran_publikasi_file']) }}" class="img-thumbnail" width="70%">
								@endif
								<hr>
							@endif
						</div>
					</div>
				</div>
				<div class="tab-pane" id="step6">
					<form action="{{ url('author/author/submission/' . $data['data']['id'] . '/6') }}" method="POST" enctype="multipart/form-data">
						@csrf
						@if ($data['data']['submission_status'] < 6)
							<x-form-group title="Upload Full Paper (format .pdf)">
								<input type="file" name="file" class="form-control-file" accept=".pdf" required>
							</x-form-group>
							<hr>
						@endif

						<x-form-group title="Judul Artikel">
							<input type="text" class="form-control" name="judul" value="{{ $data['data']['submission_judul'] }}" disabled/>
						</x-form-group>
						
						@if ($data['conference']['level'] == 1)
							<div class="row">
								<div class="col-md-6">
									<x-form-group title="Abstrak Bahasa Inggris">
										<textarea class="form-control" rows="5" disabled>{{ $data['data']['submission_abstrak'] }}</textarea>
									</x-form-group>

									<x-form-group title="Kata Kunci Bahasa Inggris">
										<input type="text" class="form-control mb-50" value="{{ implode(';', $katakunci['2']) }}" disabled/>
									</x-form-group>
								</div>

								<div class="col-md-6">
									<x-form-group title="Abstrak Bahasa Indonesia">
										<textarea class="form-control" rows="5" disabled>{{ $data['data']['submission_abstrakindo'] }}</textarea>
									</x-form-group>

									<x-form-group title="Kata Kunci Bahasa Indonesia">
										<input type="text" class="form-control mb-50" value="{{ isset($katakunci[1]) ? implode(';', $katakunci[1]) : "" }}" disabled/>
									</x-form-group>
								</div>
							</div>
						@else
							<x-form-group title="Abstrak">
								<textarea class="form-control" rows="5" disabled>{{ $data['data']['submission_abstrak'] }}</textarea>
							</x-form-group>

							<x-form-group title="Kata Kunci">
								<input type="text" class="form-control mb-50" value="{{ implode(';', $katakunci['2']) }}" disabled/>
							</x-form-group>
						@endif

						<div class="row">
							<div class="col-lg-6 col-md-12">
								<x-form-group title="Author">
									@foreach ($data['author'] as $author)
									<input type="text" class="form-control mb-50" value="{{ $author['nama'] }}" disabled/>
									@endforeach
								</x-form-group>
							</div>
							@if ($data['data']['submission_status'] == 6)
								<div class="col-lg-3 col-sm-6">
									<x-form-group title="Artikel">
										<div class="text-center">
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text mb-50" style="width:100px; height:100px"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
											<br>
											<a class="btn btn-primary" href="{{ url('files/submission/full-paper/' . $data['data']['submission_full_paper']) }}" role="button"><i data-feather="download"></i> Unduh Artikel</a>
										</div>
									</x-form-group>
								</div>

								<div class="col-lg-3 col-sm-6">
									<x-form-group title="Letter of Acceptance">
										<div class="text-center">
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-file-text mb-50" style="width:100px; height:100px"><path d="M14 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V8z"></path><polyline points="14 2 14 8 20 8"></polyline><line x1="16" y1="13" x2="8" y2="13"></line><line x1="16" y1="17" x2="8" y2="17"></line><polyline points="10 9 9 9 8 9"></polyline></svg>
											<br>
											<a class="btn btn-success" href="{{ route('author.author.submission.loa', $data['data']->id) }}" target="_blank" role="button"><i data-feather="download"></i> Unduh LOA</a>
										</div>
									</x-form-group>
								</div>
							@endif
						</div>

						@if ($data['data']['submission_status'] < 6)
							<button type="submit" class="btn btn-success"><i data-feather="check"></i> Finish Submission</button>
						@endif
					</form>
				</div>
			</div>
		</x-card-body>
		{{-- END: Table --}}
	</section>
	{{-- END: Card --}}

	{{-- BEGIN: Modal --}}
	<x-modal title="Bayar Abstrak" type="normal" class="" id="modalAbstrak">
		<form id="form" method="post" enctype="multipart/form-data">
			@csrf
			<div class="modal-body">
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="abstrak1" data-toggle="tab" aria-controls="abstrak-1" href="#abstrak-1" role="tab" aria-selected="true">Dengan Voucher</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="abstrak2" data-toggle="tab" aria-controls="abstrak-2" href="#abstrak-2" role="tab" aria-selected="false">Tanpa Voucher</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="abstrak-1" role="tabpanel" aria-labelledby="abstrak1">
						<x-form-group title="Kode">
							<input type="text" class="form-control" name="kode" maxlength="15" placeholder="Kode Voucher"/>
						</x-form-group>
					</div>
	
					<div class="tab-pane" id="abstrak-2" role="tabpanel" aria-labelledby="abstrak2">
						<x-form-group title="Tanggal Transfer">
							<input type="text" class="form-control flatpickr-basic" name="tanggal"/>
						</x-form-group>

						<x-form-group title="Nominal">
							<input type="number" class="form-control" name="nominal" placeholder="Nominal Transfer"/>
						</x-form-group>

						<x-form-group title="Bank Tujuan">
							<input type="text" class="form-control" name="bank" placeholder="Bank Tujuan Transfer"/>
						</x-form-group>

						<x-form-group title="Nama">
							<input type="text" class="form-control" name="nama" placeholder="Nama Rekening Pengirim"/>
						</x-form-group>

						<x-form-group title="No. Tagihan/Invoice">
							<input type="number" class="form-control" name="invoice" placeholder="No. Tagihan/Invoice Transfer"/>
						</x-form-group>

						<x-form-group title="Pesan">
							<textarea name="pesan" class="form-control" rows="5" placeholder="Pesan Tambahan (Tidak wajib diisi)"></textarea>
						</x-form-group>

						<x-form-group title="Upload Bukti Transfer">
							<input type="file" class="form-control-file" accept="image/*" name="file"/>
						</x-form-group>
					</div>
				</div>
			</div>
	
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary data-submit mr-1"><i data-feather="check"></i> Simpan</button>
				<button type="reset" class="btn btn-outline-danger" data-dismiss="modal"><i data-feather="x"></i> Batal</button>
			</div>
		</form>
	</x-modal>
	{{-- END: Modal --}}
@endsection

@section('js')
	{{-- Flatpickr --}}
	<script src="{{ asset('app-assets/vendors/js/pickers/flatpickr/flatpickr.min.js') }}"></script>
	<script>
		$('.flatpickr-basic').flatpickr();
	</script>

	<script>
		// Tabs
		if ($('#status').val() == 7) {
			$('#step1-tab').tab('show');
		} else {
			$('#step' + $('#status').val() + '-tab').tab('show');
		}

		// Disable Tabs
		for (let i = $('#nextStatus').val(); i <= 6; i++) {
			$('#step'+ i + '-tab').addClass('disabled')
		}

		// Datatable Button
		$('#modal').hide();
		$('.dt-button').removeClass('mr-2');

		// Tambah Author
		function tambahAuthor(id) {
			$('#author-lain').append(`<div>
										<div class="row mb-1">
											<div class="col-10">
												<select class="form-control" id="author-`+ id +`" name="author[`+ id +`][author]"></select>
											</div>
											<div class="col-2">
												<button type="button" class="btn btn-danger hapus-author">
													<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
												</button>
											</div>
										</div>
										<hr>
									</div>`);
			
			// Select2
			author(id);

			data = id + 1;
			$('#tambah-author').attr('onClick', 'tambahAuthor('+ data +')');
			$('#tambah-author-baru').attr('onClick', 'tambahAuthorBaru('+ data +')');
		};

		// Tambah Author Baru
		function tambahAuthorBaru(id) {
			$('#author-lain').append(`<div>
										<div class="row mb-1">
											<div class="col-10">
												<x-form-group title="Nama">
													<input type="text" class="form-control" placeholder="Nama Author Baru" name="author[`+ id +`][name]" required/>
												</x-form-group>
											</div>
											<div class="col-2">
												<button type="button" class="btn btn-danger hapus-author mt-2">
													<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
												</button>
											</div>
											<div class="col-12">
												<x-form-group title="No Induk">
													<p class="text-muted">Password Author Baru disesuaikan dengan No Induk, apabila kosong Password menjadi 123456</p>
													<input type="number" class="form-control" placeholder="No Induk Author Baru" name="author[`+ id +`][no_induk]"/>
												</x-form-group>
											</div>
											<div class="col-12">
												<x-form-group title="Email">
													<input type="email" class="form-control" placeholder="Email Author Baru" name="author[`+ id +`][email]" required/>
												</x-form-group>
											</div>
											<div class="col-12">
												<x-form-group title="Kewarganegaraan">
													<select class="form-control" id="kewarganegaraan-`+ id +`" name="author[`+ id +`][kewarganegaraan]"></select>
												</x-form-group>
											</div>
											<div class="col-12">
												<x-form-group title="Jenis Kelamin">
													<select class="form-control" name="author[`+ id +`][jenis_kelamin]">
														<option value="1" selected>Laki-laki</option>
														<option value="0">Perempuan</option>
													</select>
												</x-form-group>
											</div>
										</div>
										<hr>
									</div>`);

			$.get( "{{ url('author/author/data/conference') }}", function( data ) {
				var d = JSON.parse(data);
				for (var i = 0; i < d["kewarganegaraan"].length; i++) {
					$('#kewarganegaraan-' + id).append('<option value="'+ d["kewarganegaraan"][i].id +'">'+ d["kewarganegaraan"][i].ke_nama +'</option>');
				}
			});

			data = id + 1;
			$('#tambah-author').attr('onClick', 'tambahAuthor('+ data +')');
			$('#tambah-author-baru').attr('onClick', 'tambahAuthorBaru('+ data +')');
		};

		// Hapus Author
		$(document).on('click', '.hapus-author', function() {
			$(this).parent().parent().parent().remove();
		});

		$('form').submit(function() {
			$(this).find("button[type='submit']").prop('disabled', true);
		});
		
		// Modal
		$(document).ready(function() {
			$('.modalBayar').on('click', function() {
				const id = $(this).attr('data-value');
				if (id == 1) {
					$('.modal-title').text('Bayar Abstrak');
					$('#form').attr('action', "{{ url('author/author/submission/' . $data['data']['id'] . '/2') }}");
				} else {
					$('.modal-title').text('Bayar Publikasi');
					$('#form').attr('action', "{{ url('author/author/submission/' . $data['data']['id'] . '/5') }}");
				}
			});
		});
	</script>

	{{-- Select2 --}}
	<script src="{{ asset('/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
	<script>
		function author(id) {
			$(`#author-${id}`).select2({
				placeholder: "-- Pilih Author",
				width: '100%',
				ajax: {
					url: "{{ url('author/author/data/conference') }}",
					data: function (params) {
						var query = {
							search: params.term,
							page: params.page || 1
						}

						return query;
					}
				}
			})
		}
	</script>
@endsection