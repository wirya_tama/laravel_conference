@extends('layout.layout')

@section('title', 'Conference')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/dataTables.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/tables/datatable/buttons.bootstrap4.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/vendors/css/extensions/toastr.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/plugins/extensions/ext-component-toastr.min.css') }}">

    {{-- Select2 --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('/app-assets/vendors/css/forms/select/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/component/select2.css') }}">
@endsection

@section('content')
	{{-- BEGIN: Datatable Button --}}
	<section>
		{{-- BEGIN: Table --}}
		<x-datatable title="Data Conference">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th data-priority="1">Conference</th>
						<th data-priority="2" width="5%"></th>
					</tr>
				</thead>
				<tbody>
					@foreach ($data as $item)
						<tr>
							<td>{{ $item->nama }}</td>
							<td class="text-center">
								<div class="btn-group"><button type="button" class="btn btn-primary tambah" data-value="{{ $item->id }}" data-nama="{{ $item->nama }}" data-level="{{ $item->level }}"><i data-feather="plus"></i></button></div>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</x-datatable>
		{{-- END: Table --}}
	</section>
	{{-- END: Datatable --}}

	{{-- BEGIN: Modal --}}
	<x-modal title="Tambah Submission" type="normal" class="" id="modal">
		<form action="{{ route('author.author.conference.store') }}" method="post" id="form">
			@csrf
			<input type="hidden" class="form-control conference" id="conference-id" name="conference"/>
			<div class="modal-body">
				<x-form-group title="Nama Conference">
					<input type="text" class="form-control conference" id="conference" readonly required/>
				</x-form-group>
				
				<x-form-group title="Judul">
					<input type="text" class="form-control" id="judul" name="judul" placeholder="Judul Artikel" required/>
				</x-form-group>
				
				<div id="abstrak"></div>

				<x-form-group title="Author Lain">
					<div id="author-lain"></div>
					<div class="btn-group">
						<button type="button" class="btn btn-primary" id="tambah-author" onclick="tambahAuthor(0)"><i data-feather="plus"></i> Author</button>
						<button type="button" class="btn btn-success" id="tambah-author-baru" onclick="tambahAuthorBaru(0)"><i data-feather="plus"></i> Author Baru</button>
					</div>
				</x-form-group>
			</div>
			
			<div class="modal-footer">
				<button type="submit" class="btn btn-success" id="button-tambah"><i data-feather="check"></i> Simpan</button>
			</div>
		</form>
	</x-modal>
	{{-- END: Modal --}}
@endsection

@section('js')
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('app-assets/vendors/js/tables/datatable/datatables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/tables/datatable/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('app-assets/vendors/js/extensions/toastr.min.js') }}"></script>

	<script>
		// Datatable
		$('table').DataTable({
			dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
			columnDefs: [
				{
					targets: -1,
					orderable: false,
				}
			],
		});

		// Modal
		$(document).ready(function() {
			$('.tambah').on('click', function() {
				$('#conference').val($(this).attr('data-nama'));
				$('#conference-id').val($(this).attr('data-value'));
				if ($(this).attr('data-level') == 1) {
					$('#abstrak').html(`<x-form-group title="Abstrak Bahasa Inggris">
											<textarea class="form-control" name="abstrak_inggris" placeholder="Artikel Abstrak Bahasa Inggris" rows="5" required></textarea>
										</x-form-group>

										<x-form-group title="Kata Kunci Bahasa Inggris">
											<input type="text" class="form-control" id="keyword_inggris" name="keyword_inggris" placeholder="Kata Kunci Abstrak (pisahkan dengan tanda titik koma (;) )" required/>
										</x-form-group>

										<x-form-group title="Abstrak Bahasa Indonesia">
											<textarea class="form-control" name="abstrak_indonesia" placeholder="Artikel Abstrak Bahasa Indonesia" rows="5" required></textarea>
										</x-form-group>

										<x-form-group title="Kata Kunci Bahasa Indonesia">
											<input type="text" class="form-control" id="keyword_indonesia" name="keyword_indonesia" placeholder="Kata Kunci Abstrak (pisahkan dengan tanda titik koma (;) )" required/>
										</x-form-group>`);
				} else {
					$('#abstrak').html(`<x-form-group title="Abstrak">
											<textarea class="form-control" name="abstrak_inggris" placeholder="Artikel Abstrak" rows="5" required></textarea>
										</x-form-group>

										<x-form-group title="Kata Kunci Bahasa Inggris">
											<input type="text" class="form-control" id="keyword_inggris" name="keyword_inggris" placeholder="Kata Kunci Abstrak (pisahkan dengan tanda titik koma (;) )" required/>
										</x-form-group>`);
				}

				$('#modal').modal('show');
			});

			$('#modal').on('hidden.bs.modal', function() {
				$('input:not(input=[name="__token"])').val('');
				$('select').val('');
				$('#author-lain').children().remove();
			});

			$(document).on('click', '.hapus-author', function() {
				$(this).parent().parent().parent().remove();
			});

			// Tambah Data
			$('#form').on('submit', function(e) {
				e.preventDefault();

				var formData = new FormData(this);

				$.ajax({
					url: $(this).attr('action'),
					type: "POST",
					data: formData,
					contentType: false,
					processData: false
				}).done(function(response){
					if (response.id == 1) {
						$('#modal').modal('hide');
					}
					formExecuted(response);
					$('#button-tambah').attr('disabled', false);
				}).fail(function(response) {
					toastr['error'](response.statusText, response.responseJSON.message, {
						closeButton: true,
						tapToDismiss: true
					});

					$('form').find("button[type='submit']").prop('disabled', false);
				});
			});

			$('form').submit(function() {
				$(this).find("button[type='submit']").prop('disabled', true);
			});
		});

		function tambahAuthor(id) {
			$('#author-lain').append(`<div>
										<div class="row mb-1">
											<div class="col-10">
												<select class="form-control" id="author-`+ id +`" name="author[`+ id +`][author]"></select>
											</div>
											<div class="col-2">
												<button type="button" class="btn btn-danger hapus-author">
													<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
												</button>
											</div>
										</div>
										<hr>
									</div>`);
			
			// Select2
			author(id);

			data = id + 1;
			$('#tambah-author').attr('onClick', 'tambahAuthor('+ data +')');
			$('#tambah-author-baru').attr('onClick', 'tambahAuthorBaru('+ data +')');
		};

		function tambahAuthorBaru(id) {
			$('#author-lain').append(`<div>
										<div class="row mb-1">
											<div class="col-10">
												<x-form-group title="Nama">
													<input type="text" class="form-control" placeholder="Nama Author Baru" name="author[`+ id +`][name]" required/>
												</x-form-group>
											</div>
											<div class="col-2">
												<button type="button" class="btn btn-danger hapus-author mt-2">
													<svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-trash"><polyline points="3 6 5 6 21 6"></polyline><path d="M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"></path></svg>
												</button>
											</div>
											<div class="col-12">
												<x-form-group title="No Induk">
													<p class="text-muted">Password Author Baru disesuaikan dengan No Induk, apabila kosong Password menjadi 123456</p>
													<input type="number" class="form-control" placeholder="No Induk Author Baru" name="author[`+ id +`][no_induk]"/>
												</x-form-group>
											</div>
											<div class="col-12">
												<x-form-group title="Email">
													<input type="email" class="form-control" placeholder="Email Author Baru" name="author[`+ id +`][email]" required/>
												</x-form-group>
											</div>
											<div class="col-12">
												<x-form-group title="Kewarganegaraan">
													<select class="form-control" id="kewarganegaraan-`+ id +`" name="author[`+ id +`][kewarganegaraan]"></select>
												</x-form-group>
											</div>
											<div class="col-12">
												<x-form-group title="Jenis Kelamin">
													<select class="form-control" name="author[`+ id +`][jenis_kelamin]">
														<option value="1" selected>Laki-laki</option>
														<option value="0">Perempuan</option>
													</select>
												</x-form-group>
											</div>
										</div>
										<hr>
									</div>`);

			kewarganegaraan(id)
			// $.get( "{{ url('author/author/data/conference') }}", function( data ) {
			// 	var d = JSON.parse(data);
			// 	for (var i = 0; i < d["kewarganegaraan"].length; i++) {
			// 		$('#kewarganegaraan-' + id).append('<option value="'+ d["kewarganegaraan"][i].id +'">'+ d["kewarganegaraan"][i].ke_nama +'</option>');
			// 	}
			// });

			data = id + 1;
			$('#tambah-author').attr('onClick', 'tambahAuthor('+ data +')');
			$('#tambah-author-baru').attr('onClick', 'tambahAuthorBaru('+ data +')');
		};

		function formExecuted(response) {
			var status = '';
			if (response.id == 1) {
				status = 'success';
			} else {
				status = 'error';
			}
			toastr[status](response.keterangan, response.status, {
				closeButton: true,
				tapToDismiss: true
			});

			if (response.id == 1) {
				window.location.reload();
			}

			$('form').find("button[type='submit']").prop('disabled', false);
		}
	</script>

	{{-- Select2 --}}
	<script src="{{ asset('/app-assets/vendors/js/forms/select/select2.full.min.js') }}"></script>
	<script>
		function author(id) {
			$(`#author-${id}`).select2({
				placeholder: "-- Pilih Author",
				width: '100%',
				ajax: {
					url: "{{ url('author/author/data/conference') }}",
					data: function (params) {
						var query = {
							search: params.term,
							page: params.page || 1
						}

						return query;
					}
				}
			})
		}

		function kewarganegaraan(id) {
			$(`#kewarganegaraan-${id}`).select2({
				placeholder: "-- Pilih Kewarganegaraan",
				width: '100%',
				ajax: {
					url: "{{ url('author/author/kewarganegaraan/conference') }}",
					data: function (params) {
						var query = {
							search: params.term,
							page: params.page || 1
						}

						return query;
					}
				}
			})
		}
	</script>
@endsection