@extends('layout.layout')

@section('title', 'Home')

@section('content')
    {{-- BEGIN: Card Statistic --}}
    <section>
        <div class="row">
            <div class="col-lg-3 col-sm-6 col-12">
                <x-card-statistic title="1" subtitle="Jumlah Conference" color="primary" icon="cloud"/>
            </div>
        </div>
    </section>
    {{-- END: Card Statistic --}}

    {{-- BEGIN: Card Statistic --}}
    <section>
        <div class="row">
            <div class="col-md-8">
                <h4>Kegiatan Terbaru</h4>
                {{-- BEGIN: Card --}}
                <div class="card bg-primary text-white">
                    <div class="card-body pb-1">
                        <h4 class="card-title text-white mb-0">
                            <img src="{{ asset('images/logo.jpg') }}" class="img-fluid d-inline" style="height: 45px" alt="Brand logo">
                            Contoh Conference
                        </h4>

                        <hr>

                        <div class="card-body row py-0">
                            <div class="d-inline pr-2 mr-2 mb-1">
                                <p class="font-weight-bold">Tanggal</p>
                                20/12/2021
                            </div>

                            <div class="d-inline pr-2 mr-2 mb-1">
                                <p class="font-weight-bold">Jumlah Peserta</p>
                                124 Orang
                            </div>

                            <div class="d-inline pr-2 mr-2 mb-1">
                                <p class="font-weight-bold">Portal</p>
                                Contoh Portal
                            </div>
                        </div>
                    </div>
                </div>
                {{-- END: Card --}}

                <h4>Conference Lainnya</h4>
                {{-- BEGIN: Card --}}
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            {{-- BEGIN: Table --}}
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr class="text-center">
                                        <th>Nama</th>
                                        <th>Portal</th>
                                        <th>Tanggal</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Contoh Conference Lain</td>
                                        <td>Contoh Portal</td>
                                        <td class="text-center">25 Desember 2021</td>
                                    </tr>
                                </tbody>
                            </table>
                            {{-- END: Table --}}
                        </div>
                    </div>

                    <div class="card-footer py-1">
                        <a class="btn btn-primary float-right" href="#">Lihat lainnya</a>
                    </div>
                </div>
                {{-- END: Card --}}
            </div>

            <div class="col-md-4">
                <h4>Tanggal Penting Bulan Ini</h4>
                {{-- BEGIN: Card --}}
                <div class="card">
                    <div class="card-body">
                        <div class="mb-2">
                            <p class="font-weight-bold mb-0">20/12/2021</p>
                            <li>Contoh Conference</li>
                        </div>
                        <div class="mb-2">
                            <p class="font-weight-bold mb-0">25/12/2021</p>
                            <li>Contoh Conference Lain</li>
                        </div>
                    </div>
                </div>
                {{-- END: Card --}}
            </div>
        </div>
    </section>
    {{-- END: Card Statistic --}}
@endsection

@section('js')
    <script>
        $('.content-header').hide();
    </script>
@endsection