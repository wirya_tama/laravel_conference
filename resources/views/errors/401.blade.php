@extends('errors.layout')

@section('title', 'Not Authorized')
@section('status', '401')
@section('judul', 'Anda tidak Memiliki izin 🕵🏻‍♀️')
@section('deskripsi', 'Oops! 😖 sepertinya anda tidak memiliki izin untuk mengakses halaman ini')