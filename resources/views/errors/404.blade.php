@extends('errors.layout')

@section('title', 'Page Not Found')
@section('status', '404')
@section('judul', 'Halaman tidak ditemukan 🕵🏻‍♀️')
@section('deskripsi', 'Oops! 😖 sepertinya halaman yang anda buka tidak terdaftar pada sistem')