var table = $('.review-table').DataTable({
    processing: true,
    responsive: true,
    serverSide: true,
    ajax: link,
    columns: [
        {data: 'submission_judul', name: 'submission_judul'},
        {data: 'name', name: 'users.name'},
        {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
    ],
    dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    columnDefs: [
        {
            targets: -1,
            orderable: false,
        }
    ],
});

var riwayattable = $('.riwayat-table').DataTable({
    processing: true,
    responsive: true,
    serverSide: true,
    ajax: riwayatlink,
    columns: [
        {data: 'submission_judul', name: 'submission_judul'},
        {data: 'name', name: 'users.name'},
        {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
    ],
    dom: '<"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
    columnDefs: [
        {
            targets: -1,
            orderable: false,
        }
    ],
});
