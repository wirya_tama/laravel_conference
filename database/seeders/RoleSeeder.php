<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name'          => 'admin',
            'description'   => 'Admin'
        ]);

        Role::create([
            'name'          => 'admin_conference',
            'description'   => 'Admin Conference'
        ]);

        Role::create([
            'name'          => 'author',
            'description'   => 'Author'
        ]);

        Role::create([
            'name'          => 'reviewer',
            'description'   => 'Reviewer'
        ]);
    }
}
