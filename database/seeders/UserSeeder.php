<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Role;
use App\Models\DataUser;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'      => 'Admin',
            'email'     => 'admin@mail.com',
            'gambar'    => 'blank.png',
            'password'  => Hash::make('password123')
        ])
            ->roles()
            ->attach(Role::where('name', 'admin')->first());

        DataUser::create([
            'user_id'   => 1,
            'ke_id'     => 1
        ]);
    }
}
