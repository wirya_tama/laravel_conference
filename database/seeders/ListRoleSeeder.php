<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\ListRole;

class ListRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ListRole::insert([
            'user_id' => 1,
            'role_id' => 1,
        ]);
    }
}
