<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKataKunciAbstraksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kata_kunci_abstrak', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('submission_id');
            $table->string('kka_kata_kunci');
            $table->boolean('kka_level')->default(2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kata_kunci_abstrak');
    }
}
