<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submission', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('conference_id');
            $table->string('submission_judul');
            $table->text('submission_abstrak');
            $table->text('submission_abstrakindo')->nullable();
            $table->integer('submission_submitter')->nullable();
            $table->date('submission_tanggal_submit')->nullable();
            $table->string('submission_abstrak_file')->nullable();
            $table->boolean('submission_status')->default(1)->comment('1 => Submit Abstrak, 2 => Pembayaran Abstrak, 3 => Upload Full Paper, 4 => Review, 5 => Pembayaran Publikasi, 6 => Publikasi');
            $table->string('submission_full_paper')->nullable();
            $table->char('submission_pembayaran_abstrak_voucher', 15)->nullable();
            $table->string('submission_pembayaran_abstrak_file')->nullable();
            $table->boolean('submission_pembayaran_abstrak_status')->default(0)->comment('1 => Telah Bayar, 0 => Belum Bayar');
            $table->char('submission_pembayaran_publikasi_voucher', 15)->nullable();
            $table->string('submission_pembayaran_publikasi_file')->nullable();
            $table->boolean('submission_pembayaran_publikasi_status')->default(0)->comment('1 => Telah Bayar, 0 => Belum Bayar');
            $table->date('submission_pembayaran_abstrak_tanggal')->nullable();
            $table->integer('submission_pembayaran_abstrak_nominal')->nullable();
            $table->string('submission_pembayaran_abstrak_bank')->nullable();
            $table->string('submission_pembayaran_abstrak_rekening')->nullable();
            $table->string('submission_pembayaran_abstrak_invoice')->nullable();
            $table->string('submission_pembayaran_abstrak_pesan')->nullable();
            $table->date('submission_pembayaran_publikasi_tanggal')->nullable();
            $table->integer('submission_pembayaran_publikasi_nominal')->nullable();
            $table->string('submission_pembayaran_publikasi_bank')->nullable();
            $table->string('submission_pembayaran_publikasi_rekening')->nullable();
            $table->string('submission_pembayaran_publikasi_invoice')->nullable();
            $table->string('submission_pembayaran_publikasi_pesan')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submission');
    }
}
