<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKeynoteSpeakersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keynote_speaker', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('conference_id');
            $table->string('ks_nama');
            $table->string('ks_afiliasi');
            $table->text('ks_deskripsi')->nullable();
            $table->string('ks_foto')->default('blank.png');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keynote_speaker');
    }
}
