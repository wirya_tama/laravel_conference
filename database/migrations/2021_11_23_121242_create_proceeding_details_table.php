<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProceedingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proceeding_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId('proceeding_id')->constrained()->onDelete('restrict');
            $table->unsignedBigInteger('submission_id');
            $table->string('kode')->unique();
            $table->timestamps();

            $table->foreign('submission_id')->references('id')->on('submission');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proceeding_details');
    }
}
