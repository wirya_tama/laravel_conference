<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProceedingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proceedings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('conference_id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->text('editor')->nullable();
            $table->string('volume')->nullable();
            $table->string('issn')->nullable();
            $table->string('isbn')->nullable();
            $table->timestamps();

            $table->foreign('conference_id')->references('id')->on('conference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proceedings');
    }
}
