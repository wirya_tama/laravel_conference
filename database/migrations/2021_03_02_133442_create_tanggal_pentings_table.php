<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTanggalPentingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tanggal_penting', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('conference_id');
            $table->string('tp_judul');
            $table->date('tp_tanggal_awal');
            $table->date('tp_tanggal_akhir');
            $table->boolean('tp_conference')->default(0)->comment('1 => Tanggal Conference, 0 => Bukan Tanggal Conference');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tanggal_penting');
    }
}
