<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiwayatSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_submission', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('submission_id');
            $table->boolean('rs_status')->default(1)->comment('1 => Submit Abstrak, 2 => Pembayaran Abstrak, 3 => Upload Full Paper, 4 => Review, 5 => Pembayaran Publikasi, 6 => Publikasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_submission');
    }
}
