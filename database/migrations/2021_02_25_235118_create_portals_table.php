<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portal', function (Blueprint $table) {
            $table->id();
            $table->string('portal_nama');
            $table->string('portal_logo')->default('blank.png');
            $table->text('portal_deskripsi');
            $table->text('portal_tentang');
            $table->char('portal_latitude', 20);
            $table->char('portal_longitude', 20);
            $table->string('portal_kontak_nama1');
            $table->char('portal_kontak_hp1', 15);
            $table->string('portal_kontak_nama2')->nullable();
            $table->char('portal_kontak_hp2', 15)->nullable();
            $table->string('portal_email');
            $table->string('portal_instagram')->nullable();
            $table->string('portal_facebook')->nullable();
            $table->string('portal_twitter')->nullable();
            $table->string('portal_youtube')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portal');
    }
}
