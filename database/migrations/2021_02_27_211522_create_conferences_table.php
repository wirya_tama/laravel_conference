<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conference', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->bigInteger('portal_id');
            $table->string('conference_nama');
            $table->boolean('conference_nama_status')->default(1)->comment('1 => Ya, 0 => Tidak');
            $table->string('conference_afiliasi')->nullable();
            $table->text('conference_slug');
            $table->text('conference_tema')->nullable();
            $table->text('conference_tentang')->nullable();
            $table->string('conference_lokasi')->nullable();
            $table->char('conference_latitude', 20)->nullable();
            $table->char('conference_longitude', 20)->nullable();
            $table->char('conference_bayar_abstrak', 9)->default(0);
            $table->char('conference_bayar_publikasi', 9)->default(0);
            $table->string('conference_kontak_nama1')->nullable();
            $table->char('conference_kontak_hp1', 15)->nullable();
            $table->string('conference_kontak_nama2')->nullable();
            $table->char('conference_kontak_hp2', 15)->nullable();
            $table->string('conference_logo')->default('blank.png');
            $table->string('conference_pamflet')->default('blank.png');
            $table->string('conference_banner')->default('blank.png');
            $table->boolean('conference_level')->default(1)->comment('1 => Nasional, 2 => Internasional');
            $table->string('conference_email')->nullable();
            $table->string('conference_instagram')->nullable();
            $table->string('conference_facebook')->nullable();
            $table->string('conference_twitter')->nullable();
            $table->string('conference_youtube')->nullable();
            $table->boolean('conference_status')->default(0)->comment('1 => Aktif, 2=> Draft, 0 => Tidak Aktif');
            $table->text('conference_template')->nullable();
            $table->text('conference_pembayaran')->nullable();
            $table->text('conference_google_maps')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conference');
    }
}
