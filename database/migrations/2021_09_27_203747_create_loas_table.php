<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('conference_id');
            $table->string('gambar1')->nullable();
            $table->string('gambar2')->nullable();
            $table->text('kop')->default('<p></p>');
            $table->text('isi')->default('<p></p>');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loas');
    }
}
